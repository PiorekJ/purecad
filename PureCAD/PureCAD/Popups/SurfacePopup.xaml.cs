﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PureCAD.Popups
{
    /// <summary>
    /// Interaction logic for SurfacePopup.xaml
    /// </summary>
    public partial class SurfacePopup : Window
    {
        public BezierSurfaceValues Values { get; set; }

        public SurfacePopup()
        {
            Values = new BezierSurfaceValues();
            Values.InitForPlane();
            InitializeComponent();
        }

        private void CreateButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public BezierSurfaceValues OpenSurfacePopup()
        {
            if (ShowDialog() == true)
            {
                return Values;
            }
            return null;
        }

        private void TypeCombobox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TypeCombobox.SelectedIndex == 0)
                Values.InitForPlane();
            else
                Values.InitForCylinder();
        }

        public class BezierSurfaceValues
        {
            public int Type { get; set; }

            public float SizeX { get; set; }
            public float SizeY { get; set; }

            public int PatchesX { get; set; }
            public int PatchesY { get; set; }

            public void InitForPlane()
            {
                Type = 0;
                SizeX = 2;
                SizeY = 2;
                PatchesX = 1;
                PatchesY = 1;
            }

            public void InitForCylinder()
            {
                Type = 1;
                SizeX = 2;
                SizeY = 4;
                PatchesX = 8;
                PatchesY = 8;
            }
        }
    }
}
