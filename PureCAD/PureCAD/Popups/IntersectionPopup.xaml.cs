﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.UserControls;
using PureCAD.Utils.Math;
using PureCAD.Utils.ModeManagers;
using PureCAD.Utils.UIExtensionClasses;
using Color = System.Drawing.Color;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using Rectangle = System.Drawing.Rectangle;

namespace PureCAD.Popups
{
    /// <summary>
    /// Interaction logic for IntersectionPopup.xaml
    /// </summary>
    public partial class IntersectionPopup : Window
    {
        public int Canvas1Width { get; set; }
        public int Canvas1Height { get; set; }
        public int Canvas2Width { get; set; }
        public int Canvas2Height { get; set; }
        public BetterCanvasBitmap Bitmap1 { get; set; }
        public BetterCanvasBitmap Bitmap2 { get; set; }
        public SurfaceIntersectionData Surface1Data { get; set; }
        public SurfaceIntersectionData Surface2Data { get; set; }

        public List<IntersectionMath.LineSegment> CurveSegments;

        public class SurfaceIntersectionData
        {
            public string SurfaceName { get; set; }
            public string SurfaceType { get; set; }
            public string ParameterURange { get; set; }
            public string ParameterVRange { get; set; }
            public bool IsTrimming { get; set; }

            public SurfaceIntersectionData(IIntersectable surface)
            {
                if (surface is SceneObject item)
                {
                    SurfaceName = item.ObjectName;
                    var split = item.GetType().FullName?.Split('.');
                    SurfaceType = split?[split.Length - 1];
                }
                else
                {
                    SurfaceName = String.Empty;
                    SurfaceType = String.Empty;
                }

                ParameterURange = $"0 - {surface.GetURange().ToString("F")}";
                ParameterVRange = $"0 - {surface.GetVRange().ToString("F")}";
            }
        }

        public IntersectionPopup(IntersectionManager.IntersectionData intersectionData)
        {
            CurveSegments = new List<IntersectionMath.LineSegment>();
            Canvas1Width = MathHelper.Clamp((int)intersectionData.Surface1.GetURange() * 100, 100, 800);
            Canvas1Height = MathHelper.Clamp((int)intersectionData.Surface1.GetVRange() * 100, 100, 800);
            Canvas2Width = MathHelper.Clamp((int)intersectionData.Surface2.GetURange() * 100, 100, 800);
            Canvas2Height = MathHelper.Clamp((int)intersectionData.Surface2.GetVRange() * 100, 100, 800);
            Bitmap1 = new BetterCanvasBitmap(Canvas1Width, Canvas1Height);
            Bitmap2 = new BetterCanvasBitmap(Canvas2Width, Canvas2Height);
            Surface1Data = new SurfaceIntersectionData(intersectionData.Surface1);
            Surface2Data = new SurfaceIntersectionData(intersectionData.Surface2);

            if (intersectionData.Surface1 == intersectionData.Surface2)
            {
                DrawPointsSelfIntersect(intersectionData);
                if (Surface1Data.IsTrimming)
                    intersectionData.Surface1.SetTrimmingTexture(GenerateSurfaceTexture(Bitmap1.WriteableBitmap));
            }
            else
            {
                DrawPoints(intersectionData);
                if (Surface1Data.IsTrimming)
                    intersectionData.Surface1.SetTrimmingTexture(GenerateSurfaceTexture(Bitmap1.WriteableBitmap));
                if (Surface2Data.IsTrimming)
                    intersectionData.Surface2.SetTrimmingTexture(GenerateSurfaceTexture(Bitmap2.WriteableBitmap));
            }

            
            InitializeComponent();
            Canvas1.Source = Bitmap1.WriteableBitmap;
            Canvas2.Source = Bitmap2.WriteableBitmap;
        }

        public int GenerateSurfaceTexture(WriteableBitmap writableBitmap)
        {
            var bitmap = BitmapFromWriteableBitmap(writableBitmap);
            BitmapData bitMapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, bitMapData.Scan0);
            //GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

            return tex;
        }

        private System.Drawing.Bitmap BitmapFromWriteableBitmap(WriteableBitmap writeBmp)
        {
            System.Drawing.Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new System.Drawing.Bitmap(outStream);
            }
            return bmp;
        }

        private void AddLine(Vector2d lineStart, Vector2d lineEnd, BetterCanvasBitmap bitmap)
        {
            DrawLine((int)lineStart.X, (int)lineStart.Y, (int)lineEnd.X, (int)lineEnd.Y, Color.Black, bitmap);
            CurveSegments.Add(new IntersectionMath.LineSegment(lineStart, lineEnd));
        }

        public void AddLineClamped(Vector2d lineStart, Vector2d lineEnd, BetterCanvasBitmap bitmap)
        {
            if ((lineStart - lineEnd).LengthSquared < (bitmap.Width * bitmap.Height) / 2.0f)
                AddLine(lineStart, lineEnd, bitmap);
            else
            {
                var diffX = lineEnd.X - lineStart.X;
                var diffY = lineEnd.Y - lineStart.Y;

                if (Math.Abs(diffX) > Math.Abs(diffY))
                {
                    if (diffX < 0)
                    {
                        AddLine(lineStart, new Vector2d(bitmap.Width, lineStart.Y), bitmap);
                        AddLine(new Vector2d(0, lineEnd.Y), lineEnd, bitmap);
                    }
                    else
                    {
                        AddLine(lineStart, new Vector2d(0, lineStart.Y), bitmap);
                        AddLine(new Vector2d(bitmap.Width, lineEnd.Y), lineEnd, bitmap);
                    }
                }
                else
                {
                    if (diffY < 0)
                    {
                        AddLine(lineStart, new Vector2d(lineStart.X, bitmap.Height), bitmap);
                        AddLine(new Vector2d(lineEnd.X, 0), lineEnd, bitmap);
                    }
                    else
                    {
                        AddLine(lineStart, new Vector2d(lineStart.X, 0), bitmap);
                        AddLine(new Vector2d(lineEnd.X, bitmap.Height), lineEnd, bitmap);
                    }
                }

            }
        }


        private void DrawPointsSelfIntersect(IntersectionManager.IntersectionData intersectionData)
        {
            for (var i = 1; i < intersectionData.Params1.Count; i++)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params1[i - 1].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[i - 1].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params1[i].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[i].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);
            }

            for (var i = 1; i < intersectionData.Params2.Count; i++)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params2[i - 1].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[i - 1].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params2[i].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[i].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);
            }

            if (intersectionData.IsCurveClosed)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params1[intersectionData.Params1.Count - 1].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[intersectionData.Params1.Count - 1].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params1[0].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[0].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);

                lineStart = new Vector2d(intersectionData.Params2[intersectionData.Params2.Count - 1].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[intersectionData.Params2.Count - 1].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                lineEnd = new Vector2d(intersectionData.Params2[0].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[0].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);
            }

            FloodFill(Bitmap1, intersectionData.Surface1, Vector2.One * 5, Color.White, Color.CornflowerBlue);

            Surface1Data.IsTrimming = CheckIfTrimming(Bitmap1);
        }

        private void DrawPoints(IntersectionManager.IntersectionData intersectionData)
        {
            for (var i = 1; i < intersectionData.Params1.Count; i++)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params1[i - 1].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[i - 1].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params1[i].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[i].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);
            }

            for (var i = 1; i < intersectionData.Params2.Count; i++)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params2[i - 1].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[i - 1].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params2[i].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[i].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                AddLineClamped(lineStart, lineEnd, Bitmap2);
            }

            if (intersectionData.IsCurveClosed)
            {
                Vector2d lineStart = new Vector2d(intersectionData.Params1[intersectionData.Params1.Count - 1].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[intersectionData.Params1.Count - 1].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                Vector2d lineEnd = new Vector2d(intersectionData.Params1[0].X / intersectionData.Surface1.GetURange() * Canvas1Width, intersectionData.Params1[0].Y / intersectionData.Surface1.GetVRange() * Canvas1Height);
                AddLineClamped(lineStart, lineEnd, Bitmap1);

                lineStart = new Vector2d(intersectionData.Params2[intersectionData.Params2.Count - 1].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[intersectionData.Params2.Count - 1].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                lineEnd = new Vector2d(intersectionData.Params2[0].X / intersectionData.Surface2.GetURange() * Canvas2Width, intersectionData.Params2[0].Y / intersectionData.Surface2.GetVRange() * Canvas2Height);
                AddLineClamped(lineStart, lineEnd, Bitmap2);
            }

            FloodFill(Bitmap1, intersectionData.Surface1, Vector2.One * 5, Color.White, Color.CornflowerBlue);
            FloodFill(Bitmap2, intersectionData.Surface2, Vector2.One * 5, Color.White, Color.CornflowerBlue);

            Surface1Data.IsTrimming = CheckIfTrimming(Bitmap1);
            Surface2Data.IsTrimming = CheckIfTrimming(Bitmap2);
        }

        public void DrawLine(int x, int y, int x2, int y2, Color color, BetterCanvasBitmap bitmap)
        {
            int w = x2 - x;
            int h = y2 - y;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                bitmap.SetPixel(x, y, color);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x += dx1;
                    y += dy1;
                }
                else
                {
                    x += dx2;
                    y += dy2;
                }
            }
        }

        public bool CheckIfTrimming(BetterCanvasBitmap bitmap)
        {
            var set = new HashSet<int>();
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    set.Add(bitmap.GetPixelARGB(i, j));
                }
            }

            return set.Count == 3;
        }

        private void FloodFill(BetterCanvasBitmap bmp, IIntersectable surface, Vector2 point, Color targetColor, Color replacementColor)
        {
            Stack<Vector2> pixels = new Stack<Vector2>();
            targetColor = bmp.GetPixel((int)point.X, (int)point.Y);
            pixels.Push(point);

            while (pixels.Count > 0)
            {
                Vector2 a = pixels.Pop();
                a = new Vector2((float)WrapParam(bmp.Width, surface.IsUWrapped, a.X), (float)WrapParam(bmp.Height, surface.IsVWrapped, a.Y));
                if (a.X < bmp.Width && a.X >= 0 && a.Y < bmp.Height && a.Y >= 0)
                {
                    if (bmp.GetPixel((int)a.X, (int)a.Y) == targetColor)
                    {
                        bmp.SetPixel((int)a.X, (int)a.Y, replacementColor);
                        pixels.Push(new Vector2(a.X - 1, a.Y));
                        pixels.Push(new Vector2(a.X + 1, a.Y));
                        pixels.Push(new Vector2(a.X, a.Y - 1));
                        pixels.Push(new Vector2(a.X, a.Y + 1));
                    }
                }
            }
        }

        public static double WrapParam(int maxParam, bool isWrapped, double param)
        {
            if (param < 0)
            {
                if (isWrapped)
                {
                    do
                    {
                        param += maxParam;
                    } while (param < 0);
                    return param;
                }
                return 0;
            }
            if (param > maxParam)
            {
                if (isWrapped)
                {
                    do
                    {
                        param -= maxParam;
                    } while (param > maxParam);
                    return param;
                }
                return maxParam;
            }

            return param;
        }
    }
}
