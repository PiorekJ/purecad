﻿#version 330
layout(points) in;
layout(triangle_strip, max_vertices = 256) out;

#define M_PI 3.1415926535897932384626433832795

in V_Out
{
	vec3 worldPos;
} In[];

uniform float radius;

int rings = 16;
float ringsF = 16.0f;
int sections = 16;
float sectionsF = 16.0f;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection*view*model;

void main()
{
	vec3 position = In[0].worldPos;

    for(int i=0; i<rings; i++)
	{
		float point1 = i/ringsF * 2 * M_PI;
		float point2 = (i+1)/ringsF * 2 * M_PI;
	
		float ring11 = sin(point1) * radius;
		float ring12 = cos(point1) * radius;

		float ring21 = sin(point2) * radius;
		float ring22 = cos(point2) * radius; 
	
		for(int j=0; j<sections; j++) 
		{
			float section =	j/sectionsF * 2* M_PI;
			vec3 p1 = vec3(sin(section)*ring11, cos(section)*ring11 , ring12);
			gl_Position = projectionViewModel*vec4(position + p1, 1.0);
			EmitVertex();
			
			vec3 p2 = vec3(sin(section)*ring21, cos(section)*ring21 , ring22);
			gl_Position = projectionViewModel*vec4(position + p2, 1.0);
			EmitVertex();
		}
	}

	EndPrimitive();
}