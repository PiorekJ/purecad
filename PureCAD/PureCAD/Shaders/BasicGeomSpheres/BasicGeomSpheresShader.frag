﻿#version 330 core

out layout(location = 0) vec4 FragColor;

uniform vec3 color;
uniform vec3 camPos;

vec3 Phong(vec3 worldNormal, vec3 viewDir, vec3 lightDir, vec3 lightColor, vec3 materialColor, float materialShininess)
{
	float diffuseFactor = max(dot(lightDir, worldNormal), 0.0f);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specularFactor = pow(max(dot(worldNormal, halfwayDir), 0.0f), materialShininess);

	return (diffuseFactor * materialColor + specularFactor) * lightColor;
}

void main()
{	
	//vec3 worldNormal = normalize(In.normal);
	//vec3 viewDir = normalize(camPos - In.worldPos);
	//vec3 lightDir = normalize(vec3(0,20,0) - In.worldPos);
	//vec3 matColor = vec3(0,0,0);
	//matColor += Phong(worldNormal, viewDir, lightDir, vec3(1,1,1), color, 64);
	FragColor = vec4(color, 1.0f);
}
