﻿#version 330 core

in layout(location = 0) vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out V_Out
{
	vec3 worldPos;
} Out;

void main()
{
	vec4 worldPos = model * vec4(position, 1.0f);
	Out.worldPos = worldPos.xyz;
	gl_PointSize = 4;
	gl_Position = projection * view * worldPos;
}