﻿#version 330 core

layout (lines) in;
layout (line_strip, max_vertices = 2) out;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


uniform vec3 points[16];  
uniform bool drawVectors;

mat4 projectionViewModel = projection * view * model;

vec3 deCasteljau(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
	float paramT = (1-t);
	
	a = a * paramT + b*t;
	b = b * paramT + c*t;
	c = c * paramT + d*t;

	a = a * paramT + b*t;
	b = b * paramT + c*t;
	
	a = a * paramT + b*t;

	return a;
}

vec3 surfaceCalc(vec2 positionUV)
{
	vec3 sumLOne = deCasteljau(positionUV.y, points[0], points[4], points[8], points[12]);
	vec3 sumLTwo = deCasteljau(positionUV.y, points[1], points[5], points[9], points[13]);
	vec3 sumLThree = deCasteljau(positionUV.y, points[2], points[6], points[10], points[14]);
	vec3 sumLFour = deCasteljau(positionUV.y, points[3], points[7], points[11], points[15]);

	vec3 sumR = deCasteljau(positionUV.x, sumLOne, sumLTwo, sumLThree, sumLFour);
	
	return sumR;
}

void DrawPrimitive(vec4 start, vec4 end)
{
	gl_Position = start;
	EmitVertex();
	gl_Position = end;
	EmitVertex();
	EndPrimitive();
}

void main()
{
	gl_PointSize = 4;
	
	vec2 startUV = vec2(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y);
	vec2 endUV = vec2(gl_in[1].gl_Position.x, gl_in[1].gl_Position.y);

	vec3 lineStart = surfaceCalc(startUV);
	vec3 lineEnd = surfaceCalc(endUV);

	vec4 sceneLineStart = projectionViewModel * vec4(lineStart, 1.0);
	vec4 sceneLineEnd = projectionViewModel * vec4(lineEnd, 1.0);
	DrawPrimitive(sceneLineStart, sceneLineEnd);	
} 