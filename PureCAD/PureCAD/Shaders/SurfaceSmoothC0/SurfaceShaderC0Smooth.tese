﻿#version 400 core
layout (isolines, equal_spacing) in;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 points[16];
uniform int isOffset;
uniform float offset;
mat4 projectionViewModel = projection * view * model;

out vec2 uvOut;

vec3 EvaluateDerivative(vec3 p0, vec3 p1, vec3 p2, vec3 p3, float t)
{
	vec3 d0 = 3.0f * (p1 - p0);
	vec3 d1 = 3.0f * (p2 - p1);
	vec3 d2 = 3.0f * (p3 - p2);

	float tInv = 1.0f - t;

	vec3 a0 = tInv * d0 + t * d1;
	vec3 a1 = tInv * d1 + t * d2;

	return tInv * a0 + t * a1;
}

vec3 Evaluate(vec3 p0, vec3 p1, vec3 p2, vec3 p3, float t)
{
	float tInv = 1.0f - t;

	vec3 a0 = tInv * p0 + t * p1;
	vec3 a1 = tInv * p1 + t * p2;
	vec3 a2 = tInv * p2 + t * p3;

	vec3 b0 = tInv * a0 + t * a1;
	vec3 b1 = tInv * a1 + t * a2;

	return tInv * b0 + t * b1;
}

vec4 Evaluate(vec4 p0, vec4 p1, vec4 p2, vec4 p3, float t)
{
	float tInv = 1.0f - t;

	vec4 a0 = tInv * p0 + t * p1;
	vec4 a1 = tInv * p1 + t * p2;
	vec4 a2 = tInv * p2 + t * p3;

	vec4 b0 = tInv * a0 + t * a1;
	vec4 b1 = tInv * a1 + t * a2;

	return tInv * b0 + t * b1;
}

vec3 EvaluateDerivativeU(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(points[0], points[4], points[8], points[12], v),
		Evaluate(points[1], points[5], points[9], points[13], v),
		Evaluate(points[2], points[6], points[10], points[14], v),
		Evaluate(points[3], points[7], points[11], points[15], v),
		u);
}

vec3 EvaluateDerivativeV(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(points[0], points[1], points[2], points[3], u),
		Evaluate(points[4], points[5], points[6], points[7], u),
		Evaluate(points[8], points[9], points[10], points[11], u),
		Evaluate(points[12], points[13], points[14], points[15], u),
		v);
}

vec3 EvaluatePosition(float u, float v)
{
	return Evaluate(
		Evaluate(points[0], points[1], points[2], points[3], u),
		Evaluate(points[4], points[5], points[6], points[7], u),
		Evaluate(points[8], points[9], points[10], points[11], u),
		Evaluate(points[12], points[13], points[14], points[15], u),
		v
	);
}


void main()
{
	vec2 uv = mix(gl_in[0].gl_Position.xy, gl_in[1].gl_Position.xy, gl_TessCoord.x);
	uvOut = uv;
	if(isOffset == 1)
	{
		vec3 scenePoint = EvaluatePosition(uv.x, uv.y);
		vec3 du = normalize(EvaluateDerivativeU(uv.x, uv.y));
		vec3 dv = normalize(EvaluateDerivativeV(uv.x, uv.y));
		vec3 normal = normalize(cross(du, dv));
		scenePoint += (normal * (offset));
		gl_Position = projectionViewModel * vec4(scenePoint, 1.0f);
	}
	else
	{		
		gl_Position = projectionViewModel * vec4(EvaluatePosition(uv.x, uv.y), 1.0);	
	}
}