﻿#version 400 core
layout (quads, equal_spacing) in;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform int uOffset;
uniform int vOffset;
uniform float millRadius;
uniform float margin;

mat4 projectionViewModel = projection * view * model;

in TESC_Out
{
	vec3 worldPos;
} In[];

out TESE_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} Out;


float[3] GetBasis3(float t)
{
    float N[3];
    N[0] = 1;
    for (int i = 1; i < 3; i++)
    {
        float saved = 0;
        for (int j = 0; j < i; j++)
        {
            float term = N[j] / i;
            N[j] = saved + (j - t + 1) * term;
            saved = (t + i - 1 - j) * term;
        }
        N[i] = saved;
    }
    return N;
}

float[4] GetBasis4(float t)
{
    float N[4];
    N[0] = 1;
    for (int i = 1; i < 4; i++)
    {
        float saved = 0;
        for (int j = 0; j < i; j++)
        {
            float term = N[j] / i;
            N[j] = saved + (j - t + 1) * term;
            saved = (t + i - 1 - j) * term;
        }
        N[i] = saved;
    }
    return N;
}

vec3 Evaluate(vec3 a, vec3 b, vec3 c, vec3 d, float t)
{
    float N[] = GetBasis4(t); 
    
    return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
}

vec4 Evaluate(vec4 a, vec4 b, vec4 c, vec4 d, float t)
{
    float N[] = GetBasis4(t); 
    
    return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
}

vec3 EvaluateDerivative(vec3 a, vec3 b, vec3 c, vec3 d, float t)
{
	float N[] = GetBasis3(t);
    vec3 deriv0 = b - a;
    vec3 deriv1 = c - b;
    vec3 deriv2 = d - c;

    return deriv0 * N[0] + deriv1 * N[1] + deriv2 * N[2];
}

vec3 EvaluateDerivativeU(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(In[0].worldPos, In[4].worldPos, In[8].worldPos, In[12].worldPos, v),
		Evaluate(In[1].worldPos, In[5].worldPos, In[9].worldPos, In[13].worldPos, v),
		Evaluate(In[2].worldPos, In[6].worldPos, In[10].worldPos, In[14].worldPos, v),
		Evaluate(In[3].worldPos, In[7].worldPos, In[11].worldPos, In[15].worldPos, v),
		u);
}

vec3 EvaluateDerivativeV(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(In[0].worldPos, In[1].worldPos, In[2].worldPos, In[3].worldPos, u),
		Evaluate(In[4].worldPos, In[5].worldPos, In[6].worldPos, In[7].worldPos, u),
		Evaluate(In[8].worldPos, In[9].worldPos, In[10].worldPos, In[11].worldPos, u),
		Evaluate(In[12].worldPos, In[13].worldPos, In[14].worldPos, In[15].worldPos, u),
		v);
}

vec3 EvaluatePosition(float u, float v)
{
	return Evaluate(
		Evaluate(In[0].worldPos, In[1].worldPos, In[2].worldPos, In[3].worldPos, u),
		Evaluate(In[4].worldPos, In[5].worldPos, In[6].worldPos, In[7].worldPos, u),
		Evaluate(In[8].worldPos, In[9].worldPos, In[10].worldPos, In[11].worldPos, u),
		Evaluate(In[12].worldPos, In[13].worldPos, In[14].worldPos, In[15].worldPos, u),
		v
	);
}

void main()
{
	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;

	vec3 scenePoint = EvaluatePosition(u, v);

	vec3 du = normalize(EvaluateDerivativeU(u, v));
	vec3 dv = normalize(EvaluateDerivativeV(u, v));
	vec3 normal = normalize(cross(du, dv));
	scenePoint += (normal * (millRadius + margin));
	//scenePoint.y -= 0.80f;
	gl_Position = projection * view * vec4(scenePoint, 1.0f);
	Out.worldPos = scenePoint;
	Out.normal = normal;
	Out.tangent = dv;
	Out.binormal = du;
}