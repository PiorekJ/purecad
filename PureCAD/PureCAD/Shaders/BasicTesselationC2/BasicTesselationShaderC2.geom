﻿#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in TESE_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} In[];

out GEOM_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} Out;

void main()
{
    for (int i = 0; i<gl_in.length(); i++)
    {
       gl_Position = gl_in[i].gl_Position;	  
	   Out.worldPos = In[i].worldPos;
       Out.normal = In[i].normal;
	   Out.tangent = In[i].tangent;
	   Out.binormal = In[i].binormal;
       EmitVertex();	   
    }

   EndPrimitive();
}