﻿#version 400 core
layout (quads, equal_spacing) in;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform int uOffset;
uniform int vOffset;
uniform float millRadius;
uniform float margin;

mat4 projectionViewModel = projection * view * model;

in TESC_Out
{
	vec3 worldPos;
} In[];

out TESE_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} Out;

vec3 EvaluateDerivative(vec3 p0, vec3 p1, vec3 p2, vec3 p3, float t)
{
	vec3 d0 = 3.0f * (p1 - p0);
	vec3 d1 = 3.0f * (p2 - p1);
	vec3 d2 = 3.0f * (p3 - p2);

	float tInv = 1.0f - t;

	vec3 a0 = tInv * d0 + t * d1;
	vec3 a1 = tInv * d1 + t * d2;

	return tInv * a0 + t * a1;
}

vec3 Evaluate(vec3 p0, vec3 p1, vec3 p2, vec3 p3, float t)
{
	float tInv = 1.0f - t;

	vec3 a0 = tInv * p0 + t * p1;
	vec3 a1 = tInv * p1 + t * p2;
	vec3 a2 = tInv * p2 + t * p3;

	vec3 b0 = tInv * a0 + t * a1;
	vec3 b1 = tInv * a1 + t * a2;

	return tInv * b0 + t * b1;
}

vec4 Evaluate(vec4 p0, vec4 p1, vec4 p2, vec4 p3, float t)
{
	float tInv = 1.0f - t;

	vec4 a0 = tInv * p0 + t * p1;
	vec4 a1 = tInv * p1 + t * p2;
	vec4 a2 = tInv * p2 + t * p3;

	vec4 b0 = tInv * a0 + t * a1;
	vec4 b1 = tInv * a1 + t * a2;

	return tInv * b0 + t * b1;
}

vec3 EvaluateDerivativeU(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(In[0].worldPos, In[4].worldPos, In[8].worldPos, In[12].worldPos, v),
		Evaluate(In[1].worldPos, In[5].worldPos, In[9].worldPos, In[13].worldPos, v),
		Evaluate(In[2].worldPos, In[6].worldPos, In[10].worldPos, In[14].worldPos, v),
		Evaluate(In[3].worldPos, In[7].worldPos, In[11].worldPos, In[15].worldPos, v),
		u);
}

vec3 EvaluateDerivativeV(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(In[0].worldPos, In[1].worldPos, In[2].worldPos, In[3].worldPos, u),
		Evaluate(In[4].worldPos, In[5].worldPos, In[6].worldPos, In[7].worldPos, u),
		Evaluate(In[8].worldPos, In[9].worldPos, In[10].worldPos, In[11].worldPos, u),
		Evaluate(In[12].worldPos, In[13].worldPos, In[14].worldPos, In[15].worldPos, u),
		v);
}

vec3 EvaluatePosition(float u, float v)
{
	return Evaluate(
		Evaluate(In[0].worldPos, In[1].worldPos, In[2].worldPos, In[3].worldPos, u),
		Evaluate(In[4].worldPos, In[5].worldPos, In[6].worldPos, In[7].worldPos, u),
		Evaluate(In[8].worldPos, In[9].worldPos, In[10].worldPos, In[11].worldPos, u),
		Evaluate(In[12].worldPos, In[13].worldPos, In[14].worldPos, In[15].worldPos, u),
		v
	);
}

void main()
{
	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;

	vec3 scenePoint = EvaluatePosition(u, v);

	vec3 du = normalize(EvaluateDerivativeU(u, v));
	vec3 dv = normalize(EvaluateDerivativeV(u, v));
	vec3 normal = normalize(cross(du, dv));
	scenePoint += (normal * (millRadius + margin));
	//scenePoint.y -= 0.80;
	gl_Position = projection * view * vec4(scenePoint, 1.0f);
	Out.worldPos = scenePoint;
	Out.normal = normal;
	Out.tangent = dv;
	Out.binormal = du;
}