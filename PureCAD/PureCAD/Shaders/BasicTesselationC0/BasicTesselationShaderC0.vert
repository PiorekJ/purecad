﻿#version 330 core

in layout(location = 0) vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out V_Out
{
	vec3 worldPos;
	vec3 viewPos;
} Out;

void main()
{
	vec4 worldPos = model * vec4(position, 1.0f);
	vec4 viewPos = view * worldPos;

	Out.worldPos = worldPos.xyz;
	Out.viewPos = viewPos.xyz;
	gl_Position = projection * viewPos;
}