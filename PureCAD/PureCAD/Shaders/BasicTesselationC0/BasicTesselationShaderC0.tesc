﻿#version 400 core

layout (vertices = 16) out;

uniform float innerDivision;
uniform float edgeDivision;

in V_Out
{
	vec3 worldPos;
	vec3 viewPos;
} In[];

out TESC_Out
{
	vec3 worldPos;
} Out[];

float GetTesselationFactor(vec3 pos)
{
	return 16.0f / length(pos);
}

void main()
{
	if(gl_InvocationID == 0)
	{
		//vec3 p0 = In[0].viewPos;
		//vec3 p1 = In[3].viewPos;
		//vec3 p2 = In[12].viewPos;
		//vec3 p3 = In[15].viewPos;
		//
		//float f0 = GetTesselationFactor(p0);
		//float f1 = GetTesselationFactor(p1);
		//float f2 = GetTesselationFactor(p2);
		//float f3 = GetTesselationFactor(p3);

		gl_TessLevelOuter[0] = 128;//(f0 + f2) * edgeDivision;
		gl_TessLevelOuter[1] = 128;//(f0 + f1) * edgeDivision;
		gl_TessLevelOuter[2] = 128;//(f1 + f3) * edgeDivision;
		gl_TessLevelOuter[3] = 128;//(f2 + f3) * edgeDivision;

		gl_TessLevelInner[0] = 128;//(f0 + f1 + f2 + f3) * innerDivision;
		gl_TessLevelInner[1] = 128;//(f0 + f1 + f2 + f3) * innerDivision;	
	}						   

	Out[gl_InvocationID].worldPos = In[gl_InvocationID].worldPos;
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}