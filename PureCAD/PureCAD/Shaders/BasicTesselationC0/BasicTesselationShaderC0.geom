﻿#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

#define M_PI 3.1415926535897932384626433832795

in TESE_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} In[];

out GEOM_OUT
{
	vec3 worldPos;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
} Out;

void main()
{
	//int rings = 32;
	//int sectors = 32;
	//float radius = 0.8f;
	//float R = 1.0f / float((rings - 1));
    //float S = 1.0f / float((sectors - 1));
	for (int i = 0; i<gl_in.length(); i++)
    {
       gl_Position = gl_in[i].gl_Position;	  
	   Out.worldPos = In[i].worldPos;
       Out.normal = In[i].normal;
	   Out.tangent = In[i].tangent;
	   Out.binormal = In[i].binormal;
	   //for (int r = 0; r < rings; ++r)
	   //{
	   //    for (int s = 0; s < sectors; ++s)
	   //    {
	   //        float y = float(sin(-M_PI/2.0f + M_PI * r * R));
	   //        float x = float(cos(2 * M_PI * s * S) * float(sin(M_PI * r * R)));
	   //        float z = float(sin(2 * M_PI * s * S) * float(sin(M_PI * r * R)));
	   //
	   //		   vec4 position = vec4(x * radius, y * radius, z * radius, 0.0);
	   //		   vec3 normal = vec3(x, y, z);
		//	   gl_Position = gl_in[i].gl_Position + position;	 
		//	  
	   //    }
	   //}      
	   EmitVertex();
	}
	EndPrimitive();
    
}