﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in VS_OUT 
{
	vec3 color;
} In;

void main()
{
	FragColor = vec4(In.color, 1.0f);
}
