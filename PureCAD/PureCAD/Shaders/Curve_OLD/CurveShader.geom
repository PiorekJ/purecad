﻿#version 330 core

layout (lines) in;
layout (line_strip, max_vertices = 2) out; //points

in TE_OUT 
{
	vec3 color;
} In[];

out GEO_OUT 
{
	vec3 color;
} Out;

void main()
{
	gl_PointSize = 4;
	gl_Position = gl_in[0].gl_Position;
	Out.color = In[0].color;
	EmitVertex();
	gl_Position = gl_in[1].gl_Position;
	Out.color = In[1].color;
	EmitVertex();
	EndPrimitive(); //w/o for points
} 