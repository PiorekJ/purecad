﻿#version 400 core

layout (isolines, equal_spacing) in;

in TC_OUT 
{
	vec3 position;
	vec3 color;
} In[];

out TE_OUT
{
	vec3 color;
} Out;

vec3 deCasteljau(float t, vec3 a, vec3 b, vec3 c, vec3 d)
{
	float paramT = (1-t);
	
	a = a * paramT + b*t;
	b = b * paramT + c*t;
	c = c * paramT + d*t;

	a = a * paramT + b*t;
	b = b * paramT + c*t;
	
	a = a * paramT + b*t;

	return a;
}

uniform mat4 view;
uniform mat4 projection;

mat4 projectionView = projection * view;

void main()
{
	 gl_Position = projectionView * vec4(deCasteljau(gl_TessCoord.x, In[0].position, In[1].position, In[2].position, In[3].position),1);
	 Out.color = deCasteljau(gl_TessCoord.x, In[0].color, In[1].color, In[2].color, In[3].color);
}