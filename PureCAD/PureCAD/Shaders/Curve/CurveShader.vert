﻿#version 330 core

in layout(location = 0) vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

mat4 projectionViewModel = projection * view;

void main()
{
	gl_Position = projectionViewModel * vec4(position,1);
}
