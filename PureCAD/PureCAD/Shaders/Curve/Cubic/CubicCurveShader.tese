﻿#version 400 core
layout (isolines, equal_spacing) in;

out TE_OUT
{
	vec4 a;
	vec4 b;
	vec4 c;
	vec4 d;
} Out;


void main()
{	
	Out.a = gl_in[0].gl_Position;
	Out.b = gl_in[1].gl_Position;
	Out.c = gl_in[2].gl_Position;
	Out.d = gl_in[3].gl_Position;
	gl_Position.x = gl_TessCoord.x;
}