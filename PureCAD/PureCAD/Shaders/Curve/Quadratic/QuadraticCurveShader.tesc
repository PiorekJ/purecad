﻿#version 400 core

layout (vertices=3) out;

uniform vec2 screenSize;

float getLineLength(vec4 a, vec4 b)
{
	return length((a.xy/a.w - b.xy/b.w)/2 * screenSize);
}

void main()
{
	if(gl_InvocationID == 0)
	{
		float polygonLength = getLineLength(gl_in[0].gl_Position, gl_in[1].gl_Position) + getLineLength(gl_in[1].gl_Position, gl_in[2].gl_Position);
		gl_TessLevelOuter[0] = 1;
		gl_TessLevelOuter[1] = polygonLength/64.0f + 3;
	}
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}