﻿#version 330 core
layout (lines) in;
layout (line_strip, max_vertices = 64) out; //points

in TE_OUT 
{
	vec4 a;
	vec4 b;
	vec4 c;
} In[];

vec4 deCasteljau(float t, vec4 a, vec4 b, vec4 c)
{
	float paramT = (1-t);
	
	a = a * paramT + b*t;
	b = b * paramT + c*t;

	a = a * paramT + b*t;

	return a;
}

void main()
{
	float start = gl_in[0].gl_Position.x; 
	float end = gl_in[1].gl_Position.x; 

	float size = end - start;
	const int div = 64;
	for(int i = 0; i < div; i++)
	{
		gl_Position = deCasteljau(start + size * i/float(div-1), In[0].a, In[0].b, In[0].c);
		gl_PointSize = 4;
		EmitVertex();
	}

	EndPrimitive();
} 