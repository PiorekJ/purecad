﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in vec2 texCoord;

uniform sampler2D sampler;

void main()
{
	vec3 color = texture(sampler, texCoord).rgb;

	FragColor = vec4(color, 1);
}
