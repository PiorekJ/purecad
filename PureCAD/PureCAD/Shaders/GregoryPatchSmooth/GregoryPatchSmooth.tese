﻿#version 400 core
layout (isolines, equal_spacing) in;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 points[20];

mat4 projectionViewModel = projection * view * model;

float eps = 0.000001;

mat4 GetXMatrix(vec2 uv)
{
	
	float div = uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F0 = (uv.x * points[5].x + uv.y * points[6].x)/div;

	div = 1 - uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F1 = ((1-uv.x)*points[11].x + uv.y * points[12].x)/div;

	div = 2 - uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F2 = ((1 - uv.x)*points[14].x + (1-uv.y)*points[13].x)/div;

	div = 1 + uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F3 = (uv.x * points[8].x + (1-uv.y)*points[7].x)/div;


	vec4 column1 = vec4(points[0].x, points[4].x, points[10].x, points[16].x);
	vec4 column2 = vec4(points[1].x, F0, F1, points[17].x);
	vec4 column3 = vec4(points[2].x, F3, F2, points[18].x);
	vec4 column4 = vec4(points[3].x, points[9].x, points[15].x, points[19].x);
		
	return mat4(column1, column2, column3, column4);
}

mat4 GetYMatrix(vec2 uv)
{
	
	float div = uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F0 = (uv.x * points[5].y + uv.y * points[6].y)/div;

	div = 1 - uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F1 = ((1-uv.x)*points[11].y + uv.y * points[12].y)/div;

	div = 2 - uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F2 = ((1 - uv.x)*points[14].y + (1-uv.y)*points[13].y)/div;

	div = 1 + uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F3 = (uv.x * points[8].y + (1-uv.y)*points[7].y)/div;


	vec4 column1 = vec4(points[0].y, points[4].y, points[10].y, points[16].y);
	vec4 column2 = vec4(points[1].y, F0, F1, points[17].y);
	vec4 column3 = vec4(points[2].y, F3, F2, points[18].y);
	vec4 column4 = vec4(points[3].y, points[9].y, points[15].y, points[19].y);
	
	return mat4(column1, column2, column3, column4);
}

mat4 GetZMatrix(vec2 uv)
{
	
	float div = uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F0 = (uv.x * points[5].z + uv.y * points[6].z)/div;

	div = 1 - uv.x + uv.y;
	if(div == 0)
		div = eps;
	float F1 = ((1-uv.x)*points[11].z + uv.y * points[12].z)/div;

	div = 2 - uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F2 = ((1 - uv.x)*points[14].z + (1-uv.y)*points[13].z)/div;

	div = 1 + uv.x - uv.y;
	if(div == 0)
		div = eps;
	float F3 = (uv.x * points[8].z + (1-uv.y)*points[7].z)/div;


	//vec4 column1 = vec4(points[0].z,  points[1].z,  points[2].z,      points[3].z);
	//vec4 column2 = vec4(points[4].z,  F0,           F3,               points[9].z);
	//vec4 column3 = vec4(points[10].z, F1,           F2,               points[15].z);
	//vec4 column4 = vec4(points[16].z, points[17].z, points[18].z,     points[19].z);

	vec4 column1 = vec4(points[0].z, points[4].z, points[10].z, points[16].z);
	vec4 column2 = vec4(points[1].z, F0, F1, points[17].z);
	vec4 column3 = vec4(points[2].z, F3, F2, points[18].z);
	vec4 column4 = vec4(points[3].z, points[9].z, points[15].z, points[19].z);
	
	return mat4(column1, column2, column3, column4);
}

float bezier0(float t)
{
	return (1-t)*(1-t)*(1-t);
}

float bezier1(float t)
{
	return 3.0f*(1-t)*(1-t)*t;
}

float bezier2(float t)
{
	return 3.0f*(1-t)*t*t;
}

float bezier3(float t)
{
	return t*t*t;
}

vec3 surfaceCalc(vec2 uv)
{
	vec4 bezierU = vec4(bezier0(uv.x), bezier1(uv.x), bezier2(uv.x), bezier3(uv.x));
	vec4 bezierV = vec4(bezier0(uv.y), bezier1(uv.y), bezier2(uv.y), bezier3(uv.y));
	mat4 matX = GetXMatrix(uv);
	mat4 matY = GetYMatrix(uv);
	mat4 matZ = GetZMatrix(uv);

	float x = dot(bezierU, matX * bezierV);
	float y = dot(bezierU, matY * bezierV);
	float z = dot(bezierU, matZ * bezierV);

	return vec3(x,y,z);
}

void main()
{
	vec2 uv = mix(gl_in[0].gl_Position.xy, gl_in[1].gl_Position.xy, gl_TessCoord.x);
	gl_Position = projectionViewModel * vec4(surfaceCalc(uv), 1.0f);
}