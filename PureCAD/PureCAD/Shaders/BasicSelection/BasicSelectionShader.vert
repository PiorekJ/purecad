﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec2 texCoord;

out vec2 texCoords;

void main()
{
	texCoords = texCoord;
	gl_Position = vec4(position.xy, 0,1);
}
