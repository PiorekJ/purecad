﻿#version 330 core

out layout(location = 0) vec4 FragColor;

uniform float alpha;
uniform vec3 color;

uniform float borderWidth;
uniform float aspectRatio;

in vec2 texCoords;

void main()
{
	float maxX = 1.0 - borderWidth;
	float minX = borderWidth;
	float maxY = maxX / aspectRatio;
	float minY = maxX / aspectRatio;

	if (texCoords.x < maxX && texCoords.x > minX && texCoords.y < maxY && texCoords.y > minY) 
    {
		FragColor = vec4(color,1.0f);
    }
    else
	{
		FragColor = vec4(color,alpha);
    }
}
