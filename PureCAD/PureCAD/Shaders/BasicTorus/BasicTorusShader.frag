﻿#version 330 core
#define M_PI 3.1415926535897932384626433832795

out layout(location = 0) vec4 FragColor;

uniform vec3 color;
uniform sampler2D Tex;
uniform int sideIdx;

in vec2 uvOut;

void main()
{
if(sideIdx == 1)
	{
		if(texture(Tex, vec2(uvOut.x / (2*M_PI),uvOut.y / (2*M_PI))).rgb == vec3(1,1,1))
			discard;
	}
	else if(sideIdx == 2)
	{
		if(texture(Tex, vec2(uvOut.x / (2*M_PI),uvOut.y / (2*M_PI))).rgb != vec3(1,1,1))
			discard;
	}

	FragColor = vec4(color,1);
}
