﻿#version 330 core

in layout(location = 0) vec2 uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float r;
uniform float R;

mat4 projectionViewModel = projection*view*model;
out vec2 uvOut;

void main()
{
	uvOut = uv;
	float x = (r * cos(uv.x) + R) * cos(uv.y);
    float y = r * sin(uv.x);
    float z = (r * cos(uv.x) + R) * sin(uv.y);
	gl_Position = projectionViewModel * vec4(x,y,z,1);
	gl_PointSize = 4;
}
