﻿#version 330 core

out layout(location = 0) vec4 FragColor;

uniform vec3 color;  
uniform sampler2D Tex;
uniform int uOffset;
uniform int vOffset;
uniform int patchesU;
uniform int patchesV;
uniform int sideIdx;

in vec2 uvOut;


void main()
{
	if(sideIdx == 1)
	{
		if(texture(Tex, vec2(uvOut.x/patchesU + float(uOffset)/patchesU, uvOut.y/patchesV + float(vOffset)/patchesV)).rgb == vec3(1,1,1))
			discard;
	}
	else if(sideIdx == 2)
	{
		if(texture(Tex, vec2(uvOut.x/patchesU + float(uOffset)/patchesU, uvOut.y/patchesV + float(vOffset)/patchesV)).rgb != vec3(1,1,1))
			discard;
	}

	FragColor = vec4(color,1.0);
}
