﻿#version 400 core
layout (isolines, equal_spacing) in;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 points[16];
uniform int isOffset;
uniform float offset;

mat4 projectionViewModel = projection * view * model;

out vec2 uvOut;

float[3] GetBasis3(float t)
{
    float N[3];
    N[0] = 1;
    for (int i = 1; i < 3; i++)
    {
        float saved = 0;
        for (int j = 0; j < i; j++)
        {
            float term = N[j] / i;
            N[j] = saved + (j - t + 1) * term;
            saved = (t + i - 1 - j) * term;
        }
        N[i] = saved;
    }
    return N;
}

float[4] GetBasis4(float t)
{
    float N[4];
    N[0] = 1;
    for (int i = 1; i < 4; i++)
    {
        float saved = 0;
        for (int j = 0; j < i; j++)
        {
            float term = N[j] / i;
            N[j] = saved + (j - t + 1) * term;
            saved = (t + i - 1 - j) * term;
        }
        N[i] = saved;
    }
    return N;
}

vec3 Evaluate(vec3 a, vec3 b, vec3 c, vec3 d, float t)
{
    float N[] = GetBasis4(t); 
    
    return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
}

vec4 Evaluate(vec4 a, vec4 b, vec4 c, vec4 d, float t)
{
    float N[] = GetBasis4(t); 
    
    return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
}

vec3 EvaluateDerivative(vec3 a, vec3 b, vec3 c, vec3 d, float t)
{
	float N[] = GetBasis3(t);
    vec3 deriv0 = b - a;
    vec3 deriv1 = c - b;
    vec3 deriv2 = d - c;

    return deriv0 * N[0] + deriv1 * N[1] + deriv2 * N[2];
}

vec3 EvaluateDerivativeU(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(points[0], points[4], points[8],  points[12], v),
		Evaluate(points[1], points[5], points[9],  points[13], v),
		Evaluate(points[2], points[6], points[10], points[14], v),
		Evaluate(points[3], points[7], points[11], points[15], v),
		u);
}

vec3 EvaluateDerivativeV(float u, float v)
{
	return EvaluateDerivative(
		Evaluate(points[0], points[1], points[2], points[3], u),
		Evaluate(points[4], points[5], points[6], points[7], u),
		Evaluate(points[8], points[9], points[10], points[11], u),
		Evaluate(points[12], points[13], points[14], points[15], u),
		v);
}

vec3 EvaluatePosition(float u, float v)
{
	return Evaluate(
		Evaluate(points[0], points[1], points[2], points[3], u),
		Evaluate(points[4], points[5], points[6], points[7], u),
		Evaluate(points[8], points[9], points[10], points[11], u),
		Evaluate(points[12], points[13], points[14], points[15], u),
		v
	);
}

void main()
{
	vec2 uv = mix(gl_in[0].gl_Position.xy, gl_in[1].gl_Position.xy, gl_TessCoord.x);
	uvOut = uv;
	if(isOffset == 1)
	{
		vec3 scenePoint = EvaluatePosition(uv.x, uv.y);
		vec3 du = normalize(EvaluateDerivativeU(uv.x, uv.y));
		vec3 dv = normalize(EvaluateDerivativeV(uv.x, uv.y));
		vec3 normal = normalize(cross(du, dv));
		scenePoint += (normal * (offset));
		gl_Position = projectionViewModel * vec4(scenePoint, 1.0f);
	}
	else
	{		
		gl_Position = projectionViewModel * vec4(EvaluatePosition(uv.x, uv.y), 1.0);	
	}

}