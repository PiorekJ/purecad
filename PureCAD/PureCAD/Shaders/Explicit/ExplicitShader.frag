﻿#version 330 core

out layout(location = 0) vec4 FragColor;

uniform vec3 radii = vec3(1,1,1);

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float intesityParameter;

in vec2 vTexCoords;

void main()
{
	if(radii.x <= 0 || radii.y <= 0 || radii.z <= 0)
	{
		FragColor = vec4(0,0,0,1);
		return;
	}

	mat4 dMatrix = mat4(1.0f / (radii.x * radii.x), 0, 0, 0, 0, 1.0f / (radii.y * radii.y), 0, 0, 0, 0, 1.0f / (radii.z * radii.z), 0, 0, 0, 0, -1);

	mat4 invModelView = inverse(model) * inverse(view);
	mat4 invProj = inverse(projection);

	mat4 invCamera = invModelView * invProj;

	mat4 dM = transpose(invCamera) * dMatrix * invCamera;
	
	float x = vTexCoords.x * 2.0f - 1.0f;
	float y = vTexCoords.y * 2.0f - 1.0f;	

	float a = dM[2][2];
	float b = dM[2][0] * x + dM[2][1] * y + dM[0][2] * x + dM[1][2] * y + dM[3][2] + dM[2][3];
	float c = dM[0][0] * x * x + dM[1][0] * x * y + dM[3][0] * x + dM[0][1] * x * y + dM[1][1] * y * y + dM[3][1] * y + dM[0][3] * x + dM[1][3] * y + dM[3][3];
	
	float delta =  b * b - 4 * a * c;

	if(delta < 0.0f)
	{
		FragColor = vec4(0,0,0,1);
		return;
	}

	float z = 0;

	if(abs(delta) < 0.0000001)
	{
		z = -b/(2 * a);
	}
	else
	{
		float b1 = (-b - sqrt(delta))/(2 * a);
		float b2 = (-b + sqrt(delta))/(2 * a);
		z = min(b1,b2);
	}

	if(z < 0.0f)
	{
		FragColor = vec4(0,0,0,1);
		return;
	}

	vec4 scenePoint = invProj * vec4(x, y, z, 1);
	scenePoint /= scenePoint.w;
	vec3 ray = normalize(-scenePoint.xyz);

	scenePoint = invModelView * scenePoint;
	
	vec3 grad = vec3(scenePoint.x * dMatrix[0][0], scenePoint.y * dMatrix[1][1], scenePoint.z * dMatrix[2][2]);
	
	grad = normalize(transpose(mat3(invModelView)) * grad);

	float dotProduct = dot(ray,grad);

	float I = pow(max(0,dotProduct), intesityParameter);

	FragColor = vec4(I * vec3(1,1,0),1);
}
