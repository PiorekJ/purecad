﻿#version 330 core

out layout(location = 0) vec4 FragColor;

in vec2 texCoord;

uniform sampler2D leftSampler;
uniform sampler2D rightSampler;
uniform vec3 redLeftWeights;
uniform vec3 redRightWeights;
uniform vec3 greenLeftWeights;
uniform vec3 greenRightWeights;
uniform vec3 blueLeftWeights;
uniform vec3 blueRightWeights;


void main()
{
	vec3 leftColor = texture(leftSampler, texCoord).rgb;
	vec3 rightColor = texture(rightSampler, texCoord).rgb;
	
	float red = redLeftWeights.x * leftColor.x + redLeftWeights.y * leftColor.y + redLeftWeights.z * leftColor.z + redRightWeights.x * rightColor.x + redRightWeights.y * rightColor.y + redRightWeights.z * rightColor.z;
	float green = greenLeftWeights.x * leftColor.x + greenLeftWeights.y * leftColor.y + greenLeftWeights.z * leftColor.z + greenRightWeights.x * rightColor.x + greenRightWeights.y * rightColor.y + greenRightWeights.z * rightColor.z;
	float blue = blueLeftWeights.x * leftColor.x + blueLeftWeights.y * leftColor.y + blueLeftWeights.z * leftColor.z + blueRightWeights.x * rightColor.x + blueRightWeights.y * rightColor.y + blueRightWeights.z * rightColor.z;
	
	clamp(red, 0, 1);
	clamp(green, 0, 1);
	clamp(blue, 0, 1);

	FragColor = vec4(red, green, blue, 1);
	//FragColor = vec4(leftColor.x, rightColor.y, rightColor.z, 1);
}
