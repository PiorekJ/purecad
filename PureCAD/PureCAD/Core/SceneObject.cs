﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Windows.Input;
using PureCAD.Utils;
using OpenTK;
using PureCAD.Components;
using PureCAD.Models;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
    
    public interface IFilterable { }

    public interface IIntersectable
    {
        Vector2d ApproximateAt(Vector3 worldPosition);
        Vector3d EvaluateAt(Vector2d uv);
        Vector3d EvaluateUDerivativeAt(Vector2d uv);
        Vector3d EvaluateVDerivativeAt(Vector2d uv);
        double GetURange();
        double GetVRange();
        int GetUCurveCount();
        int GetVCurveCount();
        bool IsUWrapped { get; set; }
        bool IsVWrapped { get; set; }
        void SetTrimmingTexture(int texId);
        void SetTrimmingSide(int sideId);
    }

    public interface IOffsetable
    {
        Vector2d ApproximateAt(Vector3 worldPosition, float offset);
        Vector3d EvaluateAt(Vector2d uv, float offset);
        Vector3d EvaluateUDerivativeAt(Vector2d uv, float offset);
        Vector3d EvaluateVDerivativeAt(Vector2d uv, float offset);
        double GetURange();
        double GetVRange();
        int GetUCurveCount();
        int GetVCurveCount();
        bool IsUWrapped { get; set; }
        bool IsVWrapped { get; set; }
    }


    public class SceneObject : BindableObject, IDisposable
    {
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value; 
                RaisePropertyChanged();
            }
        }

        public bool IsActive
        {
            get { return _isVisible; }
            set
            {
                _isActive = value;
                RaisePropertyChanged();
            }
        }

        public bool IsDeletable = true;

        public Transform Transform { get; set; }

        private List<Component> _components;

        public delegate void DeleteHandler();

        public event DeleteHandler OnDeleteEvent;
         
        #region UI Related

        public bool IsVisibleOnList
        {
            get { return _isVisibleOnList; }
            set
            {
                _isVisibleOnList = value;
                RaisePropertyChanged();
            }
        }
        private bool _isVisibleOnList = true;

        public string ObjectName
        {
            get => _objectName;
            set
            {
                _objectName = value;
                RaisePropertyChanged();
            }
        }

        private string _objectName;
        private bool _isVisible = true;
        private bool _isActive = true;
        #endregion

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
            ObjectName = Simulation.Storage.GetNameInstance(this);
        }

        

        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            _components.Remove(component);
        }


        protected virtual void OnRender()
        {

        }

        protected virtual void OnUpdate()
        {

        }

        public void Render()
        {
            if (!IsVisible)
                return;

            OnRender();
        }

        public virtual void Update()
        {
            if (!IsActive)
                return;
            Transform.HandleKeyMovement();
            OnUpdate();
        }

        public void OnDelete()
        {
            OnDeleteEvent?.Invoke();
        }

        public virtual void Dispose()
        {
            Transform.Dispose();
            RemoveComponent(Transform);
        }
    }
}
