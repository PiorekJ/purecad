﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PureCAD.Utils;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }

    }
}
