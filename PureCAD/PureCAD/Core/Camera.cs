﻿using System;
using System.Windows.Input;
using System.Windows.Media.Animation;
using PureCAD.Utils;
using OpenTK;
using PureCAD.Components;
using PureCAD.Utils.Math;
using PureCAD.Utils.UIExtensionClasses;
using Key = System.Windows.Input.Key;
using MouseButton = System.Windows.Input.MouseButton;

namespace PureCAD.Core
{
    public class Camera : SceneObject
    {
        public float FOV { get; set; } = Constants.DefaultFOV;
        public float ZNear { get; set; } = Constants.DefaultFOV;
        public float ZFar { get; set; } = Constants.DefaultZFar;

        public float EyeSeparation
        {
            get { return _eyeSeparation; }
            set
            {
                _eyeSeparation = value;
                SetStereoscopicMatrices();
                RaisePropertyChanged();
            }
        }

        public float FocusDepth
        {
            get { return _focusDepth; }
            set
            {
                _focusDepth = value;
                SetStereoscopicMatrices();
                RaisePropertyChanged();
            }
        }

        private Vector2 _lastMousePosition = Vector2.Zero;

        public float RotationX
        {
            get { return _rotationX; }
            set
            {
                _rotationX = value;
                RaisePropertyChanged();
            }
        }

        public float RotationY
        {
            get { return _rotationY; }
            set
            {
                _rotationY = value;
                RaisePropertyChanged();
            }
        }

        #region Commands

        public ICommand ResetCommand
        {
            get;
            protected set;
        }

        #endregion

        private float _aspectRatio => Simulation.Storage.DisplayAspect;
        private float _rotationX;
        private float _rotationY;
        private Matrix4 _leftEyeProjectionMatrix = Matrix4.Zero;
        private Matrix4 _rightEyeProjectionMatrix = Matrix4.Zero;
        public bool IsMouseMoving => InputManager.IsMouseButtonDown(MouseButton.Middle);
        public bool IsKeyboardMoving => InputManager.IsKeyDown(Key.W) || InputManager.IsKeyDown(Key.S) || InputManager.IsKeyDown(Key.A) || InputManager.IsKeyDown(Key.D) || InputManager.IsKeyDown(Key.Space) || InputManager.IsKeyDown(Key.LeftCtrl);

        public bool IsRotating => InputManager.IsMouseButtonDown(MouseButton.Right);

        public bool IsOrtographic = false;
        public bool IsLeftEye = false;
        private float _focusDepth = Constants.DefaultFocusDepth;
        private float _eyeSeparation = Constants.DefaultEyeSeparation;

        private enum OrtoRot
        {
            X,
            Y,
            Z
        }

        private OrtoRot CurrOrtoRot;

        public Matrix4 GetViewMatrix()
        {
            return MathGeneralUtils.CreateTranslation(-Transform.Position) * MathGeneralUtils.CreateFromQuaternion(Transform.Rotation.Inverted());
        }

        public Matrix4 GetProjectionMatrix()
        {
            if (Simulation.Settings.StereoModeOn)
            {
                if (IsLeftEye)
                {
                    return _leftEyeProjectionMatrix;
                }
                return _rightEyeProjectionMatrix;
            }

            if (IsOrtographic)
                return Matrix4.CreateOrthographic(Constants.CameraOrthoWidth, Constants.CameraOrthoHeight, Constants.CameraOrthoNear, Constants.CameraOrthoFar);

            return MathGeneralUtils.CreatePerspectiveFOVMatrix(FOV, _aspectRatio, ZNear, ZFar);
        }

        public void SetStereoscopicMatrices()
        {
            Simulation.StereoModeManager.CreatePerspectiveStereoscopic(Constants.DefaultFOV, _aspectRatio, Constants.DefaultZNear, Constants.DefaultZFar, EyeSeparation, FocusDepth, out _leftEyeProjectionMatrix, out _rightEyeProjectionMatrix);
        }
        
        public void SetCamera(Vector3 position, Quaternion rotation, float rotX, float rotY, bool isOrtho)
        {
            Transform.Position = position;
            Transform.Rotation = rotation;
            RotationX = rotX;
            RotationY = rotY;
            IsOrtographic = isOrtho;
        }

        public Camera()
        {
            Transform.Position = Constants.DefaultCameraPosition;
            InputManager.OnMouseScrollEvent += Zoom;
            InputManager.RegisterOnKeyDownEvent(Key.NumPad5, () =>
            {
                IsOrtographic = !IsOrtographic;
            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad1, () =>
            {
                if (!IsOrtographic)
                    IsOrtographic = true;
                RotationX = 0;
                RotationY = 0;
                Transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = 3 * Vector3.UnitZ;
                CurrOrtoRot = OrtoRot.Z;
            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad3, () =>
            {
                if (!IsOrtographic)
                    IsOrtographic = true;
                RotationX = 0;
                RotationY = MathHelper.DegreesToRadians(90);
                Transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = 3 * Vector3.UnitX;
                CurrOrtoRot = OrtoRot.X;
            });
            InputManager.RegisterOnKeyDownEvent(Key.NumPad7, () =>
            {
                if (!IsOrtographic)
                    IsOrtographic = true;
                RotationX = MathHelper.DegreesToRadians(-90);
                RotationY = 0;
                Transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                                     Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
                Transform.Position = 3 * Vector3.UnitY;
                CurrOrtoRot = OrtoRot.Y;
            });

            ResetCommand = new RelayCommand(param => ResetCamera());
        }

        public void UpdateCamera(Vector2 mousePosition)
        {
            KeyboardControl();
            MouseControl(mousePosition);
        }

        private void MouseControl(Vector2 mousePosition)
        {
            if (_lastMousePosition == mousePosition)
                return;

            var diffX = mousePosition.X - _lastMousePosition.X;
            var diffY = mousePosition.Y - _lastMousePosition.Y;

            _lastMousePosition = mousePosition;

            if (IsMouseMoving)
            {
                Vector3 top = Vector3.Transform(-Vector3.UnitY, Transform.Rotation);
                Vector3 right = Vector3.Transform(Vector3.UnitX, Transform.Rotation);

                Transform.Position -= (top * diffY + right * diffX) * Constants.CameraMovementMouseSensitivity * Simulation.DeltaTime;
            }

            if (IsRotating)
            {
                RotationX -= diffY * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;
                RotationY -= diffX * Constants.CameraRotationMouseSensitivity * Simulation.DeltaTime;

                Transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, RotationY) *
                           Quaternion.FromAxisAngle(Vector3.UnitX, RotationX);
            }
        }

        private void Zoom(int delta)
        {
            if (delta < 0)
                Transform.Position += Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
            if (delta > 0)
                Transform.Position -= Transform.Rotation * Vector3.UnitZ * Constants.CameraZoomMouseSensitivity;
        }

        private void KeyboardControl()
        {
            float velocity = InputManager.IsKeyDown(Key.LeftShift) ? Constants.CameraMovementKeySlowVelocity : Constants.CameraMovementKeyVelocity;

            if (InputManager.IsKeyDown(Key.W))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.S))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitZ * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.A))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.D))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitX * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.E))
            {
                Transform.Position += Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }

            if (InputManager.IsKeyDown(Key.Q))
            {
                Transform.Position -= Transform.Rotation * Vector3.UnitY * velocity * Simulation.DeltaTime;
            }


        }

        public void SetLastMousePosition(Vector2 pos)
        {
            _lastMousePosition = pos;
        }

        public Rayline CalculateCameraScreenRayLine(Vector2 screenPoint)
        {
            if (IsOrtographic)
            {
                Vector4 screenPosNorm = new Vector4(screenPoint.X / Simulation.Storage.DisplayWidth * 2 - 1, -(screenPoint.Y / Simulation.Storage.DisplayHeight * 2 - 1), 0, 1);
                screenPosNorm = screenPosNorm * Matrix4.Invert(GetProjectionMatrix()) * Matrix4.Invert(GetViewMatrix());
                Vector3 sceneVector = Vector3.Zero;
                switch (CurrOrtoRot)
                {
                    case OrtoRot.Z:
                        sceneVector = screenPosNorm.Xyz - Vector3.UnitZ;
                        break;
                    case OrtoRot.X:
                        sceneVector = screenPosNorm.Xyz - Vector3.UnitX;
                        break;
                    case OrtoRot.Y:
                        sceneVector = screenPosNorm.Xyz - Vector3.UnitY;
                        break;
                    default:
                        break;
                }

                Vector3 cameraLookDir = screenPosNorm.Xyz - sceneVector;
                return new Rayline(screenPosNorm.Xyz, Vector3.Normalize(cameraLookDir));
            }
            else
            {
                Vector4 screenPosNorm = new Vector4(screenPoint.X / Simulation.Storage.DisplayWidth * 2 - 1, -(screenPoint.Y / Simulation.Storage.DisplayHeight * 2 - 1), 0, 1);
                screenPosNorm = screenPosNorm * Matrix4.Invert(GetProjectionMatrix()) * Matrix4.Invert(GetViewMatrix());
                Vector3 screenVector3 = new Vector3(screenPosNorm.X / screenPosNorm.W, screenPosNorm.Y / screenPosNorm.W,
                    screenPosNorm.Z / screenPosNorm.W);
                Vector3 cameraLookDir = screenVector3 - Transform.Position;
                return new Rayline(screenVector3, Vector3.Normalize(cameraLookDir));
            }

        }

        public Rayline GetCameraCenterRayline()
        {
            return CalculateCameraScreenRayLine(new Vector2(Simulation.Storage.DisplayWidth / 2f, Simulation.Storage.DisplayHeight / 2f));
        }

        public void ResetCamera()
        {
            Transform.Position = Constants.DefaultCameraPosition;
            Transform.Rotation = Quaternion.Identity;
            RotationY = 0;
            RotationX = 0;
        }

        protected override void OnRender() { }

        protected override void OnUpdate() { }
    }
}
