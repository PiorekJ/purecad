﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using PureCAD.Utils;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Core
{
    public class Settings : BindableObject
    {
        private bool _explicitModeOn = false;
        private bool _stereoModeOn = false;
        private bool _addPointsToCurve = false;
        private Color _selectionColor = Colors.LawnGreen;
        private float _cursorGrabRadius = 0.25f;
        private bool _moveCursorToSelection;
        private double _gradientStep = 0.01;
        private double _gradientBreakCondition = Constants.Epsilon;
        private double _newtonBreakCondition = 0.0001;
        private double _newtonStep = 0.02;
        private double _selfIntersectionDelta = 0.5;
        private int _newtonMaxIterations = 10000;
        private int _newtonMMaxIterations = 10000;
        private int _gradientMaxIterations = 100000;
        private bool _calculateOffsetSurfaces = false;
        private float _surfaceOffsetValue = 0.5f;

        public bool AddPointsToCurve
        {
            get => _addPointsToCurve;
            set
            {
                _addPointsToCurve = value;
                RaisePropertyChanged();
            }
        }

        public bool ExplicitModeOn
        {
            get => _explicitModeOn;
            set
            {
                _explicitModeOn = !_stereoModeOn && value;
                RaisePropertyChanged();
            }
        }

        public bool StereoModeOn
        {
            get => _stereoModeOn;
            set
            {
                _stereoModeOn = !_explicitModeOn && value;
                if (Simulation.Scene.SceneCursor != null)
                {
                    Simulation.Scene.SceneCursor.SetStereoCursor(!value);
                }

                RaisePropertyChanged();
            }
        }

        public bool MoveCursorToSelection
        {
            get { return _moveCursorToSelection; }
            set
            {
                _moveCursorToSelection = value;
                RaisePropertyChanged();
            }
        }

        public Color SelectionColor
        {
            get { return _selectionColor; }
            set
            {
                _selectionColor = value;
                RaisePropertyChanged();
            }
        }

        public float CursorGrabRadius
        {
            get { return _cursorGrabRadius; }
            set
            {
                _cursorGrabRadius = value;
                if (Simulation.Scene.SceneCursor != null)
                {
                    Simulation.Scene.SceneCursor.SetStereoCursor(!_stereoModeOn);
                }
                RaisePropertyChanged();
            }
        }

        public double GradientStep
        {
            get { return _gradientStep; }
            set
            {
                _gradientStep = value; 
                RaisePropertyChanged();
            }
        }

        public double GradientBreakCondition
        {
            get { return _gradientBreakCondition; }
            set
            {
                _gradientBreakCondition = value; 
                RaisePropertyChanged();
            }
        }

        public int GradientMaxIterations
        {
            get { return _gradientMaxIterations; }
            set
            {
                _gradientMaxIterations = value;
                RaisePropertyChanged();
            }
        }

        public double NewtonStep
        {
            get { return _newtonStep; }
            set
            {
                _newtonStep = value;
                RaisePropertyChanged();
            }
        }

        public double NewtonBreakCondition
        {
            get { return _newtonBreakCondition; }
            set
            {
                _newtonBreakCondition = value;
                RaisePropertyChanged();
            }
        }

        public double SelfIntersectionDelta
        {
            get { return _selfIntersectionDelta; }
            set
            {
                _selfIntersectionDelta = value;
                RaisePropertyChanged();
            }
        }

        public int NewtonMaxIterations
        {
            get { return _newtonMaxIterations; }
            set
            {
                _newtonMaxIterations = value;
                RaisePropertyChanged();
            }
        }

        public int NewtonMMaxIterations
        {
            get { return _newtonMMaxIterations; }
            set
            {
                _newtonMMaxIterations = value;
                RaisePropertyChanged();
            }
        }

        public bool CalculateOffsetSurfaces
        {
            get { return _calculateOffsetSurfaces; }
            set
            {
                _calculateOffsetSurfaces = value; 
                RaisePropertyChanged();
            }
        }

        public float SurfaceOffsetValue
        {
            get { return _surfaceOffsetValue; }
            set
            {
                _surfaceOffsetValue = value; 
                RaisePropertyChanged();
            }
        }
    }
}
