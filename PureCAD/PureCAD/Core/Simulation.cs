﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using OpenTK;
using PureCAD.Models;
using PureCAD.Utils;
using PureCAD.Utils.Ellipsoid;
using PureCAD.Utils.ModeManagers;
using PureCAD.Utils.Serialization;

namespace PureCAD.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }
        public static Storage Storage { get; set; }
        public static PathGenerationManager PathGenerationManager { get; set; }
        public static ExplicitModeManager ExplicitModeManager { get; set; }
        public static StereoModeManager StereoModeManager { get; set; }
        public static IntersectionManager IntersectionManager { get; set; }

        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        public Simulation()
        {
            Storage = new Storage();
            Scene = new Scene();
            Settings = new Settings();
            ExplicitModeManager = new ExplicitModeManager();
            StereoModeManager = new StereoModeManager();
            IntersectionManager = new IntersectionManager();
            PathGenerationManager = new PathGenerationManager();
            TimeCounter = new TimeCounter();
        }

        public void InitializeSimulation()
        {
            TimeCounter.Start();
            Scene.AddSceneCursor();
            Scene.AddSceneBackground();
            Scene.AddSceneSelectionRectangle();
            //Scene.AddSceneLights();
            //Scene.AddSceneMillingMachine();
        }
    }
}
