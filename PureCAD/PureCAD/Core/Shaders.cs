﻿using PureCAD.OpenTK;

namespace PureCAD.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _basicQuadShader;
        public static Shader BasicQuadShader
        {
            get
            {
                if (_basicQuadShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicQuad/BasicQuadShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicQuad/BasicQuadShader.frag"))
                    {
                        _basicQuadShader = new Shader(vert, frag);
                    }
                    return _basicQuadShader;
                }
                return _basicQuadShader;
            }
        }

        private static Shader _basicSelectionShader;
        public static Shader BasicSelectionShader
        {
            get
            {
                if (_basicSelectionShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicSelection/BasicSelectionShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicSelection/BasicSelectionShader.frag"))
                    {
                        _basicSelectionShader = new Shader(vert, frag);
                    }
                    return _basicSelectionShader;
                }
                return _basicSelectionShader;
            }
        }

        private static Shader _basicColorShader;
        public static Shader BasicColorShader
        {
            get
            {
                if (_basicColorShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicColor/BasicColorShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicColor/BasicColorShader.frag"))
                    {
                        _basicColorShader = new Shader(vert, frag);
                    }
                    return _basicColorShader;
                }
                return _basicColorShader;
            }
        }

        private static Shader _basicTorusShader;
        public static Shader BasicTorusShader
        {
            get
            {
                if (_basicTorusShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicTorus/BasicTorusShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicTorus/BasicTorusShader.frag"))
                    {
                        _basicTorusShader = new Shader(vert, frag);
                    }
                    return _basicTorusShader;
                }
                return _basicTorusShader;
            }
        }

        private static Shader _explicitShader;
        public static Shader ExplicitShader
        {
            get
            {
                if (_explicitShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Explicit/ExplicitShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Explicit/ExplicitShader.frag"))
                    {
                        _explicitShader = new Shader(vert, frag);
                    }
                    return _explicitShader;
                }
                return _explicitShader;
            }
        }

        private static Shader _depthShader;
        public static Shader DepthShader
        {
            get
            {
                if (_depthShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Depth/DepthShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Depth/DepthShader.frag"))
                    {
                        _depthShader = new Shader(vert, frag);
                    }
                    return _depthShader;
                }
                return _depthShader;
            }
        }

        private static Shader _textureSampleShader;
        public static Shader TextureSampleShader
        {
            get
            {
                if (_textureSampleShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/TextureSample/TextureSampleShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/TextureSample/TextureSampleShader.frag"))
                    {
                        _textureSampleShader = new Shader(vert, frag);
                    }
                    return _textureSampleShader;
                }
                return _textureSampleShader;
            }
        }

        private static Shader _basicTesselationShaderC0;
        public static Shader BasicTesselationShaderC0
        {
            get
            {
                if (_basicTesselationShaderC0 == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/BasicTesselationC0/BasicTesselationShaderC0.vert"),
                            frag = new FragmentShaderObject("./Shaders/BasicTesselationC0/BasicTesselationShaderC0.frag"),
                            geom = new GeometricShaderObject("./Shaders/BasicTesselationC0/BasicTesselationShaderC0.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/BasicTesselationC0/BasicTesselationShaderC0.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/BasicTesselationC0/BasicTesselationShaderC0.tese"))
                    {
                        _basicTesselationShaderC0 = new Shader(vert, frag, geom, tesc, tese);
                    }
                    return _basicTesselationShaderC0;
                }
                return _basicTesselationShaderC0;
            }
        }

        private static Shader _basicTesselationShaderC2;
        public static Shader BasicTesselationShaderC2
        {
            get
            {
                if (_basicTesselationShaderC2 == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/BasicTesselationC2/BasicTesselationShaderC2.vert"),
                            frag = new FragmentShaderObject("./Shaders/BasicTesselationC2/BasicTesselationShaderC2.frag"),
                            geom = new GeometricShaderObject("./Shaders/BasicTesselationC2/BasicTesselationShaderC2.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/BasicTesselationC2/BasicTesselationShaderC2.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/BasicTesselationC2/BasicTesselationShaderC2.tese"))
                    {
                        _basicTesselationShaderC2 = new Shader(vert, frag, geom, tesc, tese);
                    }
                    return _basicTesselationShaderC2;
                }
                return _basicTesselationShaderC2;
            }
        }

        private static Shader _basicGeomSpheresShader;
        public static Shader BasicGeomSpheresShader
        {
            get
            {
                if (_basicGeomSpheresShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/BasicGeomSpheres/BasicGeomSpheresShader.vert"),
                            frag = new FragmentShaderObject("./Shaders/BasicGeomSpheres/BasicGeomSpheresShader.frag"),
                            geom = new GeometricShaderObject("./Shaders/BasicGeomSpheres/BasicGeomSpheresShader.geom"))
                    {
                        _basicGeomSpheresShader = new Shader(vert, frag, geom);
                    }
                    return _basicGeomSpheresShader;
                }
                return _basicGeomSpheresShader;
            }
        }

        private static Shader _stereoShader;
        public static Shader StereoShader
        {
            get
            {
                if (_stereoShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Stereo/StereoShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Stereo/StereoShader.frag"))
                    {
                        _stereoShader = new Shader(vert, frag);
                    }
                    return _stereoShader;
                }
                return _stereoShader;
            }
        }

        private static Shader _quadraticCurveShader;

        public static Shader QuadraticCurveShader
        {
            get
            {
                if (_quadraticCurveShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/Curve/CurveShader.vert"),
                            frag = new FragmentShaderObject("./Shaders/Curve/CurveShader.frag"),
                            geom = new GeometricShaderObject("./Shaders/Curve/Quadratic/QuadraticCurveShader.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/Curve/Quadratic/QuadraticCurveShader.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/Curve/Quadratic/QuadraticCurveShader.tese"))
                    {
                        _quadraticCurveShader = new Shader(vert, frag, geom, tesc, tese);
                    }
                    return _quadraticCurveShader;
                }
                return _quadraticCurveShader;
            }
        }

        private static Shader _cubicCurveShader;

        public static Shader CubicCurveShader
        {
            get
            {
                if (_cubicCurveShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/Curve/CurveShader.vert"),
                            frag = new FragmentShaderObject("./Shaders/Curve/CurveShader.frag"),
                            geom = new GeometricShaderObject("./Shaders/Curve/Cubic/CubicCurveShader.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/Curve/Cubic/CubicCurveShader.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/Curve/Cubic/CubicCurveShader.tese"))

                    {
                        _cubicCurveShader = new Shader(vert, frag, geom, tesc, tese);
                    }
                    return _cubicCurveShader;
                }
                return _cubicCurveShader;
            }
        }

        private static Shader _surfaceC0Shader;

        public static Shader SurfaceC0Shader
        {
            get
            {
                if (_surfaceC0Shader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/SurfaceC0/SurfaceShaderC0.vert"),
                            frag = new FragmentShaderObject("./Shaders/SurfaceC0/SurfaceShaderC0.frag"),
                            geom = new GeometricShaderObject("./Shaders/SurfaceC0/SurfaceShaderC0.geom")
                    )
                    {
                        _surfaceC0Shader = new Shader(vert, frag, geom);
                    }
                    return _surfaceC0Shader;
                }
                return _surfaceC0Shader;
            }
        }

        private static Shader _surfaceC0SmoothShader;

        public static Shader SurfaceC0SmoothShader
        {
            get
            {
                if (_surfaceC0SmoothShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/SurfaceSmoothC0/SurfaceShaderC0Smooth.vert"),
                            frag = new FragmentShaderObject("./Shaders/SurfaceSmoothC0/SurfaceShaderC0Smooth.frag"),
                            //geom = new GeometricShaderObject("./Shaders/SurfaceSmooth/SurfaceShaderC0Smooth.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/SurfaceSmoothC0/SurfaceShaderC0Smooth.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/SurfaceSmoothC0/SurfaceShaderC0Smooth.tese"))
                    {
                        _surfaceC0SmoothShader = new Shader(vert, frag, tesc, tese);
                    }
                    return _surfaceC0SmoothShader;
                }
                return _surfaceC0SmoothShader;
            }
        }

        private static Shader _surfaceC2SmoothShader;

        public static Shader SurfaceC2SmoothShader
        {
            get
            {
                if (_surfaceC2SmoothShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/SurfaceSmoothC2/SurfaceShaderC2Smooth.vert"),
                            frag = new FragmentShaderObject("./Shaders/SurfaceSmoothC2/SurfaceShaderC2Smooth.frag"),
                            //geom = new GeometricShaderObject("./Shaders/SurfaceSmooth/SurfaceShaderC0Smooth.geom"),
                            tesc = new TesselationControlShaderObject("./Shaders/SurfaceSmoothC2/SurfaceShaderC2Smooth.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/SurfaceSmoothC2/SurfaceShaderC2Smooth.tese"))
                    {
                        _surfaceC2SmoothShader = new Shader(vert, frag, tesc, tese);
                    }
                    return _surfaceC2SmoothShader;
                }
                return _surfaceC2SmoothShader;
            }
        }

        private static Shader _gregoryPatchSmoothShader;

        public static Shader GregoryPatchSmoothShader
        {
            get
            {
                if (_gregoryPatchSmoothShader == null)
                {
                    using (
                        ShaderObject vert = new VertexShaderObject("./Shaders/GregoryPatchSmooth/GregoryPatchSmooth.vert"),
                            frag = new FragmentShaderObject("./Shaders/GregoryPatchSmooth/GregoryPatchSmooth.frag"),
                            tesc = new TesselationControlShaderObject("./Shaders/GregoryPatchSmooth/GregoryPatchSmooth.tesc"),
                            tese = new TesselationEvaluationShaderObject("./Shaders/GregoryPatchSmooth/GregoryPatchSmooth.tese"))
                    {
                        _gregoryPatchSmoothShader = new Shader(vert, frag, tesc, tese);
                    }
                    return _gregoryPatchSmoothShader;
                }
                return _gregoryPatchSmoothShader;
            }
        }

        private static Shader _lightShader;
        public static Shader LightShader
        {
            get
            {
                if (_lightShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Light/LightShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Light/LightShader.frag"))
                    {
                        _lightShader = new Shader(vert, frag);
                    }
                    return _lightShader;
                }
                return _lightShader;
            }
        }

        private static Shader _litObjectShader;
        public static Shader LitObjectShader
        {
            get
            {
                if (_litObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Object/LightObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Object/LightObjectShader.frag"))
                    {
                        _litObjectShader = new Shader(vert, frag);
                    }
                    return _litObjectShader;
                }
                return _litObjectShader;
            }
        }

        private static Shader _litTexturedObjectShader;
        public static Shader LitTexturedObjectShader
        {
            get
            {
                if (_litTexturedObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/TexturedObject/LightTexturedObjectShader.frag"))
                    {
                        _litTexturedObjectShader = new Shader(vert, frag);
                    }
                    return _litTexturedObjectShader;
                }
                return _litTexturedObjectShader;
            }
        }

        private static Shader _basicPBRShader;
        public static Shader BasicPBRShader
        {
            get
            {
                if (_basicPBRShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PBR/Basic/BasicPBR.vert"),
                        frag = new FragmentShaderObject("./Shaders/PBR/Basic/BasicPBR.frag"))
                    {
                        _basicPBRShader = new Shader(vert, frag);
                    }
                    return _basicPBRShader;
                }
                return _basicPBRShader;
            }
        }

        private static Shader _texturePBRShader;
        public static Shader TexturePBRShader
        {
            get
            {
                if (_texturePBRShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PBR/Texture/TexturePBR.vert"),
                        frag = new FragmentShaderObject("./Shaders/PBR/Texture/TexturePBR.frag"))
                    {
                        _texturePBRShader = new Shader(vert, frag);
                    }
                    return _texturePBRShader;
                }
                return _texturePBRShader;
            }
        }

        private static Shader _polyChainShader;
        public static Shader PolyChainShader
        {
            get
            {
                if (_polyChainShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PolyChain/PolyChainShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/PolyChain/PolyChainShader.frag"),
                        geom = new GeometricShaderObject("./Shaders/PolyChain/PolyChainShader.frag"))
                    {
                        _polyChainShader = new Shader(vert, frag, geom);
                    }
                    return _polyChainShader;
                }
                return _polyChainShader;
            }
        }
    }
}
