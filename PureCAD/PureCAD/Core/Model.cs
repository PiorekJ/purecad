﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using PureCAD.OpenTK;

namespace PureCAD.Core
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public interface IVirtual
    {
        
    }

    public abstract class Model : SceneObject
    {
        public IMesh Mesh;
        public Shader Shader;
        public ITexture Texture;
        public override void Dispose()
        {
            Mesh?.Dispose();
            base.Dispose();
        }


    }
}
