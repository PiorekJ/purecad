﻿using PureCAD.OpenTK;

namespace PureCAD.Core
{
    
    public class Textures
    {
        private static PBRTexture _metalPBRTexture;
        public static PBRTexture MetalPBRTexture
        {
            get
            {
                if (_metalPBRTexture == null)
                {
                    _metalPBRTexture = new PBRTexture("./Textures/MetalPBR/albedo.png", "./Textures/MetalPBR/metallic.png", "./Textures/MetalPBR/normal.png", "./Textures/MetalPBR/roughness.png", "./Textures/MetalPBR/ao.png");
                    return _metalPBRTexture;
                }
                return _metalPBRTexture;
            }
        }

        private static PBRTexture _goldPBRTexture;
        public static PBRTexture GoldPBRTexture
        {
            get
            {
                if (_goldPBRTexture == null)
                {
                    _goldPBRTexture = new PBRTexture("./Textures/GoldPBR/albedo2.png", "./Textures/GoldPBR/metallic.png", "./Textures/GoldPBR/normal.png", "./Textures/GoldPBR/roughness.png", "./Textures/GoldPBR/ao.png");
                    return _goldPBRTexture;
                }
                return _goldPBRTexture;
            }
        }

        private static PBRTexture _copperRockPBRTexture;
        public static PBRTexture CopperRockPBRTexture
        {
            get
            {
                if (_copperRockPBRTexture == null)
                {
                    _copperRockPBRTexture = new PBRTexture("./Textures/CopperRockPBR/albedo.png", "./Textures/CopperRockPBR/metallic.png", "./Textures/CopperRockPBR/normal.png", "./Textures/CopperRockPBR/roughness.png", "./Textures/CopperRockPBR/ao.png");
                    return _copperRockPBRTexture;
                }
                return _copperRockPBRTexture;
            }
        }

        private static Texture _metalTexture;
        public static Texture MetalTexture
        {
            get
            {
                if (_metalTexture == null)
                {
                    _metalTexture = new Texture("./Textures/Metal.png");
                    return _metalTexture;
                }
                return _metalTexture;
            }
        }

        private static Texture _woodTexture;
        public static Texture WoodTexture
        {
            get
            {
                if (_woodTexture == null)
                {
                    _woodTexture = new Texture("./Textures/WoodTex.jpg");
                    return _woodTexture;
                }
                return _woodTexture;
            }
        }
    }
}
