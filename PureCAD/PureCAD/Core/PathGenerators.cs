﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using PureCAD.Models;
using PureCAD.Models.PathGeneration;
using PureCAD.Models.PathGeneration.Bspline;
using PureCAD.OpenTK;
using PureCAD.Utils;
using PureCAD.Utils.Math;
using PureCAD.Utils.ModeManagers;
using PureCAD.Utils.OpenTK;
using PureCAD.Utils.Serialization;

namespace PureCAD.Core
{
    public class SurfaceDiscreteData
    {
        public SceneSurface Surface;
        public SurfaceVertex[] SurfaceVertices;

        public class SurfaceVertex
        {
            public Vector3 Position;
            public Vector3 Normal;

            public SurfaceVertex(Vector3 pos, Vector3 norm)
            {
                Position = pos;
                Normal = norm;
            }
        }
    }

    public abstract class PathGenerator
    {
        protected Vector3 ConvertPointToSceneSpace(int matX, int matZ, float height, float unitX, float unitZ, float sizeX, float sizeZ)
        {
            var x = matX / unitX - sizeX / 2f;
            var z = -matZ / unitZ + sizeZ / 2f;

            return new Vector3(x, height, z);
        }

        protected Vector2 ConvertPointToHeightMap(Vector3 scenePoint, float unitX, float unitZ, float sizeX,
            float sizeZ)
        {
            var x = (int)((scenePoint.X + sizeX / 2.0f) * unitX);
            var z = (int)(-(scenePoint.Z - sizeZ / 2.0f) * unitZ);

            return new Vector2(x, z);
        }
    }

    public class PathGenerators
    {
        public RoughingPathGenerator RoughingPathGenerator;
        public FlattingPathGenerator FlattingPathGenerator;
        public RoundingPathGenerator RoundingPathGenerator;
        public DetailPathGenerator DetailPathGenerator;
        public SignaturePathGenerator SignaturePathGenerator;
        public PathGenerators()
        {
            RoughingPathGenerator = new RoughingPathGenerator();
            FlattingPathGenerator = new FlattingPathGenerator();
            RoundingPathGenerator = new RoundingPathGenerator();
            DetailPathGenerator = new DetailPathGenerator();
            SignaturePathGenerator = new SignaturePathGenerator();
        }
    }

    public class RoughingPathGenerator : PathGenerator
    {
        public void GeneratePathLevel(float[] heightMap, int heightMapSize, float millRadius, float materialSize, float levelHeight, ref List<Vector3> pathNodes)
        {
            float unitSize = (heightMapSize - 1) / materialSize;
            //int pathWidth = (int)(2 * millRadius * unitSize) > 0 ? (int)(2 * millRadius * unitSize) : 1;
            int pathWidth = (int)((((2 * millRadius) - (0.2f * (2 * millRadius))) * unitSize));
            bool reversed = false;
            Vector3 oldPoint = Vector3.Zero;

            var safePosition = Constants.SafeMillPosition;
            var startPositionTop =
                new Vector3(-materialSize / 2.0f, safePosition.Y, materialSize / 2.0f + millRadius);
            var startPositionBottom =
                new Vector3(-materialSize / 2.0f, levelHeight, materialSize / 2.0f + millRadius);
            pathNodes.Add(safePosition);
            pathNodes.Add(startPositionTop);
            pathNodes.Add(startPositionBottom);

            for (int x = 0; x < heightMapSize; x = ((x + pathWidth) < heightMapSize) ? x + pathWidth : heightMapSize)
            {
                for (var z = 0; z < heightMapSize; z++)
                {
                    var idx = reversed ? x + (heightMapSize - 1 - z) * heightMapSize : x + z * heightMapSize;
                    var height = heightMap[idx] - millRadius;
                    if (height < levelHeight)
                        height = levelHeight;
                    var scenePoint = ConvertPointToSceneSpace(x, reversed ? (heightMapSize - 1 - z) : z, height, unitSize, unitSize, materialSize, materialSize);

                    if (z == heightMapSize - 1 || z == 0)
                    {
                        pathNodes.Add(scenePoint);
                        continue;
                    }

                    if (pathNodes.Count > 1)
                    {
                        oldPoint = pathNodes[pathNodes.Count - 1];
                        if (Math.Abs(scenePoint.Y - oldPoint.Y) > Constants.Epsilon)
                        {
                            pathNodes.Add(scenePoint);
                        }
                    }
                    else
                    {
                        pathNodes.Add(scenePoint);
                    }
                }
                reversed = !reversed;
            }

            var endPoint =
                new Vector3(pathNodes[pathNodes.Count - 1].X, levelHeight, pathNodes[pathNodes.Count - 1].Z - millRadius);
            var endPointTop = new Vector3(pathNodes[pathNodes.Count - 1].X, safePosition.Y, pathNodes[pathNodes.Count - 1].Z - millRadius);

            pathNodes.Add(endPoint);
            pathNodes.Add(endPointTop);
            pathNodes.Add(safePosition);
        }
    }

    public class FlattingPathGenerator : PathGenerator
    {
        public void GeneratePath(float[] heightMap, int heightMapSize, float millRadius, float materialSize, float safeHeight, ref List<Vector3> pathNodes)
        {
            float unitSize = (heightMapSize - 1) / materialSize;
            //int pathWidth = (int)(2 * millRadius * unitSize) > 0 ? (int)(2 * millRadius * unitSize) : 1;
            int pathWidth = (int)((((2 * millRadius) - (0.4f * (2 * millRadius))) * unitSize));
            pathWidth = pathWidth > 0 ? pathWidth : 1;
            bool reversed = false;
            var prevPoint = new Vector3();
            var safePosition = Constants.SafeMillPosition;
            var startPositionTop =
                new Vector3(-materialSize / 2.0f, safePosition.Y, materialSize / 2.0f + 2 * millRadius);
            var startPositionBottom =
                new Vector3(-materialSize / 2.0f, safeHeight, materialSize / 2.0f + 2 * millRadius);
            pathNodes.Add(safePosition);
            pathNodes.Add(startPositionTop);
            pathNodes.Add(startPositionBottom);

            var list = new List<VertexPC>();
            var startZ = 0;
            for (int x = 0; x < heightMapSize; x = ((x + pathWidth) < heightMapSize) ? x + pathWidth : heightMapSize)
            {
                for (var z = !reversed ? startZ : heightMapSize - 1 - startZ; !reversed ? z < heightMapSize : z >= 0; z = !reversed ? z += 1 : z -= 1)
                {
                    var idx = x + z * heightMapSize;
                    var height = heightMap[idx];
                    var scenePoint = ConvertPointToSceneSpace(x, z, safeHeight, unitSize, unitSize, materialSize, materialSize);

                    if (z == heightMapSize - 1 || z == 0)
                    {
                        pathNodes.Add(scenePoint);
                        startZ = 0;
                        continue;
                    }
                    if (Math.Abs(height - safeHeight) > Constants.Epsilon)
                    {
                        if (pathNodes.Count > 1)
                        {
                            pathNodes.Add(scenePoint);
                            scenePoint = ConvertPointToSceneSpace(x + pathWidth, z, safeHeight, unitSize, unitSize, materialSize, materialSize);
                            //list.Add(new VertexPC(scenePoint, Colors.Cyan.ColorToVector3()));
                            var previousX = x;
                            var previousZ = reversed ? z + 1 : z - 1;
                            for (int nextX = x + 1; nextX < (x + 1) + pathWidth && nextX < heightMapSize; nextX++)
                            {
                                idx = nextX + previousZ * heightMapSize;
                                scenePoint = ConvertPointToSceneSpace(nextX, previousZ, safeHeight, unitSize, unitSize, materialSize, materialSize);
                                //list.Add(new VertexPC(scenePoint, Colors.White.ColorToVector3()));
                                height = heightMap[idx];
                                while (Math.Abs(height - safeHeight) > Constants.Epsilon)
                                {
                                    previousZ = reversed ? previousZ + 1 : previousZ - 1;
                                    idx = nextX + previousZ * heightMapSize;
                                    height = heightMap[idx];
                                }
                                scenePoint = ConvertPointToSceneSpace(nextX, previousZ, safeHeight, unitSize, unitSize, materialSize, materialSize);
                                //list.Add(new VertexPC(scenePoint, Colors.Orange.ColorToVector3()));
                            }
                            startZ = !reversed ? (heightMapSize - 1 - previousZ) : previousZ;
                            prevPoint = pathNodes[pathNodes.Count - 1];
                            pathNodes.Add(scenePoint);
                            break;
                        }
                    }
                }
                reversed = !reversed;
            }


            startZ = 0;
            for (int x = heightMapSize - 1 - pathWidth; x > 0; x = ((x - pathWidth) > 0) ? x - pathWidth : 0)
            {
                if (x > heightMapSize * 3.0f / 4.0f)
                    pathWidth = (int)((((2 * millRadius) - (0.6f * (2 * millRadius))) * unitSize));
                else if (x > heightMapSize/ 5f && x < heightMapSize / 3.5f)
                    pathWidth = (int)((((2 * millRadius) - (0.75f * (2 * millRadius))) * unitSize));
                else
                    pathWidth = (int)((((2 * millRadius) - (0.3f * (2 * millRadius))) * unitSize));



                bool collision = false;
                if (reversed)
                {
                    for (var z = heightMapSize - 1 - startZ; z >= 0; z -= 1)
                    {
                        var idx = x + z * heightMapSize;
                        var height = heightMap[idx];
                        if (Math.Abs(height - safeHeight) > Constants.Epsilon)
                        {
                            x += 2 * pathWidth;
                            reversed = false;
                            collision = true;
                            break;
                        }
                    }
                }
                if (!collision)
                {
                    for (var z = !reversed ? startZ : heightMapSize - 1 - startZ; !reversed ? z < heightMapSize : z >= 0; z = !reversed ? z += 1 : z -= 1)
                    {
                        var idx = x + z * heightMapSize;
                        var height = heightMap[idx];
                        var scenePoint = ConvertPointToSceneSpace(x, z, safeHeight, unitSize, unitSize, materialSize, materialSize);

                        if (z == heightMapSize - 1 || z == 0)
                        {
                            pathNodes.Add(scenePoint);
                            startZ = 0;
                            continue;
                        }

                        if (Math.Abs(height - safeHeight) > Constants.Epsilon)
                        {
                            if (pathNodes.Count > 1)
                            {
                                pathNodes.Add(scenePoint);
                                scenePoint = ConvertPointToSceneSpace(x - pathWidth, z, safeHeight, unitSize, unitSize, materialSize, materialSize);
                                list.Add(new VertexPC(scenePoint, Colors.Cyan.ColorToVector3()));
                                var previousX = x;
                                var previousZ = reversed ? z + 1 : z - 1;
                                for (int nextX = x - 1; nextX > (x - 1) - pathWidth && nextX >= 0; nextX--)
                                {
                                    idx = nextX + previousZ * heightMapSize;
                                    scenePoint = ConvertPointToSceneSpace(nextX, previousZ, safeHeight, unitSize, unitSize, materialSize, materialSize);
                                    list.Add(new VertexPC(scenePoint, Colors.White.ColorToVector3()));
                                    height = heightMap[idx];
                                    int counter = 0;
                                    while (Math.Abs(height - safeHeight) > Constants.Epsilon)
                                    {
                                        previousZ = reversed ? previousZ + 1 : previousZ - 1;
                                        idx = nextX + previousZ * heightMapSize;
                                        height = heightMap[idx];
                                        counter++;
                                    }
                                    scenePoint = ConvertPointToSceneSpace(nextX, previousZ, safeHeight, unitSize, unitSize, materialSize, materialSize);
                                    list.Add(new VertexPC(scenePoint, Colors.Orange.ColorToVector3()));
                                }
                                startZ = !reversed ? (heightMapSize - 1 - previousZ) : previousZ;
                                list.Add(new VertexPC(scenePoint, Colors.Magenta.ColorToVector3()));
                                pathNodes.Add(scenePoint);
                                break;
                            }
                        }
                        //list.Add(new VertexPC(scenePoint, Colors.Magenta.ColorToVector3()));
                    }
                }

                reversed = !reversed;
            }

            var endPoint =
                new Vector3(pathNodes[pathNodes.Count - 1].X - 2* millRadius, safeHeight, pathNodes[pathNodes.Count - 1].Z);
            var endPointTop = new Vector3(pathNodes[pathNodes.Count - 1].X - 2 * millRadius, safePosition.Y, pathNodes[pathNodes.Count - 1].Z);

            pathNodes.Add(endPoint);
            pathNodes.Add(endPointTop);
            pathNodes.Add(safePosition);
            SetDebugMesh(ref list);
        }

        private void FindNextSafeIndieces(int startX, int startZ, float[] heightMap, int heightMapSize, float pathWidth, float safeHeight, bool reversed)
        {

        }

        private void SetDebugMesh(ref List<VertexPC> vertices)
        {
            var mesh = new SceneDebugMesh();
            mesh.SetMesh(vertices, null, MeshType.Points);
            mesh.GLPointSize = 4;
            Simulation.Scene.SceneObjects.Add(mesh);
        }
    }

    public class RoundingPathGenerator : PathGenerator
    {
        private void AddDebugMesh(List<Vector3> positions, Vector3 color)
        {
            var debugMesh = new SceneDebugMesh();
            var vertices = new List<VertexPC>();
            foreach (var pos in positions)
            {
                vertices.Add(new VertexPC(pos, color));
            }

            debugMesh.SetMesh(vertices, null, MeshType.Lines);
        }

        private bool CalculatePlaneOffsetSurfaceAt(SceneSurfaceSmooth surface, Vector2d uv, float offset, float planeHeight, out Vector3 result)
        {
            var scenePoint = surface.EvaluateAt(uv);

            var normal = Vector3d.Zero;
            if (surface is SceneSurfaceC0Smooth)
            {
                var u = uv.X;
                var v = uv.Y;
                uv = new Vector2d(u - 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                var uDeriv = surface.EvaluateUDerivativeAt(uv);
                var vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal1 = Vector3d.Cross(uDeriv, vDeriv);
                uv = new Vector2d(u + 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                uDeriv = surface.EvaluateUDerivativeAt(uv);
                vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal2 = Vector3d.Cross(uDeriv, vDeriv);
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal1 * offset).ToVector3() }, Colors.Red.ColorToVector3());
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal2 * offset).ToVector3() }, Colors.Blue.ColorToVector3());
                normal = ((normal1 + normal2) / 2.0f).Normalized();
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal * offset).ToVector3() }, Colors.Cyan.ColorToVector3());
            }
            else
            {
                var du = surface.EvaluateUDerivativeAt(uv);
                var dv = surface.EvaluateVDerivativeAt(uv);

                normal = Vector3d.Cross(du, dv).Normalized();
            }

            var point = (scenePoint + normal * offset);
            result = new Vector3((float)point.X, planeHeight, (float)point.Z);

            if (double.IsNaN(point.X) || double.IsNaN(point.Z))
            {
                return false;
            }
            return true;
        }

        public void GeneratePath(List<SceneSurfaceSmooth> surfaces, float millRadius, float safeHeight, ref List<Vector3> pathNodes)
        {
            //sirfaces[0] : u = 0 (outside propeller) and u = 2 (inside propeller) // C0
            //surfaces[1] : u = 5 (near pipe) and u = 11 (near propeller) // Main hull
            //surfaces[2] : u = 3 (outside of pipe) and u = 7 (inside the pipe) // Pipe
            //surfaces[3] : u = 1 (right side of propeller connector) and u = 3 (left side of propeller connector) // Connector
            //surfaces[4] : u = 3 (outside of back stabilizer) // Stabilizer
            //surfaces[5] : not needed for boundary // Wings
            //var surface = surfaces[0];
            //var u = 0.0f;
            //var vRange = surface.GetVRange();
            //for (float v = 0; v < vRange; v+=0.01f)
            //{
            //    pathNodes.Add(CalculatePlaneOffsetSurfaceAt(surface, new Vector2d(u, v), millRadius));
            //}

            //Simulation.Settings.CalculateOffsetSurfaces = true;
            //Simulation.Settings.SurfaceOffsetValue = 0.5f;
            //var surface = surfaces[0];
            //var u = 2.0f;
            //var vRange = surface.GetVRange();
            //var result = new Vector3();
            //for (float v = 0; v < vRange; v += 0.01f)
            //{
            //    if (CalculatePlaneOffsetSurfaceAt(surface, new Vector2d(u, v), millRadius, safeHeight, out result))
            //        pathNodes.Add(result);
            //}

            //HERE STARTS WORKING
            //Moving on outside of pipe

            pathNodes.Add(Constants.SafeMillPosition);
            pathNodes.Add(new Vector3(7.5f, Constants.SafeMillPosition.Y, 3));
            pathNodes.Add(new Vector3(7.5f, safeHeight, 3));
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(1.5f, 2.0f, 3.0f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            //Intersection near front main hull 
            var intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            var finalV = intersectionDataStart.Y;
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(6.0f, 2.0f, -3.8f);
            //Intersection near end of main hull
            var intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            var startV = intersectionDataStart.W;
            var endV = intersectionDataEnd.W;
            bool assignment = false;
            var finalPoint = new Vector3();
            Simulation.Settings.CalculateOffsetSurfaces = false;
            var result = new Vector3();
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(3, v), millRadius, safeHeight, out result))
                {
                    if (!assignment)
                    {
                        assignment = true;
                        finalPoint = result;
                    }
                    pathNodes.Add(result);
                }
            }
            endV = surfaces[1].GetVRange();
            startV = intersectionDataEnd.Y;
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }
            //From end of hull through stabilizer
            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(5.5f, 2.0f, -6.5f);
            intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[4]);
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(3.1f, 2.0f, -4.3f);
            intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[4]);
            Simulation.Settings.CalculateOffsetSurfaces = false;
            startV = surfaces[1].GetVRange();
            endV = intersectionDataStart.Y;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(11, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }
            startV = intersectionDataStart.W;
            endV = intersectionDataEnd.W;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[4], new Vector2d(3, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            //From stabilizer to connector
            startV = intersectionDataEnd.Y;

            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-0.7f, 2.0f, -0.8f);
            intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[3]);
            Simulation.Settings.CalculateOffsetSurfaces = false;
            endV = intersectionDataStart.Y;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(11, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }


            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-0.959f, 2.0f, -1.038f);

            var lookupList = new List<Vector3>();
            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            var pointOnConnectorUV = IntersectionMath.CursorGradientDescent(surfaces[3], Simulation.Scene.SceneCursor.Transform.Position);
            var pointOnConnectorScene = surfaces[3].EvaluateAt(pointOnConnectorUV).ToVector3();
            var closestPointUV = new Vector2d();
            var minDist = float.PositiveInfinity;
            Simulation.Settings.CalculateOffsetSurfaces = false;
            for (double v = 3.25; v < 3.5; v += 0.001)
            {
                var uv = new Vector2d(2, v);
                if (CalculatePlaneOffsetSurfaceAt(surfaces[0], uv, millRadius, safeHeight, out result))
                {
                    lookupList.Add(result);
                    var dist = (result - pointOnConnectorScene).LengthSquared;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        closestPointUV = uv;
                    }
                }
            }

            startV = intersectionDataStart.W;
            endV = pointOnConnectorUV.Y;
            for (double v = startV; v <= endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[3], new Vector2d(1, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            //From connector to c0
            startV = closestPointUV.Y;
            endV = surfaces[0].GetVRange();

            for (double v = startV; v <= endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            var point1OnBottomCloser = new Vector3();
            var point1OnBottomFurther = new Vector3();

            var point2OnBottomCloser = new Vector3();
            var point2OnBottomFurther = new Vector3();

            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, surfaces[0].GetVRange() - 0.01), millRadius, safeHeight, out point1OnBottomFurther);
            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, surfaces[0].GetVRange() - 0.02), millRadius, safeHeight, out point1OnBottomCloser);

            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(0, surfaces[0].GetVRange() - 0.02), millRadius, safeHeight, out point2OnBottomCloser);
            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(0, surfaces[0].GetVRange() - 0.01), millRadius, safeHeight, out point2OnBottomFurther);
            var intersect = MathGeneralUtils.IntersectLines(new Vector3(point1OnBottomCloser.X, 2, point1OnBottomCloser.Z), new Vector3(point1OnBottomFurther.X, 2, point1OnBottomFurther.Z), new Vector3(point2OnBottomCloser.X, 2, point2OnBottomCloser.Z), new Vector3(point2OnBottomFurther.X, 2, point2OnBottomFurther.Z));

            pathNodes.Add(intersect);

            //From end of c0 to beginning
            startV = surfaces[0].GetVRange();
            endV = 0;

            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(0, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(0, 0.01), millRadius, safeHeight, out point1OnBottomFurther);
            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(0, 0.02), millRadius, safeHeight, out point1OnBottomCloser);

            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, 0.02), millRadius, safeHeight, out point2OnBottomCloser);
            CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, 0.01), millRadius, safeHeight, out point2OnBottomFurther);
            intersect = MathGeneralUtils.IntersectLines(new Vector3(point1OnBottomCloser.X, 2, point1OnBottomCloser.Z), new Vector3(point1OnBottomFurther.X, 2, point1OnBottomFurther.Z), new Vector3(point2OnBottomCloser.X, 2, point2OnBottomCloser.Z), new Vector3(point2OnBottomFurther.X, 2, point2OnBottomFurther.Z));

            pathNodes.Add(intersect);

            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-2.2f, 2.0f, 0.0f);

            lookupList = new List<Vector3>();
            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            pointOnConnectorUV = IntersectionMath.CursorGradientDescent(surfaces[3], Simulation.Scene.SceneCursor.Transform.Position);
            pointOnConnectorScene = surfaces[3].EvaluateAt(pointOnConnectorUV).ToVector3();
            closestPointUV = new Vector2d();
            minDist = float.PositiveInfinity;
            Simulation.Settings.CalculateOffsetSurfaces = false;
            for (double v = 2.75; v >= 2.5; v -= 0.001)
            {
                var uv = new Vector2d(2, v);
                if (CalculatePlaneOffsetSurfaceAt(surfaces[0], uv, millRadius, safeHeight, out result))
                {
                    lookupList.Add(result);
                    var dist = (result - pointOnConnectorScene).LengthSquared;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        closestPointUV = uv;
                    }
                }
            }

            //From c0 to connector
            startV = 0;
            endV = closestPointUV.Y;
            for (double v = startV; v <= endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[0], new Vector2d(2, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }


            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-2f, 2.0f, 0.3f);
            intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[3]);
            Simulation.Settings.CalculateOffsetSurfaces = false;

            startV = pointOnConnectorUV.Y;
            endV = intersectionDataEnd.W;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[3], new Vector2d(3, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            startV = intersectionDataEnd.Y;
            endV = 0;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(11, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }

            startV = 0;
            endV = finalV;
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out result))
                    pathNodes.Add(result);
            }


            pathNodes.Add(finalPoint);

            pathNodes.Add(new Vector3(finalPoint.X, Constants.SafeMillPosition.Y, finalPoint.Z));
            pathNodes.Add(Constants.SafeMillPosition);

            ////////// OLD
            ////Dirty, cuts connector and propeller
            //startV = intersectionDataEnd.ApproxParams.Y;
            //endV = 0;
            //for (double v = startV; v >= endV; v -= 0.01)
            //{
            //    if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(11, v), millRadius, safeHeight, out result))
            //        pathNodes.Add(result);
            //}

            ////Final 
            //startV = 0;
            //endV = finalV;
            //for (double v = startV; v < endV; v += 0.01)
            //{
            //    if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out result))
            //        pathNodes.Add(result);
            //}
        }
    }

    public class DetailPathGenerator : PathGenerator
    {
        private void AddDebugMesh(List<Vector3> positions, Vector3 color)
        {
            var debugMesh = new SceneDebugMesh();
            var vertices = new List<VertexPC>();
            foreach (var pos in positions)
            {
                vertices.Add(new VertexPC(pos, color));
            }

            debugMesh.SetMesh(vertices, null, MeshType.Points);
        }

        private void AddOverpass(Vector3 from, Vector3 to, float overpassHeight, ref List<Vector3> pathNodes)
        {
            pathNodes.Add(from);
            pathNodes.Add(new Vector3(from.X, overpassHeight, from.Z));
            pathNodes.Add(new Vector3(to.X, overpassHeight, to.Z));
            pathNodes.Add(to);
        }

        private bool CalculateOffsetSurfaceAt(SceneSurfaceSmooth surface, Vector2d uv, float offset, float millRadius, out Vector3d result)
        {
            var scenePoint = surface.EvaluateAt(uv);

            var normal = Vector3d.Zero;
            if (surface is SceneSurfaceC0Smooth)
            {
                var u = uv.X;
                var v = uv.Y;
                uv = new Vector2d(u - 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                var uDeriv = surface.EvaluateUDerivativeAt(uv);
                var vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal1 = Vector3d.Cross(uDeriv, vDeriv);
                uv = new Vector2d(u + 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                uDeriv = surface.EvaluateUDerivativeAt(uv);
                vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal2 = Vector3d.Cross(uDeriv, vDeriv);
                normal = ((normal1 + normal2) / 2.0f).Normalized();
            }
            else
            {
                var du = surface.EvaluateUDerivativeAt(uv);
                var dv = surface.EvaluateVDerivativeAt(uv);

                normal = Vector3d.Cross(du, dv).Normalized();
            }

            result = (scenePoint + normal * offset);
            result.Y -= millRadius;

            if (double.IsNaN(result.X) || double.IsNaN(result.Z))
            {
                return false;
            }
            return true;
        }

        private bool CalculatePlaneOffsetSurfaceAt(SceneSurfaceSmooth surface, Vector2d uv, float offset, float planeHeight, out Vector3 result)
        {
            var scenePoint = surface.EvaluateAt(uv);

            var normal = Vector3d.Zero;
            if (surface is SceneSurfaceC0Smooth)
            {
                var u = uv.X;
                var v = uv.Y;
                uv = new Vector2d(u - 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                var uDeriv = surface.EvaluateUDerivativeAt(uv);
                var vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal1 = Vector3d.Cross(uDeriv, vDeriv);
                uv = new Vector2d(u + 0.0001f, v);
                IntersectionMath.WrapParam(surface.GetURange(), surface.IsUWrapped, ref uv.X);
                IntersectionMath.WrapParam(surface.GetVRange(), surface.IsVWrapped, ref uv.Y);
                uDeriv = surface.EvaluateUDerivativeAt(uv);
                vDeriv = surface.EvaluateVDerivativeAt(uv);
                var normal2 = Vector3d.Cross(uDeriv, vDeriv);
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal1 * offset).ToVector3() }, Colors.Red.ColorToVector3());
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal2 * offset).ToVector3() }, Colors.Blue.ColorToVector3());
                normal = ((normal1 + normal2) / 2.0f).Normalized();
                //AddDebugMesh(new List<Vector3>() { scenePoint.ToVector3(), (scenePoint + normal * offset).ToVector3() }, Colors.Cyan.ColorToVector3());
            }
            else
            {
                var du = surface.EvaluateUDerivativeAt(uv);
                var dv = surface.EvaluateVDerivativeAt(uv);

                normal = Vector3d.Cross(du, dv).Normalized();
            }

            var point = (scenePoint + normal * offset);
            result = new Vector3((float)point.X, planeHeight, (float)point.Z);

            if (double.IsNaN(point.X) || double.IsNaN(point.Z))
            {
                return false;
            }
            return true;
        }

        public void GeneratePath(List<SceneSurfaceSmooth> surfaces, float millRadius, float safeHeight,
            ref List<Vector3> pathNodes)
        {
            //surfaces[0] : u = 1 // C0
            //surfaces[1] : u = 5 (near pipe) and u = 11 (near propeller) // Main hull
            //surfaces[2] : u = 3 (outside of pipe) and u = 7 (inside the pipe) // Pipe
            //surfaces[3] : u = 1 (right side of propeller connector) and u = 3 (left side of propeller connector) // Connector
            //surfaces[4] : u = 3 (outside of back stabilizer) // Stabilizer
            //surfaces[5] : not needed for boundary // Wings

            var list = new List<Vector3>();

            var defaultUStep = 0.05;
            var defaultVStep = 0.025;

            var stepU = defaultUStep;
            var stepV = defaultVStep;
            var defaultSceneDistanceForOverpass = 0.8;
            var sceneDistanceForOverpass = defaultSceneDistanceForOverpass;
            var overpassHeight = 6.0f;

            

            var materialSize = 15.0f;
            var heightMapSize = 4000;
            Simulation.Settings.CalculateOffsetSurfaces = false;
            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            float[] bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 0);
            float unitSize = (heightMapSize - 1) / materialSize;

            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Settings.SurfaceOffsetValue = millRadius;

            var endPoint = new Vector3d();
            bool endpointAssigned = false;

            pathNodes.Add(Constants.SafeMillPosition);
            //Propeller front
            var startU = 0.0;
            var startV = 0.0;

            var endU = 1.0;
            var endV = surfaces[0].GetVRange();
            bool reversed = false;
            bool isSafe = true;

            overpassHeight = 3.0f;

            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var uv = new Vector2d(u, v);
                    var scenePoint = surfaces[0].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        if (!endpointAssigned)
                        {
                            endPoint = scenePoint;
                            pathNodes.Add(new Vector3((float)endPoint.X, Constants.SafeMillPosition.Y, (float)endPoint.Z));
                            endpointAssigned = true;
                        }

                        if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                        {
                            AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                            endPoint = scenePoint;
                        }
                        else
                        {
                            pathNodes.Add(scenePoint.ToVector3());
                            endPoint = scenePoint;
                        }
                    }

                }

                reversed = !reversed;
            }


            //propeller top c0
            var result = new Vector3d();

            startU = 1.0;
            startV = 0;

            endU = 1.0;
            endV = surfaces[0].GetVRange();

            Simulation.Settings.CalculateOffsetSurfaces = false;
            //for (double v = startV; v <= endV; v += stepV)
            //{
            //    if (CalculateOffsetSurfaceAt(surfaces[0], new Vector2d(1, v), millRadius, millRadius, out result))
            //    {
            //        if (result.Y > safeHeight)
            //        {
            //            AddOverpass(endPoint.ToVector3(), result.ToVector3(), overpassHeight, ref pathNodes);
            //            endPoint = result;
            //            startV = v;
            //            break;
            //        }
            //    }
            //}

            for (double v = startV; v <= endV; v += stepV)
            {
                if (CalculateOffsetSurfaceAt(surfaces[0], new Vector2d(1, v), millRadius, millRadius, out result))
                {
                    if (result.Y > safeHeight)
                        if ((endPoint - result).Length > sceneDistanceForOverpass)
                        {
                            AddOverpass(endPoint.ToVector3(), result.ToVector3(), overpassHeight, ref pathNodes);
                            endPoint = result;
                        }
                        else
                        {
                            pathNodes.Add(result.ToVector3());
                            endPoint = result;
                        }
                }
            }

            //propeller back

            Simulation.Settings.CalculateOffsetSurfaces = true;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            reversed = false;
            startU = 2.0;
            startV = 0;
            endU = 1.0;
            endV = surfaces[0].GetVRange();
            sceneDistanceForOverpass = 0.4;

            for (double u = startU; u >= endU; u -= stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var uv = new Vector2d(u, v);
                    var scenePoint = surfaces[0].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;
                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {

                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }
            sceneDistanceForOverpass = defaultSceneDistanceForOverpass;

            //Connector
            Simulation.Settings.CalculateOffsetSurfaces = false;
            reversed = false;

            startU = 3.0;
            startV = 0;

            endU = 5.0;
            endV = surfaces[3].GetVRange();

            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 3);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var wrapU = u;
                    IntersectionMath.WrapParam(surfaces[3].GetURange(), surfaces[3].IsUWrapped, ref wrapU);
                    var uv = new Vector2d(wrapU, v);
                    var scenePoint = surfaces[3].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {
                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }

            //Propeller/connector seam

            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-1.043f, 2.105f, -1.092f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            var intersectionData = Simulation.IntersectionManager.IntersectOneWay(surfaces[0], surfaces[3], Simulation.Settings.NewtonStep, 150);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            pathNodes.AddRange(intersectionData.ScenePoints.Where(x => x.Y > safeHeight));
            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            //Propeller/hull seam

            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-1.8f, 1.3f, 0.3f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[3], Simulation.Settings.NewtonStep, false);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            pathNodes.AddRange(intersectionData.ScenePoints.Where(x => x.Y > safeHeight));
            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            //AddDebugMesh(list, Colors.Red.ColorToVector3());

            overpassHeight = 4.0f;

            //Stabilizer

            Simulation.Settings.CalculateOffsetSurfaces = false;
         reversed = false;

            startU = 3.0;
            startV = 0;

            endU = 5.0;
            endV = surfaces[4].GetVRange();

            sceneDistanceForOverpass = 0.3f;

            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 4);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var wrapU = u;
                    IntersectionMath.WrapParam(surfaces[4].GetURange(), surfaces[4].IsUWrapped, ref wrapU);
                    var uv = new Vector2d(wrapU, v);
                    var scenePoint = surfaces[4].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {

                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }

            sceneDistanceForOverpass = defaultSceneDistanceForOverpass;

            //Stabilizer/hull seam

            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(3.3f, 1.3f, -4.3f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[4], Simulation.Settings.NewtonStep, false);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            var safeData = intersectionData.ScenePoints.Where(x => x.Y > safeHeight).ToList();
            AddOverpass(endPoint.ToVector3(), safeData[0], overpassHeight, ref pathNodes);
            pathNodes.AddRange(safeData);
            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            overpassHeight = 6.0f;
            //Main hull
            Simulation.Settings.CalculateOffsetSurfaces = false;
            reversed = false;

            startU = 11.0;
            startV = 0;
            sceneDistanceForOverpass = 0.4;
            endU = 17.0;
            endV = surfaces[1].GetVRange();

            stepU = 0.12;
            stepV = 0.075;

            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 1);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var wrapU = u;
                    IntersectionMath.WrapParam(surfaces[1].GetURange(), surfaces[1].IsUWrapped, ref wrapU);
                    var uv = new Vector2d(wrapU, v);
                    var scenePoint = surfaces[1].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {

                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }

            overpassHeight = 6.0f;
            //Main hull/pipe seam closer
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(5.8f, 1.3f, -3.8f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[2], Simulation.Settings.NewtonStep, false);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            safeData = intersectionData.ScenePoints.Where(x => x.Y > safeHeight).ToList();
            AddOverpass(endPoint.ToVector3(), safeData[0], overpassHeight, ref pathNodes);
            pathNodes.AddRange(safeData);

            //Main hull/pipe seam further
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(1.3f, 1.3f, 2.7f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[2], Simulation.Settings.NewtonStep, false);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            safeData = intersectionData.ScenePoints.Where(x => x.Y > safeHeight).ToList();
            AddOverpass(pathNodes[pathNodes.Count - 1], safeData[0], overpassHeight, ref pathNodes);
            pathNodes.AddRange(safeData);

            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            //Pipe
            Simulation.Settings.CalculateOffsetSurfaces = false;
            reversed = false;

            startU = 7.0;
            startV = 0;

            endU = 11.0;
            endV = surfaces[2].GetVRange();

            stepU = 0.1;
            stepV = 0.05;
            overpassHeight = 4.0f;
            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 2);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var wrapU = u;
                    IntersectionMath.WrapParam(surfaces[2].GetURange(), surfaces[1].IsUWrapped, ref wrapU);
                    var uv = new Vector2d(wrapU, v);
                    var scenePoint = surfaces[2].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {

                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }

            overpassHeight = 6.0f;
            //Wing
            Simulation.Settings.CalculateOffsetSurfaces = false;
            reversed = false;

            startU = 1.0;
            startV = 0;

            endU = 6.0;
            endV = surfaces[5].GetVRange();

            Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
                Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 5);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            for (double u = startU; u <= endU; u += stepU)
            {
                for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
                {
                    var wrapU = u;
                    IntersectionMath.WrapParam(surfaces[5].GetURange(), surfaces[5].IsUWrapped, ref wrapU);
                    var uv = new Vector2d(wrapU, v);
                    var scenePoint = surfaces[5].EvaluateAt(uv);
                    scenePoint.Y -= millRadius;
                    if (scenePoint.Y > safeHeight)
                    {
                        var sceneVec3 = scenePoint.ToVector3();
                        var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
                        var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

                        isSafe = scenePoint.Y > height;
                        if (isSafe)
                        {
                            if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
                            {
                                AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
                                endPoint = scenePoint;
                            }
                            else
                            {

                                pathNodes.Add(scenePoint.ToVector3());
                                endPoint = scenePoint;
                            }
                        }
                    }
                }

                reversed = !reversed;
            }
            overpassHeight = 5.0f;
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-0.5f, 3.5f, 4.5f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[5], Simulation.Settings.NewtonStep, true);
            for (int i = 0; i < intersectionData.ScenePoints.Count; i++)
            {
                var point = intersectionData.ScenePoints[i];
                intersectionData.ScenePoints[i] = new Vector3(point.X, point.Y - millRadius, point.Z);
            }
            pathNodes.AddRange(intersectionData.ScenePoints);
            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            //HOLE MILLING

            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(2.297f, 2.0f, -0.444f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            millRadius = 0.4f;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            //Intersection near front main hull 
            var intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(4.49f, 2.0f, -2.869f);
            //Intersection near end of main hull
            var intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            startV = intersectionDataStart.W;
            endV = intersectionDataEnd.W;
            var resultHole = new Vector3();
            Simulation.Settings.CalculateOffsetSurfaces = false;
            CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(7, startV), millRadius, safeHeight, out resultHole);
            AddOverpass(pathNodes[pathNodes.Count - 1], resultHole, overpassHeight, ref pathNodes);
            Simulation.Settings.CalculateOffsetSurfaces = false;
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(7, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }

            startV = intersectionDataEnd.Y;
            endV = intersectionDataStart.Y;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }



            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(2.6f, 2.0f, -0.8f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            millRadius = 0.5f;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            //Intersection near front main hull 
            intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(4.367f, 2.0f, -2.646f);
            //Intersection near end of main hull
            intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            startV = intersectionDataStart.W;
            endV = intersectionDataEnd.W;
            resultHole = new Vector3();
            Simulation.Settings.CalculateOffsetSurfaces = false;
            CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(7, startV), millRadius, safeHeight, out resultHole);
            AddOverpass(pathNodes[pathNodes.Count - 1], resultHole, overpassHeight, ref pathNodes);
           Simulation.Settings.CalculateOffsetSurfaces = false;
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(7, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }

            startV = intersectionDataEnd.Y;
            endV = intersectionDataStart.Y;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }


            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(3.035f, 2.0f, -1.233f);
            Simulation.Settings.CalculateOffsetSurfaces = true;
            millRadius = 0.6f;
            Simulation.Settings.SurfaceOffsetValue = millRadius;
            //Intersection near front main hull 
            intersectionDataStart = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            Simulation.Scene.SceneCursor.Transform.Position = new Vector3(4.0f, 2.0f, -2.192f);
            //Intersection near end of main hull
            intersectionDataEnd = Simulation.IntersectionManager.ApproxIntersectionPoints(surfaces[1], surfaces[2]);
            startV = intersectionDataStart.W;
            endV = intersectionDataEnd.W;
            Simulation.Settings.CalculateOffsetSurfaces = false;
            for (double v = startV; v < endV; v += 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[2], new Vector2d(7, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }

            startV = intersectionDataEnd.Y;
            endV = intersectionDataStart.Y;
            for (double v = startV; v >= endV; v -= 0.05)
            {
                if (CalculatePlaneOffsetSurfaceAt(surfaces[1], new Vector2d(5, v), millRadius, safeHeight, out resultHole))
                    pathNodes.Add(resultHole);
            }


            //Simulation.Settings.CalculateOffsetSurfaces = false;
            //reversed = false;

            //startU = 11.0;
            //startV = 0;

            //endU = 17.0;
            //endV = surfaces[1].GetVRange();

            //Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            //bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
            //    Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 1);
            //Simulation.Settings.CalculateOffsetSurfaces = true;
            //for (double u = startU; u <= endU; u += stepU)
            //{
            //    for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
            //    {
            //        var wrapU = u;
            //        IntersectionMath.WrapParam(surfaces[1].GetURange(), surfaces[1].IsUWrapped, ref wrapU);
            //        var uv = new Vector2d(wrapU, v);
            //        var scenePoint = surfaces[1].EvaluateAt(uv);
            //        scenePoint.Y -= millRadius;
            //        if (scenePoint.Y > safeHeight)
            //        {
            //            var sceneVec3 = scenePoint.ToVector3();
            //            var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
            //            var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

            //            isSafe = scenePoint.Y > height;
            //            if (isSafe)
            //            {
            //                if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
            //                {
            //                    AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
            //                    endPoint = scenePoint;
            //                }
            //                else
            //                {

            //                    pathNodes.Add(scenePoint.ToVector3());
            //                    endPoint = scenePoint;
            //                }
            //            }
            //        }
            //    }

            //    reversed = !reversed;
            //}

            //Simulation.Settings.CalculateOffsetSurfaces = false;
            //reversed = false;

            //startU = 7.0;
            //startV = 0;

            //endU = 11.0;
            //endV = surfaces[2].GetVRange();

            //Simulation.PathGenerationManager.SetupRendering(heightMapSize);
            //bitmap = Simulation.PathGenerationManager.RenderHeightMap(Simulation.Storage.DisplayWidth,
            //    Simulation.Storage.DisplayHeight, millRadius, 0.0f, heightMapSize, 2);
            //Simulation.Settings.CalculateOffsetSurfaces = true;
            //for (double u = startU; u <= endU; u += stepU)
            //{
            //    for (double v = !reversed ? startV : endV; !reversed ? v <= endV : v >= 0; v = !reversed ? v + stepV : v - stepV)
            //    {
            //        var wrapU = u;
            //        IntersectionMath.WrapParam(surfaces[2].GetURange(), surfaces[1].IsUWrapped, ref wrapU);
            //        var uv = new Vector2d(wrapU, v);
            //        var scenePoint = surfaces[2].EvaluateAt(uv);
            //        scenePoint.Y -= millRadius;
            //        if (scenePoint.Y > safeHeight)
            //        {
            //            var sceneVec3 = scenePoint.ToVector3();
            //            var idx = ConvertPointToHeightMap(sceneVec3, unitSize, unitSize, materialSize, materialSize);
            //            var height = bitmap[(int)(idx.X + idx.Y * heightMapSize)] - millRadius;

            //            isSafe = scenePoint.Y > height;
            //            if (isSafe)
            //            {
            //                if ((endPoint - scenePoint).Length > sceneDistanceForOverpass)
            //                {
            //                    AddOverpass(endPoint.ToVector3(), scenePoint.ToVector3(), overpassHeight, ref pathNodes);
            //                    endPoint = scenePoint;
            //                }
            //                else
            //                {

            //                    pathNodes.Add(scenePoint.ToVector3());
            //                    endPoint = scenePoint;
            //                }
            //            }
            //        }
            //    }

            //    reversed = !reversed;
            //}

            //Simulation.Scene.SceneCursor.Transform.Position = new Vector3(-0.5f,3.5f,4.5f);
            //Simulation.Settings.CalculateOffsetSurfaces = true;
            //var intersectionData = Simulation.IntersectionManager.Intersect(surfaces[1], surfaces[5], Simulation.Settings.NewtonStep);
            //intersectionData.ScenePoints.ForEach(vec => vec.Y -= millRadius);
            //pathNodes.AddRange(intersectionData.ScenePoints);



            endPoint = pathNodes[pathNodes.Count - 1].ToVector3d();
            pathNodes.Add(new Vector3((float)endPoint.X, Constants.SafeMillPosition.Y, (float)endPoint.Z));
            pathNodes.Add(Constants.SafeMillPosition);




        }
    }

    public class SignaturePathGenerator :PathGenerator
    {
        private void AddOverpass(Vector3 from, Vector3 to, float overpassHeight, ref List<Vector3> pathNodes)
        {
            pathNodes.Add(new Vector3(from.X, overpassHeight, from.Z));
            pathNodes.Add(new Vector3(to.X, overpassHeight, to.Z));
        }
         
        public void GeneratePath(List<ScenePoint> points, List<SceneCurve> curves, float millRadius, float safeHeight,
            ref List<Vector3> pathNodes)
        {
            var stepU = 0.02f;
            var overpassHeight = 2.2f;

            pathNodes.Add(Constants.SafeMillPosition);
            pathNodes.Add(new Vector3(curves[0].CurvePoints[0].Transform.Position.X, Constants.SafeMillPosition.Y, curves[0].CurvePoints[0].Transform.Position.Z));
            pathNodes.Add(curves[0].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[0].CurvePoints[1].Transform.Position);
            AddOverpass(curves[0].CurvePoints[1].Transform.Position, curves[1].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[1].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[1].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[2].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[2].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[3].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[3].CurvePoints[1].Transform.Position);
            AddOverpass(curves[3].CurvePoints[1].Transform.Position, curves[4].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[4].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[4].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[5].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[5].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[6].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[6].CurvePoints[1].Transform.Position);
            AddOverpass(curves[6].CurvePoints[1].Transform.Position, curves[7].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[7].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[7].CurvePoints[1].Transform.Position);
            AddOverpass(curves[7].CurvePoints[1].Transform.Position, points[12].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(points[12].Transform.Position);
            AddOverpass(points[12].Transform.Position, curves[8].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[8].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[8].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[9].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[9].CurvePoints[1].Transform.Position);

            var bspline = new BSpline(curves[31].CurvePoints.Select(x => x.Transform.Position).ToArray());

            for (float i = 1; i <= 5; i+=stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            AddOverpass(curves[31].CurvePoints[5].Transform.Position, curves[10].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[10].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[10].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[11].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[11].CurvePoints[2].Transform.Position);
            AddOverpass(curves[11].CurvePoints[2].Transform.Position, curves[12].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[12].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[12].CurvePoints[1].Transform.Position);
            AddOverpass(curves[12].CurvePoints[1].Transform.Position, curves[13].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[13].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[13].CurvePoints[1].Transform.Position);
            AddOverpass(curves[13].CurvePoints[1].Transform.Position, curves[14].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[14].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[14].CurvePoints[1].Transform.Position);
            AddOverpass(curves[14].CurvePoints[1].Transform.Position, curves[15].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[15].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[15].CurvePoints[1].Transform.Position);
            AddOverpass(curves[15].CurvePoints[1].Transform.Position, curves[16].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[16].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[16].CurvePoints[1].Transform.Position);
            bspline = new BSpline(curves[32].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 1; i <= 5; i += stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            pathNodes.Add(curves[17].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[17].CurvePoints[0].Transform.Position);
            AddOverpass(curves[17].CurvePoints[0].Transform.Position, curves[18].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[18].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[18].CurvePoints[1].Transform.Position);
            bspline = new BSpline(curves[34].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 5; i >= 1; i -= stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            bspline = new BSpline(curves[33].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 5; i >= 1; i -= stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            AddOverpass(curves[33].CurvePoints[2].Transform.Position, curves[19].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[19].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[19].CurvePoints[1].Transform.Position);
            bspline = new BSpline(curves[35].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 1; i <= 5; i += stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            AddOverpass(curves[35].CurvePoints[5].Transform.Position, curves[20].CurvePoints[1].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[20].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[20].CurvePoints[0].Transform.Position);
            AddOverpass(curves[20].CurvePoints[0].Transform.Position, curves[36].CurvePoints[2].Transform.Position, overpassHeight, ref pathNodes);
            bspline = new BSpline(curves[36].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 0; i <= 5; i += stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            bspline = new BSpline(curves[37].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 5; i >= 0; i -= stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            bspline = new BSpline(curves[36].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 0; i <= 1; i += stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }

            AddOverpass(pathNodes[pathNodes.Count - 1], curves[21].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[21].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[21].CurvePoints[1].Transform.Position);
            AddOverpass(curves[21].CurvePoints[1].Transform.Position, curves[22].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[22].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[22].CurvePoints[1].Transform.Position);
            bspline = new BSpline(curves[38].CurvePoints.Select(x => x.Transform.Position).ToArray());
            for (float i = 1; i <= 5; i += stepU)
            {
                var eval = bspline.GetValue(i);
                pathNodes.Add(eval);
            }
            pathNodes.Add(curves[23].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[23].CurvePoints[1].Transform.Position);
            AddOverpass(curves[23].CurvePoints[1].Transform.Position, curves[24].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[24].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[24].CurvePoints[1].Transform.Position);
            AddOverpass(curves[24].CurvePoints[1].Transform.Position, curves[25].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[25].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[25].CurvePoints[1].Transform.Position);
            AddOverpass(curves[25].CurvePoints[1].Transform.Position, curves[26].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[26].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[26].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[27].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[27].CurvePoints[0].Transform.Position);
            AddOverpass(curves[27].CurvePoints[0].Transform.Position, curves[28].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[28].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[28].CurvePoints[1].Transform.Position);
            AddOverpass(curves[28].CurvePoints[1].Transform.Position, curves[29].CurvePoints[0].Transform.Position, overpassHeight, ref pathNodes);
            pathNodes.Add(curves[29].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[29].CurvePoints[1].Transform.Position);
            pathNodes.Add(curves[30].CurvePoints[0].Transform.Position);
            pathNodes.Add(curves[30].CurvePoints[1].Transform.Position);

            var logoPath = FileManager.ReadPathFile("C:\\Users\\Jakub\\Desktop\\Modele\\Podpis\\logo.k01");
            var logoNodes = new List<Vector3>();
            foreach (Vector3 node in logoPath.SceneNodes)
            {
                logoNodes.Add(new Vector3(node.X + 10.0f, node.Y, node.Z - 1f));
            }
            AddOverpass(curves[30].CurvePoints[1].Transform.Position, logoNodes[0], overpassHeight, ref pathNodes);
            pathNodes.AddRange(logoNodes);

            pathNodes.Add(new Vector3(pathNodes[pathNodes.Count - 1].X, Constants.SafeMillPosition.Y, pathNodes[pathNodes.Count - 1].Z));
            pathNodes.Add(Constants.SafeMillPosition);

        }
    }
}


