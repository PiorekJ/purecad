﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json;
using OpenTK;
using PureCAD.Models;
using PureCAD.Models.PathGeneration;
using PureCAD.Popups;
using PureCAD.Utils;
using PureCAD.Utils.ModeManagers;
using PureCAD.Utils.Serialization;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Core
{
    public class Scene : BindableObject
    {
        public Camera Camera { get; set; }

        public SceneCursor SceneCursor { get; set; }
        public SceneFloorGrid SceneFloorGrid { get; set; }
        public SceneBackground SceneBackground { get; set; }
        public SceneMillingMachine SceneMillingMachine { get; set; }
        public List<SceneLight> SceneLights { get; set; }
        public SceneSelectionRectangle SceneSelectionRectangle { get; set; }
        public ObservableCollection<SceneObject> SceneObjects { get; set; }

        public SelectionManager SelectionManager { get; set; }

        #region Commands

        public ICommand AddTorusCommand
        {
            get;
            protected set;
        }

        public ICommand AddPointCommand
        {
            get;
            protected set;
        }

        public ICommand AddSceneCurveC0Command
        {
            get;
            protected set;
        }

        public ICommand AddSceneCurveC2Command
        {
            get;
            protected set;
        }

        public ICommand AddSceneCurveC2InterpolatedCommand
        {
            get;
            protected set;
        }

        public ICommand AddSceneSurfaceC0Command
        {
            get;
            protected set;
        }

        public ICommand AddSceneSurfaceC0SmoothCommand
        {
            get;
            protected set;
        }

        public ICommand AddSceneSurfaceC2SmoothCommand
        {
            get;
            protected set;
        }

        public ICommand AddSceneGregoryCommand
        {
            get;
            protected set;
        }

        public ICommand AddIntersectionCommand
        {
            get;
            protected set;
        }

        public ICommand LoadSceneCommand
        {
            get;
            protected set;
        }

        public ICommand SaveSceneCommand
        {
            get;
            protected set;
        }

        public ICommand InitializePathGenerationCommand
        {
            get;
            protected set;
        }

        public ICommand GenerateRoughingPathCommand
        {
            get; protected set;
        }

        public ICommand GenerateFlattingPathCommand
        {
            get; protected set;
        }

        public ICommand GenerateRoundingPathCommand
        {
            get; protected set;
        }

        public ICommand GenerateDetailPathCommand
        {
            get; protected set;
        }

        public ICommand GenerateSignaturePathCommand
        {
            get; protected set;
        }

        #endregion

        public Scene()
        {
            Camera = new Camera();
            SceneObjects = new ObservableCollection<SceneObject>();
            SelectionManager = new SelectionManager();
            InitializeCommands();

            InputManager.RegisterOnKeyDownEvent(Key.Space, () =>
            {
                AddScenePoint();
            });
        }

        private void InitializeCommands()
        {
            AddTorusCommand = new RelayCommand(param => AddSceneTorus());
            AddPointCommand = new RelayCommand(param => AddScenePoint());
            AddSceneCurveC0Command = new RelayCommand(param => AddSceneCurveC0(SelectionManager.SelectedSceneObject));
            AddSceneCurveC2Command = new RelayCommand(param => AddSceneCurveC2(SelectionManager.SelectedSceneObject));
            AddSceneCurveC2InterpolatedCommand = new RelayCommand(param => AddSceneCurveC2Interpolated(SelectionManager.SelectedSceneObject));
            AddSceneSurfaceC0Command = new RelayCommand(param => AddSceneSurfaceC0());
            AddSceneSurfaceC0SmoothCommand = new RelayCommand(param => AddSceneSurfaceC0Smooth());
            AddSceneSurfaceC2SmoothCommand = new RelayCommand(param => AddSceneSurfaceC2Smooth());
            AddSceneGregoryCommand = new RelayCommand(param => AddSceneGregory());
            AddIntersectionCommand = new RelayCommand(param => AddIntersection());
            LoadSceneCommand = new RelayCommand(param => LoadScene());
            SaveSceneCommand = new RelayCommand(param => SaveScene());
            InitializePathGenerationCommand = new RelayCommand(param =>
            {
                Simulation.PathGenerationManager.InitializePathGeneration();
                //Simulation.PathGenerationManager.InitializePathGeneration();
                //StreamReader streamReader = new StreamReader("C:\\Users\\Jakub\\Desktop\\Final2.json");
                //SerializedScene scene =
                //    JsonConvert.DeserializeObject<SerializedScene>(streamReader.ReadToEnd());
                //if (scene != null)
                //{
                //    if (scene.points != null)
                //        foreach (point serializedPoint in scene.points)
                //        {
                //            AddScenePoint(serializedPoint);
                //        }
                //    if (scene.curvesC0 != null)
                //        foreach (curveC0 curveC0 in scene.curvesC0)
                //        {
                //            AddSceneCurveC0(curveC0);
                //        }
                //    if (scene.curvesC2 != null)
                //        foreach (curveC2 curveC2 in scene.curvesC2)
                //        {
                //            AddSceneCurveC2(curveC2);
                //        }
                //    if (scene.curvesC2I != null)
                //        foreach (curveC2I curveC2I in scene.curvesC2I)
                //        {
                //            AddSceneCurveC2Interpolated(curveC2I);
                //        }
                //    if (scene.surfacesC0 != null)
                //        foreach (surfaceC0 surfaceC0 in scene.surfacesC0)
                //        {
                //            AddSceneSurfaceC0Smooth(surfaceC0);
                //        }
                //    if (scene.surfacesC2 != null)
                //        foreach (surfaceC2 surfaceC2 in scene.surfacesC2)
                //        {
                //            AddSceneSurfaceC2Smooth(surfaceC2);
                //        }
                //    if (scene.toruses != null)
                //        foreach (torus torus in scene.toruses)
                //        {
                //            AddSceneTorus(torus);
                //        }
                //}
                //AddSceneLights();
                //AddSceneMillingMachine();


                //var path = Simulation.PathGenerators.BasicPathGenerator.GeneratePathLevel(SceneObjects.OfType<SceneSurfaceSmooth>().ToList(), 3.5f);
                //path.IsVisible = true;
                ////path.IsActive = true;
                //path.SetupScenePaths(SceneMillingMachine.Material, 0.8f);
                //SceneObjects.Add(path);
                //var path2 = Simulation.PathGenerators.BasicPathGenerator.GeneratePathLevel(SceneObjects.OfType<SceneSurfaceSmooth>().ToList(), 2.0f);
                //path2.IsVisible = true;
                ////path2.IsActive = true;
                //path2.SetupScenePaths(SceneMillingMachine.Material, 0.8f);
                //SceneObjects.Add(path2);
                ////SceneMillingMachine.StartMilling(path2, true);
                ////SceneMillingMachine.MillInstant(path);
                //SceneMillingMachine.MillInstant(path2);
                //SceneMillingMachine.Material.UpdateAllVertices();
            });
            GenerateRoughingPathCommand = new RelayCommand(param => Simulation.PathGenerationManager.GenerateRoughingPath());
            GenerateFlattingPathCommand = new RelayCommand(param => Simulation.PathGenerationManager.GenerateFlattingPath());
            GenerateRoundingPathCommand = new RelayCommand(param => Simulation.PathGenerationManager.GenerateRoundingPath());
            GenerateDetailPathCommand = new RelayCommand(param => Simulation.PathGenerationManager.GenerateDetailPath());
            GenerateSignaturePathCommand = new RelayCommand(param => Simulation.PathGenerationManager.GenerateSignaturePath());
        }

        public ScenePoint AddScenePoint()
        {
            var item = AddScenePoint(SceneCursor.Transform.Position, Colors.White, String.Empty, true, true);

            if (Simulation.Settings.AddPointsToCurve)
            {
                if (SelectionManager.SelectedSceneObject is SceneCurve curve)
                {
                    curve.Transform.ForceAddChild(item);
                }
                else if (Simulation.Storage.CursorMode)
                {
                    var stored = SelectionManager.GetStoredObject();
                    if (stored != null && stored is SceneCurve storedCurve)
                    {
                        storedCurve.Transform.ForceAddChild(item);
                    }
                }
            }

            return item;
        }

        public ScenePoint AddScenePoint(Vector3 position, Color color, string name, bool isVisible, bool isVisibleOnList)
        {
            ScenePoint item = new ScenePoint();
            item.Transform.Position = position;
            item.Color = color;
            item.IsVisible = isVisible;
            item.IsVisibleOnList = isVisibleOnList;
            if (name != String.Empty)
                item.ObjectName = name;
            SceneObjects.Add(item);
            return item;
        }

        public SceneSelectionRectangle AddSceneSelectionRectangle()
        {
            SceneSelectionRectangle = new SceneSelectionRectangle();
            //SceneObjects.Add(SceneSelectionRectangle);
            return SceneSelectionRectangle;
        }

        public SceneCursor AddSceneCursor()
        {
            SceneCursor = new SceneCursor();
            RaisePropertyChanged("SceneCursor");
            return SceneCursor;
        }

        public SceneTorus AddSceneTorus()
        {
            SceneTorus item = new SceneTorus();
            SceneObjects.Add(item);
            item.Transform.Position = Simulation.Scene.SceneCursor.Transform.Position;
            return item;
        }

        public SceneFloorGrid AddSceneFloorGrid()
        {
            SceneFloorGrid = new SceneFloorGrid();
            SceneFloorGrid.IsVisibleOnList = false;
            SceneFloorGrid.IsVisible = false;
            SceneObjects.Add(SceneFloorGrid);
            RaisePropertyChanged("SceneFloorGrid");
            return SceneFloorGrid;
        }

        public SceneCurveC0 AddSceneCurveC0(SceneObject sourceObject)
        {
            if (sourceObject == null)
                return null;

            SceneCurveC0 addCurve = sourceObject.Transform.Children.OfType<SceneCurveC0>().FirstOrDefault();
            if (addCurve != null)
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        addCurve.Transform.ForceAddChild(point);
                }
                return addCurve;
            }


            var item = new SceneCurveC0();
            if (sourceObject.Transform.Children.Count == 0)
            {
                item.Transform.ForceAddChild(sourceObject);
            }
            else
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        item.Transform.ForceAddChild(point);
                }
            }

            SceneObjects.Add(item);
            return item;
        }

        public SceneCurveC0 AddSceneCurveC0(List<SceneObject> points)
        {
            var item = new SceneCurveC0();
            foreach (SceneObject sceneObject in points)
            {
                if (sceneObject is ScenePoint point)
                    item.Transform.ForceAddChild(point);
            }
            SceneObjects.Add(item);
            return item;
        }

        public SceneCurveC2 AddSceneCurveC2(SceneObject sourceObject)
        {
            if (sourceObject == null)
                return null;

            SceneCurveC2 addCurve = sourceObject.Transform.Children.OfType<SceneCurveC2>().FirstOrDefault();
            if (addCurve != null)
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        addCurve.Transform.ForceAddChild(point);
                }
                return addCurve;
            }


            var item = new SceneCurveC2();

            if (sourceObject.Transform.Children.Count > 3)
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        item.Transform.ForceAddChild(point);
                }
            }
            else
                return null;


            SceneObjects.Add(item);
            return item;
        }

        public SceneCurveC2Interpolated AddSceneCurveC2Interpolated(SceneObject sourceObject)
        {
            if (sourceObject == null)
                return null;

            SceneCurveC2Interpolated addCurve = sourceObject.Transform.Children.OfType<SceneCurveC2Interpolated>().FirstOrDefault();
            if (addCurve != null)
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        addCurve.Transform.ForceAddChild(point);
                }
                return addCurve;
            }


            var item = new SceneCurveC2Interpolated();

            if (sourceObject.Transform.Children.Count > 3)
            {
                foreach (SceneObject sceneObject in sourceObject.Transform.Children)
                {
                    if (sceneObject is ScenePoint point)
                        item.Transform.ForceAddChild(point);
                }
            }
            else
                return null;

            SceneObjects.Add(item);
            return item;
        }

        public BernsteinPoint AddBernsteinPoint(SceneCurveC2 owner, Vector3 position)
        {
            BernsteinPoint item = new BernsteinPoint();
            item.Transform.Position = position;
            item.ObjectName = item.ObjectName + " " + owner.ObjectName;
            item.IsVisible = owner.DisplayBernsteinPoints;
            item.IsSelected = owner.IsSelected;
            owner.AddBernsteinPoint(item);
            SceneObjects.Add(item);
            return item;
        }

        public SurfacePoint AddSurfacePoint(SceneSurface owner, Vector3 position)
        {
            SurfacePoint item = new SurfacePoint();
            item.Transform.Position = position;
            item.ObjectName = item.ObjectName + " " + owner.ObjectName;
            item.IsSelected = owner.IsSelected;
            owner.SurfacePoints.Add(item);
            SceneObjects.Add(item);
            return item;
        }

        public SurfacePoint AddSurfacePoint(SceneSurfaceSmooth owner, Vector3 position)
        {
            SurfacePoint item = new SurfacePoint();
            item.Transform.Position = position;
            item.ObjectName = item.ObjectName + " " + owner.ObjectName;
            item.IsSelected = owner.IsSelected;
            owner.SurfacePoints.Add(item);
            SceneObjects.Add(item);
            return item;
        }

        public SurfacePoint AddSurfacePointBlank(Vector3 position)
        {
            SurfacePoint item = new SurfacePoint();
            item.Transform.Position = position;
            SceneObjects.Add(item);
            return item;
        }

        public SceneSurfaceC0 AddSceneSurfaceC0()
        {
            SurfacePopup bezierSurfacePopup = new SurfacePopup();
            SurfacePopup.BezierSurfaceValues returnValues = bezierSurfacePopup.OpenSurfacePopup();
            if (returnValues != null)
            {
                SceneSurfaceC0 item = new SceneSurfaceC0();
                item.InitializeSurface(returnValues.Type, returnValues.SizeX, returnValues.SizeY, returnValues.PatchesX, returnValues.PatchesY);
                SceneObjects.Add(item);
                return item;
            }

            return null;
        }

        public SceneSurfaceC0Smooth AddSceneSurfaceC0Smooth()
        {
            SurfacePopup bezierSurfacePopup = new SurfacePopup();
            SurfacePopup.BezierSurfaceValues returnValues = bezierSurfacePopup.OpenSurfacePopup();
            if (returnValues != null)
            {
                SceneSurfaceC0Smooth item = new SceneSurfaceC0Smooth();
                item.InitializeSurface(returnValues.Type, returnValues.SizeX, returnValues.SizeY, returnValues.PatchesX, returnValues.PatchesY);
                SceneObjects.Add(item);
                return item;
            }

            return null;
        }

        public SceneSurfaceC2Smooth AddSceneSurfaceC2Smooth()
        {
            SurfacePopup bezierSurfacePopup = new SurfacePopup();
            SurfacePopup.BezierSurfaceValues returnValues = bezierSurfacePopup.OpenSurfacePopup();
            if (returnValues != null)
            {
                SceneSurfaceC2Smooth item = new SceneSurfaceC2Smooth();
                item.InitializeSurface(returnValues.Type * 2, returnValues.SizeX, returnValues.SizeY, returnValues.PatchesX, returnValues.PatchesY);
                SceneObjects.Add(item);
                return item;
            }

            return null;
        }

        public SceneGregory AddSceneGregory()
        {
            if (SelectionManager.SelectedSceneObject == null)
                return null;

            var selectedSurfaces = SelectionManager.SelectedSceneObject.Transform.Children.OfType<SceneSurfaceC0Smooth>().ToList();

            List<SurfacePatch> patches = new List<SurfacePatch>();
            foreach (SceneSurfaceC0Smooth surface in selectedSurfaces)
            {
                patches.AddRange(surface.SurfacePatches);
            }

            if (selectedSurfaces.Count != patches.Count)
                return null;

            if (selectedSurfaces.Count < 3)
                return null;

            SceneGregory item = new SceneGregory(selectedSurfaces);
            if (item.GenerateCycleEdges(selectedSurfaces))
            {
                item.CalculateRibs();
                item.CalculateGregoryPatches();
                SceneObjects.Add(item);
                return item;
            }
            return null;
        }

        public void AddIntersection()
        {
            if (SelectionManager.SelectedSceneObject?.Transform.Children.Count == 2)
            {
                if (SelectionManager.SelectedSceneObject.Transform.Children[0] is IIntersectable surface1 &&
                    SelectionManager.SelectedSceneObject.Transform.Children[1] is IIntersectable surface2)
                {
                    var intersectionData = Simulation.IntersectionManager.Intersect(surface1, surface2, Simulation.Settings.NewtonStep);
                    if (intersectionData != null)
                    {
                        //IntersectionPopup intersectionPopup = new IntersectionPopup(intersectionData); //TODO: Commented out for paths
                        //intersectionPopup.Show();
                    }
                }
            }

            if (SelectionManager.SelectedSceneObject is IIntersectable surface)
            {
                var intersectionData = Simulation.IntersectionManager.SelfIntersect(surface, Simulation.Settings.NewtonStep, Simulation.Settings.SelfIntersectionDelta);
                if (intersectionData != null)
                {
                    //IntersectionPopup intersectionPopup = new IntersectionPopup(intersectionData); //TODO: Commented out for paths
                    //intersectionPopup.Show();
                }
            }
        }

        public IntersectionManager.IntersectionData AddIntersection(IIntersectable surface1, IIntersectable surface2)
        {

            return Simulation.IntersectionManager.Intersect(surface1, surface2, Simulation.Settings.NewtonStep);
            
        }

        public SceneIntersectionCurve AddSceneIntersectionCurve(IntersectionManager.IntersectionData data)
        {
            var item = new SceneIntersectionCurve(data);
            SceneObjects.Add(item);
            return item;
        }

        public SceneMillingMachine AddSceneMillingMachine()
        {
            SceneMaterial material = new SceneMaterial();
            SceneMillFlat flatMill = new SceneMillFlat();
            SceneMillSphere sphereMill = new SceneMillSphere();
            material.IsVisibleOnList = false;
            flatMill.IsActive = false;
            flatMill.IsVisibleOnList = false;
            flatMill.IsVisible = false;
            sphereMill.IsActive = false;
            sphereMill.IsVisible = false;
            sphereMill.IsVisibleOnList = false;
            SceneMillingMachine = new SceneMillingMachine(flatMill, sphereMill, material);
            return SceneMillingMachine;
        }

        public SceneBackground AddSceneBackground()
        {
            SceneBackground = new SceneBackground();
            return SceneBackground;
        }

        public SceneMillPath AddSceneMillPath(SceneMillPath path)
        {
            SceneObjects.Add(path);
            return path;
        }

        public void AddSceneLights()
        {
            AddSceneLightAt(new Vector3(0, 10, 0));
            AddSceneLightAt(new Vector3(-5, 10, 5));
            AddSceneLightAt(new Vector3(5, 10, -5));
            AddSceneLightAt(new Vector3(5, 10, 5));
            AddSceneLightAt(new Vector3(-5, 10, -5));
        }

        private SceneLight AddSceneLightAt(Vector3 position)
        {
            if (SceneLights == null)
                SceneLights = new List<SceneLight>();
            var item = new SceneLight();
            item.Transform.Position = position;
            item.IsVisibleOnList = false;
            SceneLights.Add(item);
            SceneObjects.Add(item);
            return item;
        }

        public void RemoveSceneObject(SceneObject item)
        {
            if (item != null)
            {
                item.OnDelete();
                item.Transform.ForceRemoveAllChildren();
                item.Transform.ForceRemoveAllParents();
                SceneObjects.Remove(item);
                item.Dispose();
            }
        }

        #region Serialization
        public ScenePoint AddScenePoint(point serializedPoint)
        {
            ScenePoint item = new ScenePoint();
            item.Deserialize(serializedPoint);
            SceneObjects.Add(item);

            return item;
        }

        public SceneCurveC0 AddSceneCurveC0(curveC0 serializedCurveC0)
        {
            SceneCurveC0 item = new SceneCurveC0();
            item.Deserialize(serializedCurveC0);
            SceneObjects.Add(item);

            return item;
        }

        public SceneCurveC2 AddSceneCurveC2(curveC2 serializedCurveC2)
        {
            SceneCurveC2 item = new SceneCurveC2();
            item.Deserialize(serializedCurveC2);
            SceneObjects.Add(item);

            return item;
        }

        public SceneCurveC2Interpolated AddSceneCurveC2Interpolated(curveC2I serializedCurveC2I)
        {
            SceneCurveC2Interpolated item = new SceneCurveC2Interpolated();
            item.Deserialize(serializedCurveC2I);
            SceneObjects.Add(item);

            return item;
        }

        public SceneSurfaceC0Smooth AddSceneSurfaceC0Smooth(surfaceC0 surfaceC0)
        {
            SceneSurfaceC0Smooth item = new SceneSurfaceC0Smooth();
            item.Deserialize(surfaceC0);
            item.DisplayPoints = false;
            SceneObjects.Add(item);
            return item;
        }

        public SceneSurfaceC2Smooth AddSceneSurfaceC2Smooth(surfaceC2 surfaceC2)
        {
            SceneSurfaceC2Smooth item = new SceneSurfaceC2Smooth();
            item.Deserialize(surfaceC2);
            item.DisplayPoints = false;
            SceneObjects.Add(item);
            return item;
        }

        public SceneTorus AddSceneTorus(torus torus)
        {
            SceneTorus item = new SceneTorus();
            item.Deserialize(torus);
            SceneObjects.Add(item);
            return item;
        }

        public void LoadScene()
        {
            SerializedScene serializedScene = FileManager.DeserializeScene();
            //for (var i = SceneObjects.Count - 1; i >= 0; i--)
            //{
            //    SceneObject sceneObject = SceneObjects[i];
            //    RemoveSceneObject(sceneObject);
            //}

            if (serializedScene != null)
            {
                SelectionManager.SelectObjects(SceneObjects);
                SelectionManager.DeleteSelectedObjects();

                if (serializedScene.points != null)
                    foreach (point serializedPoint in serializedScene.points)
                    {
                        AddScenePoint(serializedPoint);
                    }
                if (serializedScene.curvesC0 != null)
                    foreach (curveC0 curveC0 in serializedScene.curvesC0)
                    {
                        AddSceneCurveC0(curveC0);
                    }
                if (serializedScene.curvesC2 != null)
                    foreach (curveC2 curveC2 in serializedScene.curvesC2)
                    {
                        AddSceneCurveC2(curveC2);
                    }
                if (serializedScene.curvesC2I != null)
                    foreach (curveC2I curveC2I in serializedScene.curvesC2I)
                    {
                        AddSceneCurveC2Interpolated(curveC2I);
                    }
                if (serializedScene.surfacesC0 != null)
                    foreach (surfaceC0 surfaceC0 in serializedScene.surfacesC0)
                    {
                        AddSceneSurfaceC0Smooth(surfaceC0);
                    }
                if (serializedScene.surfacesC2 != null)
                    foreach (surfaceC2 surfaceC2 in serializedScene.surfacesC2)
                    {
                        AddSceneSurfaceC2Smooth(surfaceC2);
                    }
                if (serializedScene.toruses != null)
                    foreach (torus torus in serializedScene.toruses)
                    {
                        AddSceneTorus(torus);
                    }
            }
        }

        public void LoadScene(string path)
        {
            SerializedScene serializedScene = FileManager.DeserializeScene(path);

            if (serializedScene != null)
            {
                SelectionManager.SelectObjects(SceneObjects);
                SelectionManager.DeleteSelectedObjects();

                if (serializedScene.points != null)
                    foreach (point serializedPoint in serializedScene.points)
                    {
                        AddScenePoint(serializedPoint);
                    }
                if (serializedScene.curvesC0 != null)
                    foreach (curveC0 curveC0 in serializedScene.curvesC0)
                    {
                        AddSceneCurveC0(curveC0);
                    }
                if (serializedScene.curvesC2 != null)
                    foreach (curveC2 curveC2 in serializedScene.curvesC2)
                    {
                        AddSceneCurveC2(curveC2);
                    }
                if (serializedScene.curvesC2I != null)
                    foreach (curveC2I curveC2I in serializedScene.curvesC2I)
                    {
                        AddSceneCurveC2Interpolated(curveC2I);
                    }
                if (serializedScene.surfacesC0 != null)
                    foreach (surfaceC0 surfaceC0 in serializedScene.surfacesC0)
                    {
                        AddSceneSurfaceC0Smooth(surfaceC0);
                    }
                if (serializedScene.surfacesC2 != null)
                    foreach (surfaceC2 surfaceC2 in serializedScene.surfacesC2)
                    {
                        AddSceneSurfaceC2Smooth(surfaceC2);
                    }
                if (serializedScene.toruses != null)
                    foreach (torus torus in serializedScene.toruses)
                    {
                        AddSceneTorus(torus);
                    }

            }
        }

        public void SaveScene()
        {
            SerializedScene serializedScene = new SerializedScene();
            List<ScenePoint> points = new List<ScenePoint>();
            for (int i = 0; i < SceneObjects.Count; i++)
            {
                if (SceneObjects[i] is ScenePoint point && !(SceneObjects[i] is BernsteinPoint))
                    points.Add(point);
            }
            serializedScene.points = new point[points.Count];
            for (int i = 0; i < points.Count; i++)
            {
                serializedScene.points[i] = points[i].Serialize();
            }

            List<SceneCurveC0> curvesC0 = SceneObjects.OfType<SceneCurveC0>().ToList();
            serializedScene.curvesC0 = new curveC0[curvesC0.Count];
            for (var i = 0; i < curvesC0.Count; i++)
            {
                serializedScene.curvesC0[i] = curvesC0[i].Serialize(points);
            }

            List<SceneCurveC2> curvesC2 = SceneObjects.OfType<SceneCurveC2>().ToList();
            serializedScene.curvesC2 = new curveC2[curvesC2.Count];
            for (var i = 0; i < curvesC2.Count; i++)
            {
                serializedScene.curvesC2[i] = curvesC2[i].Serialize(points);
            }

            List<SceneCurveC2Interpolated> curvesC2Interpolated = SceneObjects.OfType<SceneCurveC2Interpolated>().ToList();
            serializedScene.curvesC2I = new curveC2I[curvesC2Interpolated.Count];
            for (var i = 0; i < curvesC2Interpolated.Count; i++)
            {
                serializedScene.curvesC2I[i] = curvesC2Interpolated[i].Serialize(points);
            }

            List<SceneSurfaceC0Smooth> sceneSurfacesC0Smooth = SceneObjects.OfType<SceneSurfaceC0Smooth>().ToList();
            serializedScene.surfacesC0 = new surfaceC0[sceneSurfacesC0Smooth.Count];
            for (var i = 0; i < sceneSurfacesC0Smooth.Count; i++)
            {
                serializedScene.surfacesC0[i] = sceneSurfacesC0Smooth[i].Serialize(points);
            }

            List<SceneSurfaceC2Smooth> sceneSurfacesC2Smooth = SceneObjects.OfType<SceneSurfaceC2Smooth>().ToList();
            serializedScene.surfacesC2 = new surfaceC2[sceneSurfacesC2Smooth.Count];
            for (var i = 0; i < sceneSurfacesC2Smooth.Count; i++)
            {
                serializedScene.surfacesC2[i] = sceneSurfacesC2Smooth[i].Serialize(points);
            }

            List<SceneTorus> sceneTori = SceneObjects.OfType<SceneTorus>().ToList();
            serializedScene.toruses = new torus[sceneTori.Count];
            for (var i = 0; i < sceneTori.Count; i++)
            {
                serializedScene.toruses[i] = sceneTori[i].Serialize();
            }

            FileManager.SerializeScene(serializedScene);
        }

        #endregion


    }
}