﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PureCAD.UserControls
{
    /// <summary>
    /// Interaction logic for EditableTextBox.xaml
    /// </summary>
    public partial class EditableTextBox : UserControl
    {
        public EditableTextBox()
        {
            InitializeComponent();
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(EditableTextBox), new UIPropertyMetadata());

        private void TextBlock_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var root = ((Grid)((TextBlock)sender).Parent);
                var textBox = (TextBox)root.Children[1];
                ((TextBlock)sender).Visibility = Visibility.Collapsed;
                textBox.Visibility = Visibility.Visible;
                Dispatcher.BeginInvoke((Action)(() => Keyboard.Focus(textBox)), DispatcherPriority.Render);
            }
        }

        private void TextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            var textBlock = (TextBlock)((Grid)((TextBox)sender).Parent).Children[0];

            textBlock.Visibility = Visibility.Visible;
            ((TextBox)sender).Visibility = Visibility.Collapsed;
        }

        private void TextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e?.Key == Key.Return)
            {
                TextBox_OnLostFocus(sender, null);
            }
        }
    }
}
