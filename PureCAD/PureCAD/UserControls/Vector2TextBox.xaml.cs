﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK;
using InputManager = PureCAD.Core.InputManager;

namespace PureCAD.UserControls
{
    /// <summary>
    /// Interaction logic for Vector2TextBox.xaml
    /// </summary>
    public partial class Vector2TextBox : UserControl, INotifyPropertyChanged
    {
        public Vector2 Value
        {
            get
            {
                return (Vector2)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public double IncrementValue
        {
            get
            {
                return (double)GetValue(IncrementValueProperty);
            }
            set
            {
                SetValue(IncrementValueProperty, value);
            }
        }

        public double IncrementDistance
        {
            get
            {
                return (double)GetValue(IncrementDistanceProperty);
            }
            set
            {
                SetValue(IncrementDistanceProperty, value);
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return (bool)GetValue(IsReadOnlyProperty);
            }
            set
            {
                SetValue(IsReadOnlyProperty, value);
            }
        }

        private float _initialValue = 0.0f;

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof(Value), typeof(Vector2), typeof(Vector2TextBox), new PropertyMetadata(Vector2.Zero, OnValueChanged));

        public static readonly DependencyProperty IncrementValueProperty = DependencyProperty.Register(nameof(IncrementValue), typeof(double), typeof(Vector2TextBox), new PropertyMetadata(0.25, OnIncrementValueChanged));

        public static readonly DependencyProperty IncrementDistanceProperty = DependencyProperty.Register(nameof(IncrementDistance), typeof(double), typeof(Vector2TextBox), new PropertyMetadata(25.0, OnIncrementDistanceChanged));

        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(nameof(IsReadOnly), typeof(bool), typeof(Vector2TextBox), new PropertyMetadata(false, OnEditableChanged));

        public bool ReadOnlyValue
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set
            {
                IsReadOnly = value;
                OnPropertyChanged();
            }
        }

        public float X
        {
            get { return (float)Math.Round(((Vector2)GetValue(ValueProperty)).X, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector2(value, Value.Y);
                OnPropertyChanged();
            }
        }

        public float Y
        {
            get { return (float)Math.Round(((Vector2)GetValue(ValueProperty)).Y, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector2(Value.X, value);
                OnPropertyChanged();
            }
        }
        
        public Vector2TextBox()
        {
            InitializeComponent();
            ((FrameworkElement)Content).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void OnEditableChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector2TextBox control = sender as Vector2TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("ReadOnlyValue"));
            }
        }

        private static void OnIncrementDistanceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector2TextBox control = sender as Vector2TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;
                
                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementDistance"));
            }
        }

        private static void OnIncrementValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector2TextBox control = sender as Vector2TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("IncrementValue"));
            }
        }

        private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector2TextBox control = sender as Vector2TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("X"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Y"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Z"));
            }
        }

        private void XLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.X;
            XLabel.CaptureMouse();
        }

        private void YLabel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _initialValue = Value.Y;
            YLabel.CaptureMouse();
        }

        private void Label_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            var label = (Label)sender;
            if (label.IsMouseCaptured)
            {
                label.ReleaseMouseCapture();
            }
        }

        private void XLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (XLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(XLabel);
                X = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }

        private void YLabel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (YLabel.IsMouseCaptured)
            {
                var pos = e.GetPosition(YLabel);
                Y = _initialValue + (float)(IncrementValue * (int)(pos.X / IncrementDistance));
            }
        }
    }
}
