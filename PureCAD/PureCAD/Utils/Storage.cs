﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Utils
{
    public class Storage : BindableObject
    {
        public int DisplayWidth;
        public int DisplayHeight;
        public float DisplayAspect;


        public bool CursorMode
        {
            get { return _cursorMode; }
            set
            {
                _cursorMode = value;
                RaisePropertyChanged();
            }
        }

        private Dictionary<string, int> _instanceStorageDictionary;
        private bool _cursorMode = false;


        private void AddToInstanceStorage(SceneObject item)
        {
            if (_instanceStorageDictionary == null)
                _instanceStorageDictionary = new Dictionary<string, int>();

            if (!_instanceStorageDictionary.ContainsKey(item.ToString()))
            {
                _instanceStorageDictionary.Add(item.ToString(), 0);
            }
            else
            {
                _instanceStorageDictionary[item.ToString()]++;
            }
        }

        public string GetNameInstance(SceneObject item)
        {
            AddToInstanceStorage(item);

            var type = item.ToString();
            return type + " " + _instanceStorageDictionary[type];
        }

        public void SetDisplayDimensions(int displayWidth, int displayHeight, float displayAspect)
        {
            DisplayWidth = displayWidth;
            DisplayHeight = displayHeight;
            DisplayAspect = displayAspect;
        }

        
        //private static int _torusCounter;
        //public static int TorusCounter => _torusCounter++;

        //private static int _pointCounter;
        //public static int PointCounter => _pointCounter++;
    }
}
