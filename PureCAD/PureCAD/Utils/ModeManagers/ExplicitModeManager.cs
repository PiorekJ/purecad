﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Components;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Utils.Ellipsoid
{
    public class ExplicitModeManager : BindableObject, IDisposable
    {
        private int _frameBuffer;
        private int _renderBuffer;
        private int _texture;
        private Shader _shader;
        private Shader _shaderTex;
        private Mesh<VertexPT> _explicitModeMesh;
        private Mesh<VertexPT> _textureMesh;
        private ExplicitEllipsoidData _explicitEllipsoid;
        private float _refreshRate = Constants.MinRefreshRate;
        private int _userDivisor = Constants.DefaultExplicitDivisor;
        private float _countDown = 0.0f;

        private int _currentDivisor;
        private int _maxDivisor;
        public int UserDivisor
        {
            get { return _userDivisor; }
            set
            {
                _userDivisor = value > 0 ? System.Math.Min(value, _maxDivisor) : 1;
                RaisePropertyChanged();
            }
        }

        public float RefreshRate
        {
            get { return _refreshRate; }
            set
            {
                _refreshRate = value;
                if (System.Math.Abs(_userDivisor) < Constants.Epsilon)
                    _refreshRate = Constants.MinRefreshRate;
                RaisePropertyChanged();
            }
        }

        public ExplicitEllipsoidData ExplicitEllipsoid
        {
            get { return _explicitEllipsoid; }
            set
            {
                _explicitEllipsoid = value;
                RaisePropertyChanged();
            }
        }

        public int FrameBuffer => _frameBuffer;

        public int RenderBuffer => _renderBuffer;

        public int Texture => _texture;

        public void SetupExplicitMode()
        {
            var viewportWidth = Simulation.Storage.DisplayWidth;
            var viewportHeight = Simulation.Storage.DisplayHeight;
            ExplicitEllipsoid = new ExplicitEllipsoidData((obj, oldPos, newPos) =>
            {
                ExplicitEllipsoid.TransformChanged = true;
            }, (obj, value, newValue) =>
            {
                ExplicitEllipsoid.TransformChanged = true;
            });
            _currentDivisor = UserDivisor;
            _shader = Shaders.ExplicitShader;
            _shaderTex = Shaders.TextureSampleShader;
            _frameBuffer = GL.GenFramebuffer();
            _renderBuffer = GL.GenRenderbuffer();
            _texture = GL.GenTexture();

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
            GL.BindTexture(TextureTarget.Texture2D, _texture);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, viewportWidth / _currentDivisor, viewportHeight / _currentDivisor, 0, PixelFormat.Rgb, PixelType.Int, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, _texture, 0);

            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _renderBuffer);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, viewportWidth / _currentDivisor, viewportHeight / _currentDivisor);

            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);

            if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete)
                throw new Exception("Framebuffer initialization failed");

            List<VertexPT> vertices = new List<VertexPT>();
            vertices.Add(new VertexPT(-1, -1, 0, 0, 0)); // TL
            vertices.Add(new VertexPT(-1, 1, 0, 0, 1)); // BL
            vertices.Add(new VertexPT(1, 1, 0, 1, 1)); // BR
            vertices.Add(new VertexPT(1, -1, 0, 1, 0)); // TR

            List<uint> edges = new List<uint>();
            edges.Add(0);
            edges.Add(2);
            edges.Add(1);

            edges.Add(0);
            edges.Add(3);
            edges.Add(2);

            _explicitModeMesh = new Mesh<VertexPT>(vertices, edges, MeshType.Triangles, AccessType.Static);
            _textureMesh = new Mesh<VertexPT>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }
        public void ResizeFrameBuffer()
        {
            var viewportWidth = Simulation.Storage.DisplayWidth;
            var viewportHeight = Simulation.Storage.DisplayHeight;
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, viewportWidth / _currentDivisor, viewportHeight / _currentDivisor, 0, PixelFormat.Rgb, PixelType.Int, IntPtr.Zero);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _renderBuffer);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, viewportWidth / _currentDivisor, viewportHeight / _currentDivisor);
            _maxDivisor = System.Math.Min(viewportHeight, viewportWidth) / 4;
            if (UserDivisor > _maxDivisor)
                UserDivisor = _maxDivisor;
        }

        private void DrawEllipsoid()
        {
            _shader.Use();
            _shader.Bind(_shader.GetUniformLocation("model"), ExplicitEllipsoid.Transform.GetModelMatrix());
            _shader.Bind(_shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            _shader.Bind(_shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            _shader.Bind(_shader.GetUniformLocation("radii"), ExplicitEllipsoid.Radii);
            _shader.Bind(_shader.GetUniformLocation("intesityParameter"), ExplicitEllipsoid.LightParameter);
            _explicitModeMesh.Draw();
        }

        private void DrawTexture()
        {
            _shaderTex.Use();
            _shaderTex.Bind(_shaderTex.GetUniformLocation("sampler"), 0);
            _textureMesh.Draw();
        }

        public void Draw()
        {
            if (Simulation.Scene.Camera.IsMouseMoving || Simulation.Scene.Camera.IsRotating || Simulation.Scene.Camera.IsKeyboardMoving || ExplicitEllipsoid.TransformChanged)
            {
                Reset();
                ExplicitEllipsoid.TransformChanged = false;
            }

            if (_countDown < RefreshRate)
            {
                GL.Viewport(0, 0, Simulation.Storage.DisplayWidth / _currentDivisor, Simulation.Storage.DisplayHeight / _currentDivisor);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, FrameBuffer);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                DrawEllipsoid();

                GL.Viewport(0, 0, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                DrawTexture();
                _countDown += Simulation.DeltaTime;
            }
            else
            {
                _countDown = 0.0f;
                _currentDivisor /= 2;

                if (_currentDivisor == 0)
                    _currentDivisor = 1;
                ResizeFrameBuffer();
            }

        }

        private void Reset()
        {
            _currentDivisor = UserDivisor;
            _countDown = 0.0f;
            ResizeFrameBuffer();
        }


        public class ExplicitEllipsoidData : BindableObject
        {
            private Vector3 _radii;
            private float _lightParameter = 1;

            public Vector3 Radii
            {
                get { return _radii; }
                set
                {
                    _radii = value;
                    RaisePropertyChanged();
                }
            }

            public Transform Transform { get; set; }

            public float LightParameter
            {
                get { return _lightParameter; }
                set
                {
                    _lightParameter = value;
                    RaisePropertyChanged();
                }
            }

            public bool TransformChanged;

            public ExplicitEllipsoidData(Transform.Vector3UpdateEventHandler onDataVector3Update, Transform.QuaternionUpdateEventHandler onDataQuaternionUpdate)
            {
                Radii = Vector3.One;
                Transform = new Transform(onDataVector3Update, onDataQuaternionUpdate, onDataVector3Update);
            }
        }

        public void Dispose()
        {
            _textureMesh.Dispose();
            _explicitModeMesh.Dispose();
        }
    }




}
