﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.Models.PathGeneration;
using PureCAD.Utils.Math;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Utils.ModeManagers
{
    public class IntersectionManager : BindableObject
    {
        private IntersectionMath.WrapValuesUVST NewtonIterations(out Vector4d startParams, Vector4d approxParams, IIntersectable bezierSurfaceOne, IIntersectable bezierSurfaceTwo, double userStep, List<Vector2d> curvePointsSurfaceOne, List<Vector2d> curvePointsSurfaceTwo, List<Vector3> curveScenePoints, int maxIterations, bool suppress = false)
        {
            var iterCounter = 0;
            var startPointP = new Vector3d();
            var startPointQ = new Vector3d();
            var newtonWrapValues = new IntersectionMath.WrapValuesUVST();
            var stepSquared = (1.5 * userStep) * (1.5 * userStep);
            startParams = new Vector4d();
            try
            {
                do
                {
                    if (iterCounter == 0)
                    {
                        startParams = approxParams = IntersectionMath.NewtonMethod(bezierSurfaceOne, bezierSurfaceTwo, approxParams, userStep, out newtonWrapValues);
                        startPointP = bezierSurfaceOne.EvaluateAt(startParams.Xy);
                        startPointQ = bezierSurfaceTwo.EvaluateAt(startParams.Zw);
                        //Simulation.Scene.AddScenePoint(startPointP.ToVector3(), Colors.DeepPink, $"Newton {iterCounter}", true, true);
                        curveScenePoints.Add(startPointP.ToVector3());
                        curvePointsSurfaceOne.Add(startParams.Xy);
                        curvePointsSurfaceTwo.Add(startParams.Zw);
                    }
                    else if (iterCounter < 5)
                    {
                        approxParams = IntersectionMath.NewtonMethod(bezierSurfaceOne, bezierSurfaceTwo, approxParams, userStep,
                            out newtonWrapValues);
                        var pointP = bezierSurfaceOne.EvaluateAt(approxParams.Xy);
                        //Simulation.Scene.AddScenePoint(pointP.ToVector3(), Colors.DeepPink, $"Newton {iterCounter}", true, true);
                        curveScenePoints.Add(pointP.ToVector3());
                        curvePointsSurfaceOne.Add(approxParams.Xy);
                        curvePointsSurfaceTwo.Add(approxParams.Zw);
                    }
                    else
                    {
                        approxParams = IntersectionMath.NewtonMethod(bezierSurfaceOne, bezierSurfaceTwo, approxParams, userStep,
                            out newtonWrapValues);
                        var pointP = bezierSurfaceOne.EvaluateAt(approxParams.Xy);
                        var pointQ = bezierSurfaceTwo.EvaluateAt(approxParams.Zw);

                        if ((pointP - startPointP).LengthSquared < stepSquared)
                            break; // Close to start
                        if ((pointQ - startPointQ).LengthSquared < stepSquared)
                            break; // Close to start

                        //Simulation.Scene.AddScenePoint(pointP.ToVector3(), Colors.DeepPink, $"Newton {iterCounter}", true, true);
                        curveScenePoints.Add(pointP.ToVector3());
                        curvePointsSurfaceOne.Add(approxParams.Xy);
                        curvePointsSurfaceTwo.Add(approxParams.Zw);
                    }

                    iterCounter++;

                    if (iterCounter >= maxIterations)
                        throw new Exception("Newton iterations number of iterations exceeded");

                } while (!newtonWrapValues.IsParamOutside());
            }
            catch (Exception e)
            {
                if (!suppress)
                {
                    MessageBox.Show("Newton method " + e.Message);
                    return null;
                }
                return newtonWrapValues;
            }
            return newtonWrapValues;
        }

        public IntersectionData SelfIntersect(IIntersectable surface, double userStep, double delta)
        {
            Vector2d p1, p2;
            Vector4d pointParams, approxParams;

            try
            {
                p1 = IntersectionMath.CurstorSimpleDescent(surface, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface.EvaluateAt(p1).ToVector3(), Colors.Red, "Gradient descent cursor surface 1", true, true);
                p2 = IntersectionMath.CurstorSimpleDescentDistance(surface, Simulation.Scene.SceneCursor.Transform.Position, p1, delta);
                //Simulation.Scene.AddScenePoint(surface.EvaluateAt(p2).ToVector3(), Colors.Red, "Gradient descent cursor surface 2", true, true);
                pointParams = new Vector4d(p1.X, p1.Y, p2.X, p2.Y);
                approxParams = IntersectionMath.SurfacePointsGradientDescent(surface, surface, pointParams);
                //Simulation.Scene.AddScenePoint(surface.EvaluateAt(approxParams.Xy).ToVector3(), Colors.Yellow, "Approx surface 1", true, true);
                //Simulation.Scene.AddScenePoint(surface.EvaluateAt(approxParams.Zw).ToVector3(), Colors.Green, "Approx surface 2", true, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Gradients " + e.Message);
                return null;
            }

            var curvePointsSurfaceOne = new List<Vector2d>();
            var curvePointsSurfaceTwo = new List<Vector2d>();
            var curveScenePoints = new List<Vector3>();

            Vector4d startParams;

            IntersectionMath.WrapValuesUVST resultValues = NewtonIterations(out startParams, approxParams, surface, surface, userStep, curvePointsSurfaceOne, curvePointsSurfaceTwo, curveScenePoints, Simulation.Settings.NewtonMaxIterations);
            if (resultValues == null)
                return null;

            if (resultValues.IsParamOutside())
            {
                var curveScenePointsReversed = new List<Vector3>();
                var curvePointsSurfaceOneReversed = new List<Vector2d>();
                var curvePointsSurfaceTwoReversed = new List<Vector2d>();

                NewtonIterations(out startParams, startParams, surface, surface, -userStep,
                    curvePointsSurfaceOneReversed, curvePointsSurfaceTwoReversed, curveScenePointsReversed, Simulation.Settings.NewtonMaxIterations);

                curveScenePoints.Reverse();
                curveScenePoints.AddRange(curveScenePointsReversed);

                curvePointsSurfaceOne.Reverse();
                curvePointsSurfaceOne.AddRange(curvePointsSurfaceOneReversed);

                curvePointsSurfaceTwo.Reverse();
                curvePointsSurfaceTwo.AddRange(curvePointsSurfaceTwoReversed);

                return new IntersectionData(curveScenePoints, false, surface, curvePointsSurfaceOne, surface, curvePointsSurfaceTwo, approxParams);
            }

            return new IntersectionData(curveScenePoints, true, surface, curvePointsSurfaceOne, surface, curvePointsSurfaceTwo, approxParams);



        }

        public Vector4d ApproxIntersectionPoints(IIntersectable surface1, IIntersectable surface2)
        {
            Vector2d p1, p2;
            Vector4d pointParams, approxParams = new Vector4d();
            try
            {
                p1 = IntersectionMath.CursorGradientDescent(surface1, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(p1).ToVector3(), Colors.Red, "Gradient descent cursor surface 1", true, true);
                p2 = IntersectionMath.CursorGradientDescent(surface2, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(p2).ToVector3(), Colors.Blue, "Gradient descent cursor surface 2", true, true);
                pointParams = new Vector4d(p1.X, p1.Y, p2.X, p2.Y);
                approxParams = IntersectionMath.SurfacePointsGradientDescent(surface1, surface2, pointParams);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(approxParams.Xy).ToVector3(), Colors.Yellow, "Approx surface 1", true, true);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(approxParams.Zw).ToVector3(), Colors.Green, "Approx surface 2", true, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Gradients " + e.Message);
                return approxParams;
            }
            return approxParams;
        }

        public IntersectionData Intersect(IIntersectable surface1, IIntersectable surface2, double userStep, bool addCurve = true)
        {

            Vector2d p1, p2;
            Vector4d pointParams, approxParams;
            try
            {
                p1 = IntersectionMath.CursorGradientDescent(surface1, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(p1).ToVector3(), Colors.Red, "Gradient descent cursor surface 1", true, true);
                p2 = IntersectionMath.CursorGradientDescent(surface2, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(p2).ToVector3(), Colors.Blue, "Gradient descent cursor surface 2", true, true);
                pointParams = new Vector4d(p1.X, p1.Y, p2.X, p2.Y);
                approxParams = IntersectionMath.SurfacePointsGradientDescent(surface1, surface2, pointParams);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(approxParams.Xy).ToVector3(), Colors.Yellow, "Approx surface 1", true, true);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(approxParams.Zw).ToVector3(), Colors.Green, "Approx surface 2", true, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Gradients " + e.Message);
                return null;
            }


            var curvePointsSurfaceOne = new List<Vector2d>();
            var curvePointsSurfaceTwo = new List<Vector2d>();
            var curveScenePoints = new List<Vector3>();

            Vector4d startParams;

            IntersectionMath.WrapValuesUVST resultValues = NewtonIterations(out startParams, approxParams, surface1, surface2, userStep, curvePointsSurfaceOne, curvePointsSurfaceTwo, curveScenePoints, Simulation.Settings.NewtonMaxIterations);
            if (resultValues == null)
                return null;

            if (resultValues.IsParamOutside())
            {
                var curveScenePointsReversed = new List<Vector3>();
                var curvePointsSurfaceOneReversed = new List<Vector2d>();
                var curvePointsSurfaceTwoReversed = new List<Vector2d>();

                NewtonIterations(out startParams, startParams, surface1, surface2, -userStep,
                    curvePointsSurfaceOneReversed, curvePointsSurfaceTwoReversed, curveScenePointsReversed, Simulation.Settings.NewtonMaxIterations);

                curveScenePoints.Reverse();
                curveScenePoints.AddRange(curveScenePointsReversed);

                curvePointsSurfaceOne.Reverse();
                curvePointsSurfaceOne.AddRange(curvePointsSurfaceOneReversed);

                curvePointsSurfaceTwo.Reverse();
                curvePointsSurfaceTwo.AddRange(curvePointsSurfaceTwoReversed);

                return new IntersectionData(curveScenePoints, false, surface1, curvePointsSurfaceOne, surface2, curvePointsSurfaceTwo, approxParams, addCurve);
            }

            return new IntersectionData(curveScenePoints, true, surface1, curvePointsSurfaceOne, surface2, curvePointsSurfaceTwo, approxParams, addCurve);
        }

        public IntersectionData IntersectOneWay(IIntersectable surface1, IIntersectable surface2, double userStep, int maxIter)
        {

            Vector2d p1, p2;
            Vector4d pointParams, approxParams;
            try
            {
                p1 = IntersectionMath.CursorGradientDescent(surface1, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(p1).ToVector3(), Colors.Red, "Gradient descent cursor surface 1", true, true);
                p2 = IntersectionMath.CursorGradientDescent(surface2, Simulation.Scene.SceneCursor.Transform.Position);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(p2).ToVector3(), Colors.Blue, "Gradient descent cursor surface 2", true, true);
                pointParams = new Vector4d(p1.X, p1.Y, p2.X, p2.Y);
                approxParams = IntersectionMath.SurfacePointsGradientDescent(surface1, surface2, pointParams);
                //Simulation.Scene.AddScenePoint(surface1.EvaluateAt(approxParams.Xy).ToVector3(), Colors.Yellow, "Approx surface 1", true, true);
                //Simulation.Scene.AddScenePoint(surface2.EvaluateAt(approxParams.Zw).ToVector3(), Colors.Green, "Approx surface 2", true, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Gradients " + e.Message);
                return null;
            }


            var curvePointsSurfaceOne = new List<Vector2d>();
            var curvePointsSurfaceTwo = new List<Vector2d>();
            var curveScenePoints = new List<Vector3>();

            Vector4d startParams;

            IntersectionMath.WrapValuesUVST resultValues = NewtonIterations(out startParams, approxParams, surface1, surface2, userStep, curvePointsSurfaceOne, curvePointsSurfaceTwo, curveScenePoints, maxIter, true);
            if (resultValues == null)
                return null;

            return new IntersectionData(curveScenePoints, true, surface1, curvePointsSurfaceOne, surface2, curvePointsSurfaceTwo, approxParams, false);
        }

        public class IntersectionData
        {
            public Model IntersectionCurve;
            public bool IsCurveClosed;
            public IIntersectable Surface1;
            public IIntersectable Surface2;
            public List<Vector2d> Params1;
            public List<Vector2d> Params2;
            public List<Vector3> ScenePoints;
            public Vector4d ApproxParams;

            public IntersectionData(List<Vector3> scenePoints, bool isClosed, IIntersectable surface1, List<Vector2d> params1, IIntersectable surface2, List<Vector2d> params2, Vector4d approxParams, bool addCurve = true)
            {
                IsCurveClosed = isClosed;
                ScenePoints = scenePoints;
                Surface1 = surface1;
                Surface2 = surface2;
                Params1 = params1;
                Params2 = params2;
                if (addCurve)
                    IntersectionCurve = Simulation.Scene.AddSceneIntersectionCurve(this);
                ApproxParams = approxParams;
            }
        }
    }
}
