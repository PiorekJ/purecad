﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils.Math;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Utils.ModeManagers
{
    public class StereoModeManager : BindableObject, IDisposable
    {
        private int[] _textures = new int[2];
        private int[] _frameBuffers = new int[2];
        private int[] _renderBuffers = new int[2];

        private Mesh<VertexPT> _stereoMesh;
        private Vector3 _redLeftWeights = Constants.DefaultRedLeft;
        private Vector3 _greenLeftWeights = Constants.DefaultGreenLeft;
        private Vector3 _blueLeftWeights = Constants.DefaultBlueLeft;
        private Vector3 _redRightWeights = Constants.DefaultRedRight;
        private Vector3 _greenRightWeights = Constants.DefaultGreenRight;
        private Vector3 _blueRightWeights = Constants.DefaultBlueRight;


        public Vector3 RedLeftWeights
        {
            get { return _redLeftWeights; }
            set
            {
                _redLeftWeights = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 GreenLeftWeights
        {
            get { return _greenLeftWeights; }
            set
            {
                _greenLeftWeights = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 BlueLeftWeights
        {
            get { return _blueLeftWeights; }
            set
            {
                _blueLeftWeights = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 RedRightWeights
        {
            get { return _redRightWeights; }
            set
            {
                _redRightWeights = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 GreenRightWeights
        {
            get { return _greenRightWeights; }
            set
            {
                _greenRightWeights = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 BlueRightWeights
        {
            get { return _blueRightWeights; }
            set
            {
                _blueRightWeights = value;
                RaisePropertyChanged();
            }
        }

        public void SetupStereoMode()
        {
            var viewportWidth = Simulation.Storage.DisplayWidth;
            var viewportHeight = Simulation.Storage.DisplayHeight;

            GL.GenFramebuffers(2, _frameBuffers);
            GL.GenRenderbuffers(2, _renderBuffers);
            GL.GenTextures(2, _textures);

            for (int i = 0; i < 2; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffers[i]);

                GL.BindTexture(TextureTarget.Texture2D, _textures[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, viewportWidth, viewportHeight, 0, PixelFormat.Rgb, PixelType.Int, IntPtr.Zero);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, _textures[i], 0);
                GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _renderBuffers[i]);
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, viewportWidth, viewportHeight);
                GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, _renderBuffers[i]);
                GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
                if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete)
                    throw new Exception("Framebuffer not complete");

            }

            List<VertexPT> vertices = new List<VertexPT>();
            vertices.Add(new VertexPT(-1, -1, 0, 0, 0)); // TL
            vertices.Add(new VertexPT(-1, 1, 0, 0, 1)); // BL
            vertices.Add(new VertexPT(1, 1, 0, 1, 1)); // BR
            vertices.Add(new VertexPT(1, -1, 0, 1, 0)); // TR

            List<uint> edges = new List<uint>();
            edges.Add(0);
            edges.Add(2);
            edges.Add(1);

            edges.Add(0);
            edges.Add(3);
            edges.Add(2);

            _stereoMesh = new Mesh<VertexPT>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public void CreatePerspectiveStereoscopic(float fovy, float aspect, float zNear, float zFar, float eyeSeparation, float focusDepth, out Matrix4 leftEye, out Matrix4 rightEye)
        {
            if (fovy <= 0.0 || fovy > System.Math.PI)
                throw new ArgumentOutOfRangeException(nameof(fovy));
            if (aspect <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(aspect));
            if (zNear <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(zNear));
            if (zFar <= 0.0)
                throw new ArgumentOutOfRangeException(nameof(zFar));

            float halfEyeSeparation = 0.5f * eyeSeparation;

            float frustumShift = halfEyeSeparation * zNear / focusDepth;

            float top = (float)(zNear * System.Math.Tan(0.5f * fovy));
            float bottom = -top;
            float left = bottom * aspect;
            float right = top * aspect;

            leftEye = Matrix4.CreateTranslation((float)halfEyeSeparation, 0.0f, 0.0f) * MathGeneralUtils.CreatePerspectiveOffCenter(left + frustumShift, right + frustumShift, bottom, top, zNear, zFar);
            rightEye = Matrix4.CreateTranslation((float)-halfEyeSeparation, 0.0f, 0.0f) * MathGeneralUtils.CreatePerspectiveOffCenter(left - frustumShift, right - frustumShift, bottom, top, zNear, zFar);
        }

        public void ResizeFrameBuffers()
        {
            var viewportWidth = Simulation.Storage.DisplayWidth;
            var viewportHeight = Simulation.Storage.DisplayHeight;

            for (int i = 0; i < 2; i++)
            {
                GL.BindTexture(TextureTarget.Texture2D, _textures[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, viewportWidth, viewportHeight, 0, PixelFormat.Rgb, PixelType.Int, IntPtr.Zero);
                GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _renderBuffers[i]);
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, viewportWidth, viewportHeight);
            }
        }

        public void Draw()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffers[0]);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Simulation.Scene.Camera.IsLeftEye = true;
            foreach (SceneObject model in Simulation.Scene.SceneObjects)
            {
                //model.Update();
                model.Render();
            }
            Simulation.Scene.SceneCursor.Render();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffers[1]);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Simulation.Scene.Camera.IsLeftEye = false;
            foreach (SceneObject model in Simulation.Scene.SceneObjects)
            {
                //model.Update();
                model.Render();
            }
            Simulation.Scene.SceneCursor.Render();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            if (Simulation.Scene.SelectionManager.SelectedSceneObject != null)
                Simulation.Scene.SelectionManager.SelectedSceneObject.Update();
            Simulation.Scene.SceneCursor.Update();

            Shader shader = Shaders.StereoShader;
            shader.Use();
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _textures[0]);
            shader.Bind(shader.GetUniformLocation("leftSampler"), 0);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, _textures[1]);
            shader.Bind(shader.GetUniformLocation("rightSampler"), 1);

            shader.Bind(shader.GetUniformLocation("redLeftWeights"), RedLeftWeights);
            shader.Bind(shader.GetUniformLocation("redRightWeights"), RedRightWeights);
            shader.Bind(shader.GetUniformLocation("greenLeftWeights"), GreenLeftWeights);
            shader.Bind(shader.GetUniformLocation("greenRightWeights"), GreenRightWeights);
            shader.Bind(shader.GetUniformLocation("blueLeftWeights"), BlueLeftWeights);
            shader.Bind(shader.GetUniformLocation("blueRightWeights"), BlueRightWeights);

            _stereoMesh.Draw();
        }

        public void Dispose()
        {
            _stereoMesh.Dispose();
        }

        public void SetColorSet(int type)
        {
            switch (type)
            {
                case 0:
                    RedLeftWeights = new Vector3(1, 0, 0);
                    GreenLeftWeights = new Vector3(0, 0, 0);
                    BlueLeftWeights = new Vector3(0, 0, 0);
                    RedRightWeights = new Vector3(0, 0, 0);
                    GreenRightWeights = new Vector3(0, 1, 0);
                    BlueRightWeights = new Vector3(0, 0, 1);
                    break;
                case 1:
                    RedLeftWeights = new Vector3(0.299f, 0.587f, 0.114f);
                    GreenLeftWeights = new Vector3(0, 0, 0);
                    BlueLeftWeights = new Vector3(0, 0, 0);
                    RedRightWeights = new Vector3(0, 0, 0);
                    GreenRightWeights = new Vector3(0, 1, 0);
                    BlueRightWeights = new Vector3(0, 0, 1);
                    break;
                case 2:
                    RedLeftWeights = new Vector3(0.299f, 0.587f, 0.114f);
                    GreenLeftWeights = new Vector3(0, 0, 0);
                    BlueLeftWeights = new Vector3(0, 0, 0);
                    RedRightWeights = new Vector3(0, 0, 0);
                    GreenRightWeights = new Vector3(0.299f, 0.587f, 0.114f);
                    BlueRightWeights = new Vector3(0.299f, 0.587f, 0.114f);
                    break;
                case 3:
                    RedLeftWeights = new Vector3(0.4561f, 0.500484f, 0.176381f);
                    GreenLeftWeights = new Vector3(-0.0400822f, -0.0378246f, -0.0157589f);
                    BlueLeftWeights = new Vector3(-0.0152161f, -0.0205971f, -0.00546856f);
                    RedRightWeights = new Vector3(-0.0434706f, -0.0879388f, -0.00155529f);
                    GreenRightWeights = new Vector3(0.378476f, 0.73364f, -0.0184503f);
                    BlueRightWeights = new Vector3(-0.0721527f, -0.112961f, 1.2264f);
                    break;
                default:
                    RedLeftWeights = new Vector3(1, 0, 0);
                    GreenLeftWeights = new Vector3(0, 0, 0);
                    BlueLeftWeights = new Vector3(0, 0, 0);
                    RedRightWeights = new Vector3(0, 0, 0);
                    GreenRightWeights = new Vector3(0, 1, 0);
                    BlueRightWeights = new Vector3(0, 0, 1);
                    break;
            }
}
    }
}
