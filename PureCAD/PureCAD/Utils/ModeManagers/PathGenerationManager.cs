﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.Models.PathGeneration;
using PureCAD.Utils.Serialization;
using PureCAD.Utils.UIExtensionClasses;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace PureCAD.Utils.ModeManagers
{
    public class PathGenerationManager : BindableObject, IDisposable
    {
        private PathGenerators _pathGenerators;

        public PathGenerators PathGenerators
        {
            get { return _pathGenerators; }
            set
            {
                _pathGenerators = value;
                RaisePropertyChanged();
            }
        }

        private bool _drawHeightMap = false;
        public bool DrawHeightMap
        {
            get { return _drawHeightMap; }
            set
            {
                _drawHeightMap = value;
                RaisePropertyChanged();
            }
        }

        private bool _drawSurfaces = false;
        public bool DrawSurfaces
        {
            get { return _drawSurfaces; }
            set
            {
                _drawSurfaces = value;
                RaisePropertyChanged();
            }
        }

        public List<StaticSurface> LoadedSurfaces;
        public float[] CurrentHeightMap;
        private int _heightMapSize = 100;

        #region Texture

        private int _frameBuffer;
        private int _texture;
        private Quad _quad;

        #endregion

        #region Manager

        public void InitializePathGeneration()
        {
            PathGenerators = new PathGenerators();
            LoadedSurfaces = LoadSurfaces("C:\\Users\\Jakub\\Desktop\\Final8.json");
        }

        public List<StaticSurface> LoadSurfaces(string filename)
        {
            //"C:\\Users\\Jakub\\Desktop\\Final2.json"
            var surfaceList = new List<StaticSurface>();
            var points = new List<Vector3>();
            StreamReader streamReader = new StreamReader(filename);
            SerializedScene scene =
                JsonConvert.DeserializeObject<SerializedScene>(streamReader.ReadToEnd());
            if (scene != null)
            {
                if (scene.points != null)
                    foreach (point serializedPoint in scene.points)
                    {
                        var point = new Vector3(serializedPoint.x, serializedPoint.y, serializedPoint.z);
                        points.Add(point);
                    }
                if (scene.surfacesC0 != null)
                    foreach (surfaceC0 surfaceC0 in scene.surfacesC0)
                    {
                        var surface = new StaticSurfaceC0();
                        surface.Deserialize(surfaceC0, points);
                        var list = new List<Vector2d>();
                        for (float v = 0; v < surface.GetVRange(); v += 0.01f)
                        {
                            list.Add(new Vector2d(1, v));
                        }
                        surface.CreateSphereMesh(list, 0.8f);
                        surfaceList.Add(surface);
                    }
                if (scene.surfacesC2 != null)
                    for (var i = 0; i < scene.surfacesC2.Length; i++)
                    {
                        surfaceC2 surfaceC2 = scene.surfacesC2[i];
                        var surface = new StaticSurfaceC2();
                        surface.Deserialize(surfaceC2, points);
                        if (i == 0)
                        {
                            var list = new List<Vector2d>();
                            for (int u = 0; u < surface.GetURange(); u++)
                            {
                                list.Add(new Vector2d(u, 0));
                                list.Add(new Vector2d(u, surface.GetVRange()));
                            }
                            surface.CreateSphereMesh(list, 0.8f);
                        }
                        surfaceList.Add(surface);
                    }
            }

            return surfaceList;
        }

        public void SetupRendering(int texSize)
        {
            _frameBuffer = GL.GenFramebuffer();
            _texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, _texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent24, texSize, texSize, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, _texture, 0);
            GL.DrawBuffer(DrawBufferMode.None);
            GL.ReadBuffer(ReadBufferMode.None);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            _quad = new Quad();
            _quad.Tex = _texture;
        }

        public void OnUpdate()
        {

        }

        public float[] RenderHeightMap(int displayWidth, int displayHeight, float millRadius, float margin, int texSize, int excludeIdx)
        {
            if (LoadedSurfaces != null)
            {
                var cameraPos = Vector3.UnitY * 6;
                var oldCameraPos = Simulation.Scene.Camera.Transform.Position;
                var oldCameraRot = Simulation.Scene.Camera.Transform.Rotation;
                var oldCameraRotX = Simulation.Scene.Camera.RotationX;
                var oldCameraRotY = Simulation.Scene.Camera.RotationY;
                var oldCameraOrtho = Simulation.Scene.Camera.IsOrtographic;

                Simulation.Scene.Camera.SetCamera(cameraPos, Quaternion.FromAxisAngle(Vector3.UnitX, MathHelper.DegreesToRadians(-90)), MathHelper.DegreesToRadians(-90), 0, true);

                GL.Viewport(0, 0, texSize, texSize);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
                GL.Clear(ClearBufferMask.DepthBufferBit);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, _texture);
                for (var i = 0; i < LoadedSurfaces.Count; i++)
                {
                    if (i == excludeIdx)
                        continue;
                    StaticSurface loadedSurface = LoadedSurfaces[i];
                    loadedSurface.Margin = margin;
                    loadedSurface.MillRadius = millRadius;
                    loadedSurface.Render();
                }

                var data = new float[texSize * texSize];
                GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.DepthComponent, PixelType.Float, data);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                GL.Viewport(0, 0, displayWidth, displayHeight);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                var farEnd = cameraPos.Y - Constants.CameraOrthoFar; // 2.0f is because that's the end of far clipping plane
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = farEnd + Constants.CameraOrthoFar +
                              (Constants.CameraOrthoNear - Constants.CameraOrthoFar) * data[i];
                }
                Simulation.Scene.Camera.SetCamera(oldCameraPos, oldCameraRot, oldCameraRotX, oldCameraRotY, oldCameraOrtho);
                GL.DeleteFramebuffer(_frameBuffer);
                return data;
            }
            return null;
        }

        public void OnRender()
        {
            if (DrawHeightMap && _quad != null && _quad.Tex != 0)
            {
                _quad.Render();
            }

            if (DrawSurfaces && LoadedSurfaces != null)
            {
                foreach (StaticSurface surface in LoadedSurfaces)
                {
                    surface.Margin = _currentMargin;
                    surface.MillRadius = _currentMillRadius;
                    surface.Render();
                }
            }
        }

        public void Dispose()
        {
            if (LoadedSurfaces != null)
                foreach (StaticSurface surface in LoadedSurfaces)
                {
                    surface.Dispose();
                }
            _quad?.Dispose();
        }

        #endregion

        #region Paths

        private float _currentMillRadius = 0;
        private float _currentMargin = 0;

        public void GenerateRoughingPath() // TODO: needs a litte fix on removing points (goes too steep at some)
        {
            var pathNodes = new List<Vector3>();
            _currentMillRadius = 0.8f;
            _currentMargin = 0.05f;
            var materialSize = 15.0f;
            _heightMapSize = 100;
            SetupRendering(_heightMapSize);
            var heightMap = RenderHeightMap(Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight, _currentMillRadius, _currentMargin, _heightMapSize, - 1);
            PathGenerators.RoughingPathGenerator.GeneratePathLevel(heightMap, _heightMapSize, _currentMillRadius, materialSize, 3.5f, ref pathNodes);
            PathGenerators.RoughingPathGenerator.GeneratePathLevel(heightMap, _heightMapSize, _currentMillRadius, materialSize, 2.0f, ref pathNodes);
            var path = new SceneMillPath($"01.k{_currentMillRadius * 2 * 10}", MillType.Sphere, _currentMillRadius, pathNodes);
            path.SavePath();
            path.IsVisible = true;
            Simulation.Scene.AddSceneMillPath(path);
            Console.WriteLine($"Path generated. Points {pathNodes.Count}");
            //if (Simulation.Scene.SceneLights == null || Simulation.Scene.SceneLights.Count == 0)
            //    Simulation.Scene.AddSceneLights();
            //SceneMillingMachine millingMachine = Simulation.Scene.SceneMillingMachine;
            //if (millingMachine == null)
            //    millingMachine = Simulation.Scene.AddSceneMillingMachine();
            //path.SetupScenePaths(millingMachine.Material, _currentMillRadius);
            //millingMachine.AddPath(path);
            //millingMachine.MillInstant(path);
            //millingMachine.Material.UpdateAllVertices();
        }

        public void GenerateFlattingPath()
        {
            var pathNodes = new List<Vector3>();
            _currentMillRadius = 0.6f;
            _currentMargin = 0.125f;
            var materialSize = 15.0f;
            _heightMapSize = 100;
            SetupRendering(_heightMapSize);
            var heightMap = RenderHeightMap(Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight, _currentMillRadius, _currentMargin, _heightMapSize, -1);
            PathGenerators.FlattingPathGenerator.GeneratePath(heightMap, _heightMapSize, _currentMillRadius, materialSize, 2.0f, ref pathNodes);
            var path = new SceneMillPath($"02.f{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, pathNodes);
            path.SavePath();
            path.IsVisible = true;
            Simulation.Scene.AddSceneMillPath(path);
            Console.WriteLine($"Path generated. Points {pathNodes.Count}");
            if (Simulation.Scene.SceneLights == null || Simulation.Scene.SceneLights.Count == 0)
                Simulation.Scene.AddSceneLights();
            SceneMillingMachine millingMachine = Simulation.Scene.SceneMillingMachine;
            if (millingMachine == null)
                millingMachine = Simulation.Scene.AddSceneMillingMachine();
            path.SetupScenePaths(millingMachine.Material, _currentMillRadius);
            millingMachine.AddPath(path);
            millingMachine.MillInstant(path);
            millingMachine.Material.UpdateAllVertices();
        }

        public void GenerateRoundingPath()
        {
            var pathNodes = new List<Vector3>();
            Simulation.Scene.LoadScene("C:\\Users\\Jakub\\Desktop\\Final8.json");
            var sceneSurfaces = (Simulation.Scene.SceneObjects.OfType<SceneSurfaceSmooth>()).ToList();
            _currentMillRadius = 0.5f;
            _currentMargin = 0.0f;
            var safeHeight = 2.0f;

            PathGenerators.RoundingPathGenerator.GeneratePath(sceneSurfaces.ToList(), _currentMillRadius, safeHeight, ref pathNodes);
            var path = new SceneMillPath($"03.f{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, pathNodes);
            path.SavePath();
            path.IsVisible = true;
            Simulation.Settings.SurfaceOffsetValue = 0.0f;
            Simulation.Scene.AddSceneMillPath(path);
            Console.WriteLine($"Path generated. Points {pathNodes.Count}");
            //if (Simulation.Scene.SceneLights == null || Simulation.Scene.SceneLights.Count == 0)
            //    Simulation.Scene.AddSceneLights();
            //SceneMillingMachine millingMachine = Simulation.Scene.SceneMillingMachine;
            //if (millingMachine == null)
            //    millingMachine = Simulation.Scene.AddSceneMillingMachine();
            //path.SetupScenePaths(millingMachine.Material, _currentMillRadius);
            //millingMachine.AddPath(path);
            //millingMachine.MillInstant(path);
            //millingMachine.Material.UpdateAllVertices();
        }

        public void GenerateDetailPath()
        {
            var pathNodes = new List<Vector3>();
            Simulation.Scene.LoadScene("C:\\Users\\Jakub\\Desktop\\Final8.json");
            var sceneSurfaces = (Simulation.Scene.SceneObjects.OfType<SceneSurfaceSmooth>()).ToList();
            _currentMillRadius = 0.4f;
            _currentMargin = 0.0f;
            var safeHeight = 2.0f;

            PathGenerators.DetailPathGenerator.GeneratePath(sceneSurfaces.ToList(), _currentMillRadius, safeHeight, ref pathNodes);
            var path = new SceneMillPath($"04.k0{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, pathNodes);
            path.SavePath();
            path.IsVisible = true;
            Simulation.Settings.SurfaceOffsetValue = 0.0f;
            Simulation.Scene.AddSceneMillPath(path);
            Console.WriteLine($"Path generated. Points {pathNodes.Count}");

        }

        public void GenerateSignaturePath()
        {
            var pathNodes = new List<Vector3>();
            Simulation.Scene.LoadScene("C:\\Users\\Jakub\\Desktop\\Modele\\Podpis\\FinalPodpis3.json");
            var sceneCurves = (Simulation.Scene.SceneObjects.OfType<SceneCurve>()).ToList();
            var scenePoints = (Simulation.Scene.SceneObjects.OfType<ScenePoint>()).ToList();
            _currentMillRadius = 0.05f;
            _currentMargin = 0.0f;
            var safeHeight = 2.0f;

            PathGenerators.SignaturePathGenerator.GeneratePath(scenePoints,sceneCurves, _currentMillRadius, 1.95f, ref pathNodes);
            var path = new SceneMillPath($"05.k0{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, pathNodes);
            path.IsVisible = true;
            Simulation.Scene.AddSceneMillPath(path);
            path.SavePath();


            //var logoPath = FileManager.ReadPathFile("C:\\Users\\Jakub\\Desktop\\Modele\\Podpis\\logo.k01");

            //var newPathNodes = new List<Vector3>();
            //foreach (Vector3 node in logoPath.SceneNodes)
            //{
            //    newPathNodes.Add(new Vector3(node.X + 10.0f,node.Y - _currentMillRadius,node.Z - 1.5f));
            //}

            //logoPath.Dispose();
            //logoPath = new SceneMillPath($"05.k0{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, newPathNodes);
            //logoPath.IsVisible = true;
            //var savedPath = logoPath.SavePath();
            //Simulation.Scene.AddSceneMillPath(savedPath);

            //if (Simulation.Scene.SceneLights == null || Simulation.Scene.SceneLights.Count == 0)
            //    Simulation.Scene.AddSceneLights();
            //SceneMillingMachine millingMachine = Simulation.Scene.SceneMillingMachine;
            //if (millingMachine == null)
            //    millingMachine = Simulation.Scene.AddSceneMillingMachine();
            //logoPath.SetupScenePaths(millingMachine.Material, _currentMillRadius);
            //millingMachine.AddPath(logoPath);
            //millingMachine.MillInstant(logoPath);
            //millingMachine.Material.UpdateAllVertices();

            //PathGenerators.SignaturePathGenerator.GeneratePath(sceneCurves.ToList(), _currentMillRadius, safeHeight, ref pathNodes);
            //var path = new SceneMillPath($"04.k0{_currentMillRadius * 2 * 10}", MillType.Flat, _currentMillRadius, pathNodes);
            //path.SavePath();
            //path.IsVisible = true;
            //Simulation.Settings.SurfaceOffsetValue = 0.0f;
            //Simulation.Scene.AddSceneMillPath(path);
            //Console.WriteLine($"Path generated. Points {pathNodes.Count}");

        }

        #endregion
    }
}
