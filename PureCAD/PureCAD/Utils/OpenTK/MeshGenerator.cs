﻿using System;
using System.Collections.Generic;
using OpenTK;
using PureCAD.OpenTK;

namespace PureCAD.Utils.OpenTK
{
    public static class MeshGenerator
    {
        //public static Mesh<VertexPNT> GenerateSphereMesh(float radius, int rings = 64, int sectors = 64)
        //{
        //    var vertices = new List<VertexPNT>();
        //    var indices = new List<uint>();

        //    for (double i = 0; i <= rings; i++)
        //    {
        //        double theta = i * MathHelper.Pi / rings;
        //        double sinTheta = System.Math.Sin(theta);
        //        double cosTheta = System.Math.Cos(theta);

        //        for (double j = 0; j <= sectors; j++)
        //        {
        //            double phi = j * 2 * MathHelper.Pi / sectors;
        //            double sinPhi = System.Math.Sin(phi);
        //            double cosPhi = System.Math.Cos(phi);


        //            var nx = (float)(cosPhi * sinTheta);
        //            var ny = (float)(cosTheta);
        //            var nz = (float)(sinPhi * sinTheta);

        //            vertices.Add(new VertexPNT(radius / 2f * nx, radius / 2f * ny, radius / 2f * nz, nx, ny, nz, (float)(1 - (j / sectors)), (float)(1 - (i / rings))));
        //        }

        //        for (int k = 0; k < rings; k++)
        //        {
        //            for (int l = 0; l < sectors; l++)
        //            {
        //                int first = (k * (sectors + 1)) + l;
        //                int second = first + sectors + 1;

        //                indices.Add((uint)first);
        //                indices.Add((uint)second);
        //                indices.Add((uint)first + 1);

        //                indices.Add((uint)second);
        //                indices.Add((uint)second + 1);
        //                indices.Add((uint)first + 1);

        //            }
        //        }
        //    }

        //    return new Mesh<VertexPNT>(vertices, indices, MeshType.Triangles, AccessType.Static);
        //}

        public static Mesh<VertexPNT> GenerateSphereMeshStrip(float radius, int rings = 64, int sectors = 64)
        {
            var vertices = new List<VertexPNT>();
            var indices = new List<uint>();

            for (int y = 0; y <= rings; ++y)
            {
                for (int x = 0; x <= sectors; ++x)
                {

                    float xSegment = (float)x / (float)sectors;
                    float ySegment = (float)y / (float)rings;
                    float xPos = (float)(System.Math.Cos(xSegment * 2.0f * MathHelper.Pi) * System.Math.Sin(ySegment * MathHelper.Pi));
                    float yPos = (float)(System.Math.Cos(ySegment * MathHelper.Pi));
                    float zPos = (float)(System.Math.Sin(xSegment * 2.0f * MathHelper.Pi) * System.Math.Sin(ySegment * MathHelper.Pi));

                    vertices.Add(new VertexPNT(radius * xPos, radius * yPos, radius * zPos, xPos, yPos, zPos, xSegment, ySegment));
                }
            }

            bool oddRow = false;
            for (int y = 0; y < rings; ++y)
            {
                if (!oddRow) // even rows: y == 0, y == 2; and so on
                {
                    for (int x = 0; x <= sectors; ++x)
                    {
                        indices.Add((uint)(y * (sectors + 1) + x));
                        indices.Add((uint)((y + 1) * (sectors + 1) + x));
                    }
                }
                else
                {
                    for (int x = sectors; x >= 0; --x)
                    {
                        indices.Add((uint)((y + 1) * (sectors + 1) + x));
                        indices.Add((uint)(y * (sectors + 1) + x));
                    }
                }
                oddRow = !oddRow;
            }

            return new Mesh<VertexPNT>(vertices, indices, MeshType.TriangleStrip, AccessType.Static);
        }

        public static Mesh<VertexPN> GenerateCubeMesh(float sizeX, float sizeY, float sizeZ)
        {
            var sizeX2 = sizeX / 2.0f;
            var sizeY2 = sizeY / 2.0f;
            var sizeZ2 = sizeZ / 2.0f;

            Vector3 BLB = new Vector3(-sizeX2, -sizeY2, sizeZ2);
            Vector3 BLF = new Vector3(-sizeX2, -sizeY2, -sizeZ2);
            Vector3 BRB = new Vector3(sizeX2, -sizeY2, sizeZ2);
            Vector3 BRF = new Vector3(sizeX2, -sizeY2, -sizeZ2);

            Vector3 TLB = new Vector3(-sizeX2, sizeY2, sizeZ2);
            Vector3 TLF = new Vector3(-sizeX2, sizeY2, -sizeZ2);
            Vector3 TRB = new Vector3(sizeX2, sizeY2, sizeZ2);
            Vector3 TRF = new Vector3(sizeX2, sizeY2, -sizeZ2);

            Vector3 TopN = -Vector3.UnitY;
            Vector3 BottomN = Vector3.UnitY;
            Vector3 FrontN = -Vector3.UnitZ;
            Vector3 BackN = Vector3.UnitZ;
            Vector3 RightN = -Vector3.UnitX;
            Vector3 LeftN = Vector3.UnitX;

            List<VertexPN> vertices = new List<VertexPN>()
            {
                //Front face 0 1 2 3 
                new VertexPN(BLB, BackN),
                new VertexPN(BRB, BackN),
                new VertexPN(TRB, BackN),
                new VertexPN(TLB, BackN),

                //Back face 4 5 6 7
                new VertexPN(BLF, FrontN),
                new VertexPN(BRF, FrontN),
                new VertexPN(TRF, FrontN),
                new VertexPN(TLF, FrontN),

                //Top face 8 9 10 11
                new VertexPN(TLB, BottomN),
                new VertexPN(TRB, BottomN),
                new VertexPN(TRF, BottomN),
                new VertexPN(TLF, BottomN),

                //Bottom face 12 13 14 15
                new VertexPN(BLB, TopN),
                new VertexPN(BRB, TopN),
                new VertexPN(BRF, TopN),
                new VertexPN(BLF, TopN),

                //Right face 16 17 18 19
                new VertexPN(BRB, LeftN),
                new VertexPN(BRF, LeftN),
                new VertexPN(TRF, LeftN),
                new VertexPN(TRB, LeftN),

                //Left face 20 21 22 23
                new VertexPN(BLB, RightN),
                new VertexPN(BLF, RightN),
                new VertexPN(TLF, RightN),
                new VertexPN(TLB, RightN)
            };
            List<uint> edges = new List<uint>()
            {
                //Back
                1,2,0,
                2,3,0,
                //Front
                6,5,4,
                7,6,4,
                //Top
                9,10,8,
                10,11,8,
                //Bottom
                14,13,12,
                15,14,12,
                //Right
                17,18,16,
                18,19,16,
                //Left
                22,21,20,
                23,22,20
            };

            return new Mesh<VertexPN>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public static void GenerateCylinderVertEdge(float radius, float verticalOffset, float length, int divLen, int divAngle, ref List<VertexPN> vertices, ref List<uint> edges)
        {
            Func<double, double, Vector3> getPoint = (y, fi) =>
                new Vector3((float)System.Math.Cos(fi) * radius, (float)y, (float)System.Math.Sin(fi) * radius);
            Func<int, int, Vector3> getPointInd =
                (i, j) => getPoint(length / divLen * i + verticalOffset, 2 * System.Math.PI / divAngle * j);
            Func<int, int, Vector3> getNormalInd = (i, j) => getPoint(0, 2 * System.Math.PI / divAngle * j);

            //top
            uint middleVert = (uint)vertices.Count;
            vertices.Add(new VertexPN(new Vector3(0, length + verticalOffset, 0), Vector3.UnitY));

            for (int i = 0; i < divAngle; i++)
            {
                uint currentVertIndex = (uint)vertices.Count;
                vertices.Add(new VertexPN(getPoint(length + verticalOffset, 2 * System.Math.PI / divAngle * i), Vector3.UnitY));
                vertices.Add(
                    new VertexPN(getPoint(length + verticalOffset, 2 * System.Math.PI / divAngle * (i + 1) % divAngle),
                        Vector3.UnitY));
                edges.Add(middleVert);
                edges.Add(currentVertIndex);
                edges.Add(currentVertIndex + 1);
            }

            //bottom
            middleVert = (uint)vertices.Count;
            vertices.Add(new VertexPN(new Vector3(0, verticalOffset, 0), -Vector3.UnitY));

            for (int i = 0; i < divAngle; i++)
            {
                uint currentVertIndex = (uint)vertices.Count;
                vertices.Add(new VertexPN(getPoint(verticalOffset, 2 * System.Math.PI / divAngle * i), -Vector3.UnitY));
                vertices.Add(
                    new VertexPN(getPoint(verticalOffset, 2 * System.Math.PI / divAngle * (i + 1) % divAngle), -Vector3.UnitY));
                edges.Add(currentVertIndex + 1);
                edges.Add(currentVertIndex);
                edges.Add(middleVert);
            }

            //side
            for (int i = 0; i < divLen; i++)
            {
                for (int j = 0; j < divAngle; j++)
                {
                    uint currentVertIndex = (uint)vertices.Count;
                    vertices.Add(new VertexPN(getPointInd(i, j), getNormalInd(i, j).Normalized()));
                    vertices.Add(new VertexPN(getPointInd(i, (j + 1) % divAngle),
                        getNormalInd(i, (j + 1) % divAngle).Normalized()));
                    vertices.Add(new VertexPN(getPointInd(i + 1, (j + 1) % divAngle),
                        getNormalInd(i + 1, (j + 1) % divAngle).Normalized()));
                    vertices.Add(new VertexPN(getPointInd(i + 1, j), getNormalInd(i + 1, j).Normalized()));

                    edges.Add(currentVertIndex);
                    edges.Add(currentVertIndex + 1);
                    edges.Add(currentVertIndex + 3);
                    edges.Add(currentVertIndex + 1);
                    edges.Add(currentVertIndex + 2);
                    edges.Add(currentVertIndex + 3);
                }
            }
        }

        public static Mesh<VertexPN> GenerateCylinderMesh(float radius = 0.5f, float verticalOffset = 0f, float length = 3f, int divLen = 5, int divAngle = 32)
        {
            List<VertexPN> vertices = new List<VertexPN>();
            List<uint> edges = new List<uint>();
            GenerateCylinderVertEdge(radius, verticalOffset, length, divLen, divAngle, ref vertices, ref edges);
            return new Mesh<VertexPN>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public static void GenerateSphereVertEdge(float radius, int rings, int sectors, bool isHalf, float verticalOffset, int startIdx, ref List<VertexPN> vertices, ref List<uint> edges)
        {
            float R = 1.0f / (float)(rings - 1);
            float S = 1.0f / (float)(sectors - 1);

            if (isHalf)
            {
                rings /= 2;
                rings += 1;
            }


            for (int r = 0; r < rings; ++r)
            {
                for (int s = 0; s < sectors; ++s)
                {
                    float y = (float)System.Math.Sin(-MathHelper.PiOver2 + MathHelper.Pi * r * R);
                    float x = (float)System.Math.Cos(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);
                    float z = (float)System.Math.Sin(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);

                    //TexCoords.Add(new Vector2(s * S, r * R));
                    vertices.Add(new VertexPN(new Vector3(x * radius, y * radius + verticalOffset, z * radius), new Vector3(x, y, z)));
                    if (r < rings - 1)
                    {
                        int curRow = r * sectors;
                        int nextRow = (r + 1) * sectors;
                        int nextS = (s + 1) % sectors;

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + nextRow + s));
                        edges.Add((uint)(startIdx + nextRow + nextS));

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + nextRow + nextS));
                        edges.Add((uint)(startIdx + curRow + nextS));
                    }
                }
            }
        }

        public static void GenerateSphereVertEdgeWireframe(float radius, int rings, int sectors, bool isHalf, float verticalOffset, int startIdx, ref List<VertexPN> vertices, ref List<uint> edges)
        {
            float R = 1.0f / (float)(rings - 1);
            float S = 1.0f / (float)(sectors - 1);

            if (isHalf)
            {
                rings /= 2;
                rings += 1;
            }


            for (int r = 0; r < rings; ++r)
            {
                for (int s = 0; s < sectors; ++s)
                {
                    float y = (float)System.Math.Sin(-MathHelper.PiOver2 + MathHelper.Pi * r * R);
                    float x = (float)System.Math.Cos(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);
                    float z = (float)System.Math.Sin(2 * MathHelper.Pi * s * S) * (float)System.Math.Sin(MathHelper.Pi * r * R);

                    //TexCoords.Add(new Vector2(s * S, r * R));
                    vertices.Add(new VertexPN(new Vector3(x * radius, y * radius + verticalOffset, z * radius), new Vector3(x, y, z)));
                    if (r < rings - 1)
                    {
                        int curRow = r * sectors;
                        int nextRow = (r + 1) * sectors;
                        int nextS = (s + 1) % sectors;

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + nextRow + s));

                        edges.Add((uint)(startIdx + curRow + s));
                        edges.Add((uint)(startIdx + curRow + nextS));
                    }
                }
            }
        }

        public static Mesh<VertexPN> GenerateSphereMesh(float radius = 0.5f, float verticalOffset = 0, int rings = 32, int sectors = 32, bool isHalf = false)
        {
            List<VertexPN> vertices = new List<VertexPN>();
            List<uint> edges = new List<uint>();
            GenerateSphereVertEdge(radius, rings, sectors, isHalf, verticalOffset, 0, ref vertices, ref edges);
            return new Mesh<VertexPN>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public static Mesh<VertexPN> GenerateSphereMeshWireframe(float radius = 0.5f, float verticalOffset = 0, int rings = 32, int sectors = 32, bool isHalf = false)
        {
            List<VertexPN> vertices = new List<VertexPN>();
            List<uint> edges = new List<uint>();
            GenerateSphereVertEdgeWireframe(radius, rings, sectors, isHalf, verticalOffset, 0, ref vertices, ref edges);
            return new Mesh<VertexPN>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        public static Mesh<VertexPN> GenerateSphereMillMesh(float radius = 0.5f, float length = 3f, int divLen = 5, int divAngle = 32, int rings = 32, int sectors = 33)
        {
            List<VertexPN> vertices = new List<VertexPN>();
            List<uint> edges = new List<uint>();
            GenerateCylinderVertEdge(radius, radius, length - radius, divLen, divAngle, ref vertices, ref edges);
            GenerateSphereVertEdge(radius, rings, sectors, true, radius, vertices.Count, ref vertices, ref edges);
            return new Mesh<VertexPN>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }
    }
}
