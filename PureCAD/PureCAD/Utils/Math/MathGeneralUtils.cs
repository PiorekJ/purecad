﻿using System;
using System.Collections.Generic;
using System.Windows.Navigation;
using OpenTK;

namespace PureCAD.Utils.Math
{
    public static class MathGeneralUtils
    {
        public static List<Vector2> Bresenham2D(int x1, int y1, int x2, int y2)
        {
            var retEnumerable = new List<Vector2>();
            int w = x2 - x1;
            int h = y2 - y1;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = System.Math.Abs(w);
            int shortest = System.Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = System.Math.Abs(h);
                shortest = System.Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                retEnumerable.Add(new Vector2(x1, y1));
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x1 += dx1;
                    y1 += dy1;
                }
                else
                {
                    x1 += dx2;
                    y1 += dy2;
                }
            }
            return retEnumerable;
        }

        public static Matrix4 CreatePerspectiveFOVMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {

            if (fov <= 0 || fov > System.Math.PI)
                throw new ArgumentOutOfRangeException("Incorrect fov");
            if (aspectRatio <= 0)
                throw new ArgumentOutOfRangeException("Incorrect aspect ratio");
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("Incorrect zNear");

            float yMax = zNear * (float)System.Math.Tan(0.5f * fov);
            float yMin = -yMax;
            float xMin = yMin * aspectRatio;
            float xMax = yMax * aspectRatio;

            return CreatePerspectiveOffCenter(xMin, xMax, yMin, yMax, zNear, zFar);
        }

        public static Matrix4 CreatePerspectiveOffCenter(float left, float right, float bottom, float top, float zNear, float zFar)
        {
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("zNear");

            float x = (2.0f * zNear) / (right - left);
            float y = (2.0f * zNear) / (top - bottom);
            float a = (right + left) / (right - left);
            float b = (top + bottom) / (top - bottom);
            float c = -(zFar + zNear) / (zFar - zNear);
            float d = -(2.0f * zFar * zNear) / (zFar - zNear);

            return new Matrix4(x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
        }

        public static Matrix4 CreateTranslation(Vector3 position)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row3 = new Vector4(position.X, position.Y, position.Z, 1.0f);
            return result;
        }

        public static Matrix4 CreateFromQuaternion(Quaternion rotation)
        {
            rotation.ToAxisAngle(out var axis, out var angle);

            float cos = (float)System.Math.Cos(-angle);
            float sin = (float)System.Math.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4 result = Matrix4.Identity;
            result.Row0 = new Vector4(t * axis.X * axis.X + cos, t * axis.X * axis.Y - sin * axis.Z, t * axis.X * axis.Z + sin * axis.Y, 0.0f);
            result.Row1 = new Vector4(t * axis.X * axis.Y + sin * axis.Z, t * axis.Y * axis.Y + cos, t * axis.Y * axis.Z - sin * axis.X, 0.0f);
            result.Row2 = new Vector4(t * axis.X * axis.Z - sin * axis.Y, t * axis.Y * axis.Z + sin * axis.X, t * axis.Z * axis.Z + cos, 0.0f);
            result.Row3 = Vector4.UnitW;
            return result;
        }

        public static Matrix4 CreateScale(Vector3 scale)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row0.X = scale.X;
            result.Row1.Y = scale.Y;
            result.Row2.Z = scale.Z;
            return result;
        }

        public static bool SolveEquationSystem(Vector3 g, Vector3 c, Vector3 b, out Vector2 solution)
        {
            solution = Vector2.One * 0.5f;

            if (Solve2Eq(g.Xy, c.Xy, b.Xy, out solution))
                return true;
            if (Solve2Eq(g.Xz, c.Xz, b.Xz, out solution))
                return true;
            if (Solve2Eq(g.Yz, c.Yz, b.Yz, out solution))
                return true;

            return false;
        }

        private static bool Solve2Eq(Vector2 g, Vector2 c, Vector2 b, out Vector2 solution)
        {
            float W = g.X * c.Y - c.X * g.Y;

            solution = Vector2.Zero;

            if (System.Math.Abs(W) < 0.1f)
                return false;

            float Wx = b.X * c.Y - c.X * b.Y;
            float Wy = g.X * b.Y - b.X * g.Y;

            solution = new Vector2(Wx / W, Wy / W);
            return true;
        }

        public static float Lerp(float v0, float v1, float t)
        {
            return (1 - t) * v0 + t * v1;
        }

        public static Vector2d IntersectLines(double A1, double B1, double C1, double A2, double B2, double C2)
        {
            double delta = A1 * B2 - A2 * B1;

            if (System.Math.Abs(delta) < Constants.Epsilon)
                throw new ArgumentException("Lines are parallel");

            double x = (B2 * C1 - B1 * C2) / delta;
            double y = (A1 * C2 - A2 * C1) / delta;
            return new Vector2d(x,y);
        }

        public static Vector3 IntersectLines(Vector3 fromP1, Vector3 toP1, Vector3 fromP2, Vector3 toP2)
        {
                Vector3 s = toP2 - fromP2;
                Vector3 r = toP1 - fromP1;

                float t = Det2D(fromP2 - fromP1, s) / Det2D(r, s);

                return fromP1 + t * r;
        }

        private static float Det2D(Vector3 v1, Vector3 v2)
        {
            return v1.X * v2.Z - v1.Z * v2.X;
        }
    }
}
