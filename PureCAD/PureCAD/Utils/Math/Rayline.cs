﻿using OpenTK;

namespace PureCAD.Utils.Math
{
    public class Rayline
    {
        public Vector3 Origin;
        public Vector3 Direction;

        public Rayline(Vector3 origin, Vector3 direction)
        {
            Origin = origin;
            Direction = direction;
        }
    }
}
