﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;

namespace PureCAD.Utils.Math
{
    public static class IntersectionMath
    {
        public static Vector2d CurstorSimpleDescent(IIntersectable surface, Vector3 cursorWorldPosition)
        {
            var distU = surface.GetURange() / (100 * surface.GetURange());
            var distV = surface.GetVRange() / (100 * surface.GetVRange());

            var result = Vector2d.Zero;
            var dist = double.MaxValue;

            for (double i = 0; i < surface.GetURange(); i+=distU)
            {
                for (double j = 0; j < surface.GetVRange(); j += distV)
                {
                    var pointUV = new Vector2d(i,j);

                    var scenePoint = surface.EvaluateAt(pointUV);
                    var currDist = (scenePoint - cursorWorldPosition.ToVector3d()).Length;

                    if (currDist < dist)
                    {
                        dist = currDist;
                        result = pointUV;
                    }
                }
            }

            return result;
        }

        public static Vector2d CurstorSimpleDescentDistance(IIntersectable surface, Vector3 cursorWorldPosition, Vector2d referenceUV, double minDistance)
        {
            var distU = surface.GetURange() / (100 * surface.GetURange());
            var distV = surface.GetVRange() / (100 * surface.GetVRange());

            var result = Vector2d.Zero;
            var dist = double.MaxValue;
            //var borderDist = (new Vector2d(surface.GetURange(), surface.GetVRange()) - referenceUV).Length;
            //if (minDistance > borderDist)
            //    minDistance = borderDist;

            for (double i = 0; i <= surface.GetURange(); i += distU)
            {
                for (double j = 0; j <= surface.GetVRange(); j += distV)
                {
                    var pointUV = new Vector2d(i, j);

                    if((pointUV - referenceUV).Length < minDistance)
                        continue;
                    
                    var scenePoint = surface.EvaluateAt(pointUV);
                    var currDist = (scenePoint - cursorWorldPosition.ToVector3d()).Length;

                    if (currDist < dist)
                    {
                        dist = currDist;
                        result = pointUV;
                    }
                }
            }

            return result;
        }

        public static Vector2d CursorGradientDescent(IIntersectable surface, Vector3 cursorWorldPosition)
        {
            var pointUV = surface.ApproximateAt(cursorWorldPosition);
            var scenePoint = surface.EvaluateAt(pointUV);
            var gradientStep = Simulation.Settings.GradientStep;
            var currDist = double.PositiveInfinity;
            var cursorPosD = cursorWorldPosition.ToVector3d();
            var wrapResults = new WrapValuesUV();
            int maxIterations = Simulation.Settings.GradientMaxIterations;
            int currIter = 0;
            do
            {
                var constant = 2.0 * (scenePoint - cursorPosD);

                var uDeriv = constant * surface.EvaluateUDerivativeAt(pointUV);
                var vDeriv = constant * surface.EvaluateVDerivativeAt(pointUV);

                var antiGradient = new Vector2d(uDeriv.X + uDeriv.Y + uDeriv.Z, vDeriv.X + vDeriv.Y + vDeriv.Z) * -1.0;

                var calc = pointUV + gradientStep * antiGradient;

                wrapResults.ParamUResult = WrapParam(surface.GetURange(), surface.IsUWrapped, ref calc.X);
                wrapResults.ParamVResult = WrapParam(surface.GetVRange(), surface.IsVWrapped, ref calc.Y);

                if (wrapResults.IsParamOutside())
                    break;

                var nextPoint = surface.EvaluateAt(calc);

                var dist = (nextPoint - scenePoint).LengthSquared;

                if (dist > currDist)
                {
                    gradientStep *= 0.5;
                    if (gradientStep < Simulation.Settings.GradientBreakCondition)
                        break;
                }
                else
                {
                    currDist = dist;
                    pointUV = calc;
                    scenePoint = nextPoint;
                }
                currIter++;

                if (currIter >= maxIterations)
                    throw new Exception("Number of iterations exceeded");

            } while (currDist > Simulation.Settings.GradientBreakCondition * Simulation.Settings.GradientBreakCondition || currIter > 10000);

            return pointUV;
        }

        public static Vector4d SurfacePointsGradientDescent(IIntersectable surface1, IIntersectable surface2, Vector4d pointParams)
        {
            var gradientStep = Simulation.Settings.GradientStep;
            var currDiff = double.PositiveInfinity;
            var wrapResults = new WrapValuesUVST();
            int currIter = 0;
            do
            {
                var sceneP1 = surface1.EvaluateAt(pointParams.Xy);
                var sceneP2 = surface2.EvaluateAt(pointParams.Zw);

                var constantP1 = 2.0 * (sceneP1 - sceneP2);
                var constantP2 = 2.0 * (sceneP2 - sceneP1);

                var uDeriv = constantP1 * surface1.EvaluateUDerivativeAt(pointParams.Xy);
                var vDeriv = constantP1 * surface1.EvaluateVDerivativeAt(pointParams.Xy);

                var sDeriv = constantP2 * surface2.EvaluateUDerivativeAt(pointParams.Zw);
                var tDeriv = constantP2 * surface2.EvaluateVDerivativeAt(pointParams.Zw);

                var antiGradient = new Vector4d(uDeriv.X + uDeriv.Y + uDeriv.Z, vDeriv.X + vDeriv.Y + vDeriv.Z, sDeriv.X + sDeriv.Y + sDeriv.Z, tDeriv.X + tDeriv.Y + tDeriv.Z) * -1.0;

                var calc = pointParams + gradientStep * antiGradient;

                //wrapResults.ParamUResult = WrapParam(surface1.GetURange(), surface1.IsUWrapped, ref calc.X);
                //wrapResults.ParamVResult = WrapParam(surface1.GetVRange(), surface1.IsVWrapped, ref calc.Y);
                //wrapResults.ParamSResult = WrapParam(surface2.GetURange(), surface2.IsUWrapped, ref calc.Z);
                //wrapResults.ParamTResult = WrapParam(surface2.GetVRange(), surface2.IsVWrapped, ref calc.W);

                //if (wrapResults.IsParamOutside())
                //    break;

                var diff = (calc - pointParams).LengthSquared;

                if (diff >= currDiff)
                {
                    gradientStep *= 0.5;
                }
                else
                {
                    currDiff = diff;
                    pointParams = calc;
                }
                currIter++;

                if (currIter >= Simulation.Settings.GradientMaxIterations)
                    throw new Exception("Number of iterations exceeded");

            } while (currDiff > Simulation.Settings.GradientBreakCondition * Simulation.Settings.GradientBreakCondition);

            return pointParams;
        }

        public static Vector4d NewtonMethod(IIntersectable surfaceP, IIntersectable surfaceQ, Vector4d pointParams, double step, out WrapValuesUVST wrapValuesUVST)
        {
            var maxIterations = Simulation.Settings.NewtonMMaxIterations;
            var currIter = 0;
            var calc = pointParams;
            wrapValuesUVST = new WrapValuesUVST();
            var dist = Double.PositiveInfinity;
            var pointP = surfaceP.EvaluateAt(calc.Xy);
            var pointQ = surfaceQ.EvaluateAt(calc.Zw);
            var startPoint = pointP;
            while (dist > Simulation.Settings.NewtonBreakCondition * Simulation.Settings.NewtonBreakCondition && currIter < maxIterations)
            {
                var derivU = surfaceP.EvaluateUDerivativeAt(calc.Xy);
                var derivV = surfaceP.EvaluateVDerivativeAt(calc.Xy);

                var derivS = surfaceQ.EvaluateUDerivativeAt(calc.Zw);
                var derivT = surfaceQ.EvaluateVDerivativeAt(calc.Zw);

                var normalP = Vector3d.Cross(derivU, derivV);
                var normalQ = Vector3d.Cross(derivS, derivT);

                var versor = Vector3d.Divide(Vector3d.Cross(normalP, normalQ), Vector3d.Cross(normalP, normalQ).Length);

                var jacobiMatrix = GetJacobiMatrix(derivU, derivV, derivS, derivT, versor);

                var jacobiMatrixInv = jacobiMatrix.Inverted();

                var function = new Vector4d(pointP - pointQ, Vector3d.Dot(pointP - startPoint, versor) - step);
                var next = calc - Vector4d.Transform(function, jacobiMatrixInv);
                calc = next;

                #region BreakCond

                if (double.IsNaN(calc.X) || double.IsNaN(calc.Y) || double.IsNaN(calc.Z) || double.IsNaN(calc.W))
                {
                    var xx = Constants.Epsilon;
                }

                wrapValuesUVST.ParamUResult = WrapParam(surfaceP.GetURange(), surfaceP.IsUWrapped, ref calc.X);
                wrapValuesUVST.ParamVResult = WrapParam(surfaceP.GetVRange(), surfaceP.IsVWrapped, ref calc.Y);
                wrapValuesUVST.ParamSResult = WrapParam(surfaceQ.GetURange(), surfaceQ.IsUWrapped, ref calc.Z);
                wrapValuesUVST.ParamTResult = WrapParam(surfaceQ.GetVRange(), surfaceQ.IsVWrapped, ref calc.W);

                if (wrapValuesUVST.IsParamOutside())
                    break;
                
                #endregion
                pointP = surfaceP.EvaluateAt(calc.Xy);
                pointQ = surfaceQ.EvaluateAt(calc.Zw);

                dist = (pointP - pointQ).LengthSquared;
                ++currIter;

            }
            if (currIter >= maxIterations)
                throw new Exception("Number of iterations exceeded");

            return calc;
        }

        private static Matrix4d GetJacobiMatrix(Vector3d derivU, Vector3d derivV, Vector3d derivS, Vector3d derivT, Vector3d versor)
        {
            var row0 = new Vector4d(derivU.X, derivU.Y, derivU.Z, Vector3d.Dot(derivU, versor));
            var row1 = new Vector4d(derivV.X, derivV.Y, derivV.Z, Vector3d.Dot(derivV, versor));
            var row2 = new Vector4d(-derivS.X, -derivS.Y, -derivS.Z, 0);
            var row3 = new Vector4d(-derivT.X, -derivT.Y, -derivT.Z, 0);
            return new Matrix4d(row0, row1, row2, row3);
        }

        [Flags]
        public enum WrapEnum
        {
            Null = 0,
            ClampedMin = 1 << 0,
            ClampedMax = 1 << 1,
            WrappedMin = 1 << 2,
            WrappedMax = 1 << 3,
            Clamped = ClampedMin | ClampedMax,
            Wrapped = WrappedMin | WrappedMax
        }

        public class WrapValuesUV
        {
            public WrapEnum ParamUResult = WrapEnum.Null;
            public WrapEnum ParamVResult = WrapEnum.Null;

            public bool IsParamOutside()
            {
                return ((ParamUResult | ParamVResult) & WrapEnum.Clamped) != WrapEnum.Null;
            }
        }

        public class WrapValuesUVST
        {
            public WrapEnum ParamUResult = WrapEnum.Null;
            public WrapEnum ParamVResult = WrapEnum.Null;
            public WrapEnum ParamSResult = WrapEnum.Null;
            public WrapEnum ParamTResult = WrapEnum.Null;
            
            public bool IsParamOutside()
            {
                return ((ParamUResult | ParamVResult | ParamSResult | ParamTResult) & WrapEnum.Clamped) != WrapEnum.Null;
            }
        }

        public static WrapEnum WrapParam(double maxParam, bool isWrapped, ref double param)
        {
            if (param < 0)
            {
                if (isWrapped)
                {
                    do
                    {
                        param += maxParam;
                    } while (param < 0);
                    return WrapEnum.WrappedMin;
                }
                param = 0;
                return WrapEnum.ClampedMin;
            }
            if (param > maxParam)
            {
                if (isWrapped)
                {
                    do
                    {
                        param -= maxParam;
                    } while (param > maxParam);
                    return WrapEnum.WrappedMax;
                }
                param = maxParam;
                return WrapEnum.ClampedMax;
            }

            return WrapEnum.Null;
        }

        public class LineSegment
        {
            public Vector2d LineStart;
            public Vector2d LineEnd;

            public LineSegment(Vector2d start, Vector2d end)
            {
                LineStart = start;
                LineEnd = end;
            }
        }

        public static bool CheckLineIntersection(LineSegment uv, LineSegment curve)
        {
            var p1 = uv.LineStart;
            var p2 = uv.LineEnd;

            var start = curve.LineStart;
            var end = curve.LineEnd;

            double p1Site = (end.X - start.X) * (p1.Y - start.Y) - (p1.X - start.X) * (end.Y - start.Y);
            double p2Site = (end.X - start.X) * (p2.Y - start.Y) - (p2.X - start.X) * (end.Y - start.Y);

            if (p1Site > 0 && p2Site > 0)
                return false;

            if (p1Site < 0 && p2Site < 0)
                return false;

            if (p1Site != 0 && p2Site == 0)
                return false;

            if (p1Site == 0 && p2Site != 0)
                return false;

            if (p1Site == 0 && p2Site == 0)
                return false;

            return true;
        }

        public static Vector2d GetLineIntersection(LineSegment uv, LineSegment curve)
        {
            Vector2d d1 = new Vector2d(uv.LineEnd.X - uv.LineStart.X, uv.LineEnd.Y - uv.LineStart.Y);
            Vector2d d2 = new Vector2d(curve.LineEnd.X - curve.LineStart.X, curve.LineEnd.Y - curve.LineStart.Y);

            double dot = (d1.X * d2.Y) - (d1.Y * d2.X);

            Vector2d c = new Vector2d(curve.LineStart.X - uv.LineStart.X, curve.LineStart.Y - uv.LineStart.Y);

            double t = (c.X * d2.Y - c.Y * d2.X) / dot;

            return new Vector2d(uv.LineStart.X + (t * d1.X), uv.LineStart.Y + (t * d1.Y));
        }

    }
}
