﻿using OpenTK;

namespace PureCAD.Utils.Math
{
    public class Plane
    {
        public Vector3 Origin;
        public Vector3 Normal;

        public Plane(Vector3 origin, Vector3 normal)
        {
            Origin = origin;
            Normal = normal;
        }

        public Plane()
        {

        }

        public void CalculateNormal(Vector3 direction, Vector3 axis)
        {
            Vector3 uprightPlaneVector = Vector3.Cross(direction, axis);
            Normal = Vector3.Cross(uprightPlaneVector, axis).Normalized();
        }

        public Vector3 CalculateRayIntersetion(Rayline rayline)
        {
            float normalDirDot = Vector3.Dot(rayline.Direction, Normal);
            if (normalDirDot < 0.00001f && normalDirDot > -0.00001f)
                return Vector3.Zero; //TODO: CHANGE TO SOMETHING ELSE
            Vector3 paramterC = rayline.Origin - Origin;
            float paramterT = -(Vector3.Dot(paramterC, Normal) / normalDirDot);
            if (paramterT < 0)
                return Vector3.Zero;

            return rayline.Origin + paramterT * rayline.Direction;
        }
    }
}
