﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;

namespace PureCAD.Utils.UIExtensionClasses
{
    public class BetterCanvasBitmap
    {
        private Int32Rect Source { get; set; }

        public WriteableBitmap WriteableBitmap { get; private set; }

        public int Height { get; private set; }
        public int Width { get; private set; }

        private Int32[] Background { get; set; }

        public BetterCanvasBitmap(int width, int height)
        {
            Width = width;
            Height = height;

            WriteableBitmap = new WriteableBitmap(this.Width, this.Height, 5000, 5000, PixelFormats.Bgra32, null);
            Background = new Int32[4 * this.Width * this.Height];
            Source = new Int32Rect(0, 0, this.Width, this.Height);

            Color color = Color.White;

            int colorARGB = color.ToArgb();

            for (int i = 0; i < Background.Length / 4; i++)
                Background[i] = colorARGB;

            Reset();
        }

        public Color GetPixel(int x, int y)
        {
            Color resultColor = Color.White;

            if (x >= this.Width || x < 0 || y >= this.Height || y < 0) return resultColor;

            unsafe
            {
                Int32* pbuff = (Int32*)WriteableBitmap.BackBuffer.ToPointer();
                resultColor = Color.FromArgb(pbuff[y * Width + x]);
            }

            return resultColor;
        }

        public Int32 GetPixelARGB(int x, int y)
        {
            int resultARGB = -1;

            if (x >= this.Width || x < 0 || y >= this.Height || y < 0) return resultARGB;

            unsafe
            {
                Int32* pbuff = (Int32*)WriteableBitmap.BackBuffer.ToPointer();
                resultARGB = pbuff[y * Width + x];
            }

            return resultARGB;
        }

        public void SetPixel(int x, int y, Color color)
        {
            if (x >= this.Width || x < 0 || y >= this.Height || y < 0) return;

            unsafe
            {
                Int32* pbuff = (Int32*)WriteableBitmap.BackBuffer.ToPointer();
                pbuff[y * Width + x] = color.ToArgb();
            }
        }

        public void Reset()
        {
            WriteableBitmap.WritePixels(Source, Background, 4 * this.Width, 0);
        }
    }
}
