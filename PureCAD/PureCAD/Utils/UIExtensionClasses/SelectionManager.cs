﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OpenTK;
using PureCAD.Components;
using PureCAD.Core;
using PureCAD.Models;
using InputManager = PureCAD.Core.InputManager;

namespace PureCAD.Utils.UIExtensionClasses
{
    public class SelectionManager : BindableObject
    {
        private List<SceneObject> _selectedSceneObjects;
        private SceneObject _selectedSceneObject;
        private SceneObject _storedSceneObject;
        private SceneObject _temporarySceneObject;
        private ListView _selectionControl;

        public SceneObject SelectedSceneObject
        {
            get => _selectedSceneObject;
            set
            {
                _selectedSceneObject = value;
                if (value is ISelectable)
                {
                    ((ISelectable)_selectedSceneObject).IsSelected = true;
                    foreach (SceneObject child in _selectedSceneObject.Transform.Children)
                    {
                        if (child is ISelectable selectedChild)
                            selectedChild.IsSelected = true;
                    }
                }
                if (_selectedSceneObject != null && Simulation.Settings.MoveCursorToSelection)
                    Simulation.Scene.SceneCursor.Transform.Position = _selectedSceneObject.Transform.Position;
                RaisePropertyChanged();
            }
        }

        public void BindSelectionControl(ListView control)
        {
            _selectionControl = control;
        }

        public SelectionManager()
        {
            _selectedSceneObjects = new List<SceneObject>();
            InputManager.RegisterOnKeyDownEvent(Key.C, () =>
            {
                if (!Simulation.Storage.CursorMode)
                {
                    if (SelectedSceneObject != null)
                    {
                        _storedSceneObject = SelectedSceneObject;
                        _storedSceneObject.Transform.InterruptMovement();
                    }
                    MarkObjectsAsDeselected();
                    SelectedSceneObject = Simulation.Scene.SceneCursor;
                    Simulation.Storage.CursorMode = true;
                }
                else
                {
                    Simulation.Scene.SceneCursor.ResetCursorGrabStatus();
                    Simulation.Storage.CursorMode = false;
                    SelectedSceneObject = _storedSceneObject;
                    if (_temporarySceneObject != null && SelectedSceneObject == _temporarySceneObject)
                    {
                        foreach (SceneObject sceneObject in _temporarySceneObject.Transform.Children)
                        {
                            if (sceneObject is ISelectable selectable)
                                selectable.IsSelected = true;
                        }
                        SelectedSceneObject = GetMultipleSelection(_selectedSceneObjects);
                    }
                }
            });
            InputManager.RegisterOnKeyDownEvent(Key.Delete, () =>
            {
                if (SelectedSceneObject != null && SelectedSceneObject != Simulation.Scene.SceneCursor)
                {
                    if (SelectedSceneObject == _temporarySceneObject)
                    {
                        if (_selectedSceneObjects.All(x => x is IVirtual))
                            return;

                        foreach (SceneObject sceneObject in _selectedSceneObjects)
                        {
                            if (sceneObject is IVirtual virt || !sceneObject.IsDeletable)
                                continue;

                            Simulation.Scene.RemoveSceneObject(sceneObject);
                        }

                        Simulation.Scene.RemoveSceneObject(SelectedSceneObject);
                        return;
                    }

                    if (SelectedSceneObject is IVirtual v || !SelectedSceneObject.IsDeletable)
                        return;

                    foreach (SceneObject child in SelectedSceneObject.Transform.Children)
                    {
                        if (child is ISelectable selectable && selectable.IsSelected)
                            selectable.IsSelected = false;
                    }
                    Simulation.Scene.RemoveSceneObject(SelectedSceneObject);
                }
            });
            //InputManager.RegisterOnButtonDownEvent(MouseButton.Left, () =>
            //{
            //    if (!Simulation.Storage.CursorMode)
            //    {
            //        if (SelectedSceneObject == null ||
            //            !SelectedSceneObject.Transform.IsMoving && SelectedSceneObject.IsVisible)
            //        {
            //            var point = GetClosestPoint(InputManager.MousePosition);
            //            if (point != null)
            //            {
            //                if (!point.IsVisible)
            //                    return;
            //                if ((Keyboard.Modifiers & ModifierKeys.Control) != 0)
            //                {
            //                    var points = new List<SceneObject>();
            //                    if (SelectedSceneObject == _temporarySceneObject)
            //                        points.AddRange(SelectedSceneObject.Transform.Children);
            //                    else
            //                        points.Add(SelectedSceneObject);

            //                    points.Add(point);
            //                    SelectedSceneObject = GetMultipleSelection(points);
            //                    foreach (SceneObject sceneObject in points)
            //                    {
            //                        if (!_selectionControl.SelectedItems.Contains(sceneObject))
            //                        {
            //                            _selectionControl.SelectedItems.Add(sceneObject);
            //                        }
            //                    }
            //                    return;
            //                }
            //                MarkObjectsAsDeselected();
            //                SelectedSceneObject = point;
            //            }
            //        }
            //    }
            //});

            InputManager.RegisterOnButtonDownEvent(MouseButton.Left, () =>
            {
                if (!Simulation.Storage.CursorMode)
                {
                    if (SelectedSceneObject == null ||
                        !SelectedSceneObject.Transform.IsMoving && SelectedSceneObject.IsVisible)
                    {
                        _storedMousePosition = InputManager.MousePosition;
                        Simulation.Scene.SceneSelectionRectangle.StartDrawing(_storedMousePosition.ToScreenSpace());
                    }
                }
            });

            InputManager.RegisterOnButtonUpEvent(MouseButton.Left, () =>
            {
                if (!Simulation.Storage.CursorMode)
                {
                    if (SelectedSceneObject == null ||
                        !SelectedSceneObject.Transform.IsMoving && SelectedSceneObject.IsVisible)
                    {
                        Simulation.Scene.SceneSelectionRectangle.StopDrawing();
                        var list = GetPointsInSelectionRectangle(InputManager.MousePosition - _storedMousePosition);
                        if (list.Count > 0)
                        {
                            if ((Keyboard.Modifiers & ModifierKeys.Control) != 0)
                            {
                                var points = new List<ScenePoint>();
                                if (SelectedSceneObject == _temporarySceneObject)
                                    points.AddRange(SelectedSceneObject.Transform.Children.OfType<ScenePoint>());
                                else if (SelectedSceneObject is ScenePoint scenePoint)
                                    points.Add(scenePoint);


                                points.AddRange(list);
                                SelectedSceneObject = GetMultipleSelection(points);
                                _selectionControl.SelectedItems.Clear();
                                foreach (ScenePoint sceneObject in points)
                                {
                                    _selectionControl.SelectedItems.Add(sceneObject);
                                }
                                return;
                            }
                            MarkObjectsAsDeselected();
                            SelectedSceneObject = GetMultipleSelection(list);
                            _selectionControl.SelectedItems.Clear();
                            foreach (ScenePoint sceneObject in list)
                            {
                                _selectionControl.SelectedItems.Add(sceneObject);
                            }
                        }
                    }
                }
            });

            InputManager.RegisterOnKeyDownEvent(Key.M, () =>
            {
                if (!Simulation.Storage.CursorMode)
                {
                    if (SelectedSceneObject == _temporarySceneObject && !SelectedSceneObject.Transform.IsMoving)
                    {
                        MergePoints();
                        //foreach (SceneObject child in SelectedSceneObject.Transform.Children)
                        //{
                        //    if (child is ScenePoint point)
                        //    {
                        //        point.Transform.Position = SelectedSceneObject.Transform.Position;
                        //    }
                        //}
                    }
                }
            });
        }

        private Vector2 _storedMousePosition;

        public SceneObject GetStoredObject()
        {
            return _storedSceneObject;
        }

        private List<ScenePoint> GetPointsInSelectionRectangle(Vector2 selectionSize)
        {
            var pointList = new List<ScenePoint>();
            foreach (SceneObject sceneObject in Simulation.Scene.SceneObjects)
            {
                if (sceneObject is ScenePoint scenePoint)
                {
                    Vector4 screenPosition4 = Vector4.UnitW * sceneObject.Transform.GetModelMatrix() *
                                              Simulation.Scene.Camera.GetViewMatrix() *
                                              Simulation.Scene.Camera.GetProjectionMatrix();
                    if (System.Math.Abs(screenPosition4.W) < Constants.Epsilon)
                        continue;
                    Vector3 screenPosition3 = new Vector3(screenPosition4.X / screenPosition4.W,
                        screenPosition4.Y / screenPosition4.W, screenPosition4.Z / screenPosition4.W);

                    screenPosition3 = new Vector3(((screenPosition3.X + 1) / 2) * Simulation.Storage.DisplayWidth,
                        ((-screenPosition3.Y + 1) / 2) * Simulation.Storage.DisplayHeight, screenPosition3.Z);

                    if (IsPointInsideRectangle(_storedMousePosition, selectionSize, screenPosition3) &&
                        scenePoint.IsVisible)
                    {
                        pointList.Add(scenePoint);
                    }
                }
            }
            return pointList;
        }

        private bool IsPointInsideRectangle(Vector2 rectangleStart, Vector2 rectangleSize, Vector3 pointPosition)
        {
            return pointPosition.X > rectangleStart.X && pointPosition.X < rectangleStart.X + rectangleSize.X &&
                   pointPosition.Y > rectangleStart.Y && pointPosition.Y < rectangleStart.Y + rectangleSize.Y;
        }

        private ScenePoint GetClosestPoint(Vector2 mousePosition)
        {
            double dist = Double.PositiveInfinity;
            ScenePoint point = null;
            foreach (SceneObject sceneObject in Simulation.Scene.SceneObjects)
            {
                if (sceneObject is ScenePoint scenePoint)
                {
                    Vector4 screenPosition4 = Vector4.UnitW * sceneObject.Transform.GetModelMatrix() *
                                              Simulation.Scene.Camera.GetViewMatrix() *
                                              Simulation.Scene.Camera.GetProjectionMatrix();
                    if (System.Math.Abs(screenPosition4.W) < Constants.Epsilon)
                        continue;
                    Vector3 screenPosition3 = new Vector3(screenPosition4.X / screenPosition4.W,
                        screenPosition4.Y / screenPosition4.W, screenPosition4.Z / screenPosition4.W);

                    screenPosition3 = new Vector3(((screenPosition3.X + 1) / 2) * Simulation.Storage.DisplayWidth,
                        ((-screenPosition3.Y + 1) / 2) * Simulation.Storage.DisplayHeight, screenPosition3.Z);

                    if (System.Math.Abs(mousePosition.X - screenPosition3.X) < 10 &&
                        System.Math.Abs(mousePosition.Y - screenPosition3.Y) < 10)
                    {
                        if (dist > screenPosition3.Z)
                        {
                            point = scenePoint;
                            dist = screenPosition3.Z;
                        }
                    }
                }
            }
            return point;
        }

        public void MarkObjectsAsDeselected()
        {
            foreach (SceneObject sceneObject in _selectedSceneObjects)
            {
                if (sceneObject is ISelectable selectable)
                {
                    selectable.IsSelected = false;
                    foreach (SceneObject child in sceneObject.Transform.Children)
                    {
                        if (child is ISelectable selectedChild)
                            selectedChild.IsSelected = false;
                    }
                    if (sceneObject.Transform.IsMoving)
                        sceneObject.Transform.InterruptMovement();
                }
            }
            if (_temporarySceneObject != null)
            {
                Simulation.Scene.RemoveSceneObject(_temporarySceneObject);
            }
        }

        public void SelectObjects(IEnumerable<SceneObject> objects)
        {
            MarkObjectsAsDeselected();

            _selectedSceneObjects = objects.ToList();

            if (_selectedSceneObjects.Count == 0)
            {
                SelectedSceneObject = null;
                return;
            }

            if (_selectedSceneObjects.Count > 1)
            {
                SelectedSceneObject = GetMultipleSelection(_selectedSceneObjects);
                return;
            }

            if (_selectedSceneObjects[0] is ISelectable)
                SelectedSceneObject = _selectedSceneObjects[0];
            else
                SelectedSceneObject = null;
        }

        public void DeleteSelectedObjects()
        {
            if (_selectedSceneObjects.All(x => x is IVirtual))
                return;

            foreach (SceneObject sceneObject in _selectedSceneObjects)
            {
                if (sceneObject is IVirtual virt || !sceneObject.IsDeletable)
                    continue;

                Simulation.Scene.RemoveSceneObject(sceneObject);
            }

            Simulation.Scene.RemoveSceneObject(SelectedSceneObject);
        }

        private SceneObject GetMultipleSelection(IEnumerable<SceneObject> collection)
        {
            var average = Vector3.Zero;

            if (_temporarySceneObject == null)
            {
                _temporarySceneObject = new SceneObject { IsVisibleOnList = false };
            }
            else
            {
                _temporarySceneObject.Transform.ForceRemoveAllChildren();
                _temporarySceneObject.Transform.IsProxy = false;
            }
            var col = collection.ToList();
            foreach (SceneObject sceneObject in col)
            {
                if (sceneObject is ISelectable selectable)
                {
                    var transform = sceneObject.GetComponent<Transform>();
                    if (transform != null)
                    {
                        average += transform.Position;
                    }
                    selectable.IsSelected = true;
                    _temporarySceneObject.Transform.ForceAddChild(sceneObject);
                }
            }
            _temporarySceneObject.Transform.OnScaleUpdate -= SelectionScale;
            _temporarySceneObject.Transform.OnRotationUpdate -= SelectionRotation;
            _temporarySceneObject.Transform.Scale = Vector3.One;
            _temporarySceneObject.Transform.Rotation = Quaternion.Identity;
            _temporarySceneObject.Transform.Position = average / col.Count;
            _temporarySceneObject.Transform.OnRotationUpdate += SelectionRotation;
            _temporarySceneObject.Transform.OnScaleUpdate += SelectionScale;
            _temporarySceneObject.Transform.IsProxy = true;
            return _temporarySceneObject;
        }

        private void SelectionRotation(SceneObject owner, Quaternion oldRotation, Quaternion newRotation)
        {
            var modelMtx = _temporarySceneObject.Transform.GetModelMatrix();
            foreach (SceneObject child in _temporarySceneObject.Transform.Children)
            {
                var vectorToCenter = child.Transform.Position - _temporarySceneObject.Transform.Position;

                vectorToCenter = Vector3.TransformPosition(vectorToCenter, modelMtx.Inverted());
                vectorToCenter = newRotation.Normalized() * vectorToCenter;
                vectorToCenter = Vector3.TransformPosition(vectorToCenter, modelMtx);

                child.Transform.Position = vectorToCenter;
            }
        }

        private void SelectionScale(SceneObject owner, Vector3 oldScale, Vector3 newScale)
        {
            foreach (SceneObject child in _temporarySceneObject.Transform.Children)
            {
                var vectorToCenter = Vector3.Zero;
                var change = 0.0f;
                var changeVector = Vector3.Zero;
                if (System.Math.Abs(newScale.X - oldScale.X) > 0)
                {
                    vectorToCenter = new Vector3(child.Transform.Position.X - _temporarySceneObject.Transform.Position.X, child.Transform.Position.Y, child.Transform.Position.Z);
                    change = newScale.X - oldScale.X;
                    changeVector = change * vectorToCenter;
                    child.Transform.Position += new Vector3(changeVector.X, 0, 0);

                }
                else if (System.Math.Abs(newScale.Y - oldScale.Y) > 0)
                {
                    vectorToCenter = new Vector3(child.Transform.Position.X, child.Transform.Position.Y - _temporarySceneObject.Transform.Position.Y, child.Transform.Position.Z);
                    change = newScale.Y - oldScale.Y;
                    changeVector = change * vectorToCenter;
                    child.Transform.Position += new Vector3(0, changeVector.Y, 0);
                }
                else if (System.Math.Abs(newScale.Z - oldScale.Z) > 0)
                {
                    vectorToCenter = new Vector3(child.Transform.Position.X, child.Transform.Position.Y, child.Transform.Position.Z - _temporarySceneObject.Transform.Position.Z);
                    change = newScale.Z - oldScale.Z;
                    changeVector = change * vectorToCenter;
                    child.Transform.Position += new Vector3(0, 0, changeVector.Z);
                }

            }
        }

        private void MergePoints()
        {
            var points = _temporarySceneObject.Transform.Children.OfType<ScenePoint>().ToList();
            if (points.Count == 2)
            {
                foreach (ScenePoint point in points)
                {
                    bool foundSurface = false;
                    foreach (SceneObject parent in point.Transform.Parents)
                    {
                        if (parent is SceneSurfaceSmooth surface)
                        {
                            if (foundSurface)
                            {
                                MessageBox.Show("Point has already been merged", "Merge error!", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            foundSurface = true;
                        }
                    }
                }

                var distinctTest = new List<SceneObject>();
                distinctTest.AddRange(points[0].Transform.Parents.OfType<SceneSurfaceSmooth>());
                distinctTest.AddRange(points[1].Transform.Parents.OfType<SceneSurfaceSmooth>());
                var distinct = distinctTest.Distinct();
                if (distinct.ToList().Count != 2)
                {
                    MessageBox.Show("Points belong to the same surface", "Merge error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                var newPoint = Simulation.Scene.AddSurfacePointBlank(_temporarySceneObject.Transform.Position);

                for (var i = points.Count - 1; i >= 0; --i)
                {
                    ScenePoint child = points[i];
                    foreach (SceneObject parent in child.Transform.Parents)
                    {
                        if (parent != _temporarySceneObject)
                        {
                            if (parent is SceneSurfaceC0Smooth surface)
                            {
                                surface.ReplacePoint(child, newPoint);
                            }
                            if (parent is SceneCurve curve)
                            {
                                //TODO: If curves required, add merge functionality
                            }
                        }
                    }
                    Simulation.Scene.RemoveSceneObject(child);
                }
            }
            else
            {
                MessageBox.Show("Select only two points for merging.", "Merge error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Dispose()
        {
            _temporarySceneObject?.Dispose();
        }
    }
}
