﻿using System;
using System.Windows.Media;
using OpenTK;

namespace PureCAD.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 7, 16);

        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 0.01f;
        public const float DefaultZFar = 150f;

        public const float CameraMovementMouseSensitivity = 0.25f;
        public const float CameraRotationMouseSensitivity = 0.025f;
        public const float CameraZoomMouseSensitivity = 0.25f;
        public const float CameraOrthoNear = 0f;
        public const float CameraOrthoFar = 4f;
        public const float CameraOrthoWidth = 15;
        public const float CameraOrthoHeight = 15;

        public const float CameraMovementKeyVelocity = 2.5f;
        public const float CameraMovementKeySlowVelocity = 0.25f;
        
        public const float Epsilon = 0.0000001f;
        public const float MinRefreshRate = 0.1f;
        public const int DefaultExplicitDivisor = 50;

        public const float DefaultEyeSeparation = 0.015f;
        public const float DefaultFocusDepth = 5.0f;

        public static Vector3 DefaultRedLeft = new Vector3(0.4561f, 0.500484f, 0.176381f);
        public static Vector3 DefaultGreenLeft = new Vector3(-0.0400822f, -0.0378246f, -0.0157589f);
        public static Vector3 DefaultBlueLeft = new Vector3(-0.0152161f, -0.0205971f, -0.00546856f);

        public static Vector3 DefaultRedRight = new Vector3(-0.0434706f, -0.0879388f, -0.00155529f);
        public static Vector3 DefaultGreenRight = new Vector3(0.378476f, 0.73364f, -0.0184503f);
        public static Vector3 DefaultBlueRight = new Vector3(-0.0721527f, -0.112961f, 1.2264f);

        public static readonly Color BackgroundColor = (Color)ColorConverter.ConvertFromString("#FF616161");
        public const float FileScaleFactor = 0.1f;
        public static readonly Vector3 SafeMillPosition = new Vector3(0, 15, 0);
        public const float DerivativeOffset = 0.001f;
    }
}
