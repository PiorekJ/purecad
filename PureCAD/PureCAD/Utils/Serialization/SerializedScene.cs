﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Utils.Serialization
{
    public class SerializedScene
    {
        public point[] points { get; set; }

        public curveC0[] curvesC0 { get; set; }
        public curveC2[] curvesC2 { get; set; }
        public curveC2I[] curvesC2I { get; set; }

        public surfaceC0[] surfacesC0 { get; set; }
        public surfaceC2[] surfacesC2 { get; set; }

        public torus[] toruses { get; set; }
    }
}
