﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Utils.Serialization
{
    public class point
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public string name { get; set; }
    }
}
