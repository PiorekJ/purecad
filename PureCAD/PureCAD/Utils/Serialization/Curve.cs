﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Utils.Serialization
{
    public struct curveC0
    {
        public int[] pointsId { get; set; }

        public string name { get; set; }
    }

    public struct curveC2
    {
        public int[] pointsId { get; set; }

        public string name { get; set; }
    }

    public struct curveC2I
    {
        public int[] pointsId { get; set; }

        public string name { get; set; }
    }
}
