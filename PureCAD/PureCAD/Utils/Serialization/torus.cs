﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Utils.Serialization
{
    public class torus
    {
        public torus()
        {
            center = new center();
            rotation = new rotation();
            scale = new scale();
        }

        public center center { get; set; }
        public rotation rotation { get; set; }
        public scale scale { get; set; }

        public float R { get; set; }
        public float r { get; set; }

        public int u { get; set; }
        public int v { get; set; }

        public string name { get; set; }
    }

    public class center
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class rotation
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class scale
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }
}
