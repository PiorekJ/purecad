﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Utils.Serialization
{
    public class surfaceC0
    {
        public int[,] points { get; set; }

        public int u { get; set; }
        public int v { get; set; }

        public int flakeU { get; set; }
        public int flakeV { get; set; }

        public bool cylinder { get; set; }

        public string name { get; set; }
    }

    public class surfaceC2
    {
        public int[,] points { get; set; }

        public int u { get; set; }
        public int v { get; set; }

        public int flakeU { get; set; }
        public int flakeV { get; set; }

        public bool cylinder { get; set; }

        public string name { get; set; }
    }
}
