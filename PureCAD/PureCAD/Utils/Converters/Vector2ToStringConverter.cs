﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using OpenTK;

namespace PureCAD.Utils.Converters
{
    class Vector2ToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String retValue = String.Empty;
            if (value != null)
            {
                Vector2 conv = (Vector2)value;
                retValue =
                    $"X: {conv.X.ToString("F3", CultureInfo.InvariantCulture)} Y: {conv.Y.ToString("F3", CultureInfo.InvariantCulture)}";
            }

            return retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
