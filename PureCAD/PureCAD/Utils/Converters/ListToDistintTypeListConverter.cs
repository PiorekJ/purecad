﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.Properties;

namespace PureCAD.Utils.Converters
{
    class ListToDistintTypeListConverter : IMultiValueConverter
    {
        //To use with multibinding, first value is the number of elements, second the collection, third the control
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 3)
                return null;

            var vals = values[1] as IEnumerable;
            if (vals == null)
                return null;

            var types = new List<string>();
            types.Add(Resources.ComboBoxFilterDefault);
            foreach (object item in vals)
            {
                if (item is SceneObject sceneObject)
                    if (sceneObject is IFilterable)
                        types.Add(sceneObject.GetType().FullName);
            }

            if (types.Count == 1)
            {
                ((ComboBox) values[2]).SelectedIndex = 0;
            }

            return types.Distinct();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
