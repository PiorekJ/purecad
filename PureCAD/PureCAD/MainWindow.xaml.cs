﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.Models;
using OpenTK.Graphics.OpenGL;
using PureCAD.OpenTK;
using PureCAD.Utils;
using PureCAD.Utils.Math;
using PureCAD.Utils.ModeManagers;
using InputManager = PureCAD.Core.InputManager;

namespace PureCAD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
            Simulation.Scene.SelectionManager.BindSelectionControl(SceneObjectsListView);
            InputManager.RegisterOnKeyDownEvent(Key.R, () =>
            {
                if (InputManager.IsKeyDown(Key.LeftShift) && DisplayControl.ContextMenu != null)
                    DisplayControl.ContextMenu.IsOpen = true;

            });
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.LineWidth(1);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);

            Simulation.InitializeSimulation();
            Simulation.ExplicitModeManager.SetupExplicitMode();
            Simulation.StereoModeManager.SetupStereoMode();
            Simulation.Scene.Camera.SetStereoscopicMatrices();
            CompositionTarget.Rendering += OnRender;
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.TimeCounter.CountDT();
            InputManager.SetMousePosition(DisplayControl.MousePosition);
            GL.ClearColor(System.Drawing.Color.Black);
            if (!Simulation.Settings.ExplicitModeOn && !Simulation.Settings.StereoModeOn)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                Simulation.Scene.SceneBackground.Update();
                Simulation.Scene.SceneBackground.Render();
                Simulation.Scene.SceneCursor.Update();
                Simulation.Scene.SceneCursor.Render();

                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null)
                    Simulation.Scene.SelectionManager.SelectedSceneObject.Update();
                for (var i = 0; i < Simulation.Scene.SceneObjects.Count; i++)
                {
                    SceneObject item = Simulation.Scene.SceneObjects[i];

                    item.Render();
                }

                Simulation.PathGenerationManager.OnUpdate();
                Simulation.PathGenerationManager.OnRender();
                
                Simulation.Scene.SceneSelectionRectangle.Update();
                Simulation.Scene.SceneSelectionRectangle.Render();

                if (Simulation.Scene.SceneMillingMachine != null)
                {
                    Simulation.Scene.SceneMillingMachine.Update();
                    Simulation.Scene.SceneMillingMachine.Render();
                }
            }
            else if (Simulation.Settings.StereoModeOn)
            {
                Simulation.StereoModeManager.Draw();
            }
            else
            {
                Simulation.ExplicitModeManager.Draw();
            }
            Simulation.Scene.Camera.UpdateCamera(DisplayControl.MousePosition);


            DisplayControl.SwapBuffers();
        }



        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);
            Simulation.ExplicitModeManager.ResizeFrameBuffer();
            Simulation.Scene.Camera.SetStereoscopicMatrices();
            Simulation.StereoModeManager.ResizeFrameBuffers();

        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            for (var i = Simulation.Scene.SceneObjects.Count - 1; i >= 0; i--)
            {
                SceneObject item = Simulation.Scene.SceneObjects[i];
                item.Dispose();
            }
            Simulation.Scene.SelectionManager.Dispose();
            Simulation.Scene.SceneCursor.Dispose();
            Simulation.Scene.SceneSelectionRectangle.Dispose();
            Simulation.Scene.SceneBackground.Dispose();
            Simulation.ExplicitModeManager.Dispose();
            Simulation.StereoModeManager.Dispose();
            Simulation.PathGenerationManager.Dispose();
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }

        private void FilterComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var type = FilterComboBox.SelectedItem;
            if (type == null)
            {
                FilterComboBox.SelectedIndex = 0;
                return;
            }

            foreach (SceneObject item in Simulation.Scene.SceneObjects)
            {
                if (type.Equals(Properties.Resources.ComboBoxFilterDefault))
                {
                    item.IsVisibleOnList = true;
                    continue;
                }

                string fullName = item.GetType().FullName;
                if (fullName != null)
                {
                    item.IsVisibleOnList = fullName.Equals(type);
                }
            }
        }

        private void SceneObjectsListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<SceneObject> selected = SceneObjectsListView.SelectedItems.OfType<SceneObject>().ToList();
            Simulation.Scene.SelectionManager.SelectObjects(selected);
        }

        private void StereoComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Simulation.StereoModeManager.SetColorSet(StereoComboBox.SelectedIndex);
        }
    }
}
