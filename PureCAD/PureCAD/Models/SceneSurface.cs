﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public abstract class SceneSurface : Model, ISelectable, IColorable, IFilterable
    {
        public bool IsSelected { get; set; }
        public Color Color { get; set; } = Colors.White;
        public Color PolyColor { get; set; } = Colors.Red;

        private int _uParameter = 4;
        private int _vParameter = 4;
        private bool _displayPoly;

        public int PatchesX;
        public int PatchesY;

        public int UParameter
        {
            get { return _uParameter; }
            set
            {
                _uParameter = value;
                RegenerateMesh();
            }
        }

        public int VParameter
        {
            get { return _vParameter; }
            set
            {
                _vParameter = value;
                RegenerateMesh();
            }
        }

        public bool DisplayPoly
        {
            get { return _displayPoly; }
            set
            {
                _displayPoly = value;
                RaisePropertyChanged();
            }
        }

        protected VertexPC[] VerticesPoly { get; set; }
        protected Shader PolyShader { get; set; }
        protected Mesh<VertexPC> PolyLines { get; set; }
        public List<SurfacePatch> SurfacePatches;
        public ObservableCollection<ScenePoint> SurfacePoints;

        public enum SurfaceType
        {
            Plane,
            Cylinder
        }

        protected SceneSurface()
        {
            SurfacePatches = new List<SurfacePatch>();
            SurfacePoints = new ObservableCollection<ScenePoint>();
            SurfacePoints.CollectionChanged += (sender, args) =>
            {
                if (args.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (object item in args.NewItems)
                    {
                        if (item is SurfacePoint point)
                            Transform.ForceAddChild(point);
                    }
                }
                if (args.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (object item in args.NewItems)
                    {
                        if (item is SurfacePoint point)
                            Transform.ForceRemoveChild(point);
                    }
                }
            };
            OnDeleteEvent += () =>
            {
                for (int i = SurfacePoints.Count - 1; i >= 0; i--)
                {
                    Simulation.Scene.RemoveSceneObject(SurfacePoints[i]);
                }
            };
            Shader = Shaders.SurfaceC0Shader;
            Mesh = GenerateSurfaceMesh();
            PolyShader = Shaders.BasicColorShader;
        }

        private void RegenerateMesh()
        {
            Mesh = GenerateSurfaceMesh();
        }

        public void InitializeSurface(int type, float sizeX, float sizeY, int patchesCountX, int patchesCountY)
        {
            PatchesX = patchesCountX;
            PatchesY = patchesCountY;
            switch (type)
            {
                case 0:
                    CreatePlaneSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
                    CreatePlaneSurfacePatches(patchesCountX, patchesCountY);
                    ReinitializePoly(type);
                    break;
                case 1:
                    CreateCylinderSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
                    CreateCylinderSurfacePatches(patchesCountX, patchesCountY);
                    ReinitializePoly(type);
                    break;

            }
        }

        protected abstract void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY);
        

        public Mesh<Vertex2DP> GenerateSurfaceMesh()
        {
            List<Vertex2DP> vertices = new List<Vertex2DP>();
            List<uint> edges = new List<uint>();
            float distX = 1f / (UParameter - 1);
            float distY = 1f / (VParameter - 1);
            for (int i = 0; i < VParameter; i++)
            {
                for (int j = 0; j < UParameter; j++)
                {
                    vertices.Add(new Vertex2DP(j * distX, i * distY));
                    if (j != 0)
                    {
                        edges.Add((uint)(j + i * UParameter));
                        edges.Add((uint)((j - 1) + i * UParameter));
                    }
                    if (i != 0)
                    {
                        edges.Add((uint)(j + i * UParameter));
                        edges.Add((uint)(j + (i - 1) * UParameter));
                    }
                }
            }
            return new Mesh<Vertex2DP>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"), !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());

            foreach (SurfacePatch surfacePatch in SurfacePatches)
            {
                for (int i = 0; i < 16; i++)
                {
                    Shader.Bind(Shader.GetUniformLocation($"points[{i}]"), surfacePatch.PatchPoints[i].Transform.Position);
                }
                Mesh.Draw();
            }

            if (DisplayPoly)
                DrawBernstainPoly();
        }

        private void DrawBernstainPoly()
        {
            for (int i = 0; i < SurfacePoints.Count; i++)
            {
                VerticesPoly[i] = new VertexPC(SurfacePoints[i].Transform.Position, new Vector3(SurfacePoints[i].Color.ScR, SurfacePoints[i].Color.ScG, SurfacePoints[i].Color.ScB));
            }
            PolyLines.SetVertices(0, VerticesPoly);
            PolyShader.Use();
            PolyShader.Bind(PolyShader.GetUniformLocation("model"), Matrix4.Identity);
            PolyShader.Bind(PolyShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            PolyShader.Bind(PolyShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"), !IsSelected ? PolyColor.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());
            PolyLines.Draw();
        }

        protected void ReinitializePoly(int type)
        {
            VerticesPoly = new VertexPC[SurfacePoints.Count];
            List<uint> edgesPoly = new List<uint>();
            for (int i = 0; i < SurfacePoints.Count; i++)
            {
                VerticesPoly[i] = new VertexPC(SurfacePoints[i].Transform.Position, new Vector3(SurfacePoints[i].Color.ScR, SurfacePoints[i].Color.ScG, SurfacePoints[i].Color.ScB));
            }

            if (type == 0)
            {
                int pointsX = 3 * PatchesX + 1;
                int pointsY = 3 * PatchesY + 1;
                for (int i = 0; i < pointsY; i++)
                {
                    for (int j = 0; j < pointsX; j++)
                    {
                        if (j != pointsX - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)((j + 1) + i * pointsX));
                        }

                        if (i != pointsY - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + (i + 1) * pointsX));
                        }
                    }
                }
            }
            if (type == 1)
            {
                int pointsX = 3 * PatchesX;
                int pointsY = 3 * PatchesY + 1;
                for (int i = 0; i < pointsY; i++)
                {
                    for (int j = 0; j < pointsX; j++)
                    {
                        if (j != pointsX - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + 1 + i * pointsX));
                        }
                        else
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(i * pointsX));
                        }

                        if (i != pointsY - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + (i + 1) * pointsX));
                        }
                    }
                }
            }

            PolyLines?.Dispose();
            PolyLines = new Mesh<VertexPC>(VerticesPoly, edgesPoly, MeshType.Lines, AccessType.Stream);
        }

        public override void Dispose()
        {
            PolyLines?.Dispose();
            base.Dispose();
        }
    }
}
