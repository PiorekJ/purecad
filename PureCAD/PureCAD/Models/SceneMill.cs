﻿using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.OpenTK;

namespace PureCAD.Models
{
    public enum MillType
    {
        Flat,
        Sphere
    }


    public abstract class SceneMill : Model
    {
        public Color Color { get; set; } = Colors.Gray;
        public bool IsMoving { get; set; } = false;

        protected SceneMill()
        {
            Shader = Shaders.LitObjectShader;
            Transform.Position = Constants.SafeMillPosition;
        }

        public abstract void SetupMill(float sceneSize, int materialSize, SceneMaterial material);
        public abstract bool MillMaterialAt(SceneMaterial material, SceneMillPath.MaterialPoint materialPoint, float safeY, Vector3 millDirection, bool updateMaterial = true);
    }
}

