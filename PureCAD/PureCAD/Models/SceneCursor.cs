﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;
using PureCAD.Core;
using PureCAD.OpenTK;
using OpenTK;
using PureCAD.Components;
using PureCAD.Utils;
using InputManager = PureCAD.Core.InputManager;

namespace PureCAD.Models
{
    public class SceneCursor : Model
    {
        private bool _grabbing = false;

        public bool Grabbing
        {
            get { return _grabbing; }
            set
            {
                _grabbing = value;
                RaisePropertyChanged();
            }
        }

        public SceneCursor()
        {
            Shader = Shaders.BasicShader;
            Mesh = GenerateCursorMesh();
            InputManager.RegisterOnKeyDownEvent(Key.V, GrabObjects);
            Transform.OnPositionUpdate += (obj, value, newValue) =>
            {
                if (!Transform.IsMoving)
                    foreach (SceneObject sceneObject in Transform.Children)
                    {
                        sceneObject.Transform.Position += newValue - value;
                    }
            };
        }

        public void SetStereoCursor(bool isColored)
        {
            if (isColored)
            {
                ((Mesh<VertexPC>)Mesh).SetVertices(0, new[]
                {
                    new VertexPC(-Vector3.UnitX * Simulation.Settings.CursorGrabRadius, Vector3.UnitX),
                    new VertexPC(Vector3.UnitX * Simulation.Settings.CursorGrabRadius, Vector3.UnitX),
                    new VertexPC(-Vector3.UnitY * Simulation.Settings.CursorGrabRadius, Vector3.UnitY),
                    new VertexPC(Vector3.UnitY * Simulation.Settings.CursorGrabRadius, Vector3.UnitY),
                    new VertexPC(-Vector3.UnitZ * Simulation.Settings.CursorGrabRadius, Vector3.UnitZ),
                    new VertexPC(Vector3.UnitZ * Simulation.Settings.CursorGrabRadius, Vector3.UnitZ)
                });
            }
            else
            {
                ((Mesh<VertexPC>)Mesh).SetVertices(0, new[]
                {
                    new VertexPC(-Vector3.UnitX * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3()),
                    new VertexPC(Vector3.UnitX * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3()),
                    new VertexPC(-Vector3.UnitY * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3()),
                    new VertexPC(Vector3.UnitY * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3()),
                    new VertexPC(-Vector3.UnitZ * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3()),
                    new VertexPC(Vector3.UnitZ * Simulation.Settings.CursorGrabRadius, Colors.White.ColorToVector3())
                });
            }

        }

        private void GrabObjects()
        {
            if (Simulation.Storage.CursorMode)
            {
                Grabbing = !Grabbing;
                if (Grabbing)
                    foreach (SceneObject sceneObject in Simulation.Scene.SceneObjects)
                    {
                        if (sceneObject is ISelectable)
                            if ((sceneObject.Transform.Position - Transform.Position).Length <= Simulation.Settings.CursorGrabRadius)
                            {
                                Transform.ForceAddChild(sceneObject);
                            }
                    }
                else
                    Transform.ForceRemoveAllChildren();
            }
        }

        public void ResetCursorGrabStatus()
        {
            Grabbing = false;
            Transform.ForceRemoveAllChildren();
            if (Transform.IsMoving)
                Transform.InterruptMovement();
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
        }

        protected override void OnUpdate()
        {
            if (Transform != null)
            {
                var dw2 = Simulation.Storage.DisplayWidth / 2.0f;
                var dh2 = Simulation.Storage.DisplayHeight / 2.0f;

                var pos4 = Vector4.UnitW * Transform.GetModelMatrix() * Simulation.Scene.Camera.GetViewMatrix() * Simulation.Scene.Camera.GetProjectionMatrix();
                var pos2 = new Vector3(pos4.X / pos4.W * dw2,
                    pos4.Y / pos4.W * dh2, pos4.Z / pos4.W);

                if (pos2.X > dw2 || pos2.X < -dw2 || pos2.Y > dh2 || pos2.Y < -dh2)
                    pos2 = Vector3.Zero;

                if (Math.Abs((double)pos2.Z) > 1)
                    pos2 = Vector3.Zero;

                Transform.ScreenPosition = pos2;
            }
        }

        private Mesh<VertexPC> GenerateCursorMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(-Vector3.UnitX * 0.25f, Vector3.UnitX));
            vertices.Add(new VertexPC(Vector3.UnitX * 0.25f, Vector3.UnitX));
            vertices.Add(new VertexPC(-Vector3.UnitY * 0.25f, Vector3.UnitY));
            vertices.Add(new VertexPC(Vector3.UnitY * 0.25f, Vector3.UnitY));
            vertices.Add(new VertexPC(-Vector3.UnitZ * 0.25f, Vector3.UnitZ));
            vertices.Add(new VertexPC(Vector3.UnitZ * 0.25f, Vector3.UnitZ));

            return new Mesh<VertexPC>(vertices, null, MeshType.Lines, AccessType.Static);
        }
    }
}
