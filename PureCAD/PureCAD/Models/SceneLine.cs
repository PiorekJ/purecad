﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class SceneLine : Model, IColorable
    {
        public Color Color { get; set; }

        private VertexP[] _vertices = new VertexP[2];

        public SceneLine()
        {
            Mesh = GenerateLineMesh(Vector3.Zero);
            Shader = Shaders.BasicColorShader;
            IsVisibleOnList = false;
            IsVisible = false;
        }

        public Mesh<VertexP> GenerateLineMesh(Vector3 lineDirection)
        {
            Mesh?.Dispose();
            List<VertexP> vertices = new List<VertexP>();
            vertices.Add(new VertexP(lineDirection * 100));
            vertices.Add(new VertexP(-lineDirection * 100));

            Color = lineDirection.Vector3ToColor();

            List<uint> edges = new List<uint>();
            edges.Add(0);
            edges.Add(1);
            return new Mesh<VertexP>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        public void Activate(Vector3 lineDirection, Vector3 position)
        {
            Mesh = GenerateLineMesh(lineDirection);
            Color = lineDirection.Vector3ToColor();
            IsVisible = true;
            Transform.Position = position;
            Simulation.Scene.SceneObjects.Add(this);
        }

        public void Deactivate()
        {
            IsVisible = false;
            Simulation.Scene.SceneObjects.Remove(this);
        }


        protected override void OnUpdate()
        {

        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"),
                !Simulation.Settings.StereoModeOn ? Color.ColorToVector3() : Colors.White.ColorToVector3());
            Mesh.Draw();
        }
    }
}
