﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PureCAD.Core;

namespace PureCAD.Models
{
    public class SurfacePoint : ScenePoint, IVirtual, IFilterable
    {
        public SurfacePoint()
        {
            IsVisibleOnList = true;
        }
    }
}
