﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.Serialization;

namespace PureCAD.Models
{
    public class SceneCurveC0 : SceneCurve
    {
        private List<CurveDrawPoint> _drawPoints;

        protected override void OnRender()
        {
            if (CurvePoints.Count < 2)
                return;

            CreateDrawPoints();

            if (DisplayPoly)
                DrawPoly(_drawPoints);

            DrawC0(_drawPoints);
        }

        private List<CurveDrawPoint> CreateDrawPoints()
        {
            _drawPoints.Clear();
            foreach (ScenePoint point in CurvePoints)
            {
                _drawPoints.Add(new CurveDrawPoint(point.Transform.Position, point.Color.ColorToVector3()));
            }
            return _drawPoints;
        }

        public SceneCurveC0()
        {
            _drawPoints = new List<CurveDrawPoint>();
            Transform.OnChildAdd += (child) =>
            {
                if (child is ScenePoint point)
                {
                    CurvePoints.Add(point);
                    CreateDrawPoints();
                    ReinitializePoly(_drawPoints);
                    if (IsSelected)
                        point.IsSelected = true;
                }
            };

            Transform.OnChildRemove += (child) =>
            {
                if (child is ScenePoint point)
                {
                    CurvePoints.Remove(point);
                    CreateDrawPoints();
                    if (IsSelected)
                        point.IsSelected = false;
                    if (CurvePoints.Count == 0)
                    {
                        Simulation.Scene.RemoveSceneObject(this);
                    }
                    else
                    {
                        ReinitializePoly(_drawPoints);
                    }
                }
            };
        }

        public void Deserialize(curveC0 serializedCurveC0)
        {
            foreach (int point in serializedCurveC0.pointsId)
            {
                //CurvePoints.Add((ScenePoint)Simulation.Scene.SceneObjects[point]);
                Transform.ForceAddChild((ScenePoint)Simulation.Scene.SceneObjects[point]);
            }
            ObjectName = serializedCurveC0.name;
        }

        public curveC0 Serialize(List<ScenePoint> points)
        {
            var curveC0 = new curveC0();
            curveC0.name = ObjectName;
            curveC0.pointsId = new int[CurvePoints.Count];
            for (int i = 0; i < CurvePoints.Count; i++)
            {
                curveC0.pointsId[i] = points.IndexOf(CurvePoints[i]);
            }
            
            return curveC0;
        }

       
    }
}
