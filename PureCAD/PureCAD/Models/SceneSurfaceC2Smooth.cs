﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils.Serialization;

namespace PureCAD.Models
{
    public class SceneSurfaceC2Smooth : SceneSurfaceSmooth
    {
        public SceneSurfaceC2Smooth()
        {
            Shader = Shaders.SurfaceC2SmoothShader;
        }

        protected override void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY)
        {
            int pointsX = patchesCountX + 3;
            int pointsY = patchesCountY + 3;

            float distX = width / (pointsX - 1);
            float distY = height / (pointsY - 1);
            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    Simulation.Scene.AddSurfacePoint(this, new Vector3(i * distX, 0, j * distY));
                }
            }
        }

        protected override void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX, int patchesCountY)
        {
            int pointsX = patchesCountX;
            int pointsY = patchesCountY + 3;
            float angle = 360f / pointsX;
            float distY = 0;
            if (pointsY - 1 != 0)
                distY = height / (pointsY - 1);

            IsUWrapped = true;

            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    float yCoord = (float)(Math.Cos(i * angle * Math.PI / 180f) * radius);
                    float zCoord = (float)(Math.Sin(i * angle * Math.PI / 180f) * radius);
                    Simulation.Scene.AddSurfacePoint(this, new Vector3(j * distY, yCoord, zCoord));
                }
            }
        }

        protected override void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY)
        {
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    SurfacePatch bezierSurfacePatch = new SurfacePatch(j, i);
                    int cornerIndexX = j;
                    int cornerIndexY = i;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;
                            bezierSurfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (patchesCountX + 3)]);
                        }
                    }
                    SurfacePatches.Add(bezierSurfacePatch);
                }
            }
        }

        protected override void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY)
        {
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    SurfacePatch surfacePatch = new SurfacePatch(j, i);
                    int cornerIndexX = j;
                    int cornerIndexY = i;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;

                            pointX = pointX % patchesCountX;
                            surfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (patchesCountX)]);
                        }
                    }
                    SurfacePatches.Add(surfacePatch);
                }
            }
        }

        private double[] GetBasis(double t, int degree)
        {
            double[] N = new double[degree];
            N[0] = 1;
            for (int i = 1; i < degree; i++)
            {
                double saved = 0;
                for (int j = 0; j < i; j++)
                {
                    var term = N[j] / i;
                    N[j] = saved + (j - t + 1) * term;
                    saved = (t + i - 1 - j) * term;
                }
                N[i] = saved;
            }
            return N;
        }

        protected override Vector3d SurfaceCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d)
        {
            var N = GetBasis(t, 4); 
            
            return N[0] * a + N[1] * b + N[2] * c + N[3] * d;
        }

        protected override Vector3d DerivativeCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d)
        {
            var N = GetBasis(t, 3);
            var deriv0 = b - a;
            var deriv1 = c - b;
            var deriv2 = d - c;

            return deriv0 * N[0] + deriv1 * N[1] + deriv2 * N[2];
        }

        public void Deserialize(surfaceC2 surfaceC2)
        {
            ObjectName = surfaceC2.name;
            PatchesX = surfaceC2.points.GetLength(0) - 3;
            PatchesY = surfaceC2.points.GetLength(1) - 3;
            UParameter = surfaceC2.u;
            VParameter = surfaceC2.v;

            IsUWrapped = surfaceC2.cylinder;

            for (var y = 0; y < surfaceC2.points.GetLength(1); y++)
            {
                for (var x = 0; x < surfaceC2.points.GetLength(0); x++)
                {
                    int surfaceC2Point = surfaceC2.points[x, y];
                    var point = (ScenePoint)Simulation.Scene.SceneObjects[surfaceC2Point];
                    Simulation.Scene.SceneObjects[surfaceC2Point].IsDeletable = false;
                    SurfacePoints.Add(point);
                }
            }

            CreatePlaneSurfacePatches(PatchesX, PatchesY);

            if (surfaceC2.cylinder)
            {
                SurfacePoints.Clear();

                for (var y = 0; y < surfaceC2.points.GetLength(1); y++)
                {
                    for (var x = 0; x < surfaceC2.points.GetLength(0); x++)
                    {
                        int surfaceC2Point = surfaceC2.points[x, y];
                        var point = (ScenePoint)Simulation.Scene.SceneObjects[surfaceC2Point];
                        Simulation.Scene.SceneObjects[surfaceC2Point].IsDeletable = false;
                        if (!SurfacePoints.Contains(point))
                            SurfacePoints.Add(point);
                    }
                }
            }

            int value = surfaceC2.cylinder ? 1 : 0;
            ReinitializePoly(value * 2);
        }

        public surfaceC2 Serialize(List<ScenePoint> points)
        {
            var surfaceC2 = new surfaceC2();

            surfaceC2.name = ObjectName;
            surfaceC2.u = UParameter;
            surfaceC2.v = VParameter;
            surfaceC2.flakeU = PatchesX;
            surfaceC2.flakeV = PatchesY;
            surfaceC2.cylinder = IsUWrapped;

            int pointsX = PatchesX + 3;
            int pointsY = PatchesY + 3;
            surfaceC2.points = new int[pointsX, pointsY];

            int arraySize = surfaceC2.cylinder ? pointsX - 3 : pointsX;

            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    var pointX = i;
                    var pointY = j;

                    if (surfaceC2.cylinder)
                        pointX = pointX % PatchesX;

                    surfaceC2.points[i, j] = points.IndexOf(SurfacePoints[pointX + pointY * arraySize]);
                }
            }

            return surfaceC2;
        }
    }
}
