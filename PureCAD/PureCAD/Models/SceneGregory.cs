﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;
using PureCAD.Utils.Math;

namespace PureCAD.Models
{
    public class SceneGregory : Model, ISelectable
    {
        private List<GregoryEdge> Edges;
        private List<GregoryPatch> Patches;

        public ScenePoint P0;
        private int _uParameter = 16;
        private int _vParameter = 16;

        public int UParameter
        {
            get { return _uParameter; }
            set
            {
                _uParameter = value;
                foreach (GregoryPatch patch in Patches)
                {
                    patch.Mesh = patch.GeneratePatchMesh(UParameter, VParameter);
                }
            }
        }

        public int VParameter
        {
            get { return _vParameter; }
            set
            {
                _vParameter = value;
                foreach (GregoryPatch patch in Patches)
                {
                    patch.Mesh = patch.GeneratePatchMesh(UParameter, VParameter);
                }
            }
        }

        public bool DisplayVectors { get; set; }
        public bool IsSelected { get; set; }

        private List<SceneSurfaceC0Smooth> _surfaces;

        private class GregoryEdge
        {
            public List<ScenePoint> FirstRowPoints;
            public List<ScenePoint> SecondRowPoints;
            private Shader Shader;
            private Mesh<VertexP> Mesh;

            public List<ScenePoint> DividedPointsFirstRow;
            public List<Vector3> DividedPointsSecondRow;
            private SceneGregory _owner;
            public ScenePoint P3i;
            public ScenePoint P2i;
            public ScenePoint P1i;
            public ScenePoint Qi;

            private List<Vector3> firstRowRPoints;
            private List<Vector3> secondRowRPoints;
            private List<Vector3> firstRowSPoints;
            private List<Vector3> secondRowSPoints;

            public float RandomScale = 1.0f;

            public bool IsVisible = false;
            private bool _showDerivatives = false;

            public GregoryEdge(SceneGregory owner)
            {
                FirstRowPoints = new List<ScenePoint>();
                SecondRowPoints = new List<ScenePoint>();
                _owner = owner;
            }

            private List<Vector3> ConstructFirstRowRPoints()
            {
                var list = new List<Vector3>();
                for (int i = 1; i < FirstRowPoints.Count; i++)
                {
                    list.Add((FirstRowPoints[i].Transform.Position + FirstRowPoints[i - 1].Transform.Position) / 2);
                }

                return list;
            }

            private List<Vector3> ConstructSecondRowRPoints()
            {
                var list = new List<Vector3>();
                for (int i = 1; i < SecondRowPoints.Count; i++)
                {
                    list.Add((SecondRowPoints[i].Transform.Position + SecondRowPoints[i - 1].Transform.Position) / 2);
                }

                return list;
            }

            private List<Vector3> ConstructFirstRowSPoints()
            {
                var list = new List<Vector3>();
                for (int i = 1; i < firstRowRPoints.Count; i++)
                {
                    list.Add((firstRowRPoints[i] + firstRowRPoints[i - 1]) / 2);
                }

                return list;
            }

            private List<Vector3> ConstructSecondRowSPoints()
            {
                var list = new List<Vector3>();
                for (int i = 1; i < secondRowRPoints.Count; i++)
                {
                    list.Add((secondRowRPoints[i] + secondRowRPoints[i - 1]) / 2);
                }

                return list;
            }

            private ScenePoint GenerateBoundaryPoint(Vector3 position)
            {
                var point = Simulation.Scene.AddScenePoint(position, Colors.DarkOrange, String.Empty, IsVisible, IsVisible);
                _owner.OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(point);
                };
                return point;
            }

            public void CalculateBoundaryPoints()
            {
                firstRowRPoints = ConstructFirstRowRPoints();
                secondRowRPoints = ConstructSecondRowRPoints();
                firstRowSPoints = ConstructFirstRowSPoints();
                secondRowSPoints = ConstructSecondRowSPoints();

                DividedPointsFirstRow = new List<ScenePoint>();
                //1/6 2/6 3/6 4/6 5/6

                DividedPointsFirstRow.Add(GenerateBoundaryPoint(firstRowRPoints[0]));
                DividedPointsFirstRow.Add(GenerateBoundaryPoint(firstRowSPoints[0]));
                DividedPointsFirstRow.Add(GenerateBoundaryPoint(DeCasteljau(0.5f, FirstRowPoints[0].Transform.Position, FirstRowPoints[1].Transform.Position, FirstRowPoints[2].Transform.Position, FirstRowPoints[3].Transform.Position)));
                DividedPointsFirstRow.Add(GenerateBoundaryPoint(firstRowSPoints[1]));
                DividedPointsFirstRow.Add(GenerateBoundaryPoint(firstRowRPoints[2]));


                DividedPointsSecondRow = new List<Vector3>();
                DividedPointsSecondRow.Add(secondRowRPoints[0]);
                DividedPointsSecondRow.Add(secondRowSPoints[0]);
                DividedPointsSecondRow.Add(DeCasteljau(0.5f, SecondRowPoints[0].Transform.Position, SecondRowPoints[1].Transform.Position, SecondRowPoints[2].Transform.Position, SecondRowPoints[3].Transform.Position));
                DividedPointsSecondRow.Add(secondRowSPoints[1]);
                DividedPointsSecondRow.Add(secondRowRPoints[2]);
            }

            public void CalculateP3i()
            {
                P3i = DividedPointsFirstRow[2];
                P3i.IsVisible = IsVisible;
                P3i.IsVisibleOnList = IsVisible;
                P3i.ObjectName += "P3i";
            }

            public void CalculateP2i()
            {
                var tmp = DeCasteljau(0.5f, SecondRowPoints[0].Transform.Position, SecondRowPoints[1].Transform.Position, SecondRowPoints[2].Transform.Position, SecondRowPoints[3].Transform.Position);
                P2i = Simulation.Scene.AddScenePoint(P3i.Transform.Position + RandomScale * (P3i.Transform.Position - tmp), Colors.DeepSkyBlue, "P2i" ,IsVisible, IsVisible);
            }

            public void CalculateQi()
            {
                Qi = Simulation.Scene.AddScenePoint(P3i.Transform.Position + (3 * (P2i.Transform.Position - P3i.Transform.Position)) / 2.0f, Colors.DeepPink, "Qi", IsVisible, IsVisible);
            }

            public void CalculateP1i()
            {
                P1i = Simulation.Scene.AddScenePoint((2 * Qi.Transform.Position + _owner.P0.Transform.Position) / 3.0f, Colors.Yellow, "P1i", IsVisible, IsVisible);
            }

            private void RecalculateFirstRowRPoints()
            {
                for (int i = 1; i < FirstRowPoints.Count; i++)
                {
                    firstRowRPoints[i - 1] = (FirstRowPoints[i].Transform.Position + FirstRowPoints[i - 1].Transform.Position) / 2;
                }
            }

            private void RecalculateSecondRowRPoints()
            {
                for (int i = 1; i < SecondRowPoints.Count; i++)
                {
                    secondRowRPoints[i - 1] = (SecondRowPoints[i].Transform.Position + SecondRowPoints[i - 1].Transform.Position) / 2;
                }
            }

            private void RecalculateFirstRowSPoints()
            {
                for (int i = 1; i < firstRowRPoints.Count; i++)
                {
                    firstRowSPoints[i - 1] = (firstRowRPoints[i] + firstRowRPoints[i - 1]) / 2;
                }
            }

            private void RecalculateSecondRowSPoints()
            {
                for (int i = 1; i < secondRowRPoints.Count; i++)
                {
                    secondRowSPoints[i - 1] = (secondRowRPoints[i] + secondRowRPoints[i - 1]) / 2;
                }
            }

            private void RecalculateBoundary()
            {
                DividedPointsFirstRow[0].Transform.Position = firstRowRPoints[0];
                DividedPointsFirstRow[1].Transform.Position = firstRowSPoints[0];
                DividedPointsFirstRow[2].Transform.Position = DeCasteljau(0.5f, FirstRowPoints[0].Transform.Position, FirstRowPoints[1].Transform.Position, FirstRowPoints[2].Transform.Position, FirstRowPoints[3].Transform.Position);
                DividedPointsFirstRow[3].Transform.Position = firstRowSPoints[1];
                DividedPointsFirstRow[4].Transform.Position = firstRowRPoints[2];

                DividedPointsSecondRow[0] = secondRowRPoints[0];
                DividedPointsSecondRow[1] = secondRowSPoints[0];
                DividedPointsSecondRow[2] = DeCasteljau(0.5f, SecondRowPoints[0].Transform.Position, SecondRowPoints[1].Transform.Position, SecondRowPoints[2].Transform.Position, SecondRowPoints[3].Transform.Position);
                DividedPointsSecondRow[3] = secondRowSPoints[1];
                DividedPointsSecondRow[4] = secondRowRPoints[2];
            }

            public void Recalculate()
            {
                RecalculateFirstRowRPoints();
                RecalculateSecondRowRPoints();
                RecalculateFirstRowSPoints();
                RecalculateSecondRowSPoints();
                RecalculateBoundary();

                P3i = DividedPointsFirstRow[2];
                var tmp = DeCasteljau(0.5f, SecondRowPoints[0].Transform.Position, SecondRowPoints[1].Transform.Position, SecondRowPoints[2].Transform.Position, SecondRowPoints[3].Transform.Position);
                P2i.Transform.Position = P3i.Transform.Position + RandomScale * (P3i.Transform.Position - tmp);
                Qi.Transform.Position = P3i.Transform.Position + (3 * (P2i.Transform.Position - P3i.Transform.Position)) / 2.0f;
                P1i.Transform.Position = (2 * Qi.Transform.Position + _owner.P0.Transform.Position) / 3.0f;
            }

            public void Dispose()
            {
                Simulation.Scene.RemoveSceneObject(P1i);
                Simulation.Scene.RemoveSceneObject(P2i);
                Simulation.Scene.RemoveSceneObject(P3i);
                Simulation.Scene.RemoveSceneObject(Qi);
            }

            public List<ScenePoint> GetP3iLeftSide()
            {
                var list = new List<ScenePoint>();
                list.Add(FirstRowPoints[0]);
                list.Add(DividedPointsFirstRow[0]);
                list.Add(DividedPointsFirstRow[1]);
                list.Add(P3i);
                return list;
            }

            public List<ScenePoint> GetP3iLeftSideDerivatives()
            {
                var list = new List<ScenePoint>();
                var d1 = Simulation.Scene.AddScenePoint(DividedPointsFirstRow[0].Transform.Position + RandomScale * (DividedPointsFirstRow[0].Transform.Position - DividedPointsSecondRow[0]), Colors.Blue, "Left side d1", IsVisible, IsVisible);
                list.Add(d1);
                var d2 = Simulation.Scene.AddScenePoint(DividedPointsFirstRow[1].Transform.Position + RandomScale * (DividedPointsFirstRow[1].Transform.Position - DividedPointsSecondRow[1]), Colors.Blue, "Left side d2", IsVisible, IsVisible);
                list.Add(d2);
                _owner.OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(d1);
                    Simulation.Scene.RemoveSceneObject(d2);
                };
                return list;
            }

            public List<ScenePoint> GetP3iRightSide()
            {
                var list = new List<ScenePoint>();
                list.Add(P3i);
                list.Add(DividedPointsFirstRow[3]);
                list.Add(DividedPointsFirstRow[4]);
                list.Add(FirstRowPoints[FirstRowPoints.Count - 1]);
                return list;
            }

            public List<ScenePoint> GetP3iRightSideDerivatives()
            {
                var list = new List<ScenePoint>();
                var d1 = Simulation.Scene.AddScenePoint(DividedPointsFirstRow[3].Transform.Position + RandomScale * (DividedPointsFirstRow[3].Transform.Position - DividedPointsSecondRow[3]), Colors.Blue, "Right side d1", IsVisible, IsVisible);
                list.Add(d1);
                var d2 = Simulation.Scene.AddScenePoint(DividedPointsFirstRow[4].Transform.Position + RandomScale * (DividedPointsFirstRow[4].Transform.Position - DividedPointsSecondRow[4]), Colors.Blue, "Right side d2", IsVisible, IsVisible);
                list.Add(d2);
                _owner.OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(d1);
                    Simulation.Scene.RemoveSceneObject(d2);
                };
                return list;
            }

            public List<ScenePoint> GetP3iToCenter()
            {
                var list = new List<ScenePoint>();
                list.Add(P3i);
                list.Add(P2i);
                list.Add(P1i);
                list.Add(_owner.P0);
                return list;
            }

            //public List<ScenePoint> GetP3iToCenterDerivatives(Vector3 a3)
            //{
            //    var list = new List<ScenePoint>();
            //    var d1 = Simulation.Scene.AddScenePoint();

            //    var a0 = RandomScale * (((DividedPointsFirstRow[1].Transform.Position - P3i.Transform.Position) +
            //               (P3i.Transform.Position - DividedPointsFirstRow[3].Transform.Position)) / 2);
            //    //var a0 = RandomScale * (DividedPointsFirstRow[1].Transform.Position - P3i.Transform.Position);
            //    a3 *= RandomScale;
            //    d1.Transform.Position = P2i.Transform.Position + Vector3.Lerp(a0, a3, 1.0f / 3.0f); //TODO: WEIRD SCALING AGAIN
            //    d1.Color = Colors.Red;
            //    d1.IsVisible = true;
            //    d1.IsVisibleOnList = IsVisible;
            //    d1.ObjectName += " to center d1";
            //    list.Add(d1);
            //    var d2 = Simulation.Scene.AddScenePoint();
            //    d2.Transform.Position = P1i.Transform.Position + Vector3.Lerp(a0, a3, 2.0f / 3.0f); //TODO: WEIRD SCALING AGAIN
            //    d2.Color = Colors.Red;
            //    d2.IsVisible = true;
            //    d2.IsVisibleOnList = IsVisible;
            //    d2.ObjectName += " to center d2";
            //    list.Add(d2);
            //    _owner.OnDeleteEvent += () =>
            //    {
            //        Simulation.Scene.RemoveSceneObject(d1);
            //        Simulation.Scene.RemoveSceneObject(d2);
            //    };
            //    return list;
            //}

            public List<ScenePoint> GetP3iToCenterDerivatives(Vector3 a3, Vector3 b3)
            {
                var list = new List<ScenePoint>();
                
                var a0 = RandomScale * (P3i.Transform.Position - DividedPointsFirstRow[3].Transform.Position);
                var b0 = RandomScale * (DividedPointsFirstRow[1].Transform.Position - P3i.Transform.Position);

                var d1 = Simulation.Scene.AddScenePoint(P2i.Transform.Position + GetD(1 / 3.0f, a0, b0, a3, b3), Colors.Blue, "To center d1", _showDerivatives, _showDerivatives);
                var d2 = Simulation.Scene.AddScenePoint(P1i.Transform.Position + GetD(2 / 3.0f, a0, b0, a3, b3), Colors.Blue, "To center d2", _showDerivatives, _showDerivatives);

                list.Add(d1);
                list.Add(d2);

                _owner.OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(d1);
                    Simulation.Scene.RemoveSceneObject(d2);
                };
                return list;
            }

            public List<ScenePoint> GetP3iFromCenter()
            {
                var list = new List<ScenePoint>();
                list.Add(_owner.P0);
                list.Add(P1i);
                list.Add(P2i);
                list.Add(P3i);
                return list;
            }

            //public List<ScenePoint> GetP3iFromCenterDerivatives(Vector3 a3)
            //{
            //    var list = new List<ScenePoint>();
            //    var d1 = Simulation.Scene.AddScenePoint();

            //    var a0 = RandomScale * (((DividedPointsFirstRow[3].Transform.Position - P3i.Transform.Position) + (P3i.Transform.Position - DividedPointsFirstRow[1].Transform.Position)) / 2);
            //    a3 *= RandomScale;
            //    d1.Transform.Position = P1i.Transform.Position + Vector3.Lerp(a3, a0, 1.0f / 3.0f); //TODO: WEIRD SCALING AGAIN
            //    d1.Color = Colors.Blue;
            //    d1.IsVisible = true;
            //    d1.IsVisibleOnList = IsVisible;
            //    d1.ObjectName += " from center d1";
            //    list.Add(d1);
            //    var d2 = Simulation.Scene.AddScenePoint();
            //    d2.Transform.Position = P2i.Transform.Position + Vector3.Lerp(a3, a0, 2.0f / 3.0f); //TODO: WEIRD SCALING AGAIN
            //    d2.Color = Colors.Blue;
            //    d2.IsVisible = true;
            //    d2.IsVisibleOnList = IsVisible;
            //    d2.ObjectName += " from center d2";
            //    list.Add(d2);
            //    _owner.OnDeleteEvent += () =>
            //    {
            //        Simulation.Scene.RemoveSceneObject(d1);
            //        Simulation.Scene.RemoveSceneObject(d2);
            //    };

            //    return list;
            //}

            public List<ScenePoint> GetP3iFromCenterDerivatives(Vector3 a3, Vector3 b3)
            {
                var list = new List<ScenePoint>();

                var a0 = RandomScale * (P3i.Transform.Position - DividedPointsFirstRow[1].Transform.Position);
                var b0 = RandomScale * (DividedPointsFirstRow[3].Transform.Position - P3i.Transform.Position);

                var d1 = Simulation.Scene.AddScenePoint(P1i.Transform.Position + GetD(2 / 3.0f, a0, b0, a3, b3), Colors.Blue, "From center d1", _showDerivatives, _showDerivatives);
                var d2 = Simulation.Scene.AddScenePoint(P2i.Transform.Position + GetD(1 / 3.0f, a0, b0, a3, b3), Colors.Blue, "From center d2", _showDerivatives, _showDerivatives);
                
                list.Add(d1);
                list.Add(d2);

                _owner.OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(d1);
                    Simulation.Scene.RemoveSceneObject(d2);
                };

                return list;
            }

            public Vector3 GetD(float t, Vector3 a0, Vector3 b0, Vector3 a3, Vector3 b3)
            {
                var g0 = (a0 + b0) / 2;
                var g2 = (a3 + b3) / 2;
                var g1 = (g0 + g2) / 2;

                var c0 = P2i.Transform.Position - P3i.Transform.Position;
                var c1 = P1i.Transform.Position - P2i.Transform.Position;
                var c2 = _owner.P0.Transform.Position - P1i.Transform.Position;

                Vector2 solution0, solution1;
                if (!MathGeneralUtils.SolveEquationSystem(g0, c0, b0, out solution0))
                    //solution0 = Vector2.One * 0.5f;
                    solution0 = Vector2.Zero;
                if (!MathGeneralUtils.SolveEquationSystem(g2, c2, b3, out solution1))
                    //solution1 = Vector2.One * 0.5f;
                    solution1 = Vector2.Zero;

                //if (solution0.X + solution0.Y > 1)
                //    solution0 = Vector2.One * 0.5f;

                //if (solution1.X + solution1.Y > 1)
                //    solution1 = Vector2.One * 0.5f;

                return (GetK(t, solution0.X, solution1.X) * GetG(t, g0, g1, g2) +
                       GetH(t, solution0.Y, solution1.Y) * GetC(t, c0, c1, c2));
            }

            private float GetK(float t, float k0, float k1)
            {
                return k0 * (1 - t) + k1 * t;
            }

            private float GetH(float t, float h0, float h1)
            {
                return h0 * (1 - t) + h1 * t;
            }

            private Vector3 GetC(float t, Vector3 c0, Vector3 c1, Vector3 c2)
            {
                return c0 * (1 - t) * (1 - t) + c1 * 2 * t * (1 - t) + c2 * t * t * t;
            }

            private Vector3 GetG(float t, Vector3 g0, Vector3 g1, Vector3 g2)
            {
                return g0 * (1 - t) * (1 - t) + g1 * 2 * t * (1 - t) + g2 * t * t * t;
            }

            private Vector3 DeCasteljau(float t, Vector3 a, Vector3 b, Vector3 c, Vector3 d)
            {
                float paramT = (1 - t);

                a = a * paramT + b * t;
                b = b * paramT + c * t;
                c = c * paramT + d * t;

                a = a * paramT + b * t;
                b = b * paramT + c * t;

                a = a * paramT + b * t;

                return a;
            }

        }

        private class GregoryPatch
        {
            public class Edge
            {
                public List<ScenePoint> Points;
                public List<ScenePoint> Derivatives;
                public Edge()
                {
                    Points = new List<ScenePoint>();
                    Derivatives = new List<ScenePoint>();
                }
            }

            public List<Edge> Edges;

            public PatchMesh<Vertex2DP> Mesh;
            public Mesh<VertexP> VectorMesh;
            private VertexP[] VectorVertices;
            public Shader Shader;
            public Shader VectorShader;

            Vector3[] Points = new Vector3[20];

            public GregoryPatch(int u, int v)
            {
                Edges = new List<Edge>();
                Mesh = GeneratePatchMesh(u, v);
                Shader = Shaders.GregoryPatchSmoothShader;
                VectorShader = Shaders.BasicColorShader;
            }

            public Mesh<VertexP> GenerateVectorMesh()
            {
                VectorVertices = GetVectorVertices();

                return new Mesh<VertexP>(VectorVertices, null, MeshType.Lines, AccessType.Stream);
            }

            public PatchMesh<Vertex2DP> GeneratePatchMesh(int u, int v)
            {
                List<Vertex2DP> vertices = new List<Vertex2DP>();
                List<uint> edges = new List<uint>();
                float distX = 1f / (u - 1);
                float distY = 1f / (v - 1);
                for (int i = 0; i < v; i++)
                {
                    for (int j = 0; j < u; j++)
                    {
                        vertices.Add(new Vertex2DP(j * distX, i * distY));
                    }
                }
                for (int i = 0; i < v; i++)
                {
                    for (int j = 0; j < u; j++)
                    {
                        vertices.Add(new Vertex2DP(j * distX, i * distY));
                        if (j != 0)
                        {
                            edges.Add((uint)(j + i * u));
                            edges.Add((uint)((j - 1) + i * u));
                        }
                        if (i != 0)
                        {
                            edges.Add((uint)(j + i * u));
                            edges.Add((uint)(j + (i - 1) * u));
                        }
                    }
                }
                return new PatchMesh<Vertex2DP>(vertices, edges, 2);
            }

            public void CalculatePoints()
            {

                Points[0] = Edges[1].Points[0].Transform.Position;
                Points[1] = Edges[1].Points[1].Transform.Position;
                Points[2] = Edges[1].Points[2].Transform.Position;
                Points[3] = Edges[1].Points[3].Transform.Position;

                Points[4] = Edges[0].Points[2].Transform.Position;
                Points[5] = Edges[0].Derivatives[1].Transform.Position;
                Points[6] = Edges[1].Derivatives[0].Transform.Position;
                Points[7] = Edges[1].Derivatives[1].Transform.Position;
                Points[8] = Edges[2].Derivatives[0].Transform.Position;
                Points[9] = Edges[2].Points[1].Transform.Position;

                Points[10] = Edges[0].Points[1].Transform.Position;
                Points[11] = Edges[0].Derivatives[0].Transform.Position;
                Points[12] = Edges[3].Derivatives[1].Transform.Position;
                Points[13] = Edges[3].Derivatives[0].Transform.Position;
                Points[14] = Edges[2].Derivatives[1].Transform.Position;
                Points[15] = Edges[2].Points[2].Transform.Position;

                Points[16] = Edges[3].Points[3].Transform.Position;
                Points[17] = Edges[3].Points[2].Transform.Position;
                Points[18] = Edges[3].Points[1].Transform.Position;
                Points[19] = Edges[3].Points[0].Transform.Position;

                //Points[0] = Edges[0].Points[0].Transform.Position;
                //Points[1] = Edges[0].Points[1].Transform.Position;
                //Points[2] = Edges[0].Points[2].Transform.Position;
                //Points[3] = Edges[0].Points[3].Transform.Position;

                //Points[4] = Edges[3].Points[2].Transform.Position;
                //Points[5] = Edges[3].Derivatives[1].Transform.Position;
                //Points[6] = Edges[0].Derivatives[0].Transform.Position;
                //Points[7] = Edges[0].Derivatives[1].Transform.Position;
                //Points[8] = Edges[1].Derivatives[0].Transform.Position;
                //Points[9] = Edges[1].Points[1].Transform.Position;

                //Points[10] = Edges[3].Points[1].Transform.Position;
                //Points[11] = Edges[3].Derivatives[0].Transform.Position;
                //Points[12] = Edges[2].Derivatives[1].Transform.Position;
                //Points[13] = Edges[2].Derivatives[0].Transform.Position;
                //Points[14] = Edges[1].Derivatives[1].Transform.Position;
                //Points[15] = Edges[1].Points[2].Transform.Position;

                //Points[16] = Edges[2].Points[3].Transform.Position;
                //Points[17] = Edges[2].Points[2].Transform.Position;
                //Points[18] = Edges[2].Points[1].Transform.Position;
                //Points[19] = Edges[2].Points[0].Transform.Position;
            }

            private VertexP[] GetVectorVertices()
            {
                List<VertexP> vertices = new List<VertexP>();
                for (var i = 0; i < 2; i++)
                {
                    Edge edge = Edges[i];
                    for (int j = 0; j < 2; j++)
                    {
                        vertices.Add(new VertexP(edge.Points[j + 1].Transform.Position));
                        vertices.Add(new VertexP(edge.Derivatives[j].Transform.Position));
                    }
                }

                return vertices.ToArray();
            }

            public void DrawVectors()
            {
                if (VectorMesh == null)
                    VectorMesh = GenerateVectorMesh();

                VectorVertices = GetVectorVertices();
                VectorMesh.SetVertices(0, VectorVertices);

                VectorShader.Use();
                VectorShader.Bind(VectorShader.GetUniformLocation("model"), Matrix4.Identity);
                VectorShader.Bind(VectorShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                VectorShader.Bind(VectorShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                VectorShader.Bind(VectorShader.GetUniformLocation("color"), Colors.Red.ColorToVector3());

                VectorMesh.Draw();
            }

            public void Draw()
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
                Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());
                CalculatePoints();
                for (int i = 0; i < 20; i++)
                {
                    Shader.Bind(Shader.GetUniformLocation($"points[{i}]"), Points[i]);
                }
                Mesh.Draw();
            }

            public void Dispose()
            {
                Mesh?.Dispose();
                VectorMesh?.Dispose();
            }
        }

        public void CalculateRibs()
        {
            var avg = Vector3.Zero;
            foreach (GregoryEdge edge in Edges)
            {
                edge.CalculateBoundaryPoints();
                edge.CalculateP3i();
                edge.CalculateP2i();
                edge.CalculateQi();
                avg += edge.Qi.Transform.Position;
            }
            P0 = Simulation.Scene.AddScenePoint(avg / Edges.Count, Colors.Orange, String.Empty, false, false);

            foreach (GregoryEdge edge in Edges)
            {
                edge.CalculateP1i();
            }
        }

        public void CalculateGregoryPatches()
        {
            for (int i = 0; i < Edges.Count; i++)
            {
                var currEdge = Edges[i];
                var nextEdge = Edges[(i + 1) % Edges.Count];
                var nextestEdge = Edges[(i + 2) % Edges.Count];
                var patch = new GregoryPatch(UParameter, VParameter);
                var patchEdge1 = new GregoryPatch.Edge();
                patchEdge1.Points.AddRange(currEdge.GetP3iRightSide());
                patchEdge1.Derivatives.AddRange(currEdge.GetP3iRightSideDerivatives());
                patch.Edges.Add(patchEdge1);

                var patchEdge2 = new GregoryPatch.Edge();
                patchEdge2.Points.AddRange(nextEdge.GetP3iLeftSide());
                patchEdge2.Derivatives.AddRange(nextEdge.GetP3iLeftSideDerivatives());
                patch.Edges.Add(patchEdge2);

                var patchEdge3 = new GregoryPatch.Edge();
                patchEdge3.Points.AddRange(nextEdge.GetP3iToCenter());
                //patchEdge3.Derivatives.AddRange(nextEdge.GetP3iToCenterDerivatives(((currEdge.P1i.Transform.Position - P0.Transform.Position) + (P0.Transform.Position - nextestEdge.P1i.Transform.Position)) / 2));
                var a3 = nextEdge.RandomScale * (P0.Transform.Position - nextestEdge.P1i.Transform.Position);
                var b3 = nextEdge.RandomScale * (currEdge.P1i.Transform.Position - P0.Transform.Position);
                patchEdge3.Derivatives.AddRange(nextEdge.GetP3iToCenterDerivatives(a3, b3));
                patch.Edges.Add(patchEdge3);

                var patchEdge4 = new GregoryPatch.Edge();
                patchEdge4.Points.AddRange(currEdge.GetP3iFromCenter());
                a3 = currEdge.RandomScale * (P0.Transform.Position - nextestEdge.P1i.Transform.Position);
                b3 = currEdge.RandomScale * (nextEdge.P1i.Transform.Position - P0.Transform.Position);
                patchEdge4.Derivatives.AddRange(currEdge.GetP3iFromCenterDerivatives(a3, b3));
                patch.Edges.Add(patchEdge4);

                Patches.Add(patch);
            }
        }

        #region Cycles

        public bool GenerateCycleEdges(List<SceneSurfaceC0Smooth> surfaces)
        {
            var pointsList = new List<SceneObject>();
            foreach (SceneSurfaceC0Smooth surface in surfaces)
            {
                pointsList.AddRange(surface.SurfacePoints);
            }
            var mergePoints = pointsList.GroupBy(x => x).Where(g => g.Count() == 2).SelectMany(g => g).Distinct().ToList();

            if (mergePoints.Count != surfaces.Count)
            {
                MessageBox.Show("Cycle won't close, not enough merge points");
                return false;
            }

            var start = FindCycleStart(surfaces, mergePoints);
            SceneObject end = null;
            SceneSurfaceC0Smooth currSurface = null;
            for (int i = 0; i < mergePoints.Count; i++)
            {
                var surfaceParents = start.Transform.Parents.OfType<SceneSurfaceC0Smooth>().ToList();
                if (surfaceParents.Count != 2)
                {
                    MessageBox.Show("A point named: " + start.ObjectName + " has more than two surface parents");
                    return false;
                }

                if (end == null)
                {
                    currSurface = surfaceParents[0];
                }
                else
                {
                    surfaceParents.Remove(currSurface);
                    currSurface = surfaceParents[0];
                }

                end = FindEdgeEnd(currSurface, start, mergePoints);
                var edge = TraverseToNextPoint(currSurface, start, end);
                if (edge == null)
                    return false;
                Edges.Add(edge);
                start = end;
            }

            if (Edges[0].FirstRowPoints[0] ==
                Edges[Edges.Count - 1].FirstRowPoints[Edges[Edges.Count - 1].FirstRowPoints.Count - 1])
            {
                return true;
            }

            MessageBox.Show("Cycle doesn't close, aborting");
            return false;
        }

        private SceneObject FindCycleStart(List<SceneSurfaceC0Smooth> surfaces, List<SceneObject> mergePoints)
        {
            foreach (SceneSurfaceC0Smooth surface in surfaces)
            {
                foreach (SceneObject mergePoint in mergePoints)
                {
                    if (surface.SurfacePoints.Contains(mergePoint))
                    {
                        return mergePoint;
                    }
                }
            }
            return null;
        }

        private SceneObject FindEdgeEnd(SceneSurfaceC0Smooth surface, SceneObject currentPoint, List<SceneObject> mergePoints)
        {
            foreach (SceneObject child in surface.Transform.Children)
            {
                if (child != currentPoint && mergePoints.Contains(child))
                    return child;
            }
            return null;
        }

        private GregoryEdge TraverseToNextPoint(SceneSurfaceC0Smooth surface, SceneObject startPoint, SceneObject endPoint)
        {
            if (surface.IsUWrapped || surface.IsVWrapped)
            {
                //TODO: For cylinders
            }
            else
            {
                int pointsX = 3 * surface.PatchesX + 1;
                int pointsY = 3 * surface.PatchesY + 1;

                var edge = new GregoryEdge(this);
                Console.WriteLine("-----------------------------");
                //Console.WriteLine(surface.ObjectName);
                //Console.WriteLine(startPoint.ObjectName);
                //Console.WriteLine(endPoint.ObjectName);
                if (surface.SurfacePoints[0] == startPoint)
                {
                    Console.WriteLine("start TL");
                    if (surface.SurfacePoints[pointsX - 1] == endPoint)
                    {
                        Console.WriteLine("end TR");
                        for (int i = 0; i < pointsX; i++)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[i]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[i + pointsX]);
                            Console.WriteLine(surface.SurfacePoints[i].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[i + pointsX].ObjectName);
                        }
                        return edge;
                    }
                    if (surface.SurfacePoints[pointsX * (pointsY - 1)] == endPoint)
                    {
                        Console.WriteLine("end BL");
                        for (int i = 0, j = 0; i < pointsY; i++, j += pointsX)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[j]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[j + 1]);
                            Console.WriteLine(surface.SurfacePoints[j].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[j + 1].ObjectName);
                        }
                        return edge;
                    }

                    {
                        MessageBox.Show("Incorrect end point");
                        return null;
                    }
                }
                if (surface.SurfacePoints[pointsX - 1] == startPoint)
                {
                    Console.WriteLine("start TR");
                    if (surface.SurfacePoints[0] == endPoint)
                    {
                        Console.WriteLine("end TL");
                        for (int i = pointsX - 1; i >= 0; i--)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[i]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[i + pointsX]);
                            Console.WriteLine(surface.SurfacePoints[i].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[i + pointsX].ObjectName);
                        }
                        return edge;
                    }
                    if (surface.SurfacePoints[surface.SurfacePoints.Count - 1] == endPoint)
                    {
                        Console.WriteLine("end BR");
                        for (int i = 0, j = pointsX - 1; i < pointsY; i++, j += pointsY)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[j]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[j - 1]);
                            Console.WriteLine(surface.SurfacePoints[j].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[j - 1].ObjectName);
                        }
                        return edge;
                    }

                    {
                        MessageBox.Show("Incorrect end point");
                        return null;
                    }
                }
                if (surface.SurfacePoints[pointsX * (pointsY - 1)] == startPoint)
                {
                    Console.WriteLine("start BL");
                    if (surface.SurfacePoints[0] == endPoint)
                    {
                        Console.WriteLine("end TL");
                        for (int i = pointsY - 1, j = pointsX * (pointsY - 1); i >= 0; i--, j -= pointsX)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[j]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[j + 1]);
                            Console.WriteLine(surface.SurfacePoints[j].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[j + 1].ObjectName);
                        }
                        return edge;

                    }
                    if (surface.SurfacePoints[surface.SurfacePoints.Count - 1] == endPoint)
                    {
                        Console.WriteLine("end BR");
                        for (int i = pointsX * (pointsY - 1); i <= surface.SurfacePoints.Count - 1; i++)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[i]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[i - pointsX]);
                            Console.WriteLine(surface.SurfacePoints[i].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[i - pointsX].ObjectName);
                        }
                        return edge;
                    }

                    {
                        MessageBox.Show("Incorrect end point");
                        return null;
                    }
                }
                if (surface.SurfacePoints[surface.SurfacePoints.Count - 1] == startPoint)
                {
                    Console.WriteLine("start BR");
                    if (surface.SurfacePoints[pointsX - 1] == endPoint)
                    {
                        Console.WriteLine("end TR");
                        for (int i = pointsY - 1, j = surface.SurfacePoints.Count - 1; i >= 0; i--, j -= pointsX)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[j]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[j - 1]);
                            Console.WriteLine(surface.SurfacePoints[j].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[j - 1].ObjectName);
                        }
                        return edge;
                    }
                    if (surface.SurfacePoints[pointsX * (pointsY - 1)] == endPoint)
                    {
                        Console.WriteLine("end BL");
                        for (int i = surface.SurfacePoints.Count - 1; i >= pointsX * (pointsY - 1); i--)
                        {
                            edge.FirstRowPoints.Add(surface.SurfacePoints[i]);
                            edge.SecondRowPoints.Add(surface.SurfacePoints[i - pointsX]);
                            Console.WriteLine(surface.SurfacePoints[i].ObjectName);
                            Console.WriteLine(surface.SurfacePoints[i - pointsX].ObjectName);
                        }
                        return edge;
                    }
                    {
                        MessageBox.Show("Incorrect end point");
                        return null;
                    }
                }
                MessageBox.Show("Incorrect start point");
                return null;
            }

            return null;
        }

        #endregion

        public SceneGregory(List<SceneSurfaceC0Smooth> surfaces)
        {
            Edges = new List<GregoryEdge>();
            Patches = new List<GregoryPatch>();
            OnDeleteEvent += OnDeleteFunc;
            _surfaces = surfaces;
            foreach (SceneSurfaceC0Smooth surface in _surfaces)
            {
                surface.OnDeleteEvent += SurfaceOnDelete;
            }
        }

        protected override void OnRender()
        {
            Recalculate();
            foreach (GregoryPatch patch in Patches)
            {
                if (DisplayVectors)
                    patch.DrawVectors();
                patch.Draw();
            }
        }

        public void Recalculate()
        {
            var avg = Vector3.Zero;
            foreach (GregoryEdge edge in Edges)
            {
                edge.Recalculate();
                avg += edge.Qi.Transform.Position;
            }

            P0.Transform.Position = avg / Edges.Count;

            for (int i = 0; i < Edges.Count; i++)
            {
                var currEdge = Edges[i];
                var nextEdge = Edges[(i + 1) % Edges.Count];
                var nextestEdge = Edges[(i + 2) % Edges.Count];

                Patches[i].Edges[0].Points[0] = currEdge.P3i;
                Patches[i].Edges[0].Points[1] = currEdge.DividedPointsFirstRow[3];
                Patches[i].Edges[0].Points[2] = currEdge.DividedPointsFirstRow[4];
                Patches[i].Edges[0].Points[3] = currEdge.FirstRowPoints[currEdge.FirstRowPoints.Count - 1];
                Patches[i].Edges[0].Derivatives[0].Transform.Position = currEdge.DividedPointsFirstRow[3].Transform.Position + currEdge.RandomScale * (currEdge.DividedPointsFirstRow[3].Transform.Position - currEdge.DividedPointsSecondRow[3]);
                Patches[i].Edges[0].Derivatives[1].Transform.Position = currEdge.DividedPointsFirstRow[4].Transform.Position + currEdge.RandomScale * (currEdge.DividedPointsFirstRow[4].Transform.Position - currEdge.DividedPointsSecondRow[4]);

                Patches[i].Edges[1].Points[0] = nextEdge.FirstRowPoints[0];
                Patches[i].Edges[1].Points[1] = nextEdge.DividedPointsFirstRow[0];
                Patches[i].Edges[1].Points[2] = nextEdge.DividedPointsFirstRow[1];
                Patches[i].Edges[1].Points[3] = nextEdge.P3i;
                Patches[i].Edges[1].Derivatives[0].Transform.Position = nextEdge.DividedPointsFirstRow[0].Transform.Position + nextEdge.RandomScale * (nextEdge.DividedPointsFirstRow[0].Transform.Position - nextEdge.DividedPointsSecondRow[0]); //TODO: WEIRD SCALING AGAIN
                Patches[i].Edges[1].Derivatives[1].Transform.Position = nextEdge.DividedPointsFirstRow[1].Transform.Position + nextEdge.RandomScale * (nextEdge.DividedPointsFirstRow[1].Transform.Position - nextEdge.DividedPointsSecondRow[1]); //TODO: WEIRD SCALING AGAIN

                Patches[i].Edges[2].Points[0] = nextEdge.P3i;
                Patches[i].Edges[2].Points[1] = nextEdge.P2i;
                Patches[i].Edges[2].Points[2] = nextEdge.P1i;
                Patches[i].Edges[2].Points[3] = P0;
                var a0 = nextEdge.RandomScale * (nextEdge.P3i.Transform.Position - nextEdge.DividedPointsFirstRow[3].Transform.Position);
                var b0 = nextEdge.RandomScale * (nextEdge.DividedPointsFirstRow[1].Transform.Position - nextEdge.P3i.Transform.Position);
                var a3 = nextEdge.RandomScale * (P0.Transform.Position - nextestEdge.P1i.Transform.Position);
                var b3 = nextEdge.RandomScale * (currEdge.P1i.Transform.Position - P0.Transform.Position);
                //var a0 = nextEdge.RandomScale * (((nextEdge.DividedPointsFirstRow[1].Transform.Position - nextEdge.P3i.Transform.Position) + (nextEdge.P3i.Transform.Position - nextEdge.DividedPointsFirstRow[3].Transform.Position)) / 2);
                //var a3 = nextEdge.RandomScale * (((currEdge.P1i.Transform.Position - P0.Transform.Position) + (P0.Transform.Position - nextestEdge.P1i.Transform.Position)) / 2);
                //var a0 = nextEdge.RandomScale * (nextEdge.DividedPointsFirstRow[1].Transform.Position - nextEdge.P3i.Transform.Position);
                //var a3 = nextEdge.RandomScale * (currEdge.P1i.Transform.Position - P0.Transform.Position);
                Patches[i].Edges[2].Derivatives[0].Transform.Position = nextEdge.P2i.Transform.Position + nextEdge.GetD(1 / 3.0f, a0, b0, a3, b3);
                Patches[i].Edges[2].Derivatives[1].Transform.Position = nextEdge.P1i.Transform.Position + nextEdge.GetD(2 / 3.0f, a0, b0, a3, b3);

                Patches[i].Edges[3].Points[0] = P0;
                Patches[i].Edges[3].Points[1] = currEdge.P1i;
                Patches[i].Edges[3].Points[2] = currEdge.P2i;
                Patches[i].Edges[3].Points[3] = currEdge.P3i;
                a0 = currEdge.RandomScale * (currEdge.P3i.Transform.Position - currEdge.DividedPointsFirstRow[1].Transform.Position);
                b0 = currEdge.RandomScale * (currEdge.DividedPointsFirstRow[3].Transform.Position - currEdge.P3i.Transform.Position);
                a3 = currEdge.RandomScale * (P0.Transform.Position - nextestEdge.P1i.Transform.Position);
                b3 = currEdge.RandomScale * (nextEdge.P1i.Transform.Position - P0.Transform.Position);
                //a0 = currEdge.RandomScale * (((currEdge.DividedPointsFirstRow[3].Transform.Position - currEdge.P3i.Transform.Position) + (currEdge.P3i.Transform.Position - currEdge.DividedPointsFirstRow[1].Transform.Position)) / 2);
                //a3 = currEdge.RandomScale * (((nextEdge.P1i.Transform.Position - P0.Transform.Position) + (P0.Transform.Position - nextestEdge.P1i.Transform.Position)) / 2);
                //a0 = currEdge.RandomScale * (currEdge.DividedPointsFirstRow[3].Transform.Position - currEdge.P3i.Transform.Position);
                //a3 = currEdge.RandomScale * (nextEdge.P1i.Transform.Position - P0.Transform.Position);
                Patches[i].Edges[3].Derivatives[0].Transform.Position = currEdge.P1i.Transform.Position + currEdge.GetD(2 / 3.0f, a0, b0, a3, b3);
                Patches[i].Edges[3].Derivatives[1].Transform.Position = currEdge.P2i.Transform.Position + currEdge.GetD(1 / 3.0f, a0, b0, a3, b3);
            }
        }

        private void SurfaceOnDelete()
        {
            Simulation.Scene.RemoveSceneObject(this);
        }

        private void OnDeleteFunc()
        {
            Simulation.Scene.RemoveSceneObject(P0);

            foreach (SceneSurfaceC0Smooth surface in _surfaces)
            {
                surface.OnDeleteEvent -= SurfaceOnDelete;

            }
            foreach (GregoryEdge edge in Edges)
            {
                edge.Dispose();
            }
            foreach (GregoryPatch patch in Patches)
            {
                patch.Dispose();
            }
        }
    }
}
