﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.Serialization;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Models
{
    public class SceneCurveC2Interpolated : SceneCurve
    {
        private List<Tuple<Vector3, Vector3, Vector3, Vector3>> _sections = new List<Tuple<Vector3, Vector3, Vector3, Vector3>>();

        private List<CurveDrawPoint> _drawPoints;
        private List<ScenePoint> _points;


        private List<CurveDrawPoint> CreateDrawPoints()
        {
            _drawPoints.Clear();
            foreach (ScenePoint point in CurvePoints)
            {
                _drawPoints.Add(new CurveDrawPoint(point.Transform.Position, point.Color.ColorToVector3()));
            }
            return _drawPoints;
        }

        public SceneCurveC2Interpolated()
        {
            _drawPoints = new List<CurveDrawPoint>();
            _points = new List<ScenePoint>();
            Transform.OnChildAdd += (child) =>
            {
                if (child is ScenePoint point)
                {
                    if (child is IVirtual v)
                        return;

                    CurvePoints.Add(point);
                    if (CurvePoints.Count < 3)
                        return;
                    CreateDrawPoints();
                    ReinitializePoly(_drawPoints);
                    if (IsSelected)
                        point.IsSelected = true;
                }
            };

            Transform.OnChildRemove += (child) =>
            {
                if (child is ScenePoint point)
                {
                    if (child is IVirtual v)
                        return;

                    CurvePoints.Remove(point);
                    CreateDrawPoints();
                    ReinitializePoly(_drawPoints);
                    if (IsSelected)
                        point.IsSelected = false;
                    if (CurvePoints.Count < 4)
                    {
                        foreach (ScenePoint scenePoint in CurvePoints)
                        {
                            if (IsSelected)
                                scenePoint.IsSelected = false;
                        }
                        Simulation.Scene.RemoveSceneObject(this);
                    }
                }
            };
        }

        public void CreateFromIntersectionCurve(SceneIntersectionCurve curve)
        {
            foreach (Vector3 position in curve.IntersectionData.ScenePoints)
            {
                var point = Simulation.Scene.AddScenePoint(position, Color,
                    ObjectName + $" interpolation point {curve.IntersectionData.ScenePoints.IndexOf(position)}", false, false);
                Transform.ForceAddChild(point);
                OnDeleteEvent += () =>
                {
                    Simulation.Scene.RemoveSceneObject(point);
                };
            }

            if (curve.IntersectionData.IsCurveClosed)
            {
                CurvePoints.Add((ScenePoint)Transform.Children[0]);
                CreateDrawPoints();
                ReinitializePoly(_drawPoints);
            }

            _drawWidth = curve.DrawWidth/2;
        }

        protected override void OnRender()
        {
            if (CurvePoints.Count == 0) //TODO: wczytywanie rzeczy innych tutaj czasami ma zero???
                return;

            _points.Clear();
            _points.Add(CurvePoints[0]);
            for (int i = 1; i < CurvePoints.Count; i++)
            {
                if ((_points.Last().Transform.Position - CurvePoints[i].Transform.Position).LengthSquared >
                    Constants.Epsilon * Constants.Epsilon)
                {
                    _points.Add(CurvePoints[i]);
                }
            }


            if (_points.Count < 4)
                return;

            CalculateAdditionalDrawPoints(_points);

            if (DisplayPoly)
                DrawPoly(CreateDrawPoints());
        }

        private Vector3[] Calculate3DiagonalMatrix(float[] upperDiagonal, float[] middleDiagonal, float[] lowerDiagonal, Vector3[] rhsVector, float[] distArray)
        {
            float[] c = new float[rhsVector.Length - 1];
            Vector3[] d = new Vector3[rhsVector.Length];
            Vector3[] cArray = new Vector3[rhsVector.Length];
            c[0] = upperDiagonal[0] / middleDiagonal[0];

            for (int i = 1; i < c.Length; i++)
            {
                c[i] = (upperDiagonal[i] / (middleDiagonal[i] - lowerDiagonal[i - 1] * c[i - 1]));
            }
            d[0] = rhsVector[0] / middleDiagonal[0];
            for (int i = 1; i < d.Length; i++)
            {
                d[i] = (rhsVector[i] - lowerDiagonal[i - 1] * d[i - 1]) / (middleDiagonal[i] - lowerDiagonal[i - 1] * c[i - 1]);
            }
            cArray[cArray.Length - 1] = d[cArray.Length - 1];
            for (int i = cArray.Length - 2; i >= 0; i--)
            {
                cArray[i] = d[i] - (c[i] * cArray[i + 1]);
            }
            return cArray;
        }

        private float CalculateDistance(Vector3 vectorOne, Vector3 vectorTwo)
        {
            return (vectorOne - vectorTwo).Length;
        }

        private float[] CalculateDistanceArray(IList<ScenePoint> points)
        {
            float[] distArray = new float[points.Count - 1];
            for (int i = 0; i < distArray.Length; i++)
            {
                distArray[i] = CalculateDistance(points[i + 1].Transform.Position, points[i].Transform.Position);
            }

            return distArray;
        }

        private Vector3[] CalculateDCoefficient(Vector3[] cArray, float[] distArray)
        {
            Vector3[] dArray = new Vector3[cArray.Length - 1];
            for (int i = 0; i < cArray.Length - 1; i++)
            {
                dArray[i] = (cArray[i + 1] - cArray[i]) / (3 * distArray[i]);
            }
            return dArray;
        }

        private Vector3[] CalculateBCoefficient(List<ScenePoint> points, Vector3[] cArray, Vector3[] dArray, float[] distArray)
        {
            Vector3[] bArray = new Vector3[cArray.Length - 1];
            for (int i = 0; i < cArray.Length - 1; i++)
            {
                bArray[i] = (points[i + 1].Transform.Position - points[i].Transform.Position - cArray[i] * (distArray[i] * distArray[i]) - dArray[i] * (distArray[i] * distArray[i] * distArray[i])) / distArray[i];
            }

            return bArray;
        }

        private Tuple<Vector3, Vector3, Vector3, Vector3> PowerToBernstein(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            return new Tuple<Vector3, Vector3, Vector3, Vector3>(a, a + (1 / 3f) * b, a + (2 / 3f) * b + (1 / 3f) * c, a + b + c + d);
        }

        private void CalculateAdditionalDrawPoints(List<ScenePoint> points)
        {
            float[] upperDiagonal = new float[points.Count - 3];
            float[] middleDiagonal = new float[points.Count - 2];
            float[] lowerDiagonal = new float[points.Count - 3];
            Vector3[] rhsVector = new Vector3[points.Count - 2];
            float[] distArray = CalculateDistanceArray(points);

            for (int i = 0; i < lowerDiagonal.Length; i++)
            {
                lowerDiagonal[i] = distArray[i + 1] / (distArray[i + 1] + distArray[i + 2]);
            }

            for (int i = 0; i < middleDiagonal.Length; i++)
            {
                middleDiagonal[i] = 2;
            }

            for (int i = 0; i < upperDiagonal.Length; i++)
            {
                upperDiagonal[i] = distArray[i + 1] / (distArray[i] + distArray[i + 1]);
            }

            for (int i = 0; i < rhsVector.Length; i++)
            {
                rhsVector[i] = 3 * ((points[i + 2].Transform.Position - points[i + 1].Transform.Position) / distArray[i + 1] - (points[i + 1].Transform.Position - points[i].Transform.Position) / distArray[i]) / (distArray[i] + distArray[i + 1]);
            }

            Vector3[] cTempArray = Calculate3DiagonalMatrix(upperDiagonal, middleDiagonal, lowerDiagonal, rhsVector, distArray);
            Vector3[] cArray = new Vector3[cTempArray.Length + 2];
            cArray[0] = Vector3.Zero;
            cArray[cArray.Length - 1] = Vector3.Zero;
            for (int i = 0; i < cTempArray.Length; i++)
            {
                cArray[i + 1] = cTempArray[i];
            }
            Vector3[] dArray = CalculateDCoefficient(cArray, distArray);
            Vector3[] bArray = CalculateBCoefficient(points, cArray, dArray, distArray);
            _sections.Clear();
            for (int i = 0; i < bArray.Length; i++)
            {
                _sections.Add(PowerToBernstein(points[i].Transform.Position, bArray[i] * distArray[i], cArray[i] * distArray[i] * distArray[i], dArray[i] * distArray[i] * distArray[i] * distArray[i]));
            }
            for (int i = 0; i < _sections.Count; i++)
            {
                _drawPoints.Clear();
                _drawPoints.Add(new CurveDrawPoint(_sections[i].Item1, Vector3.One));
                _drawPoints.Add(new CurveDrawPoint(_sections[i].Item2, Vector3.One));
                _drawPoints.Add(new CurveDrawPoint(_sections[i].Item3, Vector3.One));
                _drawPoints.Add(new CurveDrawPoint(_sections[i].Item4, Vector3.One));
                DrawC0(_drawPoints);
            }
        }

        public void Deserialize(curveC2I serializedCurveC2I)
        {
            foreach (int point in serializedCurveC2I.pointsId)
            {
                CurvePoints.Add((ScenePoint)Simulation.Scene.SceneObjects[point]);
            }
            ObjectName = serializedCurveC2I.name;
        }

        public curveC2I Serialize(List<ScenePoint> points)
        {
            var curveC2I = new curveC2I();
            curveC2I.name = ObjectName;
            curveC2I.pointsId = new int[CurvePoints.Count];
            for (int i = 0; i < CurvePoints.Count; i++)
            {
                curveC2I.pointsId[i] = points.IndexOf(CurvePoints[i]);
            }

            return curveC2I;
        }
    }
}
