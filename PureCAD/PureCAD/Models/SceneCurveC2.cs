﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.Serialization;

namespace PureCAD.Models
{
    public class SceneCurveC2 : SceneCurve
    {
        public bool DisplayBernsteinPoints
        {
            get { return _displayBernsteinPoints; }
            set
            {
                _displayBernsteinPoints = value;
                foreach (BernsteinPoint point in _bernsteinPoints)
                {
                    point.IsVisible = value;
                }
                RaisePropertyChanged();
            }
        }

        private bool _calculatingPositions = false;
        private bool _deBoorMoving = false;
        private bool _bernsteinMoving = false;

        private List<CurveDrawPoint> _drawPoints;
        private List<BernsteinPoint> _bernsteinPoints;
        private bool _displayBernsteinPoints;

        protected override void OnRender()
        {
            if (CurvePoints.Count < 4)
                return;

            if (DisplayPoly)
                DrawPoly(_drawPoints);

            DrawC0(_drawPoints);
        }

        public SceneCurveC2()
        {
            _drawPoints = new List<CurveDrawPoint>();
            _bernsteinPoints = new List<BernsteinPoint>();

            OnDeleteEvent += delegate
            {
                foreach (BernsteinPoint sceneObject in _bernsteinPoints)
                {
                    Simulation.Scene.RemoveSceneObject(sceneObject);
                }
            };

            Transform.OnChildAdd += (child) =>
            {
                if (child is ScenePoint point)
                {
                    if (child is IVirtual v)
                        return;

                    CurvePoints.Add(point);
                    point.Transform.OnPositionUpdate += OnDeBoorMove;
                    if (CurvePoints.Count < 3)
                        return;

                    CalculateBernstainFromDeBoor(-1);
                    ReinitializePoly(_drawPoints);
                    if (IsSelected)
                        point.IsSelected = true;
                }
            };

            Transform.OnChildRemove += (child) =>
            {
                if (child is ScenePoint point)
                {
                    if (child is IVirtual v)
                        return;

                    CurvePoints.Remove(point);
                    point.Transform.OnPositionUpdate -= OnDeBoorMove;
                    CalculateBernstainFromDeBoor(-1);
                    ReinitializePoly(_drawPoints);
                    if (IsSelected)
                        point.IsSelected = false;
                    if (CurvePoints.Count < 4)
                    {
                        foreach (ScenePoint scenePoint in CurvePoints)
                        {
                            if (IsSelected)
                                scenePoint.IsSelected = false;
                        }
                        Simulation.Scene.RemoveSceneObject(this);
                    }
                }
            };
        }

        private void CalculateBernstainFromDeBoor(int omit)
        {
            _calculatingPositions = true;
            List<Vector3> controlPoints = new List<Vector3>();
            _drawPoints.Clear();

            for (int i = 0; i < CurvePoints.Count - 1; i++)
            {
                var direction = new Vector3(CurvePoints[i + 1].Transform.Position - CurvePoints[i].Transform.Position);
                controlPoints.Add(CurvePoints[i].Transform.Position + direction * 1.0f / 3.0f);
                controlPoints.Add(CurvePoints[i].Transform.Position + direction * 2.0f / 3.0f);
            }

            _drawPoints.Add(new CurveDrawPoint((controlPoints[2] + controlPoints[1]) / 2, Color.ColorToVector3()));
            for (int i = 0; i < CurvePoints.Count - 3; i++)
            {
                _drawPoints.Add(new CurveDrawPoint(controlPoints[3 * i + 2 - i], Color.ColorToVector3()));
                _drawPoints.Add(new CurveDrawPoint(controlPoints[3 * i + 3 - i], Color.ColorToVector3()));
                _drawPoints.Add(new CurveDrawPoint((controlPoints[3 * i + 4 - i] + controlPoints[3 * i + 3 - i]) / 2, Color.ColorToVector3()));
            }

            if (_bernsteinPoints.Count != _drawPoints.Count)
                CreateBernsteinPointModels();

            if (_bernsteinPoints.Count > 0)
            {
                for (int i = 0; i < _bernsteinPoints.Count; i++)
                {
                    if (i != omit)
                        _bernsteinPoints[i].Transform.Position = _drawPoints[i].Position;
                }
            }
            _calculatingPositions = false;
        }

        private void OnDeBoorMove(SceneObject obj, Vector3 from, Vector3 to)
        {
            if (_bernsteinMoving)
                return;
            _deBoorMoving = true;
            CalculateBernstainFromDeBoor(-1);
            _deBoorMoving = false;
        }

        private void OnBernsteinMove(SceneObject obj, Vector3 from, Vector3 to)
        {
            if (_deBoorMoving || _calculatingPositions)
                return;
            _bernsteinMoving = true;
            int bernsteinIndex = _bernsteinPoints.IndexOf((BernsteinPoint)obj);
            int closestIndex = (bernsteinIndex + 1) / 3 + 1;
            switch (bernsteinIndex % 3)
            {
                case 0:
                    CurvePoints[closestIndex].Transform.Position = 3 / 2f * obj.Transform.Position - 1 / 4f * CurvePoints[closestIndex - 1].Transform.Position - 1 / 4f * CurvePoints[closestIndex + 1].Transform.Position;
                    break;
                case 1:
                    CurvePoints[closestIndex].Transform.Position = 3 / 2f * obj.Transform.Position - 1 / 2f * CurvePoints[closestIndex + 1].Transform.Position;
                    break;
                case 2:
                    CurvePoints[closestIndex].Transform.Position = 3 / 2f * obj.Transform.Position - 1 / 2f * CurvePoints[closestIndex - 1].Transform.Position;
                    break;
            }
            CalculateBernstainFromDeBoor(bernsteinIndex);
            _bernsteinMoving = false;
        }

        private void ClearBernsteinPointModels()
        {
            foreach (BernsteinPoint point in _bernsteinPoints)
            {
                point.Transform.OnPositionUpdate -= OnBernsteinMove;
                Simulation.Scene.RemoveSceneObject(point);
            }
            _bernsteinPoints.Clear();

        }

        public void AddBernsteinPoint(BernsteinPoint point)
        {
            _bernsteinPoints.Add(point);
            point.Transform.OnPositionUpdate += OnBernsteinMove;
            Transform.ForceAddChild(point);
        }

        private void CreateBernsteinPointModels()
        {
            ClearBernsteinPointModels();
            for (int i = 0; i < _drawPoints.Count; i++)
            {
                Simulation.Scene.AddBernsteinPoint(this, _drawPoints[i].Position);
            }
        }

        public void Deserialize(curveC2 serializedCurveC2)
        {
            foreach (int point in serializedCurveC2.pointsId)
            {
                Transform.ForceAddChild((ScenePoint)Simulation.Scene.SceneObjects[point]);
            }
            ObjectName = serializedCurveC2.name;
            CalculateBernstainFromDeBoor(-1);
            ReinitializePoly(_drawPoints);
        }

        public curveC2 Serialize(List<ScenePoint> points)
        {
            var curveC2 = new curveC2();
            curveC2.name = ObjectName;
            curveC2.pointsId = new int[CurvePoints.Count];
            for (int i = 0; i < CurvePoints.Count; i++)
            {
                curveC2.pointsId[i] = points.IndexOf(CurvePoints[i]);
            }

            return curveC2;
        }

        
    }
}
