﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;
using PureCAD.Utils.Math;

namespace PureCAD.Models.PathGeneration
{
    public abstract class StaticSurface : Model, IIntersectable
    {
        public class StaticSurfacePatch
        {
            public int PatchU = 0;
            public int PatchV = 0;
            public List<Vector3> PatchPoints;
            public PatchMesh<VertexP> PatchMesh;
            public Color Color { get; set; } = Colors.Red;
            public StaticSurfacePatch(int u, int v)
            {
                PatchU = u;
                PatchV = v;
                PatchPoints = new List<Vector3>();
            }

            public void AddPointToPatch(Vector3 point)
            {
                PatchPoints.Add(point);
            }

            public void GeneratePatchMesh()
            {
                List<VertexP> vertices = new List<VertexP>();

                foreach (Vector3 point in PatchPoints)
                {
                    vertices.Add(new VertexP(point));
                }

                PatchMesh = new PatchMesh<VertexP>(vertices, null, 16);
            }

            public void Dispose()
            {
                PatchMesh.Dispose();
            }
        }

        protected int UParameter = 4;
        protected int VParameter = 4;
        public Color Color { get; set; } = Colors.White;
        public List<StaticSurfacePatch> SurfacePatches;
        public List<Vector3> SurfacePoints;
        public Mesh<VertexP> SpheresMesh;
        public Shader SpheresShader;
        public bool DrawSphereMesh = true;

        public bool IsUWrapped { get; set; }
        public bool IsVWrapped { get; set; }
        

        public float MillRadius = 0;
        public float Margin = 0;

        protected int PatchesX;
        protected int PatchesY;
        
        protected abstract void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY);

        public float EdgeDivision = 0.5f;
        public float InnerDivision = 0.25f;

        protected StaticSurface()
        {
            SurfacePatches = new List<StaticSurfacePatch>();
            SurfacePoints = new List<Vector3>();
            SpheresShader = Shaders.BasicGeomSpheresShader;
        }

        #region Render

        //protected void RegenerateMesh()
        //{
        //    Mesh = GenerateSurfaceMesh();
        //}

        //private PatchMesh<Vertex2DP> GenerateSurfaceMesh()
        //{
        //    List<Vertex2DP> vertices = new List<Vertex2DP>();
        //    List<uint> edges = new List<uint>();
        //    float distX = 1f / (UParameter - 1);
        //    float distY = 1f / (UParameter - 1);
        //    for (int i = 0; i < VParameter; i++)
        //    {
        //        for (int j = 0; j < UParameter; j++)
        //        {
        //            vertices.Add(new Vertex2DP(j * distX, i * distY));
        //        }
        //    }

        //    for (int i = 0; i < VParameter; i++)
        //    {
        //        for (int j = 0; j < UParameter; j++)
        //        {
        //            vertices.Add(new Vertex2DP(j * distX, i * distY));
        //            if (j != 0)
        //            {
        //                edges.Add((uint)(j + i * UParameter));
        //                edges.Add((uint)((j - 1) + i * UParameter));
        //            }
        //            if (i != 0)
        //            {
        //                edges.Add((uint)(j + i * UParameter));
        //                edges.Add((uint)(j + (i - 1) * UParameter));
        //            }
        //        }
        //    }
        //    return new PatchMesh<Vertex2DP>(vertices, edges, 2);
        //}

        public void CreateSphereMesh(List<Vector2d> uv, float levelHeight)
        {
            var vertices = new List<VertexP>();
            foreach (Vector2d vec2 in uv)
            {
                var pos = EvaluateAt(vec2).ToVector3();
                //pos.Y = levelHeight;
                var vert = new VertexP(pos);
                vertices.Add(vert);
            }

            SpheresMesh = new Mesh<VertexP>(vertices, null, MeshType.Points, AccessType.Static);
        }

        protected override void OnRender()
        {
            for (int i = 0; i < PatchesY; i++)
            {
                for (int j = 0; j < PatchesX; j++)
                {
                    StaticSurfacePatch surfacePatch = SurfacePatches[j + i * PatchesX];

                    //for (int k = 0; k < 16; k++)
                    //{
                    //    Shader.Bind(Shader.GetUniformLocation($"points[{k}]"), SurfacePatches[j + i * PatchesX].PatchPoints[k]);
                    //}

                    Shader.Use();
                    Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
                    Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                    Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                    Shader.Bind(Shader.GetUniformLocation("color"), surfacePatch.Color.ColorToVector3());
                    Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
                    Shader.Bind(Shader.GetUniformLocation("edgeDivision"), EdgeDivision);
                    Shader.Bind(Shader.GetUniformLocation("innerDivision"), InnerDivision);
                    Shader.Bind(Shader.GetUniformLocation("uOffset"), j);
                    Shader.Bind(Shader.GetUniformLocation("vOffset"), i);
                    Shader.Bind(Shader.GetUniformLocation("millRadius"), MillRadius);
                    Shader.Bind(Shader.GetUniformLocation("margin"), Margin);

                    surfacePatch.PatchMesh.Draw();
                }
            }

            if (DrawSphereMesh && SpheresMesh != null)
            {
                SpheresShader.Use();
                SpheresShader.Bind(SpheresShader.GetUniformLocation("model"), Transform.GetModelMatrix());
                SpheresShader.Bind(SpheresShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                SpheresShader.Bind(SpheresShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                SpheresShader.Bind(SpheresShader.GetUniformLocation("color"), Colors.Lime.ColorToVector3());
                SpheresShader.Bind(SpheresShader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
                SpheresShader.Bind(SpheresShader.GetUniformLocation("radius"), MillRadius + Margin);
                SpheresMesh.Draw();
            }
        }

        #endregion


        #region Logic

        protected abstract Vector3d SurfaceCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d);
        protected abstract Vector3d DerivativeCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d);

        public Vector2d ApproximateAt(Vector3 worldPosition)
        {
            double distX = 1 / 3f;
            double distY = 1 / 3f;
            double minDist = double.PositiveInfinity;
            Vector2d pointUV = new Vector2d();
            var worldPosD = worldPosition.ToVector3d();
            for (int i = 0; i < 3 * PatchesY + 1; i++)
            {
                for (int j = 0; j < 3 * PatchesX + 1; j++)
                {
                    var param = new Vector2d(j * distX, i * distY);
                    Vector3d surfacePoint = EvaluateAt(param);
                    double dist = (worldPosD - surfacePoint).Length;
                    if (dist < minDist)
                    {
                        pointUV = param;
                        minDist = dist;
                    }
                }
            }

            return pointUV;
        }

        public Vector2d ApproximateAt(Vector3 worldPosition, float offset)
        {
            double distX = 1 / 3f;
            double distY = 1 / 3f;
            double minDist = double.PositiveInfinity;
            Vector2d pointUV = new Vector2d();
            var worldPosD = worldPosition.ToVector3d();
            for (int i = 0; i < 3 * PatchesY + 1; i++)
            {
                for (int j = 0; j < 3 * PatchesX + 1; j++)
                {
                    var param = new Vector2d(j * distX, i * distY);
                    Vector3d surfacePoint = EvaluateAt(param, offset);
                    double dist = (worldPosD - surfacePoint).Length;
                    if (dist < minDist)
                    {
                        pointUV = param;
                        minDist = dist;
                    }
                }
            }

            return pointUV;
        }

        private StaticSurfacePatch GetPatchFromGlobalUV(Vector2d param)
        {
            int u = (int)param.X;
            int v = (int)param.Y;

            if (u < 0)
                u = 0;
            else if (u >= PatchesX)
                u = PatchesX - 1;
            if (v >= PatchesY)
                v = PatchesY - 1;
            else if (v < 0)
                v = 0;

            return SurfacePatches.Find(x => x.PatchU == u && x.PatchV == v);
        }

        private Vector2d TransformToPatchUV(StaticSurfacePatch patch, Vector2d param)
        {
            return new Vector2d(param.X - patch.PatchU, param.Y - patch.PatchV);
        }

        public Vector3d EvaluateAt(Vector2d uv)
        {
            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var valueOne = SurfaceCalculation(patchUV.Y, patch.PatchPoints[0].ToVector3d(), patch.PatchPoints[4].ToVector3d(), patch.PatchPoints[8].ToVector3d(), patch.PatchPoints[12].ToVector3d());
            var valueTwo = SurfaceCalculation(patchUV.Y, patch.PatchPoints[1].ToVector3d(), patch.PatchPoints[5].ToVector3d(), patch.PatchPoints[9].ToVector3d(), patch.PatchPoints[13].ToVector3d());
            var valueThree = SurfaceCalculation(patchUV.Y, patch.PatchPoints[2].ToVector3d(), patch.PatchPoints[6].ToVector3d(), patch.PatchPoints[10].ToVector3d(), patch.PatchPoints[14].ToVector3d());
            var valueFour = SurfaceCalculation(patchUV.Y, patch.PatchPoints[3].ToVector3d(), patch.PatchPoints[7].ToVector3d(), patch.PatchPoints[11].ToVector3d(), patch.PatchPoints[15].ToVector3d());

            return SurfaceCalculation(patchUV.X, valueOne, valueTwo, valueThree, valueFour);
        }

        public Vector3d EvaluateAt(Vector2d uv, float offset)
        {
            var scenePoint = EvaluateAt(uv);
            var derivU = EvaluateUDerivativeAt(uv);
            var derivV = EvaluateVDerivativeAt(uv);
            var normal = Vector3d.Cross(derivU, derivV).Normalized();

            return scenePoint + normal * offset;
        }

        public Vector3d EvaluateUDerivativeAt(Vector2d uv)
        {
            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var deriv0 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[0].ToVector3d(), patch.PatchPoints[4].ToVector3d(), patch.PatchPoints[8].ToVector3d(), patch.PatchPoints[12].ToVector3d());
            var deriv1 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[1].ToVector3d(), patch.PatchPoints[5].ToVector3d(), patch.PatchPoints[9].ToVector3d(), patch.PatchPoints[13].ToVector3d());
            var deriv2 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[2].ToVector3d(), patch.PatchPoints[6].ToVector3d(), patch.PatchPoints[10].ToVector3d(), patch.PatchPoints[14].ToVector3d());
            var deriv3 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[3].ToVector3d(), patch.PatchPoints[7].ToVector3d(), patch.PatchPoints[11].ToVector3d(), patch.PatchPoints[15].ToVector3d());

            return DerivativeCalculation(patchUV.X, deriv0, deriv1, deriv2, deriv3);
        }

        public Vector3d EvaluateUDerivativeAt(Vector2d uv, float offset)
        {
            var diffM = new Vector2d(uv.X - Constants.Epsilon, uv.Y);
            IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffM.X);
            var diffP = new Vector2d(uv.X + Constants.Epsilon, uv.Y);
            IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffP.X);
            var evalM = EvaluateAt(diffM, offset);
            var evalP = EvaluateAt(diffP, offset);

            return (evalM + evalP) / (2 * Constants.Epsilon);
        }
        
        public Vector3d EvaluateVDerivativeAt(Vector2d uv)
        {
            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var deriv0 = SurfaceCalculation(patchUV.X, patch.PatchPoints[0].ToVector3d(), patch.PatchPoints[1].ToVector3d(), patch.PatchPoints[2].ToVector3d(), patch.PatchPoints[3].ToVector3d());
            var deriv1 = SurfaceCalculation(patchUV.X, patch.PatchPoints[4].ToVector3d(), patch.PatchPoints[5].ToVector3d(), patch.PatchPoints[6].ToVector3d(), patch.PatchPoints[7].ToVector3d());
            var deriv2 = SurfaceCalculation(patchUV.X, patch.PatchPoints[8].ToVector3d(), patch.PatchPoints[9].ToVector3d(), patch.PatchPoints[10].ToVector3d(), patch.PatchPoints[11].ToVector3d());
            var deriv3 = SurfaceCalculation(patchUV.X, patch.PatchPoints[12].ToVector3d(), patch.PatchPoints[13].ToVector3d(), patch.PatchPoints[14].ToVector3d(), patch.PatchPoints[15].ToVector3d());

            return DerivativeCalculation(patchUV.Y, deriv0, deriv1, deriv2, deriv3);
        }

        public Vector3d EvaluateVDerivativeAt(Vector2d uv, float offset)
        {
            var diffM = new Vector2d(uv.X, uv.Y - Constants.Epsilon);
            IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffM.Y);
            var diffP = new Vector2d(uv.X, uv.Y + Constants.Epsilon);
            IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffP.Y);
            var evalM = EvaluateAt(diffM, offset);
            var evalP = EvaluateAt(diffP, offset);

            return (evalM + evalP) / (2 * Constants.Epsilon);
        }

        public double GetURange()
        {
            return PatchesX;
        }

        public double GetVRange()
        {
            return PatchesY;
        }

        public int GetUCurveCount()
        {
            return UParameter * PatchesX;
        }

        public int GetVCurveCount()
        {
            return VParameter * PatchesY;
        }

        public void SetTrimmingTexture(int texId)
        {
            throw new NotImplementedException();
        }

        public void SetTrimmingSide(int sideId)
        {
            throw new NotImplementedException();
        }

        

        public override void Dispose()
        {
            foreach (StaticSurfacePatch staticSurfacePatch in SurfacePatches)
            {
                staticSurfacePatch.Dispose();
            }
            SpheresMesh?.Dispose();
        }

        #endregion
    }
}
