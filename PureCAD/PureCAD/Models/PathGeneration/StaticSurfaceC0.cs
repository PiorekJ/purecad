﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils.Serialization;

namespace PureCAD.Models.PathGeneration
{
    public class StaticSurfaceC0 : StaticSurface
    {
        public StaticSurfaceC0()
        {
            Shader = Shaders.BasicTesselationShaderC0;
        }

        protected override void CreatePlaneSurfacePoints(float width, float height, int patchesCountX,
            int patchesCountY)
        {
            int pointsX = 3 * patchesCountX + 1;
            int pointsY = 3 * patchesCountY + 1;

            float distX = width / (pointsX - 1);
            float distY = height / (pointsY - 1);
            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    SurfacePoints.Add(new Vector3(i * distX, 0, j * distY));
                }
            }
        }

        protected override void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX,
            int patchesCountY)
        {
            int pointsX = 3 * patchesCountX;
            int pointsY = 3 * patchesCountY + 1;
            float angle = 360f / pointsX;

            float distY = height / (pointsY - 1);

            IsUWrapped = true;

            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    float yCoord = (float)(Math.Cos(i * angle * Math.PI / 180f) * radius);
                    float zCoord = (float)(Math.Sin(i * angle * Math.PI / 180f) * radius);
                    SurfacePoints.Add(new Vector3(j * distY, yCoord, zCoord));
                }
            }
        }

        protected override void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY)
        {
            SurfacePatches.Clear();
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    StaticSurfacePatch surfacePatch = new StaticSurfacePatch(j, i);
                    int cornerIndexX = j * 3;
                    int cornerIndexY = i * 3;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;

                            surfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (3 * patchesCountX + 1)]);
                        }
                    }
                    SurfacePatches.Add(surfacePatch);
                    surfacePatch.GeneratePatchMesh();
                }
            }
        }

        protected override void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY)
        {
            SurfacePatches.Clear();
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    StaticSurfacePatch surfacePatch = new StaticSurfacePatch(j, i);
                    int cornerIndexX = j * 3;
                    int cornerIndexY = i * 3;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;
                            if (pointX == patchesCountX * 3)
                                pointX = 0;

                            surfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (3 * patchesCountX)]);
                        }
                    }
                    SurfacePatches.Add(surfacePatch);
                    surfacePatch.GeneratePatchMesh();
                }
            }
        }

        protected override Vector3d SurfaceCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d)
        {
            var paramT = 1 - t;

            var r00 = a * paramT + b * t;
            var r10 = b * paramT + c * t;
            var r20 = c * paramT + d * t;

            var r01 = r00 * paramT + r10 * t;
            var r11 = r10 * paramT + r20 * t;

            return r01 * paramT + r11 * t;
        }

        protected override Vector3d DerivativeCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d)
        {
            var paramT = 1 - t;

            var deriv0 = 3 * (b - a);
            var deriv1 = 3 * (c - b);
            var deriv2 = 3 * (d - c);

            var r00 = deriv0 * paramT + deriv1 * t;
            var r10 = deriv1 * paramT + deriv2 * t;

            return r00 * paramT + r10 * t;
        }

        public void Deserialize(surfaceC0 surfaceC0, List<Vector3> points)
        {
            ObjectName = surfaceC0.name;
            PatchesX = (surfaceC0.points.GetLength(0) - 1) / 3;
            PatchesY = (surfaceC0.points.GetLength(1) - 1) / 3;
            UParameter = surfaceC0.u;
            VParameter = surfaceC0.v;

            IsUWrapped = surfaceC0.cylinder;

            for (var y = 0; y < surfaceC0.points.GetLength(1); y++)
            {
                for (var x = 0; x < surfaceC0.points.GetLength(0); x++)
                {
                    int surfaceC0Point = surfaceC0.points[x, y];
                    var point = points[surfaceC0Point];
                    SurfacePoints.Add(point);
                }
            }
            CreatePlaneSurfacePatches(PatchesX, PatchesY);

            if (surfaceC0.cylinder)
            {
                SurfacePoints.Clear();

                for (var y = 0; y < surfaceC0.points.GetLength(1); y++)
                {
                    for (var x = 0; x < surfaceC0.points.GetLength(0); x++)
                    {
                        int surfaceC0Point = surfaceC0.points[x, y];
                        var point = points[surfaceC0Point];
                        if (!SurfacePoints.Contains(point))
                            SurfacePoints.Add(point);
                    }
                }
            }
            //RegenerateMesh();
        }
    }
}
