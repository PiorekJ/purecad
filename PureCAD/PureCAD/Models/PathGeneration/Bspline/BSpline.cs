﻿using System;
using System.Linq;
using System.Linq.Expressions;
using OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models.PathGeneration.Bspline
{
    public class BSpline : Curve
    {
        private static uint count;

        private Vector3[] ControlPoints;

        public BSpline(Vector3[] points, string name = null) : base(points)
        {
            GenerateControlPoints();
            MaxU = Points.Count - 3;
        }

        private void GenerateControlPoints()
        {
            if (Points.Count > 3)
            {
                ControlPoints = new Vector3[3 * (Points.Count - 2) - 2];
                SetControlPointsPosition();
            }
            else
                ControlPoints = new Vector3[0];
        }

        private void SetControlPointsPosition()
        {
            int index = -1;
            for (int k = 1; k < Points.Count - 1; k++)
            {
                var p1 = (2 * Points[k] + Points[k - 1]) / 3;
                var p2 = (2 * Points[k] + Points[k + 1]) / 3;
                if (index >= 0) ControlPoints[index++] = p1;
                else
                    index++;
                ControlPoints[index++] = (p1 + p2) / 2;
                if (index < ControlPoints.Length) ControlPoints[index++] = p2;
            }
        }

        public override Vector3 GetValue(float u)
        {
            if (float.IsNaN(u) || u > MaxU || u < 0) return Vector3.Zero;

            int x = (int)u;
            if (x != 0 && Math.Abs(x - Math.Ceiling(u)) < Constants.Epsilon)
            {
                u = 1.0f;
                x--;
            }
            else
                u = u % 1.0f;

            Vector3[] temp = ControlPoints.Skip(3 * x).Take(4).ToArray();


            return Invoke(u, temp);
        }


        public static Vector3 Invoke(float t, params Vector3[] points)
        {
            float u = 1 - t;
            int n = points.Length;
            Vector3[][] b = new Vector3[n][];
            b[0] = points;

            for (int i = 1; i < n; i++)
                b[i] = new Vector3[n - i];

            for (int j = 1; j < n; j++)
            for (int i = 0; i < n - j; i++)
                b[j][i] = (u * b[j - 1][i] + t * b[j - 1][i + 1]);

            return b[n - 1][0];
        }
    }
}
