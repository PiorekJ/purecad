﻿using System.Collections.ObjectModel;
using OpenTK;

namespace PureCAD.Models.PathGeneration.Bspline
{
    public abstract class Curve
    {
        public float MaxU { get; protected set; }

        public ObservableCollection<Vector3> Points
        {
            get; private set;
        }

        public Curve(Vector3[] points)
        {
            Points = new ObservableCollection<Vector3>(points);
        }

        public abstract Vector3 GetValue(float u);
    }
}
