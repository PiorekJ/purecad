﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class SceneDebugMesh : Model, IColorable
    {
        public Color Color { get; set; } = Colors.Red;

        public int GLLineWidth = 1;
        public int GLPointSize = 1;

        public SceneDebugMesh()
        {
            Shader = Shaders.BasicShader;
        }

        public void SetMesh<T>(IEnumerable<T> vertices, IEnumerable<uint> edges, MeshType type)
            where T : struct, IVertex
        {
            Mesh = new Mesh<T>(vertices, edges, type, AccessType.Static);
            Simulation.Scene.SceneObjects.Add(this);
        }

        public void SetMesh<T>(Mesh<T> mesh)
            where T : struct, IVertex
        {
            Mesh = mesh;
            Simulation.Scene.SceneObjects.Add(this);
        }

        protected override void OnRender()
        {
            if (Mesh == null)
                return;

            GL.LineWidth(GLLineWidth);
            GL.PointSize(GLPointSize);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Mesh.Draw();
            GL.LineWidth(1);
            GL.PointSize(1);
        }

    }
}

