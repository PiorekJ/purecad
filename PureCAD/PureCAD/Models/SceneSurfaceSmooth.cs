﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Popups;
using PureCAD.Utils;
using PureCAD.Utils.Math;

namespace PureCAD.Models
{
    public abstract class SceneSurfaceSmooth : Model, ISelectable, IColorable, IFilterable, IIntersectable
    {
        public bool IsSelected { get; set; }
        public Color Color { get; set; } = Colors.White;
        public Color PolyColor { get; set; } = Colors.Red;

        private int _uParameter = 4;
        private int _vParameter = 4;
        private bool _displayPoly;
        private bool _displayPoints = true;

        public int PatchesX;
        public int PatchesY;

        public bool IsUWrapped { get; set; }
        public bool IsVWrapped { get; set; }

        public int UParameter
        {
            get { return _uParameter; }
            set
            {
                if (value > 1)
                {
                    _uParameter = value;
                    RegenerateMesh();
                }
            }
        }

        public int VParameter
        {
            get { return _vParameter; }
            set
            {
                if (value > 1)
                {
                    _vParameter = value;
                    RegenerateMesh();
                }
            }
        }

        public bool DisplayPoly
        {
            get { return _displayPoly; }
            set
            {
                _displayPoly = value;
                RaisePropertyChanged();
            }
        }

        public bool DisplayPoints
        {
            get { return _displayPoints; }
            set
            {
                _displayPoints = value;
                RaisePropertyChanged();
                foreach (ScenePoint point in SurfacePoints)
                {
                    point.IsVisible = value;
                }
            }
        }

        protected VertexPC[] VerticesPoly { get; set; }
        protected Shader PolyShader { get; set; }
        protected Mesh<VertexPC> PolyLines { get; set; }
        public List<SurfacePatch> SurfacePatches;
        public ObservableCollection<ScenePoint> SurfacePoints;

        protected PatchMesh<Vertex2DP> SurfaceMesh;

        private int _trimmingTextureId;
        private int _trimmingSide;

        protected SceneSurfaceSmooth()
        {
            SurfacePatches = new List<SurfacePatch>();
            SurfacePoints = new ObservableCollection<ScenePoint>();
            SurfacePoints.CollectionChanged += (sender, args) =>
            {
                if (args.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (object item in args.NewItems)
                    {
                        if (item is ScenePoint point)
                            Transform.ForceAddChild(point);
                    }
                }
                if (args.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (object item in args.NewItems)
                    {
                        if (item is ScenePoint point)
                            Transform.ForceRemoveChild(point);
                    }
                }
            };
            OnDeleteEvent += () =>
            {
                for (int i = SurfacePoints.Count - 1; i >= 0; i--)
                {
                    var surfaces = SurfacePoints[i].Transform.Parents.OfType<SceneSurfaceSmooth>().ToList();
                    if (surfaces.Count != 1)
                        continue;
                    Simulation.Scene.RemoveSceneObject(SurfacePoints[i]);
                }
            };
            Shader = Shaders.SurfaceC0SmoothShader;
            SurfaceMesh = GenerateSurfaceMesh();
            PolyShader = Shaders.BasicColorShader;
        }

        private void RegenerateMesh()
        {
            SurfaceMesh = GenerateSurfaceMesh();
        }

        public void ReplacePoint(ScenePoint currentPoint, ScenePoint newPoint)
        {
            var surfacePointsIdx = SurfacePoints.IndexOf((ScenePoint)currentPoint);
            SurfacePoints[surfacePointsIdx] = newPoint;
            Transform.ForceAddChild(newPoint);
            foreach (SurfacePatch patch in SurfacePatches)
            {
                if (patch.PatchPoints.Contains(currentPoint))
                {
                    var surfacePatchIdx = patch.PatchPoints.IndexOf((ScenePoint)currentPoint);
                    patch.PatchPoints[surfacePatchIdx] = newPoint;
                }
            }
        }

        public void InitializeSurface(int type, float sizeX, float sizeY, int patchesCountX, int patchesCountY)
        {
            PatchesX = patchesCountX;
            PatchesY = patchesCountY;
            switch (type)
            {
                case 0:
                    CreatePlaneSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
                    CreatePlaneSurfacePatches(patchesCountX, patchesCountY);
                    ReinitializePoly(type);
                    break;
                case 1:
                    CreateCylinderSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
                    CreateCylinderSurfacePatches(patchesCountX, patchesCountY);
                    ReinitializePoly(type);
                    IsUWrapped = true;
                    break;
                case 2:
                    CreateCylinderSurfacePoints(sizeX, sizeY, patchesCountX, patchesCountY);
                    CreateCylinderSurfacePatches(patchesCountX, patchesCountY);
                    ReinitializePoly(type);
                    IsUWrapped = true;
                    break;

            }
        }

        protected abstract void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX, int patchesCountY);

        protected abstract void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY);

        protected abstract void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY);

        public PatchMesh<Vertex2DP> GenerateSurfaceMesh()
        {
            List<Vertex2DP> vertices = new List<Vertex2DP>();
            List<uint> edges = new List<uint>();
            float distX = 1f / (UParameter - 1);
            float distY = 1f / (VParameter - 1);
            for (int i = 0; i < VParameter; i++)
            {
                for (int j = 0; j < UParameter; j++)
                {
                    vertices.Add(new Vertex2DP(j * distX, i * distY));
                }
            }

            for (int i = 0; i < VParameter; i++)
            {
                for (int j = 0; j < UParameter; j++)
                {
                    vertices.Add(new Vertex2DP(j * distX, i * distY));
                    if (j != 0)
                    {
                        edges.Add((uint)(j + i * UParameter));
                        edges.Add((uint)((j - 1) + i * UParameter));
                    }
                    if (i != 0)
                    {
                        edges.Add((uint)(j + i * UParameter));
                        edges.Add((uint)(j + (i - 1) * UParameter));
                    }
                }
            }
            return new PatchMesh<Vertex2DP>(vertices, edges, 2);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"), !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());

            Shader.Bind(Shader.GetUniformLocation("patchesU"), PatchesX);
            Shader.Bind(Shader.GetUniformLocation("patchesV"), PatchesY);
            Shader.Bind(Shader.GetUniformLocation("isOffset"), Simulation.Settings.CalculateOffsetSurfaces ? 1 : 0);
            Shader.Bind(Shader.GetUniformLocation("offset"), Simulation.Settings.SurfaceOffsetValue);
            if (_trimmingTextureId != 0)
            {
                if (_trimmingSide != 0)
                {

                    Shader.Bind(Shader.GetUniformLocation("sideIdx"), _trimmingSide);
                    GL.ActiveTexture(TextureUnit.Texture0);
                    GL.BindTexture(TextureTarget.Texture2D, _trimmingTextureId);
                    Shader.Bind(Shader.GetUniformLocation("Tex"), 0);
                }
                else
                {
                    Shader.Bind(Shader.GetUniformLocation("sideIdx"), _trimmingSide);
                    Shader.Bind(Shader.GetUniformLocation("Tex"), -1);
                }
            }
            else
            {
                Shader.Bind(Shader.GetUniformLocation("Tex"), -1);
            }


            for (int i = 0; i < PatchesY; i++)
            {
                for (int j = 0; j < PatchesX; j++)
                {
                    for (int k = 0; k < 16; k++)
                    {
                        Shader.Bind(Shader.GetUniformLocation($"points[{k}]"), SurfacePatches[j + i * PatchesX].PatchPoints[k].Transform.Position);
                    }

                    Shader.Bind(Shader.GetUniformLocation("uOffset"), j);
                    Shader.Bind(Shader.GetUniformLocation("vOffset"), i);

                    SurfaceMesh.Draw();
                }
            }
            if (DisplayPoly)
                DrawBernstainPoly();
        }

        private void DrawBernstainPoly()
        {
            for (int i = 0; i < SurfacePoints.Count; i++)
            {
                VerticesPoly[i] = new VertexPC(SurfacePoints[i].Transform.Position, new Vector3(SurfacePoints[i].Color.ScR, SurfacePoints[i].Color.ScG, SurfacePoints[i].Color.ScB));
            }
            PolyLines.SetVertices(0, VerticesPoly);
            PolyShader.Use();
            PolyShader.Bind(PolyShader.GetUniformLocation("model"), Matrix4.Identity);
            PolyShader.Bind(PolyShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            PolyShader.Bind(PolyShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"), !IsSelected ? PolyColor.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());
            PolyLines.Draw();
        }

        protected void ReinitializePoly(int type)
        {
            VerticesPoly = new VertexPC[SurfacePoints.Count];
            List<uint> edgesPoly = new List<uint>();
            for (int i = 0; i < SurfacePoints.Count; i++)
            {
                VerticesPoly[i] = new VertexPC(SurfacePoints[i].Transform.Position, new Vector3(SurfacePoints[i].Color.ScR, SurfacePoints[i].Color.ScG, SurfacePoints[i].Color.ScB));
            }

            if (type == 0)
            {
                int pointsX = 3 * PatchesX + 1;
                int pointsY = 3 * PatchesY + 1;
                for (int i = 0; i < pointsY; i++)
                {
                    for (int j = 0; j < pointsX; j++)
                    {
                        if (j != pointsX - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)((j + 1) + i * pointsX));
                        }

                        if (i != pointsY - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + (i + 1) * pointsX));
                        }
                    }
                }
            }
            if (type == 1)
            {
                int pointsX = 3 * PatchesX;
                int pointsY = 3 * PatchesY + 1;
                for (int i = 0; i < pointsY; i++)
                {
                    for (int j = 0; j < pointsX; j++)
                    {
                        if (j != pointsX - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + 1 + i * pointsX));
                        }
                        else
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(i * pointsX));
                        }

                        if (i != pointsY - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + (i + 1) * pointsX));
                        }
                    }
                }
            }
            if (type == 2)
            {
                int pointsX = PatchesX;
                int pointsY = PatchesY + 3;
                for (int i = 0; i < pointsY; i++)
                {
                    for (int j = 0; j < pointsX; j++)
                    {
                        if (j != pointsX - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + 1 + i * pointsX));
                        }
                        else
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(i * pointsX));
                        }

                        if (i != pointsY - 1)
                        {
                            edgesPoly.Add((uint)(j + i * pointsX));
                            edgesPoly.Add((uint)(j + (i + 1) * pointsX));
                        }
                    }
                }
            }

            PolyLines?.Dispose();
            PolyLines = new Mesh<VertexPC>(VerticesPoly, edgesPoly, MeshType.Lines, AccessType.Stream);
        }

        public override void Dispose()
        {
            PolyLines?.Dispose();
            SurfaceMesh?.Dispose();
            base.Dispose();
        }

        #region Intersections

        protected abstract Vector3d SurfaceCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d);
        protected abstract Vector3d DerivativeCalculation(double t, Vector3d a, Vector3d b, Vector3d c, Vector3d d);

        public Vector2d ApproximateAt(Vector3 worldPosition)
        {
            double distX = 1 / 3f;
            double distY = 1 / 3f;
            double minDist = double.PositiveInfinity;
            Vector2d pointUV = new Vector2d();
            var worldPosD = worldPosition.ToVector3d();
            for (int i = 0; i < 3 * PatchesY + 1; i++)
            {
                for (int j = 0; j < 3 * PatchesX + 1; j++)
                {
                    var param = new Vector2d(j * distX, i * distY);
                    Vector3d surfacePoint = EvaluateAt(param);
                    double dist = (worldPosD - surfacePoint).Length;
                    if (dist < minDist)
                    {
                        pointUV = param;
                        minDist = dist;
                    }
                }
            }

            return pointUV;
        }

        private SurfacePatch GetPatchFromGlobalUV(Vector2d param)
        {
            int u = (int)param.X;
            int v = (int)param.Y;

            if (u < 0)
                u = 0;
            else if (u >= PatchesX)
                u = PatchesX - 1;
            if (v >= PatchesY)
                v = PatchesY - 1;
            else if (v < 0)
                v = 0;

            return SurfacePatches.Find(x => x.PatchU == u && x.PatchV == v);
        }

        private Vector2d TransformToPatchUV(SurfacePatch patch, Vector2d param)
        {
            return new Vector2d(param.X - patch.PatchU, param.Y - patch.PatchV);
        }

        private Vector3d EvaluateNormalDifference(Vector2d uv)
        {
            var startUV = uv;
            uv = new Vector2d(startUV.X - 4 *Constants.Epsilon, startUV.Y);
            var dU1 = EvaluateUDerivativeAt(uv);
            var dV1 = EvaluateVDerivativeAt(uv);
            var normal1 = Vector3d.Cross(dU1, dV1);
            uv = new Vector2d(startUV.X + 4 * Constants.Epsilon, startUV.Y);

            var dU2 = EvaluateUDerivativeAt(uv);
            var dV2 = EvaluateVDerivativeAt(uv);
            var normal2 = Vector3d.Cross(dU2, dV2);

            return (normal1 + normal2) / 2.0f;
        }

        public Vector3d EvaluateAt(Vector2d uv)
        {
            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var valueOne = SurfaceCalculation(patchUV.Y, patch.PatchPoints[0].Transform.Position.ToVector3d(), patch.PatchPoints[4].Transform.Position.ToVector3d(), patch.PatchPoints[8].Transform.Position.ToVector3d(), patch.PatchPoints[12].Transform.Position.ToVector3d());
            var valueTwo = SurfaceCalculation(patchUV.Y, patch.PatchPoints[1].Transform.Position.ToVector3d(), patch.PatchPoints[5].Transform.Position.ToVector3d(), patch.PatchPoints[9].Transform.Position.ToVector3d(), patch.PatchPoints[13].Transform.Position.ToVector3d());
            var valueThree = SurfaceCalculation(patchUV.Y, patch.PatchPoints[2].Transform.Position.ToVector3d(), patch.PatchPoints[6].Transform.Position.ToVector3d(), patch.PatchPoints[10].Transform.Position.ToVector3d(), patch.PatchPoints[14].Transform.Position.ToVector3d());
            var valueFour = SurfaceCalculation(patchUV.Y, patch.PatchPoints[3].Transform.Position.ToVector3d(), patch.PatchPoints[7].Transform.Position.ToVector3d(), patch.PatchPoints[11].Transform.Position.ToVector3d(), patch.PatchPoints[15].Transform.Position.ToVector3d());
            var eval = SurfaceCalculation(patchUV.X, valueOne, valueTwo, valueThree, valueFour);

            if (Simulation.Settings.CalculateOffsetSurfaces)
            {
                var scenePoint = eval;
                Simulation.Settings.CalculateOffsetSurfaces = false;
                var derivU = EvaluateUDerivativeAt(uv);
                var derivV = EvaluateVDerivativeAt(uv);
                var normal = Vector3d.Cross(derivU, derivV).Normalized();
                if (double.IsNaN(normal.X) || double.IsNaN(normal.Y) || double.IsNaN(normal.Z) || normal.X == 0 ||
                    normal.Y == 0 || normal.Z == 0)
                {
                    normal = EvaluateNormalDifference(uv);
                }
                Simulation.Settings.CalculateOffsetSurfaces = true;
                return scenePoint + normal * Simulation.Settings.SurfaceOffsetValue;
            }
            return eval;
        }

        public Vector3d EvaluateUDerivativeAt(Vector2d uv)
        {
            if (Simulation.Settings.CalculateOffsetSurfaces)
            {
                var diffM = new Vector2d(uv.X - Constants.DerivativeOffset, uv.Y);
                IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffM.X);
                var diffP = new Vector2d(uv.X + Constants.DerivativeOffset, uv.Y);
                IntersectionMath.WrapParam(GetURange(), IsUWrapped, ref diffP.X);
                var evalM = EvaluateAt(diffM);
                var evalP = EvaluateAt(diffP);

                return (evalP - evalM) / (2 * Constants.DerivativeOffset);
            }

            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var deriv0 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[0].Transform.Position.ToVector3d(), patch.PatchPoints[4].Transform.Position.ToVector3d(), patch.PatchPoints[8].Transform.Position.ToVector3d(), patch.PatchPoints[12].Transform.Position.ToVector3d());
            var deriv1 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[1].Transform.Position.ToVector3d(), patch.PatchPoints[5].Transform.Position.ToVector3d(), patch.PatchPoints[9].Transform.Position.ToVector3d(), patch.PatchPoints[13].Transform.Position.ToVector3d());
            var deriv2 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[2].Transform.Position.ToVector3d(), patch.PatchPoints[6].Transform.Position.ToVector3d(), patch.PatchPoints[10].Transform.Position.ToVector3d(), patch.PatchPoints[14].Transform.Position.ToVector3d());
            var deriv3 = SurfaceCalculation(patchUV.Y, patch.PatchPoints[3].Transform.Position.ToVector3d(), patch.PatchPoints[7].Transform.Position.ToVector3d(), patch.PatchPoints[11].Transform.Position.ToVector3d(), patch.PatchPoints[15].Transform.Position.ToVector3d());

            return DerivativeCalculation(patchUV.X, deriv0, deriv1, deriv2, deriv3);
        }

        public Vector3d EvaluateVDerivativeAt(Vector2d uv)
        {
            if (Simulation.Settings.CalculateOffsetSurfaces)
            {
                var diffM = new Vector2d(uv.X, uv.Y - Constants.DerivativeOffset);
                IntersectionMath.WrapParam(GetVRange(), IsVWrapped, ref diffM.Y);
                var diffP = new Vector2d(uv.X, uv.Y + Constants.DerivativeOffset);
                IntersectionMath.WrapParam(GetVRange(), IsVWrapped, ref diffP.Y);
                var evalM = EvaluateAt(diffM);
                var evalP = EvaluateAt(diffP);

                return (evalP - evalM) / (2 * Constants.DerivativeOffset);
            }

            var patch = GetPatchFromGlobalUV(uv);
            var patchUV = TransformToPatchUV(patch, uv);

            var deriv0 = SurfaceCalculation(patchUV.X, patch.PatchPoints[0].Transform.Position.ToVector3d(), patch.PatchPoints[1].Transform.Position.ToVector3d(), patch.PatchPoints[2].Transform.Position.ToVector3d(), patch.PatchPoints[3].Transform.Position.ToVector3d());
            var deriv1 = SurfaceCalculation(patchUV.X, patch.PatchPoints[4].Transform.Position.ToVector3d(), patch.PatchPoints[5].Transform.Position.ToVector3d(), patch.PatchPoints[6].Transform.Position.ToVector3d(), patch.PatchPoints[7].Transform.Position.ToVector3d());
            var deriv2 = SurfaceCalculation(patchUV.X, patch.PatchPoints[8].Transform.Position.ToVector3d(), patch.PatchPoints[9].Transform.Position.ToVector3d(), patch.PatchPoints[10].Transform.Position.ToVector3d(), patch.PatchPoints[11].Transform.Position.ToVector3d());
            var deriv3 = SurfaceCalculation(patchUV.X, patch.PatchPoints[12].Transform.Position.ToVector3d(), patch.PatchPoints[13].Transform.Position.ToVector3d(), patch.PatchPoints[14].Transform.Position.ToVector3d(), patch.PatchPoints[15].Transform.Position.ToVector3d());

            return DerivativeCalculation(patchUV.Y, deriv0, deriv1, deriv2, deriv3);
        }

        public double GetURange()
        {
            return PatchesX;
        }

        public double GetVRange()
        {
            return PatchesY;
        }

        public int GetUCurveCount()
        {
            return UParameter * PatchesX;
        }

        public int GetVCurveCount()
        {
            return VParameter * PatchesY;
        }

        public void SetTrimmingTexture(int texId)
        {
            _trimmingTextureId = texId;
        }

        public void SetTrimmingSide(int sideId)
        {
            _trimmingSide = sideId;
        }

        #endregion


    }
}
