﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public abstract class SceneCurve : Model, ISelectable, IColorable, IFilterable
    {
        public bool IsSelected { get; set; }
        public Color Color { get; set; } = Colors.White;
        public Color PolyColor { get; set; } = Colors.Red;
        private bool _displayPolynomial;
        public bool DisplayPoly
        {
            get { return _displayPolynomial; }
            set
            {
                _displayPolynomial = value;
                RaisePropertyChanged();
            }
        }

        protected int _drawWidth = 1;

        protected Mesh<VertexP> LinearCurve { get; set; }
        protected PatchMesh<VertexP> QuadraticCurve { get; set; }
        protected PatchMesh<VertexP> CubicCurve { get; set; }
        protected Mesh<VertexP> PolyLines { get; set; }

        protected Shader LinearShader { get; set; }
        protected Shader QuadraticShader { get; set; }
        protected Shader CubicShader { get; set; }
        protected Shader PolyShader { get; set; }

        protected VertexP[] VerticesLinear { get; set; }
        protected VertexP[] VerticesQuadratic { get; set; }
        protected VertexP[] VerticesCubic { get; set; }
        protected VertexP[] VerticesPoly { get; set; }

        public ObservableCollection<ScenePoint> CurvePoints;

        protected SceneCurve()
        {
            VerticesLinear = new VertexP[2];
            VerticesQuadratic = new VertexP[3];
            VerticesCubic = new VertexP[4];

            LinearShader = Shaders.BasicColorShader;
            QuadraticShader = Shaders.QuadraticCurveShader;
            CubicShader = Shaders.CubicCurveShader;
            PolyShader = Shaders.BasicColorShader;

            LinearCurve = new Mesh<VertexP>(VerticesLinear, null, MeshType.Lines, AccessType.Stream);
            QuadraticCurve = new PatchMesh<VertexP>(VerticesQuadratic, null, 3, AccessType.Stream);
            CubicCurve = new PatchMesh<VertexP>(VerticesCubic, null, 4, AccessType.Stream);

            CurvePoints = new ObservableCollection<ScenePoint>();
        }

        public override void Dispose()
        {
            LinearCurve?.Dispose();
            QuadraticCurve?.Dispose();
            CubicCurve?.Dispose();
            PolyLines?.Dispose();
            base.Dispose();
        }

        public void SetDrawWidth(int width)
        {
            _drawWidth = width;
        }
        
        protected void DrawPoly(List<CurveDrawPoint> points)
        {
            for (int i = 0; i < points.Count; i++)
            {
                VerticesPoly[i] = new VertexP(points[i].Position);
            }
            PolyLines.SetVertices(0, VerticesPoly);
            PolyShader.Use();
            PolyShader.Bind(PolyShader.GetUniformLocation("model"), Matrix4.Identity);
            PolyShader.Bind(PolyShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            PolyShader.Bind(PolyShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());

            if (!Simulation.Settings.StereoModeOn)
                PolyShader.Bind(PolyShader.GetUniformLocation("color"), !IsSelected ? PolyColor.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                PolyShader.Bind(PolyShader.GetUniformLocation("color"), Colors.White.ColorToVector3());


            PolyLines.Draw();
        }

        protected void DrawC0(List<CurveDrawPoint> points)
        {
            GL.LineWidth(_drawWidth);
            for (int i = 0; i < (points.Count - 1) / 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    VerticesCubic[j] = new VertexP(points[3 * i + j].Position);
                }

                CubicCurve.SetVertices(0, VerticesCubic);
                CubicShader.Use();
                CubicShader.Bind(CubicShader.GetUniformLocation("screenSize"), new Vector2(Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight));
                CubicShader.Bind(CubicShader.GetUniformLocation("model"), Transform.GetModelMatrix());
                CubicShader.Bind(CubicShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                CubicShader.Bind(CubicShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                if (!Simulation.Settings.StereoModeOn)
                    CubicShader.Bind(CubicShader.GetUniformLocation("color"), !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
                else
                    CubicShader.Bind(CubicShader.GetUniformLocation("color"), Colors.White.ColorToVector3());
                CubicCurve.Draw();
            }

            switch ((points.Count - 1) % 3)
            {
                case 1:
                    VerticesLinear[0] = new VertexP(points[points.Count - 2].Position);
                    VerticesLinear[1] = new VertexP(points[points.Count - 1].Position);
                    LinearCurve.SetVertices(0, VerticesLinear);
                    LinearShader.Use();
                    LinearShader.Bind(LinearShader.GetUniformLocation("model"), Matrix4.Identity);
                    LinearShader.Bind(LinearShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                    LinearShader.Bind(LinearShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                    if (!Simulation.Settings.StereoModeOn)
                        LinearShader.Bind(LinearShader.GetUniformLocation("color"), !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
                    else
                        LinearShader.Bind(LinearShader.GetUniformLocation("color"), Colors.White.ColorToVector3());
                    LinearCurve.Draw();
                    break;
                case 2:
                    VerticesQuadratic[0] = new VertexP(points[points.Count - 3].Position);
                    VerticesQuadratic[1] = new VertexP(points[points.Count - 2].Position);
                    VerticesQuadratic[2] = new VertexP(points[points.Count - 1].Position);
                    QuadraticCurve.SetVertices(0, VerticesQuadratic);
                    QuadraticShader.Use();
                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("screenSize"), new Vector2(Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight));
                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("model"), Transform.GetModelMatrix());
                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                    QuadraticShader.Bind(QuadraticShader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                    if (!Simulation.Settings.StereoModeOn)
                        QuadraticShader.Bind(QuadraticShader.GetUniformLocation("color"), !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
                    else
                        QuadraticShader.Bind(QuadraticShader.GetUniformLocation("color"), Colors.White.ColorToVector3());
                    QuadraticCurve.Draw();
                    break;
            }
            GL.LineWidth(1);
        }

        protected void ReinitializePoly(List<CurveDrawPoint> points)
        {
            VerticesPoly = new VertexP[points.Count];
            uint[] edgesPoly = new uint[points.Count * 2 - 2];
            for (int i = 0; i < points.Count; i++)
            {
                VerticesPoly[i] = new VertexP(points[i].Position);
            }
            for (int i = 1; i < edgesPoly.Length + 1; i++)
            {
                edgesPoly[i - 1] = (uint)Math.Floor((double)i / 2);
            }
            PolyLines?.Dispose();
            PolyLines = new Mesh<VertexP>(VerticesPoly, edgesPoly, MeshType.Lines, AccessType.Stream);
        }

        protected struct CurveDrawPoint
        {
            public Vector3 Position;
            public Vector3 Color;

            public CurveDrawPoint(Vector3 pos, Vector3 col)
            {
                Position = pos;
                Color = col;
            }
        }
    }
}

