﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Popups;
using PureCAD.Utils;
using PureCAD.Utils.ModeManagers;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Models
{
    public class SceneIntersectionCurve : Model, IColorable, ISelectable
    {
        public Color Color { get; set; } = Colors.DeepPink;
        public bool IsSelected { get; set; }
        public int DrawWidth { get; private set; }
        
        public IntersectionManager.IntersectionData IntersectionData;
        private int _surface1TrimSide = 0;
        private int _surface2TrimSide = 0;

        public int Surface1TrimSide
        {
            get { return _surface1TrimSide; }
            set
            {
                _surface1TrimSide = value; 
                IntersectionData.Surface1.SetTrimmingSide(_surface1TrimSide);
                RaisePropertyChanged();
            }
        }

        public int Surface2TrimSide
        {
            get { return _surface2TrimSide; }
            set
            {
                _surface2TrimSide = value; 
                IntersectionData.Surface2.SetTrimmingSide(_surface2TrimSide);
                RaisePropertyChanged();
            }
        }

        public ICommand ConvertToInterpolatingCommand
        {
            get;
            protected set;
        }

        public ICommand OpenParametersPopupCommand
        {
            get;
            protected set;
        }

        public SceneIntersectionCurve(IntersectionManager.IntersectionData data)
        {
            DrawWidth = 4;
            IntersectionData = data;
            Shader = Shaders.BasicColorShader;
            Mesh = GenerateLineMesh();
            ConvertToInterpolatingCommand = new RelayCommand(param => ConvertToInterpolatingCurve());
            OpenParametersPopupCommand = new RelayCommand(param => OpenParameterPopUp());
        }

        public void OpenParameterPopUp()
        {
            IntersectionPopup popup = new IntersectionPopup(IntersectionData);
            popup.Show();
        }

        public void ConvertToInterpolatingCurve()
        {
            OnDeleteEvent += () =>
            {
                var item = new SceneCurveC2Interpolated();
                item.ObjectName = ObjectName;
                item.Color = Color;
                item.CreateFromIntersectionCurve(this);
                Simulation.Scene.SceneObjects.Add(item);
            };
            Surface1TrimSide = 0;
            Surface2TrimSide = 0;
            Simulation.Scene.RemoveSceneObject(this);
        }

        protected override void OnRender()
        {
            GL.LineWidth(DrawWidth);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Mesh.Draw();
            GL.LineWidth(1);
        }

        private Mesh<VertexP> GenerateLineMesh()
        {
            List<VertexP> vertices = new List<VertexP>();
            List<uint> edges = new List<uint>();

            for (int i = 0; i < IntersectionData.ScenePoints.Count; i++)
            {
                vertices.Add(new VertexP(IntersectionData.ScenePoints[i]));
                if (i + 1 < IntersectionData.ScenePoints.Count)
                {
                    edges.Add((uint)i);
                    edges.Add((uint)i + 1);
                }
                else if (IntersectionData.IsCurveClosed)
                {
                    edges.Add((uint)i);
                    edges.Add(0);
                }
            }

            return new Mesh<VertexP>(vertices, edges, MeshType.Lines, AccessType.Static);
        }
    }
}
