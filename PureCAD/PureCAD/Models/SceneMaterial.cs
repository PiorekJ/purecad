﻿using System.Collections.Generic;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class SceneMaterial : Model, IColorable
    {
        public int PointsX
        {
            get { return _pointsX; }
            set
            {
                _pointsX = value;
                RaisePropertyChanged();
                SetMesh();
                //Simulation.UIManager.ResetSimulation();
            }
        }

        public int PointsZ
        {
            get { return _pointsZ; }
            set
            {
                _pointsZ = value;
                RaisePropertyChanged();
                SetMesh();
                //Simulation.UIManager.ResetSimulation();
            }
        }

        public float SizeX
        {
            get { return _sizeX; }
            set
            {
                _sizeX = value;
                RaisePropertyChanged();
                SetMesh();
                //Simulation.UIManager.ResetSimulation();
            }
        }

        public float SizeY
        {
            get { return _sizeY; }
            set
            {
                _sizeY = value;
                RaisePropertyChanged();
                SetMesh();
                //Simulation.UIManager.ResetSimulation();
            }
        }

        public float SizeZ
        {
            get { return _sizeZ; }
            set
            {
                _sizeZ = value;
                RaisePropertyChanged();
                SetMesh();
                //Simulation.UIManager.ResetSimulation();
            }
        }

        public float SafeY
        {
            get { return _safeY; }
            set
            {
                _safeY = value;
                RaisePropertyChanged();
            }
        }

        public int VerticesLength => _vertices.Length;
        public Color Color { get; set; } = Color.FromScRgb(1.0f, 0.6f, 0, 0);
        private float _unitLengthX;
        private float _unitLengthY;
        private float _unitLengthZ;
        private VertexPNT[] _vertices;
        private List<uint> _triangles;
        private int _pointsX = 500;
        private int _pointsZ = 500;
        private float _sizeX = 15;
        private float _sizeZ = 15;
        private float _sizeY = 5f;
        private float _safeY = -2;

        public class Triangle
        {
            public VertexPNT V1;
            public VertexPNT V2;
            public VertexPNT V3;

            public Triangle(VertexPNT v1, VertexPNT v2, VertexPNT v3)
            {
                V1 = v1;
                V2 = v2;
                V3 = v3;
            }
        }

        public SceneMaterial()
        {
            Texture = Textures.MetalTexture;
            Shader = Shaders.BasicPBRShader;
            SetMesh();
        }

        private void SetMesh()
        {
            _triangles = new List<uint>();
            _vertices = new VertexPNT[PointsX * PointsZ];
            _unitLengthX = CalculateUnitLengthX();
            _unitLengthZ = CalculateUnitLengthZ();
            Mesh = GenerateMillMaterialTriangleMesh();
        }

        private Mesh<VertexPNT> GenerateMillMaterialTriangleMesh()
        {
            float distX = SizeX / (float)(PointsX - 1);
            float distZ = SizeZ / (float)(PointsZ - 1);

            float startX = (-SizeX / 2.0f);
            float startZ = (-SizeZ / 2.0f);

            float interX = 1 / (float)(PointsX - 1);
            float interZ = 1 / (float)(PointsZ - 1);

            for (int i = 0; i < PointsZ; i++)
            {
                for (int j = 0; j < PointsX; j++)
                {
                    _vertices[j + i * PointsX] = new VertexPNT(new Vector3(startX + j * distX, SizeY, startZ + i * distZ),
                        Vector3.UnitY, new Vector2(j * interX, i * interZ));

                    if (i != PointsZ - 1 && j != PointsX - 1)
                    {
                        _triangles.Add((uint)(j + (i + 1) * PointsX));
                        _triangles.Add((uint)(j + 1 + i * PointsX));
                        _triangles.Add((uint)(j + i * PointsX));

                        _triangles.Add((uint)(j + (i + 1) * PointsX));
                        _triangles.Add((uint)(j + 1 + (i + 1) * PointsX));
                        _triangles.Add((uint)(j + 1 + i * PointsX));
                    }
                }
            }

            return new Mesh<VertexPNT>(_vertices, _triangles, MeshType.Triangles, AccessType.Dynamic);
        }

        public void RegenerateMesh()
        {
            for (var i = 0; i < _vertices.Length; i++)
            {
                _vertices[i].Position.Y = SizeY;
                _vertices[i].Normal = Vector3.UnitY;
            }
            ((Mesh<VertexPNT>)Mesh).SetVertices(0, _vertices);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());

            //GL.ActiveTexture(TextureUnit.Texture0);
            //GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Albedo.TexID);
            //Shader.Bind(Shader.GetUniformLocation("albedoMap"), 0);
            //GL.ActiveTexture(TextureUnit.Texture1);
            //GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Normal.TexID);
            //Shader.Bind(Shader.GetUniformLocation("normalMap"), 1);
            //GL.ActiveTexture(TextureUnit.Texture2);
            //GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Metallic.TexID);
            //Shader.Bind(Shader.GetUniformLocation("metallicMap"), 2);
            //GL.ActiveTexture(TextureUnit.Texture3);
            //GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Roughness.TexID);
            //Shader.Bind(Shader.GetUniformLocation("roughnessMap"), 3);
            //GL.ActiveTexture(TextureUnit.Texture4);
            //GL.BindTexture(TextureTarget.Texture2D, ((PBRTexture)Texture).Ao.TexID);

            for (var i = 0; i < Simulation.Scene.SceneLights.Count; i++)
            {
                SceneLight sceneLight = Simulation.Scene.SceneLights[i];
                Shader.Bind(Shader.GetUniformLocation($"lightPositions[{i}]"), sceneLight.Transform.Position);
                Shader.Bind(Shader.GetUniformLocation($"lightColors[{i}]"), sceneLight.Color.ColorToVector3() * sceneLight.LightIntensity);
            }

            Shader.Bind(Shader.GetUniformLocation("aoMap"), 1);
            Shader.Bind(Shader.GetUniformLocation("albedo"), Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("metallic"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("roughness"), 0.3f);
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);

            //Shader.Use();
            //Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            //Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            //Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            //Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            //Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.SceneLights[0].Color.ColorToVector3());
            //Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.SceneLights[0].Transform.Position);
            //Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            //Shader.Bind(Shader.GetUniformLocation("ks"), 0.4f);
            //Shader.Bind(Shader.GetUniformLocation("kd"), 0.8f);
            //Shader.Bind(Shader.GetUniformLocation("ka"), 0.35f);
            //Shader.Bind(Shader.GetUniformLocation("m"), 50.0f);
            //GL.ActiveTexture(TextureUnit.Texture0);
            //GL.BindTexture(TextureTarget.Texture2D, ((Texture)Texture).TexID);
            //Shader.Bind(Shader.GetUniformLocation("tex"), 0);

            Mesh.Draw();
        }

        public Vector3 GetPositionAtIndex(int index)
        {
            return _vertices[index].Position;
        }

        public void UpdateVertexStrip(int startIndex, int endIndex)
        {
            ((Mesh<VertexPNT>)Mesh).SetVertices((uint)startIndex, (uint)endIndex, _vertices);
        }

        public void UpdateAllVertices()
        {
            for (var index = 0; index < _vertices.Length; index++)
            {
                CalculateNormalAt(index);
            }
            ((Mesh<VertexPNT>)Mesh).SetVertices(0, _vertices);
        }

        private float GetHeightAt(int pointID)
        {
            return _vertices[pointID].Position.Y;
        }

        private void SetHeightAt(int pointID, float height)
        {
            _vertices[pointID].Position.Y = height;
        }

        private void CalculateNormalAt(int pointID)
        {
            var N = Vector3.Zero;
            int i = pointID % PointsX;
            int j = pointID / PointsX;

            if (i == 0 && j == 0)
            {
                _vertices[pointID].Normal = NormalFromFace(0, 1, PointsZ).Normalized();
            }
            else if (i == PointsX - 1 && j == PointsZ - 1)
            {
                _vertices[pointID].Normal = NormalFromFace(i + j * PointsX, i + (j - 1) * PointsX, i - 1 + (j - 1) * PointsX).Normalized();
            }
            else if (i == PointsX - 1 && j == 0)
            {
                N += NormalFromFace(i, i - 1, (i - 1) + (j + 1) * PointsX);
                N += NormalFromFace(i, (i - 1) + (j + 1) * PointsX, i + (j + 1) * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else if (i == 0 && j == PointsZ - 1)
            {
                N += NormalFromFace(i + j * PointsX, i + 1 + (j - 1) * PointsX, i + (j - 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i + 1 + j * PointsX, i + 1 + (j - 1) * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else if (i > 0 && i < PointsX - 1 && j == 0)
            {
                N += NormalFromFace(i + j * PointsX, i - 1 + j * PointsX, i - 1 + (j + 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i - 1 + (j + 1) * PointsX, i + (j + 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i + (j + 1) * PointsX, i + 1 + j * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else if (i > 0 && i < PointsX - 1 && j == PointsZ - 1)
            {
                N += NormalFromFace(i + j * PointsX, i + (j - 1) * PointsX, i - 1 + j * PointsX);
                N += NormalFromFace(i + j * PointsX, i + 1 + (j - 1) * PointsX, i + (j - 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i + 1 + j * PointsX, i + 1 + (j - 1) * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else if (i == 0 && j > 0 && j < PointsZ - 1)
            {
                N += NormalFromFace(i + j * PointsX, i + 1 + j * PointsX, i + 1 + (j - 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i + 1 + (j - 1) * PointsX, i + (j - 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i + (j + 1) * PointsX, i + 1 + j * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else if (i == PointsX - 1 && j > 0 && j < PointsZ - 1)
            {
                N += NormalFromFace(i + j * PointsX, i + (j - 1) * PointsX, i - 1 + j * PointsX);
                N += NormalFromFace(i + j * PointsX, i - 1 + j * PointsX, i - 1 + (j + 1) * PointsX);
                N += NormalFromFace(i + j * PointsX, i - 1 + (j + 1) * PointsX, i + (j + 1) * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
            else
            {
                var s = i + j * PointsX;
                N += NormalFromFace(s, i + (j - 1) * PointsX, i - 1 + j * PointsX);
                N += NormalFromFace(s, i - 1 + j * PointsX, i - 1 + (j + 1) * PointsX);
                N += NormalFromFace(s, i - 1 + (j + 1) * PointsX, i + (j + 1) * PointsX);
                N += NormalFromFace(s, i + (j + 1) * PointsX, i + 1 + j * PointsX);
                N += NormalFromFace(s, i + 1 + j * PointsX, i + 1 + (j - 1) * PointsX);
                N += NormalFromFace(s, i + 1 + (j - 1) * PointsX, i + (j - 1) * PointsX);
                _vertices[pointID].Normal = N.Normalized();
            }
        }

        public byte MillMaterialAt(int pointID, float newHeight, bool update)
        {
            if (newHeight < SafeY)
                return 0;

            if (GetHeightAt(pointID) >= newHeight)
            {
                SetHeightAt(pointID, newHeight);
                if (update)
                {
                    CalculateNormalAt(pointID);
                }
                return 1;
            }

            return 2;
        }

        private Vector3 NormalFromFace(int id1, int id2, int id3)
        {
            var v1 = _vertices[id1].Position;
            var v2 = _vertices[id2].Position;
            var v3 = _vertices[id3].Position;

            var u = v2 - v1;
            var v = v3 - v1;

            return Vector3.Cross(u, v);
        }

        public int ConvertRadiusToModelSpace(float radius)
        {
            return _unitLengthX >= _unitLengthZ ? (int)(radius * _unitLengthX) : (int)(radius * _unitLengthZ);
        }

        public int ConvertRadiusXToModelSpace(float radius)
        {
            return (int)(radius * _unitLengthX);
        }

        public int ConvertRadiusZToModelSpace(float radius)
        {
            return (int)(radius * _unitLengthZ);
        }

        public float ConvertRadiusToSceneSpace(int radius)
        {
            return _unitLengthX >= _unitLengthZ ? (radius / _unitLengthX) : (radius / _unitLengthZ);
        }

        public Vector2 ConvertPointToModelSpace(Vector3 point)
        {
            var x = (point.X + SizeX / 2f) * _unitLengthX;
            var z = (point.Z + SizeZ / 2f) * _unitLengthZ;

            return new Vector2(x, z);
        }

        public Vector3 ConvertPointToSceneSpace(Vector2 point)
        {
            var x = (point.X) / _unitLengthX - SizeX / 2f;
            var z = (point.Y) / _unitLengthZ - SizeZ / 2f;

            return new Vector3(x, 0, z);
        }

        private float CalculateUnitLengthX()
        {
            return PointsX / (float)SizeX;
        }

        private float CalculateUnitLengthZ()
        {
            return PointsZ / (float)SizeZ;
        }

        public float GetVerticalStep()
        {
            return _unitLengthX > _unitLengthZ ? _unitLengthX : _unitLengthZ;
        }
    }
}
