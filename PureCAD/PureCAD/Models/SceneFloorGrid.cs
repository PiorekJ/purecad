﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;
using PureCAD.Utils.UIExtensionClasses;

namespace PureCAD.Models
{
    public class SceneFloorGrid : Model, IColorable
    {
        public Color Color { get; set; } = Colors.White;

        public bool IsDisplayed
        {
            get { return _isDisplayed; }
            set
            {
                _isDisplayed = value;
                RaisePropertyChanged();
            }
        }

        public int SizeX { get; set; } = 10;
        public int SizeZ { get; set; } = 10;

        public int GridDivisionsX { get; set; } = 10;
        public int GridDivisionsZ { get; set; } = 10;

        private bool _isDisplayed = true;

        public SceneFloorGrid()
        {
            Shader = Shaders.BasicShader;
            Mesh = GenerateFloorGridMesh();
        }

        protected override void OnRender()
        {
            if (IsDisplayed)
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
                Mesh.Draw();
            }
        }

        public Mesh<VertexPC> GenerateFloorGridMesh()
        {
            List<VertexPC> vertices = new List<VertexPC>();
            List<uint> edges = new List<uint>();

            float distX = SizeX / (float)(GridDivisionsX);
            float distZ = SizeZ / (float)(GridDivisionsZ);

            float startX = (-SizeX / 2.0f);
            float startZ = (-SizeZ / 2.0f);

            for (int i = 0; i <= GridDivisionsZ; i++)
            {
                for (int j = 0; j <= GridDivisionsX; j++)
                {
                    vertices.Add(new VertexPC(new Vector3(startX + j * distX, 0, startZ + i * distZ), Color.ColorToVector3()));

                    if (j != 0)
                    {
                        edges.Add((uint)(j + i * (GridDivisionsX + 1)));
                        edges.Add((uint)((j - 1) + i * (GridDivisionsX + 1)));
                    }
                    if (i != 0)
                    {
                        edges.Add((uint)(j + i * (GridDivisionsX + 1)));
                        edges.Add((uint)(j + (i - 1) * (GridDivisionsX + 1)));
                    }
                }
            }

            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }
    }
}
