﻿using System.Collections.Generic;
using System.Windows.Media;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Popups;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class Quad : Model
    {
        public int Tex;

        public Quad()
        {
            Mesh = CreatePlane();
            Shader = Shaders.DepthShader;
        }

        private Mesh<VertexPT> CreatePlane()
        {
            List<VertexPT> vertices = new List<VertexPT>();

            vertices.Add(new VertexPT(-1, 1, 0, 0, 1));
            vertices.Add(new VertexPT(-1,- 1, 0, 0, 0));
            vertices.Add(new VertexPT(1, -1, 0, 1, 0));
            vertices.Add(new VertexPT(1, 1, 0, 1, 1));

            List<uint> edges = new List<uint>();

            edges.Add(0);
            edges.Add(3);
            edges.Add(1);

            edges.Add(1);
            edges.Add(3);
            edges.Add(2);
            return new Mesh<VertexPT>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        protected override void OnRender()
        {

            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("Tex"), Tex);
            Shader.Bind(Shader.GetUniformLocation("near_plane"), Constants.DefaultZNear);
            Shader.Bind(Shader.GetUniformLocation("far_plane"), Constants.DefaultZFar);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Tex);
            Mesh.Draw();

        }
    }
}
