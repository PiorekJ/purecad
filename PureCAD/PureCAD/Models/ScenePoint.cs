﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;
using OpenTK;
using PureCAD.Utils.Serialization;
using Transform = PureCAD.Components.Transform;

namespace PureCAD.Models
{
    public class ScenePoint : Model, IColorable, ISelectable, IFilterable
    {
        private Color _color = Colors.White;
        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                RaisePropertyChanged();
            }
        }

        public bool IsSelected { get; set; } = false;

        public ScenePoint()
        {
            Shader = Shaders.BasicColorShader;
            Mesh = GeneratePointMesh();
        }

        private IMesh GeneratePointMesh()
        {
            Mesh?.Dispose();
            return new Mesh<VertexP>(new List<VertexP>() { new VertexP(Transform.Position) }, null, MeshType.Points, AccessType.Static);
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"),
                    !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());

            Mesh.Draw();
        }

        public void Deserialize(point point)
        {
            Transform.Position = new Vector3(point.x, point.y, point.z);
            ObjectName = point.name;
        }

        public point Serialize()
        {
            var point = new point();
            point.name = ObjectName;
            point.x = Transform.Position.X;
            point.y = Transform.Position.Y;
            point.z = Transform.Position.Z;
            return point;
        }
    }
}
