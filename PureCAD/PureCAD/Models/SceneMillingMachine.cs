﻿using System.Collections.ObjectModel;
using System.Windows;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class SceneMillingMachine : SceneObject, IVirtual, ISelectable
    {
        private SceneMillFlat _flatMill;
        private SceneMillSphere _sphereMill;

        public SceneMill Mill = null;
        public SceneMaterial Material;
        private ObservableCollection<SceneMillPath> _paths;

        private SceneMillPath _currentSceneMillPath;
        private bool _isRunning;

        private float _moveTime;
        private int _millingSpeed = 10;

        public bool IsSelected { get; set; }

        public ObservableCollection<SceneMillPath> Paths
        {
            get { return _paths; }
            set
            {
                _paths = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged();
            }
        }

        public int MillingSpeed
        {
            get { return _millingSpeed; }
            set
            {
                _millingSpeed = value;
                RaisePropertyChanged();
            }
        }

        public SceneMillingMachine(SceneMillFlat flatMill, SceneMillSphere sphereMill, SceneMaterial material)
        {
            Paths = new ObservableCollection<SceneMillPath>();
            ObjectName = "Milling machine";
            _flatMill = flatMill;
            _sphereMill = sphereMill;
            Material = material;
            OnDeleteEvent += () =>
            {
                _flatMill.Dispose();
                _sphereMill.Dispose();
                Material.Dispose();
            };
        }

        public void AddPath(SceneMillPath path)
        {
            Paths.Add(path);
        }

        public void ClearPaths()
        {
            Paths.Clear();
            _currentSceneMillPath = null;
        }

        private void SetupMill(MillType type, float size, bool isActive = true)
        {
            if (Mill != null)
            {
                Mill.IsActive = false;
                Mill.IsVisible = false;
            }
            switch (type)
            {
                case MillType.Flat:
                    Mill = _flatMill;
                    break;
                case MillType.Sphere:
                    Mill = _sphereMill;
                    break;
                default:
                    MessageBox.Show("Couldn't setup the mill, check file", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
            }
            Mill.IsActive = isActive;
            Mill.IsVisible = true;
            Mill.SetupMill(size, Material.ConvertRadiusToModelSpace(size), Material);
        }

        public void StartMilling(SceneMillPath path, bool startSimulation = true)
        {
            SetupMill(path.MillType, path.MillSize, startSimulation);
            _currentSceneMillPath = path;
            IsRunning = startSimulation;
        }

        public void MillInstant(SceneMillPath path)
        {
            StartMilling(path, false);

            while (path.CurrentSceneIndex < path.ScenePaths.Count)
            {
                if (path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex <
                    path.ScenePaths[path.CurrentSceneIndex].MaterialPoints.Count)
                {
                    Mill.Transform.Position = Vector3.Lerp(path.ScenePaths[path.CurrentSceneIndex].Start,
                        path.ScenePaths[path.CurrentSceneIndex].End,
                        path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex * path.ScenePaths[path.CurrentSceneIndex].TravelDistance);
                    var materialPoint = path.ScenePaths[path.CurrentSceneIndex].MaterialPoints[path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex];
                    if (!Mill.MillMaterialAt(Material, materialPoint, Material.SafeY,
                        path.ScenePaths[path.CurrentSceneIndex].MoveDirection, false))
                    {
                        IsRunning = false;
                        return;
                    }
                    path.ScenePaths[path.CurrentSceneIndex].CurrentMaterialIndex++;
                }
                else
                {
                    path.CurrentSceneIndex++;
                }
            }
            path.CurrentSceneIndex--;
        }

        public void DeletePath(SceneMillPath path)
        {
            if (IsRunning && path == _currentSceneMillPath)
            {
                _currentSceneMillPath = null;
                IsRunning = false;
            }
            Paths.Remove(path);
        }

        private float _defaultFrameRate = 1 / 1f;
        private float _physTime = 1 / 1f;
        private float _timer = 0.0f;

        protected override void OnUpdate()
        {
            if (!IsRunning || _currentSceneMillPath == null)
                return;

            _physTime = _defaultFrameRate + _timer;

            for (int i = 0; i < MillingSpeed; i++)
            {
                if (_currentSceneMillPath.CurrentSceneIndex < _currentSceneMillPath.ScenePaths.Count)
                {
                    if (_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex <
                        _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MaterialPoints.Count)
                    {
                        Mill.Transform.Position = Vector3.Lerp(_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].Start,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].End,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex * _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].TravelDistance);
                        var materialPoint = _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MaterialPoints[_currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex];
                        if (!Mill.MillMaterialAt(Material, materialPoint, Material.SafeY,
                            _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].MoveDirection))
                        {
                            IsRunning = false;
                            return;
                        }
                        _currentSceneMillPath.ScenePaths[_currentSceneMillPath.CurrentSceneIndex].CurrentMaterialIndex++;
                    }
                    else
                    {
                        _currentSceneMillPath.CurrentSceneIndex++;
                    }
                }
                else
                {
                    _paths.Remove(_currentSceneMillPath);
                    if (_paths.Count >= 1)
                        StartMilling(_paths[0]);
                    else
                    {
                        IsRunning = false;
                    }
                }

                _timer += Simulation.DeltaTime;
                if (_physTime - _timer <= 0)
                {
                    _timer -= _physTime;
                    break;
                }
            }
        }

        protected override void OnRender()
        {
            Material.Render();
            _flatMill.Render();
            _sphereMill.Render();
        }
        
    }
}
