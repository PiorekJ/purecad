﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureCAD.Models
{
    public class SurfacePatch
    {
        public int PatchU = 0;
        public int PatchV = 0;
        public List<ScenePoint> PatchPoints;

        public SurfacePatch(int u, int v)
        {
            PatchU = u;
            PatchV = v;
            PatchPoints = new List<ScenePoint>();
        }

        public void AddPointToPatch(ScenePoint point)
        {
            PatchPoints.Add(point);
        }
    }
}
