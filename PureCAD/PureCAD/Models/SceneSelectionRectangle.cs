﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Utils;

namespace PureCAD.Models
{
    public class SceneSelectionRectangle : Model, IColorable
    {
        public Color Color { get; set; } = Colors.White;

        public VertexPT[] VertexPositions;

        private Vector2 _anchor;
        private float _aspectRatio;

        public SceneSelectionRectangle()
        {
            VertexPositions = new VertexPT[4];
            Mesh = CreatePlane();
            Shader = Shaders.BasicSelectionShader;
            IsVisibleOnList = false;
            IsVisible = false;
        }

        private Mesh<VertexPT> CreatePlane()
        {
            List<VertexPT> vertices = new List<VertexPT>();

            vertices.Add(new VertexPT(-1, 1, 0, 0, 1));
            vertices.Add(new VertexPT(1, 1, 0, 1, 1));
            vertices.Add(new VertexPT(1, -1, 0, 1, 0));
            vertices.Add(new VertexPT(-1, -1, 0, 0, 0));

            List<uint> edges = new List<uint>();

            edges.Add(0);
            edges.Add(3);
            edges.Add(1);

            edges.Add(1);
            edges.Add(3);
            edges.Add(2);
            return new Mesh<VertexPT>(vertices, edges, MeshType.Triangles, AccessType.Stream);
        }

        public void StartDrawing(Vector2 anchor)
        {
            _anchor = anchor;
            VertexPositions[0] = new VertexPT(_anchor.X, -_anchor.Y, 0.0f, 0, 1);
            UpdateRectangleVertices();
            IsVisible = true;
        }

        public void StopDrawing()
        {
            IsVisible = false;
        }

        private void UpdateRectangleVertices()
        {
            var size = _anchor - InputManager.MousePosition.ToScreenSpace();

            _aspectRatio = Math.Abs(size.X / size.Y);

            VertexPositions[1] = new VertexPT(_anchor.X - size.X, -_anchor.Y, 0.0f, 1, 1);
            VertexPositions[2] = new VertexPT(_anchor.X - size.X, -_anchor.Y + size.Y, 0.0f, 1, 0);
            VertexPositions[3] = new VertexPT(_anchor.X, -_anchor.Y + size.Y, 0.0f, 0, 0);
        }

        protected override void OnUpdate()
        {
            if (IsVisible)
            {
                UpdateRectangleVertices();
            }
        }

        protected override void OnRender()
        {
            GL.Disable(EnableCap.DepthTest);
            ((Mesh<VertexPT>)Mesh).SetVertices(0, VertexPositions);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("aspectRatio"), _aspectRatio);
            Shader.Bind(Shader.GetUniformLocation("borderWidth"), 0.1f);
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 0.2f);
            Mesh.Draw();
            GL.Enable(EnableCap.DepthTest);
        }


    }
}
