﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using PureCAD.Core;

namespace PureCAD.Models
{
    public class SceneSurfaceC0 : SceneSurface
    {
        protected override void CreatePlaneSurfacePoints(float width, float height, int patchesCountX, int patchesCountY)
        {
            int pointsX = 3 * patchesCountX + 1;
            int pointsY = 3 * patchesCountY + 1;

            float distX = width / (pointsX - 1);
            float distY = height / (pointsY - 1);
            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    Simulation.Scene.AddSurfacePoint(this, new Vector3(i * distX, 0, -j * distY));
                }
            }
        }

        protected override void CreateCylinderSurfacePoints(float radius, float height, int patchesCountX, int patchesCountY)
        {
            int pointsX = 3 * patchesCountX;
            int pointsY = 3 * patchesCountY + 1;
            float angle = 360f / pointsX;

            float distY = height / (pointsY - 1);
            for (int j = 0; j < pointsY; j++)
            {
                for (int i = 0; i < pointsX; i++)
                {
                    float xCoord = (float)(Math.Cos(i * angle * Math.PI / 180f) * radius);
                    float zCoord = (float)(Math.Sin(i * angle * Math.PI / 180f) * radius);
                    Simulation.Scene.AddSurfacePoint(this, new Vector3(xCoord, j * distY, zCoord));
                }
            }
        }

        protected override void CreatePlaneSurfacePatches(int patchesCountX, int patchesCountY)
        {
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    SurfacePatch surfacePatch = new SurfacePatch(j, i);
                    int cornerIndexX = j * 3;
                    int cornerIndexY = i * 3;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;

                            surfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (3 * patchesCountX + 1)]);
                        }
                    }
                    SurfacePatches.Add(surfacePatch);
                }
            }
        }

        protected override void CreateCylinderSurfacePatches(int patchesCountX, int patchesCountY)
        {
            for (int i = 0; i < patchesCountY; i++)
            {
                for (int j = 0; j < patchesCountX; j++)
                {
                    SurfacePatch surfacePatch = new SurfacePatch(j, i);
                    int cornerIndexX = j * 3;
                    int cornerIndexY = i * 3;
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            int pointX = cornerIndexX + l;
                            int pointY = cornerIndexY + k;
                            if (pointX == patchesCountX * 3)
                                pointX = 0;

                            surfacePatch.AddPointToPatch(SurfacePoints[pointX + pointY * (3 * patchesCountX)]);
                        }
                    }
                    SurfacePatches.Add(surfacePatch);
                }
            }
        }

    }
}
