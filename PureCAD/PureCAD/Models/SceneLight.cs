﻿using System.Windows.Media;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.OpenTK;

namespace PureCAD.Models
{
    public class SceneLight : Model
    {
        public Color Color { get; set; } = Colors.White;
        public int LightIntensity = 300;
        public SceneLight()
        {
            Shader = Shaders.LightShader;
            Mesh = MeshGenerator.GenerateCubeMesh(0.25f, 0.25f, 0.25f);
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Mesh.Draw();
        }
    }
}
