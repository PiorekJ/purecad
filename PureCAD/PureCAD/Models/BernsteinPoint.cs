﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using PureCAD.Core;

namespace PureCAD.Models
{
    public class BernsteinPoint : ScenePoint, IVirtual
    {
        public BernsteinPoint()
        {
            IsVisibleOnList = false;
            IsVisible = false;
            Color = Colors.Red;
        }
    }
}
