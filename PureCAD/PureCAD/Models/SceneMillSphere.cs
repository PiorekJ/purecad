﻿using System;
using System.Windows;
using OpenTK;
using PureCAD.Core;
using PureCAD.Utils;
using PureCAD.Utils.OpenTK;

namespace PureCAD.Models
{
    public class SceneMillSphere : SceneMill
    {
        private float[] _offsets;


        public override void SetupMill(float size, int materialSize, SceneMaterial material)
        {
            Mesh = MeshGenerator.GenerateSphereMillMesh(size);
            _offsets = GenerateSphereOffsets(size, materialSize, material);
        }

        private float[] GenerateSphereOffsets(float sceneSize, int materialSize, SceneMaterial material)
        {
            var radius = materialSize;
            var diameter = (radius * 2 + 1);
            var offsets = new float[diameter * diameter];
            var radiusSquared = radius * radius;
            for (int z = -radius, zz = 0; z <= radius; z++, zz++)
            {
                for (int x = -radius, xx = 0; x <= radius; x++, xx++)
                {
                    if (x * x + z * z <= radiusSquared)
                    {
                        var scenePoint = material.ConvertPointToSceneSpace(new Vector2(x, z));
                        scenePoint = new Vector3(scenePoint.X + material.SizeX / 2.0f, 0, scenePoint.Z + material.SizeZ / 2.0f);
                        offsets[xx + zz * diameter] = (float)Math.Sqrt((sceneSize * sceneSize) - scenePoint.X * scenePoint.X - scenePoint.Z * scenePoint.Z) - sceneSize;
                    }
                }
            }

            return offsets;
        }

        public override bool MillMaterialAt(SceneMaterial material, SceneMillPath.MaterialPoint materialPoint, float safeY, Vector3 millDirection, bool updateMaterial)
        {
            for (int z = (int)(materialPoint.Position.Y - materialPoint.Radius), j = 0; z <= (int)(materialPoint.Position.Y + materialPoint.Radius); z++, j++)
            {
                var startIdx = -1;
                var endIdx = -1;

                if (z < 0 || z >= material.PointsZ)
                    continue;

                for (int x = (int)(materialPoint.Position.X - materialPoint.Radius), i = 0; x <= (int)(materialPoint.Position.X + materialPoint.Radius); x++, i++)
                {

                    if (x < 0 || x >= material.PointsX)
                        continue;

                    int localX = (int)(x - materialPoint.Position.X);
                    int localZ = (int)(z - materialPoint.Position.Y);
                    var idx = x + z * material.PointsX;

                    if (idx < material.VerticesLength && idx >= 0)
                    {
                        if (localX * localX + localZ * localZ <= materialPoint.RadiusSquared)
                        {
                            if (startIdx == -1)
                                startIdx = idx;
                            endIdx = idx;
                            var result = material.MillMaterialAt(idx,
                                Transform.Position.Y - _offsets[i + j * materialPoint.Diameter], updateMaterial);
                            if (result == 0)
                            {
                                MessageBox.Show($"Milling below safe height, safe: {material.SafeY}, milled: {Transform.Position.Y - _offsets[i + j * materialPoint.Diameter]}", "Error", MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                                return false;
                            }
                        }
                    }
                }

                if (updateMaterial && startIdx != -1 && endIdx != -1)
                {
                    material.UpdateVertexStrip(startIdx, endIdx);
                }
            }
            return true;
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            Shader.Bind(Shader.GetUniformLocation("lightColor"), Simulation.Scene.SceneLights[0].Color.ColorToVector3());
            Shader.Bind(Shader.GetUniformLocation("lightPos"), Simulation.Scene.SceneLights[0].Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("camPos"), Simulation.Scene.Camera.Transform.Position);
            Shader.Bind(Shader.GetUniformLocation("ks"), 0.3f);
            Shader.Bind(Shader.GetUniformLocation("kd"), 2f);
            Shader.Bind(Shader.GetUniformLocation("ka"), 0.5f);
            Shader.Bind(Shader.GetUniformLocation("m"), 100.0f);
            Mesh.Draw();
        }
    }
}
