﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Components;
using PureCAD.Core;
using PureCAD.OpenTK;
using PureCAD.Popups;
using PureCAD.Utils;
using PureCAD.Utils.Serialization;
using PureCAD.Utils.UIExtensionClasses;
using InputManager = PureCAD.Core.InputManager;
using Transform = PureCAD.Components.Transform;

namespace PureCAD.Models
{
    public class SceneTorus : Model, IColorable, ISelectable, IFilterable, IIntersectable
    {
        public Color Color { get; set; } = Colors.White;
        public TorusParameters TorusParameters { get; set; }
        public bool IsSelected { get; set; } = false;

        private int _trimmingTextureId;
        private int _trimmingSide;

        public SceneTorus()
        {
            TorusParameters = AddComponent<TorusParameters>();
            Shader = Shaders.BasicTorusShader;
            Mesh = GenerateTorusMesh2D();
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());

            Shader.Bind(Shader.GetUniformLocation("r"), TorusParameters.SmallR);
            Shader.Bind(Shader.GetUniformLocation("R"), TorusParameters.BigR);

            if (!Simulation.Settings.StereoModeOn)
                Shader.Bind(Shader.GetUniformLocation("color"),
                    !IsSelected ? Color.ColorToVector3() : Simulation.Settings.SelectionColor.ColorToVector3());
            else
                Shader.Bind(Shader.GetUniformLocation("color"), Colors.White.ColorToVector3());

            if (_trimmingTextureId != 0)
            {
                Shader.Bind(Shader.GetUniformLocation("sideIdx"), _trimmingSide);
                if (_trimmingSide != 0)
                {
                    GL.ActiveTexture(TextureUnit.Texture0);
                    GL.BindTexture(TextureTarget.Texture2D, _trimmingTextureId);
                    Shader.Bind(Shader.GetUniformLocation("Tex"), 0);
                }
            }


            Mesh.Draw();
        }

        public void RegenerateMesh()
        {
            Mesh = GenerateTorusMesh2D();
        }

        protected override void OnUpdate()
        {
        }

        private Mesh<VertexP> GenerateTorusMesh()
        {
            List<VertexP> vertices = new List<VertexP>();
            List<uint> indices = new List<uint>();
            Mesh?.Dispose();
            float intervalU = (float)(2 * Math.PI / TorusParameters.UParameter);
            float intervalV = (float)(2 * Math.PI / TorusParameters.VParameter);

            for (int i = 0; i < TorusParameters.UParameter; ++i)
            {
                for (int j = 0; j < TorusParameters.VParameter; ++j)
                {
                    float u = i * intervalU;
                    float v = j * intervalV;

                    float X = (float)((TorusParameters.SmallR * Math.Cos(u) + TorusParameters.BigR) * Math.Cos(v));
                    float Y = (float)(TorusParameters.SmallR * Math.Sin(u));
                    float Z = (float)((TorusParameters.SmallR * Math.Cos(u) + TorusParameters.BigR) * Math.Sin(v));

                    vertices.Add(new VertexP(X, Y, Z));

                    int currentIndex = TorusParameters.VParameter * i + j;
                    int topNeighbour = TorusParameters.VParameter * i + (j + 1) % TorusParameters.VParameter;
                    int rightNeighbour = TorusParameters.VParameter * ((i + 1) % TorusParameters.UParameter) + j;
                    indices.Add((uint)currentIndex);
                    indices.Add((uint)topNeighbour);
                    indices.Add((uint)currentIndex);
                    indices.Add((uint)rightNeighbour);
                }
            }

            return new Mesh<VertexP>(vertices, indices, MeshType.Lines, AccessType.Static);
        }

        private Mesh<Vertex2DP> GenerateTorusMesh2D()
        {
            List<Vertex2DP> vertices = new List<Vertex2DP>();
            List<uint> indices = new List<uint>();
            Mesh?.Dispose();
            float intervalU = (float)(2 * Math.PI / TorusParameters.UParameter);
            float intervalV = (float)(2 * Math.PI / TorusParameters.VParameter);

            for (int i = 0; i < TorusParameters.UParameter; ++i)
            {
                for (int j = 0; j < TorusParameters.VParameter; ++j)
                {
                    float u = i * intervalU;
                    float v = j * intervalV;

                    vertices.Add(new Vertex2DP(u, v));

                    int currentIndex = TorusParameters.VParameter * i + j;
                    int topNeighbour = TorusParameters.VParameter * i + (j + 1) % TorusParameters.VParameter;
                    int rightNeighbour = TorusParameters.VParameter * ((i + 1) % TorusParameters.UParameter) + j;
                    indices.Add((uint)currentIndex);
                    indices.Add((uint)topNeighbour);
                    indices.Add((uint)currentIndex);
                    indices.Add((uint)rightNeighbour);
                }
            }

            return new Mesh<Vertex2DP>(vertices, indices, MeshType.Lines, AccessType.Static);
        }

        public torus Serialize()
        {
            torus torus = new torus();
            var euler = Transform.Rotation.EulerAngles();

            torus.center.x = Transform.Position.X;
            torus.center.y = Transform.Position.Y;
            torus.center.z = Transform.Position.Z;

            torus.rotation.x = euler.X;
            torus.rotation.y = euler.Y;
            torus.rotation.z = euler.Z;

            torus.scale.x = Transform.Scale.X;
            torus.scale.y = Transform.Scale.Y;
            torus.scale.z = Transform.Scale.Z;

            torus.R = TorusParameters.BigR;
            torus.r = TorusParameters.SmallR;
            torus.u = TorusParameters.UParameter;
            torus.v = TorusParameters.VParameter;
            torus.name = ObjectName;

            return torus;
        }

        public void Deserialize(torus torus)
        {
            Transform.Position = new Vector3(torus.center.x, torus.center.z, torus.center.y);
            var vec = new Vector3(torus.rotation.x, torus.rotation.y, torus.rotation.z);
            Transform.Rotation = new Quaternion(vec.Zyx * (float)System.Math.PI / 180);
            Transform.Scale = new Vector3(torus.scale.x, torus.scale.z, torus.scale.y);
            TorusParameters.BigR = torus.R;
            TorusParameters.SmallR = torus.r;
            TorusParameters.UParameter = torus.u;
            TorusParameters.VParameter = torus.v;
            ObjectName = torus.name;
        }

        public Vector2d ApproximateAt(Vector3 worldPosition)
        {
            double distX = (float)(2 * Math.PI / TorusParameters.UParameter);
            double distY = (float)(2 * Math.PI / TorusParameters.VParameter);
            double minDist = double.PositiveInfinity;
            Vector2d pointUV = new Vector2d();
            var worldPosD = worldPosition.ToVector3d();
            for (int i = 0; i < TorusParameters.UParameter; i++)
            {
                for (int j = 0; j < TorusParameters.VParameter; j++)
                {
                    var param = new Vector2d(j * distX, i * distY);
                    Vector3d surfacePoint = EvaluateAt(param);
                    double dist = (worldPosD - surfacePoint).Length;
                    if (dist < minDist)
                    {
                        pointUV = param;
                        minDist = dist;
                    }
                }
            }

            return pointUV;
        }

        public Vector3d EvaluateAt(Vector2d uv)
        {
            float X = (float)(((TorusParameters.SmallR * Math.Cos(uv.X) + TorusParameters.BigR) * Math.Cos(uv.Y)));
            float Y = (float)((TorusParameters.SmallR * Math.Sin(uv.X)));
            float Z = (float)(((TorusParameters.SmallR * Math.Cos(uv.X) + TorusParameters.BigR) * Math.Sin(uv.Y)));

            return (Transform.Position + Transform.Rotation * new Vector3(X, Y, Z)).ToVector3d();
        }

        public Vector3d EvaluateUDerivativeAt(Vector2d uv)
        {
            double X = (-TorusParameters.SmallR * Math.Sin(uv.X) * Math.Cos(uv.Y));
            double Y = ((TorusParameters.SmallR * Math.Cos(uv.X)));
            double Z = (-TorusParameters.SmallR * Math.Sin(uv.X) * Math.Sin(uv.Y));

            return (Transform.Rotation *  new Vector3d(X, Y, Z).ToVector3()).ToVector3d();
        }

        public Vector3d EvaluateVDerivativeAt(Vector2d uv)
        {
            double X = (-(TorusParameters.SmallR * Math.Cos(uv.X) + TorusParameters.BigR) * Math.Sin(uv.Y));
            double Y = 0;
            double Z = ((TorusParameters.SmallR * Math.Cos(uv.X) + TorusParameters.BigR) * Math.Cos(uv.Y));

            return (Transform.Rotation *  new Vector3d(X, Y, Z).ToVector3()).ToVector3d();
        }

        public double GetURange()
        {
            return (2 * Math.PI);
        }

        public double GetVRange()
        {
            return (2 * Math.PI);
        }

        public int GetUCurveCount()
        {
            return TorusParameters.UParameter;
        }

        public int GetVCurveCount()
        {
            return TorusParameters.VParameter;
        }

        public bool IsUWrapped { get; set; } = true;
        public bool IsVWrapped { get; set; } = true;

        public void SetTrimmingTexture(int texId)
        {
            _trimmingTextureId = texId;
        }

        public void SetTrimmingSide(int sideId)
        {
            _trimmingSide = sideId;
        }
    }
}
