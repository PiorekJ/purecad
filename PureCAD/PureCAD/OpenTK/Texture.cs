﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace PureCAD.OpenTK
{
    public interface ITexture { }

    public class Texture : ITexture
    {
        public int TexID { get; private set; }

        public Texture(string filepath)
        {
            TexID = CreateFromFile(filepath);
        }


        private int CreateFromFile(string filepath)
        {
            Bitmap bitMap;
            try
            {
                bitMap = new Bitmap(filepath);
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Creating bitmap: " + e.Message);
            }
            
            var tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, tex);
            BitmapData bitMapData = bitMap.LockBits(new Rectangle(0, 0, bitMap.Width, bitMap.Height),
                ImageLockMode.ReadOnly, bitMap.PixelFormat);
            if (bitMap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppArgb1555 || bitMap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb || bitMap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppPArgb || bitMap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format64bppArgb || bitMap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format64bppPArgb)
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, bitMapData.Scan0);
            else
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, bitMapData.Width, bitMapData.Height, 0, PixelFormat.Bgr, PixelType.UnsignedByte, bitMapData.Scan0);


            bitMap.UnlockBits(bitMapData);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return tex;
        }
    }

    public class PBRTexture : ITexture
    {
        public Texture Albedo;
        public Texture Metallic;
        public Texture Normal;
        public Texture Roughness;
        public Texture Ao;

        public PBRTexture(string albedo, string metallic, string normal, string roughness, string ao)
        {
            Albedo = new Texture(albedo);
            Metallic = new Texture(metallic);
            Normal = new Texture(normal);
            Roughness = new Texture(roughness);
            Ao = new Texture(ao);
        }
    }
}
