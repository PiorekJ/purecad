﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace PureCAD.OpenTK
{
    public class PatchMesh<TVertex> : IMesh
        where TVertex : struct, IVertex
    {
        private TVertex[] _vertices;
        private uint[] _edges;

        private int _vao;
        private int _vbo;
        private int _ebo;

        public int PatchSize { get; private set; }

        public PatchMesh(IEnumerable<TVertex> vertices, IEnumerable<uint> indices, int patchSize, AccessType type = AccessType.Static)
        {
            _vertices = vertices.ToArray();

            if (indices != null)
                _edges = indices.ToArray();
            else
            {
                _edges = new uint[_vertices.Length];
                for (uint i = 0; i < _edges.Length; ++i)
                    _edges[i] = i;
            }
            PatchSize = patchSize;

            var usageHint = AccessTypeToBufferUsageHint(type);

            _vao = GL.GenVertexArray();
            _vbo = GL.GenBuffer();
            _ebo = GL.GenBuffer();

            GL.BindVertexArray(_vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_vertices.Length * Marshal.SizeOf<TVertex>()), _vertices, usageHint);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(_edges.Length * sizeof(uint)), _edges, usageHint);

            TVertex vertex = new TVertex();
            vertex.Initialize();
            GL.BindVertexArray(0);
        }

        public void SetVertices(uint startIndex, TVertex[] vertices)
        {
            for (int i = 0; i < vertices.Length; ++i)
                this._vertices[startIndex + i] = vertices[i];

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(startIndex * Marshal.SizeOf<TVertex>()), (IntPtr)(vertices.Length * Marshal.SizeOf<TVertex>()), vertices);
        }

        public void Draw()
        {
            GL.BindVertexArray(_vao);
            GL.PatchParameter(PatchParameterInt.PatchVertices, PatchSize);
            GL.DrawElements(PrimitiveType.Patches, _edges.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }
        
        public void Dispose()
        {
            if (_ebo != 0)
            {
                GL.DeleteBuffer(this._ebo);
                _ebo = 0;
            }

            if (this._vbo != 0)
            {
                GL.DeleteBuffer(this._vbo);
                _vbo = 0;
            }

            if (this._vao != 0)
            {
                GL.DeleteVertexArray(this._vao);
                _vao = 0;
            }
        }

        private static BufferUsageHint AccessTypeToBufferUsageHint(AccessType type)
        {
            switch (type)
            {
                case AccessType.Dynamic:
                    return BufferUsageHint.DynamicDraw;
                case AccessType.Static:
                    return BufferUsageHint.StaticDraw;
                case AccessType.Stream:
                    return BufferUsageHint.StreamDraw;
                default:
                    throw new ArgumentOutOfRangeException("Access type not found");
            }
        }
    }

}
