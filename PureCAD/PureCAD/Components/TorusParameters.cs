﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PureCAD.Core;
using PureCAD.Models;

namespace PureCAD.Components
{
    public class TorusParameters : Component
    {
        private float _smallR = 0.5f;
        private float _bigR = 1.5f;
        private int _uParameter = 64;
        private int _vParameter = 128;

        public float BigR
        {
            get => _bigR;
            set
            {
                _bigR = value;
                RaisePropertyChanged();
                (Owner as SceneTorus)?.RegenerateMesh();
            }
        }

        public float SmallR
        {
            get => _smallR;
            set
            {
                _smallR = value;
                RaisePropertyChanged();
                (Owner as SceneTorus)?.RegenerateMesh();
            }
        }

        public int UParameter
        {
            get => _uParameter;
            set
            {
                _uParameter = value;
                RaisePropertyChanged();
                (Owner as SceneTorus)?.RegenerateMesh();
            }
        }

        public int VParameter
        {
            get => _vParameter;
            set
            {
                _vParameter = value;
                RaisePropertyChanged();
                (Owner as SceneTorus)?.RegenerateMesh();
            }
        }
    }
}
