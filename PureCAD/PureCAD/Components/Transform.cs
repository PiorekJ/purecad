﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using OpenTK;
using PureCAD.Core;
using PureCAD.Models;
using PureCAD.Utils;
using PureCAD.Utils.Math;
using PureCAD.Utils.UIExtensionClasses;
using InputManager = PureCAD.Core.InputManager;

namespace PureCAD.Components
{
    public class Transform : Component
    {
        private Vector3 _position = Vector3.Zero;
        private Quaternion _rotation = Quaternion.Identity;
        private Vector3 _scale = Vector3.One;
        private Vector3 _screenPosition;
        private SceneLine _sceneLine;
        public ObservableCollection<SceneObject> Children { get; set; }
        public ObservableCollection<SceneObject> Parents { get; set; }
        public bool IsMoving = false;

        public delegate void Vector3UpdateEventHandler(SceneObject obj, Vector3 oldValue, Vector3 newValue);
        public delegate void QuaternionUpdateEventHandler(SceneObject obj, Quaternion oldValue, Quaternion newValue);
        public delegate void ChildActionEventHandler(SceneObject child);

        public event Vector3UpdateEventHandler OnPositionUpdate;
        public event QuaternionUpdateEventHandler OnRotationUpdate;
        public event Vector3UpdateEventHandler OnScaleUpdate;
        public event ChildActionEventHandler OnChildAdd;
        public event ChildActionEventHandler OnChildRemove;

        public bool IsProxy = false;
        #region KeyMovement

        private Plane _movementPlane;
        private Vector3 _movementIntersect;
        private Vector3 _initialPosition;
        private List<Vector3> _childrenInitialPositions;
        private List<Vector3> _childrenInitialIntersections;
        private Vector3 _movementAxis;

        #endregion

        #region UIRelated

        public ICommand SelectOnSceneCommand
        {
            get;
            private set;
        }

        public ICommand RemoveParentCommand
        {
            get;
            private set;
        }

        public ICommand RemoveChildCommand
        {
            get;
            private set;
        }

        #endregion

        public Vector3 ScreenPosition
        {
            get => _screenPosition;
            set
            {
                _screenPosition = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                //changed order
                var oldPos = _position;
                _position = value;

                if (Children != null && Children.Count > 0 && !IsMoving && IsProxy)
                {
                    var moveDir = _position - oldPos;
                    foreach (SceneObject child in Children)
                    {
                        child.Transform.Position += moveDir;
                    }
                }

                OnPositionUpdate?.Invoke(Owner, oldPos, _position);
                RaisePropertyChanged();
            }
        }

        public Quaternion Rotation
        {
            get { return _rotation; }
            set
            {
                OnRotationUpdate?.Invoke(Owner, _rotation, value);
                _rotation = value;
                RaisePropertyChanged();
            }
        }

        public Vector3 Scale
        {
            get { return _scale; }
            set
            {
                OnScaleUpdate?.Invoke(Owner, _scale, value);
                _scale = value;
                RaisePropertyChanged();
            }
        }

        public Transform()
        {
            _movementPlane = new Plane();
            Children = new ObservableCollection<SceneObject>();
            Parents = new ObservableCollection<SceneObject>();
            RegisterKeyEvents();
            InitializeCommands();
        }

        public Transform(Vector3UpdateEventHandler onPositionVector3Update, QuaternionUpdateEventHandler onRotationVector3Update, Vector3UpdateEventHandler onScaleVector3Update)
        {
            _movementPlane = new Plane();
            Children = new ObservableCollection<SceneObject>();
            Parents = new ObservableCollection<SceneObject>();
            RegisterKeyEvents();
            OnPositionUpdate += onPositionVector3Update;
            OnRotationUpdate += onRotationVector3Update;
            OnScaleUpdate += onScaleVector3Update;
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            SelectOnSceneCommand = new RelayCommand(param =>
            {
                Simulation.Scene.SelectionManager.SelectedSceneObject = Owner;
            });
            //param -> object to remove "this" from
            //example: param = curve, this = point 
            RemoveChildCommand = new RelayCommand(param =>
            {
                if (param is SceneObject parent)
                {
                    parent.Transform.ForceRemoveChild(Owner);
                }
            });
            //param -> remove param from this 
            //example: param = point, this = curve
            RemoveParentCommand = new RelayCommand(param =>
            {
                if (param is SceneObject toRemoveFrom)
                {
                    ForceRemoveChild(toRemoveFrom);
                }
            });
        }

        public Matrix4 GetModelMatrix()
        {
            return MathGeneralUtils.CreateScale(Scale) * MathGeneralUtils.CreateFromQuaternion(Rotation) * MathGeneralUtils.CreateTranslation(Position);
        }

        private void RegisterKeyEvents()
        {
            InputManager.RegisterOnKeyDownEvent(Key.X, () =>
            {
                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null && Simulation.Scene.SelectionManager.SelectedSceneObject == Owner)
                {
                    BeginMovementHandler(Vector3.UnitX);
                }
            });
            InputManager.RegisterOnKeyDownEvent(Key.Y, () =>
            {
                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null && Simulation.Scene.SelectionManager.SelectedSceneObject == Owner)
                {
                    BeginMovementHandler(Vector3.UnitY);
                }
            });
            InputManager.RegisterOnKeyDownEvent(Key.Z, () =>
            {
                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null && Simulation.Scene.SelectionManager.SelectedSceneObject == Owner)
                {
                    BeginMovementHandler(Vector3.UnitZ);
                }
            });
            InputManager.RegisterOnKeyDownEvent(Key.Escape, () =>
            {
                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null && Simulation.Scene.SelectionManager.SelectedSceneObject == Owner && IsMoving)
                {
                    EndMovementHandler(true);
                }
            });

            InputManager.RegisterOnButtonDownEvent(MouseButton.Left, () =>
            {
                if (Simulation.Scene.SelectionManager.SelectedSceneObject != null && Simulation.Scene.SelectionManager.SelectedSceneObject == Owner && IsMoving)
                {
                    EndMovementHandler(false);
                }
            });
        }

        public void Dispose()
        {
            if (Owner.GetType() != typeof(SceneLine))
            {
                _sceneLine?.Deactivate();
                _sceneLine?.Dispose();
            }
        }

        /// <summary>
        /// Use only when needed.
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        public SceneObject ForceAddChild(SceneObject child)
        {
            if (Children.Contains(child))
                return child;

            if (IsMoving)
            {
                _childrenInitialPositions.Add(child.Transform.Position);
                _childrenInitialIntersections.Add(GetIntersectionWithMovementPlane());
            }
            child.Transform.Parents.Add(Owner);
            Children.Add(child);
            OnChildAdd?.Invoke(child);
            return child;
        }

        /// <summary>
        /// Use only when needed.
        /// </summary>
        /// <param name="child"></param>
        public void ForceRemoveChild(SceneObject child)
        {
            if (IsMoving)
            {
                var i = _childrenInitialPositions.IndexOf(child.Transform.Position);
                _childrenInitialPositions.Remove(child.Transform.Position);
                if (i >= 0)
                    _childrenInitialIntersections.RemoveAt(i);
            }
            child.Transform.Parents.Remove(Owner);
            Children.Remove(child);
            OnChildRemove?.Invoke(child);
        }

        /// <summary>
        /// Use only when needed.
        /// </summary>
        /// <param name="parent"></param>
        public void ForceRemoveParent(SceneObject parent)
        {
            parent.Transform.Children.Remove(Owner);
            Parents.Remove(parent);
        }
        /// <summary>
        /// Use only when needed.
        /// </summary>
        public void ForceRemoveAllChildren()
        {
            if (IsMoving)
            {
                _childrenInitialPositions.Clear();
                _childrenInitialIntersections.Clear();
            }
            foreach (SceneObject child in Children)
            {
                child.Transform.Parents.Remove(Owner);
            }
            Children.Clear();
        }

        /// <summary>
        /// Use only when needed.
        /// </summary>
        public void ForceRemoveAllParents()
        {
            for (var i = Parents.Count - 1; i >= 0; i--)
            {
                SceneObject parent = Parents[i];
                parent.Transform.ForceRemoveChild(Owner);
            }
            Parents.Clear();
        }

        public void InterruptMovement()
        {
            IsMoving = false;
            if (Owner.GetType() != typeof(SceneLine))
            {
                _sceneLine?.Deactivate();
            }
        }

        public SceneObject GetChild(int i)
        {
            return i < Children.Count ? Children[i] : null;
        }

        public void HandleKeyMovement()
        {
            if (IsMoving)
            {
                var projection = GetObjectMovementProjection(_movementIntersect);
                var axisProjection = _movementAxis * projection;
                Position = _initialPosition + axisProjection;
                for (var i = 0; i < Children.Count; i++)
                {
                    projection = GetObjectMovementProjection(_childrenInitialIntersections[i]);
                    axisProjection = _movementAxis * projection;
                    Children[i].Transform.Position = _childrenInitialPositions[i] + axisProjection;
                }
            }
        }

        private float GetObjectMovementProjection(Vector3 intersection)
        {
            Rayline currentRayLine = Simulation.Scene.Camera.CalculateCameraScreenRayLine(InputManager.MousePosition);
            Vector3 mousePlaneDir = _movementPlane.CalculateRayIntersetion(currentRayLine) - intersection;
            return Vector3.Dot(mousePlaneDir, _movementAxis);
        }

        private void SetMovementPlane(Vector3 axis)
        {
            var centerRayline = Simulation.Scene.Camera.CalculateCameraScreenRayLine(InputManager.MousePosition);
            _movementPlane.Origin = Position;
            _movementPlane.CalculateNormal(centerRayline.Direction, axis);
            _movementIntersect = _movementPlane.CalculateRayIntersetion(centerRayline);
        }

        private Vector3 GetIntersectionWithMovementPlane()
        {
            var centerRayline = Simulation.Scene.Camera.CalculateCameraScreenRayLine(InputManager.MousePosition);
            return _movementPlane.CalculateRayIntersetion(centerRayline);
        }

        private void BeginMovementHandler(Vector3 axis)
        {
            _movementAxis = axis;
            if (Owner.GetType() != typeof(SceneLine))
            {
                if (_sceneLine == null)
                    _sceneLine = new SceneLine();
                _sceneLine.Activate(axis, Position);
            }

            IsMoving = true;
            SetMovementPlane(axis);
            _initialPosition = Position;
            _childrenInitialPositions = new List<Vector3>();
            _childrenInitialIntersections = new List<Vector3>();
            foreach (SceneObject child in Children)
            {
                _childrenInitialPositions.Add(child.Transform.Position);
                _childrenInitialIntersections.Add(_movementIntersect);
            }
        }

        private void EndMovementHandler(bool resetPositions)
        {
            IsMoving = false;
            if (Owner.GetType() != typeof(SceneLine))
            {
                _sceneLine?.Deactivate();
            }
            if (resetPositions)
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    Children[i].Transform.Position = _childrenInitialPositions[i];
                }
                Position = _initialPosition;
            }
        }
    }
}
